package com.najcom.server.dao;

import java.util.List;











import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.najcom.server.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings({"unchecked", "deprecation"})
	public List<User> listAllUser() {
		Criteria criteria = getSession().createCriteria(User.class);
		return (List<User>) criteria.list();
	}

	public void saveOrUpdate(User user) {
		getSession().saveOrUpdate(user);
	}

	public User findUserById(Integer id) {
		User user = (User) getSession().get(User.class, id);
		return user;
	}
	
	public User findUserByUsername(String username) {
		Session session = getSession();
		Criteria cr = session.createCriteria(User.class);
		cr.add(Restrictions.eq("phone", username));
		
		return cr.list().isEmpty() ? null :(User) cr.list().get(0);
	}
	
	public User findUserByUsernameAndPassword(String username, String password) {
		Session session = getSession();
		Criteria cr = session.createCriteria(User.class);
		Criterion usrname = Restrictions.eq("phone", username);
		Criterion pass = Restrictions.eq("password", password);
		LogicalExpression andExp = Restrictions.and(usrname, pass);
		cr.add(andExp);
		return cr.list().isEmpty() ? null :(User) cr.list().get(0);
	}

	public void deleteUser(Integer id) {
		User user = (User) getSession().get(User.class, id);
		getSession().delete(user);
	}

}
