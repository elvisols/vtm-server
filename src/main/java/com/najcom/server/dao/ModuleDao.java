package com.najcom.server.dao;

import java.util.List;

import com.najcom.server.model.Entity;
import com.najcom.server.model.Module;

public interface ModuleDao {

	public List<Module> listAllModules();
	
	public void saveOrUpdate(Module module);

	public void deleteModule(Integer id);
	
	public Module findModuleById(Integer id);
	
	public List<Module> findModuleByVtm(Integer id);
	
	public List<Entity> findModuleAncestor(Integer id);
	
}
