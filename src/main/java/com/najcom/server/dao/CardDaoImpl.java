package com.najcom.server.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.najcom.server.model.Card;

@Repository
public class CardDaoImpl implements CardDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public void saveOrUpdate(Card card) {
		getSession().saveOrUpdate(card);
	}
	
}
