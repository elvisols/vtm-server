package com.najcom.server.dao;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.najcom.server.model.Vtm;

@Repository
public class VtmUserDaoImpl implements VtmUserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public Vtm loadUserByUsername(String username) {
		return (Vtm) sessionFactory.getCurrentSession()
				.createCriteria(Vtm.class)
				.add(Restrictions.eq("username", username))
				.uniqueResult();
	}
	
}
