package com.najcom.server.dao;

import java.util.Iterator;
import java.util.List;
















import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.najcom.server.model.Entity;
import com.najcom.server.model.Module;

@Repository
public class ModuleDaoImpl implements ModuleDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings({"unchecked", "deprecation"})
	public List<Module> listAllModules() {
		Criteria criteria = getSession().createCriteria(Module.class);
		return (List<Module>) criteria.list();
	}

	public void saveOrUpdate(Module module) {
		getSession().saveOrUpdate(module);
	}

	public Module findModuleById(Integer id) {
		Module module = (Module) getSession().get(Module.class, id);
		return module;
	}
	
	public void deleteModule(Integer id) {
		Module module = (Module) getSession().get(Module.class, id);
		getSession().delete(module);
	}
	
	public List<Module> findModuleByVtm(Integer id) {
		Session session = getSession();
		Criteria cr = session.createCriteria(Module.class);
		cr.add(Restrictions.eq("vtm.id", id.longValue()));
		System.out.println(cr.list());
		return (List<Module>) cr.list();
	}
	
	@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
	public List<Entity> findModuleAncestor(Integer id) {
		Session session = getSession();
		SQLQuery q = session.createSQLQuery("SELECT T2.id, T2.name , T2.visibility, T2.description, T2.parent FROM (SELECT @r AS _id, (SELECT @r \\:= parent FROM entities WHERE id = _id) AS parent, @l \\:= @l + 1 AS lvl FROM (SELECT @r \\:= :id, @l \\:= 0) vars, entities h WHERE @r <> 0) T1 JOIN entities T2 ON T1._id = T2.id ORDER BY T1.lvl ASC");
		q.addEntity(Entity.class);
		q.setParameter("id", id);
		List results = q.list();
		System.out.println("Results: " + results);
//		List results = new java.util.ArrayList();
//		for(Iterator it=q.iterate();it.hasNext();)  
//		{  
//		   Entity entity = new Entity();
//		   Object[] row = (Object[]) it.next();
//		   entity.setId((String)row[0]); 
//		   entity.setName((String)row[1]); 
//		   entity.setVisibility((Boolean)row[2]);
//		   results.add(entity);
//		}
		return (List<Entity>) results;
	}

}
