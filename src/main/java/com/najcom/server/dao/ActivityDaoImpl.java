package com.najcom.server.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.najcom.server.model.Activity;

@Repository
public class ActivityDaoImpl implements ActivityDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public void saveOrUpdate(Activity a) {
		getSession().saveOrUpdate(a);
	}
	
}
