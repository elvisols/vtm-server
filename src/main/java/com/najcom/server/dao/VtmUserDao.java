package com.najcom.server.dao;

import com.najcom.server.model.Vtm;

public interface VtmUserDao {

	Vtm loadUserByUsername(String username);
	
}
