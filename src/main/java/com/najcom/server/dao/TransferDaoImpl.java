package com.najcom.server.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.najcom.server.model.Transfer;

@Repository
public class TransferDaoImpl implements TransferDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public void saveOrUpdate(Transfer transfer) {
		getSession().saveOrUpdate(transfer);
	}

}
