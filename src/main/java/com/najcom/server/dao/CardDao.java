package com.najcom.server.dao;

import com.najcom.server.model.Card;

public interface CardDao {

	public void saveOrUpdate(Card card);
	
	
}
