package com.najcom.server.dao;

import java.util.List;

import com.najcom.server.model.User;

public interface UserDao {

	public List<User> listAllUser();
	
	public void saveOrUpdate(User user);
	
	public void deleteUser(Integer id);
	
	public User findUserById(Integer id);

	public User findUserByUsername(String username);
	
	public User findUserByUsernameAndPassword(String username, String password);
	
}
