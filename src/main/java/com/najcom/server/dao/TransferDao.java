package com.najcom.server.dao;

import com.najcom.server.model.Transfer;

public interface TransferDao {

	public void saveOrUpdate(Transfer transfer);
	
}
