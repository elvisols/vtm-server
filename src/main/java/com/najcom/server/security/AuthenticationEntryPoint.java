package com.najcom.server.security;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component(value = "authenticationEntryPoint")
public class AuthenticationEntryPoint implements org.springframework.security.web.AuthenticationEntryPoint {

    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        // This is invoked when user tries to access a secured REST resource without supplying any credentials
        // We should just send a 401 Unauthorized response because there is no 'login page' to redirect to
        response.setHeader("X-Reason","Oops! Unauthorized Request");
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
		response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Max-Age", "120"); // in seconds
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Methods", "HEAD, GET, OPTIONS, POST, PUT, DELETE");
        response.addHeader("Access-Control-Allow-Headers", "origin, content-type, x-reason, accept, x-requested-with, authorization");
        response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		JSONObject param = new JSONObject();
		param.put("code", 401);
		param.put("message", authException.getMessage());
        response.getWriter().write(param.toString());
    }
    
}
