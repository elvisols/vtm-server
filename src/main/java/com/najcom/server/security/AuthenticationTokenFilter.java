package com.najcom.server.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.najcom.server.util.Bank;

public class AuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

    private final Log logger = LogFactory.getLog(this.getClass());
    
    BCryptPasswordEncoder str = new BCryptPasswordEncoder();

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
//        String authToken = request.getHeader("Authorization");
//        if (authToken == null) {
//        	chain.doFilter(request, response);
//        	return;
//        }
        // authToken.startsWith("Bearer ")
        // String authToken = header.substring(7);
        tokenUtil = WebApplicationContextUtils
        		.getRequiredWebApplicationContext(this.getServletContext())
        		.getBean(TokenUtil.class);
        userDetailsService = WebApplicationContextUtils
        		.getRequiredWebApplicationContext(this.getServletContext())
        		.getBean(UserDetailsService.class);
        
        HttpServletResponse resp = (HttpServletResponse) response;
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        resp.setHeader("Access-Control-Max-Age", "3600");
        resp.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String authToken = httpRequest.getHeader("Authorization");
        String username = tokenUtil.getUsernameFromToken(authToken);

        logger.info("checking authentication for user " + username);

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
//            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
        	UserDetails userDetails = new SpringSecurityUser (
    				Long.parseLong(tokenUtil.getUsernameFromToken(httpRequest.getHeader("Authorization"))), // Use user phone as Id
    				tokenUtil.getUsernameFromToken(httpRequest.getHeader("Authorization")), // Use user phone as username
    				str.encode(tokenUtil.getUsertokenFromToken(httpRequest.getHeader("Authorization"))), // Use user token as password
    				null,
    				null,
    				AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER")
    			);
            if (tokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                logger.info(" *** Authenticated user " + username + ", setting security context");
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        chain.doFilter(request, response);
    }
}
