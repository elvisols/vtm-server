package com.najcom.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.najcom.server.util.Bank;

@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
    private TokenUtil tokenUtil;
	
	BCryptPasswordEncoder str = new BCryptPasswordEncoder();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//	Local authentication skipped!!!
//		Vtm vtmUser = this.vtmUserService.loadUserByUsername(username);
//		if(vtmUser == null) {
//			throw new UsernameNotFoundException(String.format("No vtmUser found with username '%s'.", username));
//		} else {
			return new SpringSecurityUser (
				Long.parseLong(Bank.getPhone()), // Use user phone as Id
				Bank.getPhone(), // Use user phone as username
				str.encode(Bank.getToken()), // Use user token as password
				null,
				null,
				AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER")
			);
//		}
	}

}
