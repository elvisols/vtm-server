package com.najcom.server.security;

import java.io.Serializable;

public class AuthenticationRequest implements Serializable {

	private static final long serialVersionUID = -8445943548965154778L;

    private String username; // Users phone number
    private String password; // Users token
    private Integer vtm; // Requesting VTM ID

    public AuthenticationRequest() {
        super();
    }

    public AuthenticationRequest(String username, String password, Integer vtm) {
        this.setUsername(username);
        this.setPassword(password);
        this.setVtm(vtm);
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public Integer getVtm() {
		return vtm;
	}

	public void setVtm(Integer vtm) {
		this.vtm = vtm;
	}
    
}
