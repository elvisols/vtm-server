package com.najcom.server.controller;

import java.sql.Timestamp;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.server.model.Card;
import com.najcom.server.service.CardService;

@RestController
@RequestMapping("card")
public class CardController {
	
	private Timestamp timeNow;
	
	@Autowired
	private CardService cardService;
	
	@RequestMapping(path = "/issuance", method = RequestMethod.POST, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> sendCardIssuance(@RequestBody Card card, HttpServletRequest request) {
		// Log Card Issuance Request
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		card.setId(null);
		card.setCreated(timeNow);
		card.setModified(timeNow);
		cardService.saveOrUpdate(card);
		// Send card request
		Map<String, Object> response = cardService.sendCardIssuance(card);
		request.setAttribute("action", "Card issuance request... " + card);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/hotlisting", method = RequestMethod.POST, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> sendCardHotlisting(@RequestBody Card card, HttpServletRequest request) {
		// Log Card Hotlisting Request
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		card.setId(null);
		card.setCreated(timeNow);
		card.setModified(timeNow);
		cardService.saveOrUpdate(card);
		// Send card request
		Map<String, Object> response = cardService.sendCardHotlisting(card);
		request.setAttribute("action", "Card hotlisting request... " + card);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
}
