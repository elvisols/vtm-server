package com.najcom.server.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.server.service.BalanceService;

@RestController
@RequestMapping("balance")
public class BalanceController {
	
	@Autowired
	BalanceService balanceService;
	
	@RequestMapping(path = "{id}", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> getUserBalance(@PathVariable("id") String id, HttpServletRequest request) {
		Map<String, Object> balance = balanceService.findUserBalance(id);
		request.setAttribute("action", "Balance check " + request.getRequestURL().toString() + " from " + id);
		return new ResponseEntity<Map<String, Object>>(balance, HttpStatus.OK);
	}
	
}