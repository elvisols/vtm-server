package com.najcom.server.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("static")
public class StaticFileController {
 
	@Value("#{'${vtm.titles}'.split(',')}") 
    private List<String> titleList;
	
	@Value("#{'${vtm.states}'.split(',')}") 
	private List<String> stateList;
	
	@Value("#{'${vtm.meansofid}'.split(',')}") 
	private List<String> meansList;

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/titles", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getTitles(HttpServletRequest request){
    	List titles = new ArrayList<>();
    	Map<String, Object> r = new HashMap<>();
    	for(String t: titleList) {
    		Map<String, Object> tmp = new HashMap<>();
    		tmp.put("code", t);
    		tmp.put("name", t);
    		titles.add(tmp);
    	}
    	r.put("titles", titles);
    	r.put("status", Integer.valueOf(200));
        r.put("message", "Success");
    	return new ResponseEntity<Map<String, Object>>(r, HttpStatus.OK);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/states", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getStates(HttpServletRequest request){
    	List states = new ArrayList<>();
    	Map<String, Object> r = new HashMap<>();
    	for(String st: stateList) {
    		Map<String, Object> tmp = new HashMap<>();
    		String[] s = st.split("-");
    		tmp.put("code", s[1]);
    		tmp.put("name", s[0]);
    		states.add(tmp);
    	}
    	r.put("states", states);
    	r.put("status", Integer.valueOf(200));
        r.put("message", "Success");
    	
        return new ResponseEntity<Map<String, Object>>(r, HttpStatus.OK);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @RequestMapping(value = "/meansOfId", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getMeansOfId(HttpServletRequest request){
    	List means = new ArrayList<>();
    	Map<String, Object> r = new HashMap<>();
    	for(String st: meansList) {
    		Map<String, Object> tmp = new HashMap<>();
    		String[] s = st.split("-");
    		tmp.put("name", s[1]);
    		tmp.put("code", s[0]);
    		means.add(tmp);
    	}
    	r.put("meansOfId", means);
    	r.put("status", Integer.valueOf(200));
    	r.put("message", "Success");
    	
    	return new ResponseEntity<Map<String, Object>>(r, HttpStatus.OK);
    }
    
}