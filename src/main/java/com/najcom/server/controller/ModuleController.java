package com.najcom.server.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.server.model.Module;
import com.najcom.server.service.ModuleService;

@RestController
@RequestMapping("modules")
public class ModuleController {
	
	@Autowired
	ModuleService moduleService;
	
	/* List all modules, available, to Admins only */
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public ResponseEntity<List<Module>> listAllModules() {
		List<Module> list = moduleService.listAllModules();
		if(list.size() == 0) {
			return new ResponseEntity<List<Module>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Module>>(list, HttpStatus.OK);
	}
	
	/* Find the modules available to a VTM by its ID. This is an open endpoint */
	@RequestMapping(path = "/vtm/{id}", method = RequestMethod.GET, headers="Accept=application/json")
	public ResponseEntity<Map<String, Object>> getVtmModule(@PathVariable("id") int id, HttpServletRequest request) {
		Map<String, Object> mapping = moduleService.findModuleByVtm(id);
		request.setAttribute("action", "Modules request...");
		return new ResponseEntity<Map<String, Object>>(mapping, HttpStatus.OK);
	}
	
}
