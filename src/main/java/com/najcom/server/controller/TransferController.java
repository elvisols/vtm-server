package com.najcom.server.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.access.service.NipService;
import com.najcom.server.model.Transfer;
import com.najcom.server.service.TransferService;

@RestController
@RequestMapping("transfer")
public class TransferController {
	
	@Value("#{'${vtm.banks}'.split(',')}") 
	private List<String> bankList;
	
	private Timestamp timeNow;
	
	@Autowired
	TransferService transferService;
	
	@Autowired
	private NipService nipService;
	
	@RequestMapping(path = "/interbank", method = RequestMethod.POST, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> interbankTransfer(@RequestBody Transfer transfer, HttpServletRequest request) {
		// Log Interbank Transfer Request
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		transfer.setId(null);
		transfer.setCreated(timeNow);
		transfer.setModified(timeNow);
		transferService.saveOrUpdate(transfer);
		// Send transfer request
		Map<String, Object> response = transferService.transferInterbank(transfer);
		request.setAttribute("action", "Inter-bank fund transfer request... " + transfer);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/intrabank", method = RequestMethod.POST, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> intrabankTransfer(@RequestBody Transfer transfer, HttpServletRequest request) {
		// Log Intrabank Transfer Request
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		transfer.setId(null);
		transfer.setCreated(timeNow);
		transfer.setModified(timeNow);
		transferService.saveOrUpdate(transfer);
		// Send transfer request
		Map<String, Object> response = transferService.transferIntrabank(transfer);
		request.setAttribute("action", "Intra-bank fund transfer request... " + transfer);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/own", method = RequestMethod.POST, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> ownTransfer(@RequestBody Transfer transfer, HttpServletRequest request) {
		// Log Intrabank Transfer Request
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		transfer.setId(null);
		transfer.setCreated(timeNow);
		transfer.setModified(timeNow);
		transferService.saveOrUpdate(transfer);
		// Send transfer request
		Map<String, Object> response = transferService.transferOwn(transfer);
		request.setAttribute("action", "Own fund transfer request... " + transfer);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/credit-account-details/{acc}", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> getCreditAccDetailsIntra(@PathVariable("acc") String acc, HttpServletRequest request) {

		Map<String, Object> response = transferService.getCreditAccount(acc);
		request.setAttribute("action", "Intra credit account details request... " + acc);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	
	}
	
	@RequestMapping(path = "/credit-account-details/{acc}/bankcode/{code}", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> getCreditAccDetailsInter(@PathVariable("acc") String acc, @PathVariable("code") String code, HttpServletRequest request) {
	
		Map<String, Object> response = transferService.getCreditAccount(acc, code);
		request.setAttribute("action", "Inter credit account details request... " + acc + " " + code);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/banks", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getBanks(HttpServletRequest request){
    	List banks = new ArrayList<>();
    	Map<String, Object> r = new HashMap<>();
    	for(String t: bankList) {
    		Map<String, Object> tmp = new HashMap<>();
    		tmp.put("code", t.replace(" ", "-"));
    		tmp.put("name", t);
    		banks.add(tmp);
    	}
    	r.put("status", Integer.valueOf(200));
		r.put("message", "Success");
		r.put("banks", banks);
//    	Map<String, String> req = new HashMap<>();
//    	Map<String, Object> fresponse = nipService.getAllBanks(req);
//		if(fresponse.get("status").equals("200")) {
//			r.put("status", Integer.valueOf(200));
//			r.put("message", "Success");
//			r.put("banks", (List<Map<String, Object>>) fresponse.get("message"));
//		} else {
//			r.put("status", 400);
//			r.put("message", "Fail");
//		}
		
    	return new ResponseEntity<Map<String, Object>>(r, HttpStatus.OK);
    
	}
	
}
