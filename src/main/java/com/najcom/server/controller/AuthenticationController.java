package com.najcom.server.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.najcom.access.service.EntrustService;
import com.najcom.access.service.FlexicubeService;
import com.najcom.access.service.NipService;
import com.najcom.server.security.AuthenticationRequest;
import com.najcom.server.security.TokenUtil;
import com.najcom.server.util.Bank;

@RestController
@RequestMapping("auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    private NipService nipService;
    
    @Autowired
	private EntrustService ent;
    
    @Autowired
    private FlexicubeService fcub;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest, HttpServletRequest request) throws AuthenticationException {
    	Map<String, Object> details;
    	request.setAttribute("action", "Authentication request!");
    	// Call Banks Endpoint to authenticate user
    	details = Bank.authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword(), ent, fcub);
//        if(details.get("status").toString().equals("200")) { // Commented out for testing
        	UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        	String token = this.tokenUtil.generateToken(userDetails);
        	details.put("token", token);
//        }
        
		return new ResponseEntity<Map<String, Object>>(details, HttpStatus.OK);
		
    }

    @RequestMapping(value = "getusername", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getUsername(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String username = this.tokenUtil.getUsernameFromToken(token);
        
        Map<String, Object> details = new HashMap<>();
        details.put("username", username);
        details.put("status", Integer.valueOf(200));
        details.put("message", "Success");
        
        return new ResponseEntity<Map<String, Object>>(details, HttpStatus.OK);
        
    }
    
    /*
    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        String username = this.tokenUtil.getUsernameFromToken(token);
        SpringSecurityUser user = (SpringSecurityUser) userDetailsService.loadUserByUsername(username);

        if (this.tokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = this.tokenUtil.refreshToken(token);
            return ResponseEntity.ok(new AuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }
     */

}