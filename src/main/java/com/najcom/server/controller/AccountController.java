package com.najcom.server.controller;

import java.io.File;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.najcom.server.model.User;
import com.najcom.server.security.TokenUtil;
import com.najcom.server.service.UserService;
import com.najcom.server.util.FileInfo;

@RestController
@RequestMapping("account")
public class AccountController {
	
	private Timestamp timeNow;
	
	@Autowired
	UserService userService;
	
	@Autowired
    private TokenUtil tokenUtil;
	
	@Value("${server.upload.path}")
    private String uploadPath;
	
	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public ResponseEntity<List<User>> listAllUser() {
		List<User> list = userService.listAllUser();
		if(list.size() == 0) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
	}
	
	@RequestMapping(path = "{id}", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public ResponseEntity<User> getuser(@PathVariable("id") int id) {
		User user = userService.findUserById(id);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/intra/{id}", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> getIntraCreditAccount(@PathVariable("id") String id, HttpServletRequest request) {
		Map<String, Object> detail = userService.getIntraCreditAccount(id);
		request.setAttribute("action", "Getting intra credit account for " + id);
		return new ResponseEntity<Map<String, Object>>(detail, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/inter/{id}", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> getInterCreditAccount(@PathVariable("id") String id, HttpServletRequest request) {
		Map<String, Object> detail = userService.getInterCreditAccount(id);
		request.setAttribute("action", "Getting inter credit account for " + id);
		return new ResponseEntity<Map<String, Object>>(detail, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/statements", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> getAccountStatements(HttpServletRequest request) {
		String token = request.getHeader("Authorization");
		String username = this.tokenUtil.getUsernameFromToken(token);
		Map<String, Object> detail = userService.getAccountStatements(username);
		request.setAttribute("action", "Getting account statements for " + username);
		return new ResponseEntity<Map<String, Object>>(detail, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, headers="Accept=application/json")
//	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> add(
			@RequestPart(value = "passport") MultipartFile passport, 
    		@RequestPart(value = "utility") MultipartFile utility, 
    		@RequestPart(value = "identification") MultipartFile identification,
    		@RequestPart(value = "reference", required = true) String reference,
			@RequestPart User user, HttpServletRequest request) throws java.io.IOException {
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		Map<String, Object> msg = new HashMap<>();
		// Process files
		if (!passport.isEmpty() && !utility.isEmpty() && !identification.isEmpty() && !reference.isEmpty()) {
        	try {
				  String passportFilename = passport.getOriginalFilename();
				  String utilityFilename = utility.getOriginalFilename();
				  String identificationFilename = identification.getOriginalFilename();
				  String directoryName = uploadPath +  File.separator + reference;
				  File directory = new File(directoryName);
                  if (! directory.exists()){
				      directory.mkdir();
				  }
				  File passportDestinationFile = new File(directoryName + File.separator + passportFilename);
				  passport.transferTo(passportDestinationFile);
				  File utilityDestinationFile = new File(directoryName + File.separator + utilityFilename);
				  utility.transferTo(utilityDestinationFile);
				  File identificationDestinationFile = new File(directoryName + File.separator + identificationFilename);
				  identification.transferTo(identificationDestinationFile);
				  
				  user.setIdentification(identificationFilename);
				  user.setUtilitybill(utilityFilename);
				  user.setPassport(passportFilename);
				  
			 } catch (Exception e) { 
				 msg.put("status", 500);
	    	 	 msg.put("message", "Error! uploading... " + e.getMessage());
	    		 return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
			 }
        }else{
        	msg.put("status", 401);
    		msg.put("message", "Could not validate request!");
    		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.UNAUTHORIZED);
        }
		
		user.setId(null);
		user.setCreated(timeNow);
		user.setModified(timeNow);
		// Test parameters below: Delete after test
//		user.setAccountNumber("0982323232");
//		user.setCustomerNumber("0099997732");
//		user.setLogger(StringEscapeUtils.unescapeXml("&lt;?xml version='1.0' encoding='UTF-8'?&gt;&lt;S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\"&gt;&lt;S:Body&gt;&lt;CREATECUSTACC_FSFS_RES xmlns=\"http://fcubs.ofss.com/service/FCUBSAccService\"&gt;&lt;FCUBS_HEADER&gt;&lt;SOURCE&gt;FLEXCUBE&lt;/SOURCE&gt;&lt;UBSCOMP&gt;FCUBS&lt;/UBSCOMP&gt;&lt;MSGID&gt;&lt;/MSGID&gt;&lt;CORRELID&gt;&lt;/CORRELID&gt;&lt;USERID&gt;BWORLDUSER&lt;/USERID&gt;&lt;BRANCH&gt;099&lt;/BRANCH&gt;&lt;SERVICE&gt;FCUBSAccService&lt;/SERVICE&gt;&lt;OPERATION&gt;CreateCustAcc&lt;/OPERATION&gt;&lt;SOURCE_OPERATION&gt;&lt;/SOURCE_OPERATION&gt;&lt;DESTINATION&gt;BWONLINE&lt;/DESTINATION&gt;&lt;FUNCTIONID&gt;STDCUSAC&lt;/FUNCTIONID&gt;&lt;ACTION&gt;NEW&lt;/ACTION&gt;&lt;MSGSTAT&gt;SUCCESS&lt;/MSGSTAT&gt;&lt;ADDL/&gt;&lt;/FCUBS_HEADER&gt;&lt;FCUBS_BODY&gt;&lt;Cust-Account-Full&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;CUSTNO&gt;009627594&lt;/CUSTNO&gt;&lt;ACCLS&gt;020003&lt;/ACCLS&gt;&lt;CCY&gt;NGN&lt;/CCY&gt;&lt;CUSTNAME&gt;Cust New&lt;/CUSTNAME&gt;&lt;ADESC&gt;Obi Ukaegbu Kanu&lt;/ADESC&gt;&lt;ACSTATNODR&gt;Y&lt;/ACSTATNODR&gt;&lt;ACSTATNOCR&gt;N&lt;/ACSTATNOCR&gt;&lt;ACSTATSTPAY&gt;N&lt;/ACSTATSTPAY&gt;&lt;DORM&gt;N&lt;/DORM&gt;&lt;ACCTYPE&gt;S&lt;/ACCTYPE&gt;&lt;ACCOPENDT&gt;2017-08-29&lt;/ACCOPENDT&gt;&lt;ALTACC&gt;0727697358&lt;/ALTACC&gt;&lt;FROZEN&gt;N&lt;/FROZEN&gt;&lt;POSTALLOWED&gt;Y&lt;/POSTALLOWED&gt;&lt;CLRBNKCD&gt;044&lt;/CLRBNKCD&gt;&lt;CLRACNO&gt;0727697358&lt;/CLRACNO&gt;&lt;TRKREC&gt;N&lt;/TRKREC&gt;&lt;REFREQ&gt;N&lt;/REFREQ&gt;&lt;ACCSTAT&gt;NORM&lt;/ACCSTAT&gt;&lt;STATSINCE&gt;2017-08-29&lt;/STATSINCE&gt;&lt;INHERITREP&gt;N&lt;/INHERITREP&gt;&lt;AUTOSTATCHANGE&gt;N&lt;/AUTOSTATCHANGE&gt;&lt;DORMPRM&gt;M&lt;/DORMPRM&gt;&lt;CHQBOOK&gt;Y&lt;/CHQBOOK&gt;&lt;PASSBOOK&gt;Y&lt;/PASSBOOK&gt;&lt;CASACC&gt;N&lt;/CASACC&gt;&lt;MT210REQD&gt;N&lt;/MT210REQD&gt;&lt;AUTOREORDERCHKREQ&gt;N&lt;/AUTOREORDERCHKREQ&gt;&lt;LODGEBKFAC&gt;N&lt;/LODGEBKFAC&gt;&lt;ALLWBKPERENTRY&gt;Y&lt;/ALLWBKPERENTRY&gt;&lt;AUTOPROVREQ&gt;N&lt;/AUTOPROVREQ&gt;&lt;PROVCCYTYPE&gt;L&lt;/PROVCCYTYPE&gt;&lt;DEFRECON&gt;N&lt;/DEFRECON&gt;&lt;CONSREQD&gt;N&lt;/CONSREQD&gt;&lt;FUNDING&gt;N&lt;/FUNDING&gt;&lt;ACSTMTDAY&gt;31&lt;/ACSTMTDAY&gt;&lt;ACSTMTCYCLE&gt;M&lt;/ACSTMTCYCLE&gt;&lt;ATM&gt;Y&lt;/ATM&gt;&lt;ACSTMTTYPEP&gt;D&lt;/ACSTMTTYPEP&gt;&lt;ACSTMTTYPS&gt;D&lt;/ACSTMTTYPS&gt;&lt;ACSTMTDAY2&gt;12&lt;/ACSTMTDAY2&gt;&lt;ACSTMTCYCLE2&gt;A&lt;/ACSTMTCYCLE2&gt;&lt;ACSTMTTYPE3&gt;N&lt;/ACSTMTTYPE3&gt;&lt;FLGEXCLRVRTRANS&gt;Y&lt;/FLGEXCLRVRTRANS&gt;&lt;SWPTYPE&gt;1&lt;/SWPTYPE&gt;&lt;MINREQBAL&gt;0&lt;/MINREQBAL&gt;&lt;REGDAPP&gt;N&lt;/REGDAPP&gt;&lt;REGDPER&gt;N&lt;/REGDPER&gt;&lt;PRDLST&gt;D&lt;/PRDLST&gt;&lt;TXNLST&gt;D&lt;/TXNLST&gt;&lt;SPCONDLST&gt;N&lt;/SPCONDLST&gt;&lt;SPCONDTXN&gt;N&lt;/SPCONDTXN&gt;&lt;COUNTRY&gt;NG&lt;/COUNTRY&gt;&lt;ODREQ&gt;N&lt;/ODREQ&gt;&lt;DAYLIGHTLIMIT&gt;0&lt;/DAYLIGHTLIMIT&gt;&lt;WAIVE_ACC_OPEN_CHARGE&gt;N&lt;/WAIVE_ACC_OPEN_CHARGE&gt;&lt;ESCROWTRN&gt;N&lt;/ESCROWTRN&gt;&lt;SALARY_ACCOUNT&gt;N&lt;/SALARY_ACCOUNT&gt;&lt;REPL_CUST_SIG&gt;Y&lt;/REPL_CUST_SIG&gt;&lt;ACCOUNTAUTOCLOSED&gt;N&lt;/ACCOUNTAUTOCLOSED&gt;&lt;CRS_STST_REQD&gt;N&lt;/CRS_STST_REQD&gt;&lt;PROJACC&gt;N&lt;/PROJACC&gt;&lt;PRIVATE_CUSTOMER&gt;N&lt;/PRIVATE_CUSTOMER&gt;&lt;LIMIT_AUTO_CREATE_POOL&gt;N&lt;/LIMIT_AUTO_CREATE_POOL&gt;&lt;DFLT_WAIVER&gt;N&lt;/DFLT_WAIVER&gt;&lt;AUTO_DEBIT_CARD_REQUEST&gt;N&lt;/AUTO_DEBIT_CARD_REQUEST&gt;&lt;AUTO_CHEQUE_BOOK_REQ&gt;N&lt;/AUTO_CHEQUE_BOOK_REQ&gt;&lt;INTERMEDIARY_REQUIRED&gt;N&lt;/INTERMEDIARY_REQUIRED&gt;&lt;ENABLE_SWEEP_IN&gt;Y&lt;/ENABLE_SWEEP_IN&gt;&lt;ENABLE_REV_SWEEP_IN&gt;Y&lt;/ENABLE_REV_SWEEP_IN&gt;&lt;SPDANLSYS&gt;Y&lt;/SPDANLSYS&gt;&lt;MAKER&gt;BWORLDUSER&lt;/MAKER&gt;&lt;MAKERSTAMP&gt;2017-08-29 17:09:21&lt;/MAKERSTAMP&gt;&lt;CHECKER&gt;BWORLDUSER&lt;/CHECKER&gt;&lt;CHECKERSTAMP&gt;2017-08-29 17:09:22&lt;/CHECKERSTAMP&gt;&lt;MODNO&gt;1&lt;/MODNO&gt;&lt;TXNSTAT&gt;O&lt;/TXNSTAT&gt;&lt;AUTHSTAT&gt;A&lt;/AUTHSTAT&gt;&lt;Provision-Main&gt;&lt;PRVACCUI&gt;DUMMY&lt;/PRVACCUI&gt;&lt;/Provision-Main&gt;&lt;Accmaintinstr&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;DTOFLSTMAINT&gt;2017-08-29&lt;/DTOFLSTMAINT&gt;&lt;/Accmaintinstr&gt;&lt;Interim-Details&gt;&lt;GENINTRMSTMT&gt;N&lt;/GENINTRMSTMT&gt;&lt;GENINTRMSTMTMVMT&gt;N&lt;/GENINTRMSTMTMVMT&gt;&lt;GENBALRPT&gt;N&lt;/GENBALRPT&gt;&lt;BALRPTSINCE&gt;950&lt;/BALRPTSINCE&gt;&lt;/Interim-Details&gt;&lt;Acstatuslines&gt;&lt;ACSTATUS&gt;DBTF&lt;/ACSTATUS&gt;&lt;DRGL&gt;120120370&lt;/DRGL&gt;&lt;CRGL&gt;211010803&lt;/CRGL&gt;&lt;DESC&gt;DOUBTFUL STATUS&lt;/DESC&gt;&lt;/Acstatuslines&gt;&lt;Acstatuslines&gt;&lt;ACSTATUS&gt;LOSS&lt;/ACSTATUS&gt;&lt;DRGL&gt;120120370&lt;/DRGL&gt;&lt;CRGL&gt;211010803&lt;/CRGL&gt;&lt;DESC&gt;LOSS STATUS&lt;/DESC&gt;&lt;/Acstatuslines&gt;&lt;Acstatuslines&gt;&lt;ACSTATUS&gt;NORM&lt;/ACSTATUS&gt;&lt;DRGL&gt;120120370&lt;/DRGL&gt;&lt;CRGL&gt;211010803&lt;/CRGL&gt;&lt;DESC&gt;NORMAL STATUS&lt;/DESC&gt;&lt;/Acstatuslines&gt;&lt;Acstatuslines&gt;&lt;ACSTATUS&gt;SUBS&lt;/ACSTATUS&gt;&lt;DRGL&gt;120120370&lt;/DRGL&gt;&lt;CRGL&gt;211010803&lt;/CRGL&gt;&lt;DESC&gt;SUBSTANDARD&lt;/DESC&gt;&lt;/Acstatuslines&gt;&lt;Acstatuslines&gt;&lt;ACSTATUS&gt;WACH&lt;/ACSTATUS&gt;&lt;DRGL&gt;120120370&lt;/DRGL&gt;&lt;CRGL&gt;211010803&lt;/CRGL&gt;&lt;DESC&gt;WATCHFUL STATUS&lt;/DESC&gt;&lt;/Acstatuslines&gt;&lt;Intdetails&gt;&lt;CALCACC&gt;0727697358&lt;/CALCACC&gt;&lt;BOOKACC&gt;0727697358&lt;/BOOKACC&gt;&lt;HASIS&gt;Y&lt;/HASIS&gt;&lt;INTSTARTDT&gt;2017-08-29&lt;/INTSTARTDT&gt;&lt;BOOKACCBRN&gt;099&lt;/BOOKACCBRN&gt;&lt;DRCRADV&gt;N&lt;/DRCRADV&gt;&lt;CHGBOOKACCBRN&gt;099&lt;/CHGBOOKACCBRN&gt;&lt;CHGBOOKACC&gt;0727697358&lt;/CHGBOOKACC&gt;&lt;CHGSTARTDT&gt;2017-08-29&lt;/CHGSTARTDT&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;CONSOLCHGBRN&gt;099&lt;/CONSOLCHGBRN&gt;&lt;/Intdetails&gt;&lt;Tddetails&gt;&lt;AUTOROLL&gt;N&lt;/AUTOROLL&gt;&lt;CLONMAT&gt;N&lt;/CLONMAT&gt;&lt;MOVINTUNCLM&gt;N&lt;/MOVINTUNCLM&gt;&lt;ROLLTYPE&gt;P&lt;/ROLLTYPE&gt;&lt;MOVPRIUNCLM&gt;N&lt;/MOVPRIUNCLM&gt;&lt;INTSTDT&gt;2017-08-29&lt;/INTSTDT&gt;&lt;/Tddetails&gt;&lt;Amount-Dates&gt;&lt;DR_INT_DUE&gt;0&lt;/DR_INT_DUE&gt;&lt;PROVAMT&gt;0&lt;/PROVAMT&gt;&lt;CHG_DUE&gt;0&lt;/CHG_DUE&gt;&lt;WITHDRAWABLE_UNCOLLED_FUND&gt;0&lt;/WITHDRAWABLE_UNCOLLED_FUND&gt;&lt;ACY_OPENING_BAL&gt;0&lt;/ACY_OPENING_BAL&gt;&lt;LCY_OPENING_BAL&gt;0&lt;/LCY_OPENING_BAL&gt;&lt;ACY_TODAY_TOVER_DR&gt;0&lt;/ACY_TODAY_TOVER_DR&gt;&lt;LCY_TODAY_TOVER_DR&gt;0&lt;/LCY_TODAY_TOVER_DR&gt;&lt;ACY_TODAY_TOVER_CR&gt;0&lt;/ACY_TODAY_TOVER_CR&gt;&lt;LCY_TODAY_TOVER_CR&gt;0&lt;/LCY_TODAY_TOVER_CR&gt;&lt;ACY_TANK_CR&gt;0&lt;/ACY_TANK_CR&gt;&lt;ACY_TANK_DR&gt;0&lt;/ACY_TANK_DR&gt;&lt;LCY_TANK_CR&gt;0&lt;/LCY_TANK_CR&gt;&lt;LCY_TANK_DR&gt;0&lt;/LCY_TANK_DR&gt;&lt;ACY_TANK_UNCOLLECTED&gt;0&lt;/ACY_TANK_UNCOLLECTED&gt;&lt;ACY_CURR_BALANCE&gt;0&lt;/ACY_CURR_BALANCE&gt;&lt;LCY_CURR_BALANCE&gt;0&lt;/LCY_CURR_BALANCE&gt;&lt;ACY_BLOCKED_AMOUNT&gt;0&lt;/ACY_BLOCKED_AMOUNT&gt;&lt;ACY_AVL_BAL&gt;0&lt;/ACY_AVL_BAL&gt;&lt;ACY_UNAUTH_DR&gt;0&lt;/ACY_UNAUTH_DR&gt;&lt;ACY_UNAUTH_TANK_DR&gt;0&lt;/ACY_UNAUTH_TANK_DR&gt;&lt;ACY_UNAUTH_CR&gt;0&lt;/ACY_UNAUTH_CR&gt;&lt;ACY_UNAUTH_TANK_CR&gt;0&lt;/ACY_UNAUTH_TANK_CR&gt;&lt;ACY_UNAUTH_UNCOLLECTED&gt;0&lt;/ACY_UNAUTH_UNCOLLECTED&gt;&lt;ACY_UNAUTH_TANK_UNCOLLECTED&gt;0&lt;/ACY_UNAUTH_TANK_UNCOLLECTED&gt;&lt;ACY_ACCRUED_DR_IC&gt;0&lt;/ACY_ACCRUED_DR_IC&gt;&lt;ACY_ACCRUED_CR_IC&gt;0&lt;/ACY_ACCRUED_CR_IC&gt;&lt;DATE_LAST_CR_ACTIVITY&gt;2017-08-29&lt;/DATE_LAST_CR_ACTIVITY&gt;&lt;DATE_LAST_DR_ACTIVITY&gt;2017-08-29&lt;/DATE_LAST_DR_ACTIVITY&gt;&lt;DATE_LAST_DR&gt;2017-08-29&lt;/DATE_LAST_DR&gt;&lt;DATE_LAST_CR&gt;2017-08-29&lt;/DATE_LAST_CR&gt;&lt;ACY_UNCOLLECTED&gt;0&lt;/ACY_UNCOLLECTED&gt;&lt;RECEIVABLE_AMOUNT&gt;0&lt;/RECEIVABLE_AMOUNT&gt;&lt;UTILIZED_AMT&gt;0&lt;/UTILIZED_AMT&gt;&lt;LIMIT_AMOUNT&gt;0&lt;/LIMIT_AMOUNT&gt;&lt;Turnovers&gt;&lt;ACY_MTD_TOVER_DR&gt;0&lt;/ACY_MTD_TOVER_DR&gt;&lt;LCY_MTD_TOVER_DR&gt;0&lt;/LCY_MTD_TOVER_DR&gt;&lt;ACY_MTD_TOVER_CR&gt;0&lt;/ACY_MTD_TOVER_CR&gt;&lt;LCY_MTD_TOVER_CR&gt;0&lt;/LCY_MTD_TOVER_CR&gt;&lt;/Turnovers&gt;&lt;/Amount-Dates&gt;&lt;Noticepref&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;ADVINTREQD&gt;N&lt;/ADVINTREQD&gt;&lt;/Noticepref&gt;&lt;Tod-Renew&gt;&lt;RNW_FLG&gt;N&lt;/RNW_FLG&gt;&lt;/Tod-Renew&gt;&lt;Cust-Acc-Check/&gt;&lt;Custacc-Icccspcn&gt;&lt;BRNCD&gt;099&lt;/BRNCD&gt;&lt;ACCNO&gt;0727697358&lt;/ACCNO&gt;&lt;/Custacc-Icccspcn&gt;&lt;Custacc-Icchspcn&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;/Custacc-Icchspcn&gt;&lt;Custacc-Iccinstr&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;Autodepdetails&gt;&lt;SEQNO&gt;1&lt;/SEQNO&gt;&lt;/Autodepdetails&gt;&lt;/Custacc-Iccinstr&gt;&lt;CustAcc&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACC&gt;0727697358&lt;/ACC&gt;&lt;/CustAcc&gt;&lt;Custacc-Sicdiary/&gt;&lt;Custacc-Stccusbl&gt;&lt;BRN&gt;099&lt;/BRN&gt;&lt;ACCNO&gt;0727697358&lt;/ACCNO&gt;&lt;/Custacc-Stccusbl&gt;&lt;UDFDETAILS&gt;&lt;FLDNAM&gt;REGISTRATION NUMBER&lt;/FLDNAM&gt;&lt;/UDFDETAILS&gt;&lt;UDFDETAILS&gt;&lt;FLDNAM&gt;WEDDING ANNIVERSARY DATE&lt;/FLDNAM&gt;&lt;/UDFDETAILS&gt;&lt;UDFDETAILS&gt;&lt;FLDNAM&gt;ACCOUNT_OLD_NAME&lt;/FLDNAM&gt;&lt;/UDFDETAILS&gt;&lt;Acc-Svcacsig&gt;&lt;ACBRN1&gt;099&lt;/ACBRN1&gt;&lt;ACTNO&gt;0727697358&lt;/ACTNO&gt;&lt;ACDESC&gt;Obi Ukaegbu Kanu&lt;/ACDESC&gt;&lt;ACCCY&gt;NGN&lt;/ACCCY&gt;&lt;/Acc-Svcacsig&gt;&lt;Custacc-Iccintpo&gt;&lt;BRANCH_CODE&gt;099&lt;/BRANCH_CODE&gt;&lt;CUST_AC_NO&gt;0727697358&lt;/CUST_AC_NO&gt;&lt;CCY&gt;NGN&lt;/CCY&gt;&lt;/Custacc-Iccintpo&gt;&lt;Customer-Acc&gt;&lt;BRANCH_CODE&gt;099&lt;/BRANCH_CODE&gt;&lt;CUST_AC_NO&gt;0727697358&lt;/CUST_AC_NO&gt;&lt;/Customer-Acc&gt;&lt;Customer-Accis&gt;&lt;BRANCHCODE&gt;099&lt;/BRANCHCODE&gt;&lt;CUST_AC_NO&gt;0727697358&lt;/CUST_AC_NO&gt;&lt;/Customer-Accis&gt;&lt;Master/&gt;&lt;Sttms-Cust-Acc-Swp&gt;&lt;BRANCH_CODE&gt;099&lt;/BRANCH_CODE&gt;&lt;CUST_AC_NO&gt;0727697358&lt;/CUST_AC_NO&gt;&lt;/Sttms-Cust-Acc-Swp&gt;&lt;/Cust-Account-Full&gt;&lt;FCUBS_WARNING_RESP&gt;&lt;WARNING&gt;&lt;WCODE&gt;ST-CUS62&lt;/WCODE&gt;&lt;WDESC&gt;Cheque Book Facility changed from the default value.&lt;/WDESC&gt;&lt;/WARNING&gt;&lt;WARNING&gt;&lt;WCODE&gt;ST-SAVE-002&lt;/WCODE&gt;&lt;WDESC&gt;Record Successfully Saved and Authorized&lt;/WDESC&gt;&lt;/WARNING&gt;&lt;/FCUBS_WARNING_RESP&gt;&lt;/FCUBS_BODY&gt;&lt;/CREATECUSTACC_FSFS_RES&gt;&lt;/S:Body&gt;&lt;/S:Envelope&gt;"));
		msg = userService.saveOrUpdate(user); 
		// Test parameters below: Delete after test
//		msg.put("status", Integer.valueOf(200));
//		msg.put("accountNumber", "0078273234");
//		msg.put("ticketNumber", "321233232");
//		msg.put("message", "Account created successfully! ");
		
		request.setAttribute("action", "Creating a new account["+user.getPhone()+"] for user " + user.getFirstname() + " " + user.getLastname());
		return new ResponseEntity<Map<String, Object>>(msg, HttpStatus.CREATED);
	}
	
	@RequestMapping(path = "{id}", method = RequestMethod.PUT, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public ResponseEntity<Void> update(@PathVariable("id") int id, @RequestBody User user) throws NoSuchFieldException {
		User u = userService.findUserById(id);
		if(u != null) {
			user.setId(id);
			user.setCreated(timeNow);
			user.setModified(timeNow);
			user.setFirstname(user.getFirstname() == null ? u.getFirstname() : user.getFirstname());
			user.setLastname(user.getLastname() == null ? u.getLastname() : user.getLastname());
			user.setPhone(user.getPhone() == null ? u.getPhone() : user.getPhone());
			user.setEmail(user.getEmail() == null ? u.getEmail() : user.getEmail());
			userService.saveOrUpdate(user);
		} else {
			throw new NoSuchFieldException("Incorrect user's ID !");
		}
		HttpHeaders header =  new HttpHeaders();
		return new ResponseEntity<Void>(header, HttpStatus.OK);
	}
	
	@RequestMapping(path = "{id}", method = RequestMethod.DELETE, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public ResponseEntity<Void> delete(@PathVariable("id") int id) {
		userService.deleteUser(id);
		HttpHeaders header =  new HttpHeaders();
		return new ResponseEntity<Void>(header, HttpStatus.NO_CONTENT);
	}
	
/*	
	@RequestMapping(path = "/mappings", method = RequestMethod.GET, headers="Accept=application/json")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<Map<String, Object>> listAllUserMap() {
		List<User> list = userService.listAllUser();
		if(list.size() == 0) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.NO_CONTENT);
		}
		List<Map<String, Object>> allContent = new ArrayList<>();
		Map<String, Object> content = new HashMap<>();
		content.put("address", "35 Lady Lak");
		content.put("profession", "Programmer");
		allContent.add(content);
		Map<String, Object> content2 = new HashMap<>();
		content2.put("address", "Dolphin Estate");
		content2.put("profession", "Jave Developer");
		allContent.add(content2);
		Map<String, Object> testMap = new HashMap<>();
		testMap.put("firstname", "Elvis");
		testMap.put("lastname", "Orji");
		testMap.put("email", "elvisouk@yahoo.com");
		testMap.put("meta", allContent);
		return new ResponseEntity<Map<String, Object>>(testMap, HttpStatus.OK);
	}
*/
}
