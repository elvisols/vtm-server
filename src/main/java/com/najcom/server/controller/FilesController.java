package com.najcom.server.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Arrays.asList;

import com.najcom.server.model.User;
import com.najcom.server.security.AuthenticationRequest;
import com.najcom.server.security.TokenUtil;
import com.najcom.server.service.UserService;
import com.najcom.server.util.Bank;
import com.najcom.server.util.FileDescription;
import com.najcom.server.util.FileInfo;
import com.najcom.server.util.FileWithParam;

@RestController
@RequestMapping("files")
public class FilesController {
 
    @Autowired
    private TokenUtil tokenUtil;
    
    @Autowired
	UserService userService;
    
    @Value("${server.upload.path}")
    private String uploadPath;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<FileInfo> handleFileUpload(
    		@RequestParam(value = "passport") MultipartFile passport, 
    		@RequestParam(value = "utility") MultipartFile utility, 
    		@RequestParam(value = "identification") MultipartFile identification,
    		@RequestParam(value = "reference", required = true) String reference, HttpServletRequest request){
        FileInfo fileInfo = new FileInfo();
        HttpHeaders headers = new HttpHeaders();
        if (!passport.isEmpty() && !utility.isEmpty() && !identification.isEmpty() && !reference.isEmpty() && tokenUtil.getUsernameFromToken(reference) != null) {
        	System.out.println("Validated !"); 
        	try {
				  String passportFilename = passport.getOriginalFilename();
				  String utilityFilename = utility.getOriginalFilename();
				  String identificationFilename = identification.getOriginalFilename();
				  String directoryName = uploadPath +  File.separator + tokenUtil.getUsernameFromToken(reference);
				  File directory = new File(directoryName);
                  if (! directory.exists()){
				      directory.mkdir();
				  }
				  File passportDestinationFile = new File(directoryName + File.separator + passportFilename);
				  passport.transferTo(passportDestinationFile);
				  File utilityDestinationFile = new File(directoryName + File.separator + utilityFilename);
				  utility.transferTo(utilityDestinationFile);
				  File identificationDestinationFile = new File(directoryName + File.separator + identificationFilename);
				  identification.transferTo(identificationDestinationFile);
				  
				  fileInfo.setFileName(passportDestinationFile.getPath()+" "+utilityDestinationFile.getPath()+" "+identificationDestinationFile.getPath());
				  fileInfo.setStatus(200);
				  fileInfo.setMessage("File Uploaded Successfully.");
				  headers.add("File Uploaded Successfully - ", passportFilename+", "+utilityFilename+", "+identificationFilename);
				  /*** Save to Db  ***/
				  //        byte[] bytes = file.getBytes();
				  //        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("D:/najcom/serverUploads" +  File.separator + originalFilename)));
				  //        stream.write(bytes); ---- asList(fd1, fd2);
				  //        stream.close();
				  /**** Update user account table  *****/
				  User user = userService.findUserByUsername(tokenUtil.getUsernameFromToken(reference));
				  user.setIdentification(identificationFilename);
				  user.setUtilitybill(utilityFilename);
				  user.setPassport(passportFilename);
				  userService.saveOrUpdate(user);
				  request.setAttribute("action", tokenUtil.getUsernameFromToken(reference) + " - File received " + fileInfo.getFileName());
				  return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			 } catch (Exception e) {    
				 fileInfo.setStatus(500);
				 fileInfo.setMessage("Error! uploading... " + e.getMessage());
				 return new ResponseEntity<FileInfo>(fileInfo, HttpStatus.INTERNAL_SERVER_ERROR);
			 }
        }else{
        	fileInfo.setStatus(401);
        	fileInfo.setMessage("Could not validate request!");
        	return new ResponseEntity<FileInfo>(fileInfo, HttpStatus.UNAUTHORIZED);
        }
        
    }
    
    @RequestMapping(value = "/nonMultipartFileUpload", method = RequestMethod.POST, consumes = MediaType.APPLICATION_OCTET_STREAM_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String nonMultipartFileUpload(@RequestBody String is) throws IOException {
        return "{ \"size\" : " + is.length() + ", \"content\":\"" + is + "\" }";
    }

    @RequestMapping(value = "/fileUploadWithControlNameEqualToSomething", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String fileUploadWithControlNameEqualToSomething(@RequestParam(value = "something") MultipartFile file) {
        return "{ \"size\" : "+file.getSize()+", \"name\" : \""+file.getName()+"\", \"originalName\" : \""+file.getOriginalFilename() + "\", \"mimeType\" : \""+file.getContentType()+"\" }";
    }

    @RequestMapping(value = "/textAndReturnHeader", method = RequestMethod.POST, consumes = "multipart/mixed", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> fileUploadWithControlNameEqualToSomething(
            @RequestHeader("Content-Type") String requestContentType,
            @RequestParam(value = "something") MultipartFile file) {
        return ResponseEntity.ok().header(MediaType.APPLICATION_JSON_VALUE).header("X-Request-Header", requestContentType).body("{ \"size\" : " + file.getSize() + ", \"name\" : \"" + file.getName() + "\", \"originalName\" : \"" + file.getOriginalFilename() + "\", \"mimeType\" : \"" + file.getContentType() + "\" }");
    }

}