package com.najcom.server.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
//@ComponentScan(basePackages = "com.najcom.server")
@PropertySource(value = {"classpath:application.properties"})
public class HibernateConfig {

	@Autowired
	private Environment environment;
	
	@Autowired
	private ApplicationContext appContext;
	
	@Bean
	public HikariDataSource getDataSource() {
		HikariDataSource dataSource =  new HikariDataSource();
		dataSource.setDataSourceClassName(environment.getRequiredProperty("jdbc.driverClassname"));
		dataSource.addDataSourceProperty("databaseName",  environment.getRequiredProperty("jdbc.dbName"));
		dataSource.addDataSourceProperty("user", environment.getRequiredProperty("jdbc.username"));
		dataSource.addDataSourceProperty("password", environment.getRequiredProperty("jdbc.password"));
		dataSource.addDataSourceProperty("portNumber", environment.getRequiredProperty("jdbc.port"));
		dataSource.addDataSourceProperty("serverName", environment.getRequiredProperty("jdbc.serverName"));
		return dataSource;
	}
	
	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager manager = new HibernateTransactionManager();
		manager.setSessionFactory(hibernate5SessionFactoryBean().getObject());
		return manager;
	}
	
	@Bean(name = "sessionFactory")
	public LocalSessionFactoryBean hibernate5SessionFactoryBean() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(appContext.getBean(HikariDataSource.class));
//		localSessionFactoryBean.setAnnotatedClasses(VtmUser.class, User.class);
		localSessionFactoryBean.setPackagesToScan("com.najcom.server.model");
		
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		
		localSessionFactoryBean.setHibernateProperties(properties);
		
		return localSessionFactoryBean;
	}
	
}
