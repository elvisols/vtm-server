package com.najcom.server.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.najcom.server.util.ActivityInterceptor;
import com.najcom.server.util.TransactionInterceptor;

@EnableWebMvc
@ComponentScan("com.najcom.server")
public class WebConfig {
    
}
