package com.najcom.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.najcom.access.service.CardService;
import com.najcom.access.service.EntrustService;
import com.najcom.access.service.FlexicubeService;
import com.najcom.access.service.NipService;
import com.najcom.server.util.ActivityInterceptor;
import com.najcom.server.util.TransactionInterceptor;
    
@Configuration
@ComponentScan(basePackages={"com.najcom.server"}, 
				excludeFilters={@ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class)})
public class RootConfig extends WebMvcConfigurerAdapter {
	 
	@Bean
	public ActivityInterceptor activityInterceptor() {
	    return new ActivityInterceptor();
	}
	 
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(activityInterceptor());
	    registry.addInterceptor(new TransactionInterceptor()).addPathPatterns("/protected/*");
	}
	 
	@Bean
    public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver mr = new CommonsMultipartResolver();
//		mr.setMaxUploadSizePerFile(123456789);
        return mr;
    }
	
	@Bean
    public NipService nipService(){
		System.out.println("\n***Creating NipService bean...\n");
        return new NipService();
    }
	
	@Bean
	public FlexicubeService fcubsService(){
		System.out.println("\n***Creating FlexicubeService bean...\n");
		return new FlexicubeService();
	}
	
	@Bean
	public EntrustService entrustService(){
		System.out.println("\n***Creating EntrustService bean...\n");
		return new EntrustService();
	}
	
	@Bean
	public CardService cardService(){
		System.out.println("\n***Creating CardService bean...\n");
		return new CardService();
	}
	
}
