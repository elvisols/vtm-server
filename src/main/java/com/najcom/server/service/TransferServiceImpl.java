package com.najcom.server.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.expr.Instanceof;

import javax.transaction.Transactional;
import javax.xml.datatype.DatatypeConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.najcom.access.service.FlexicubeService;
import com.najcom.access.service.NipService;
import com.najcom.server.dao.TransferDao;
import com.najcom.server.model.Transfer;

@Service
public class TransferServiceImpl implements TransferService {

	@Autowired
	TransferDao transferdao;
	
	@Autowired
	private FlexicubeService fcubs;
	
	@Autowired
	private NipService nipService;
	
	final String[] accountsInter = {"10112233443", "10112233445"};
	final String[] accountsIntra = {"22112233443", "22112233445"};

	@Override
	@Transactional
	public void saveOrUpdate(Transfer transfer) {
		transferdao.saveOrUpdate(transfer);
	}

	@Override
	public Map<String, Object> transferIntrabank(Transfer transfer) { // Flexcube
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to transfer    ************/
//		if(Arrays.asList(accountsIntra).contains(transfer.getDebitAccount())) {
//			System.out.println("Timeeeee: " + (new Date()).getTime());
//			msg.put("tranId", "NJ"+(new Date()).getTime());
//			msg.put("status", Integer.valueOf(200));
//			msg.put("message", "Intra Bank transfer successfully!");
//		} else {
//			msg.put("status", -1);
//			msg.put("message", "Oops transfer not successful");
//		}
		
//		// Send request to bank
//		Map<String, String> reqEntity = new HashMap<>();
//		reqEntity.put("accCcy", "NGN");
//		reqEntity.put("beneficiaryAcctNo", transfer.getCreditAccount());
//		reqEntity.put("branchCode", transfer.getBankCode());
//		reqEntity.put("userBranchCode", transfer.getBankCode());
//		reqEntity.put("payerAccount", transfer.getDebitAccount());
//		reqEntity.put("payerInstrNO", transfer.getDebitAccount());
//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		String now = dateFormat.format(new Date());
//		reqEntity.put("tranDate", now);
//		reqEntity.put("instrAmt", transfer.getAmount().toString());
//		reqEntity.put("instrCcy", "NGN");
//		reqEntity.put("txnCcy", "NGN");
//		reqEntity.put("remarks", transfer.getNarration());
//		Map<String, Object> fresponse = fcubs.doIntraTransfer(reqEntity);
//		Map<String, String> intraResp = (Map<String, String>) fresponse.get("message");
//		if(intraResp.get("responseCode").equals("00")) {
//			msg.put("tranId", (new Date()).getTime());
//			msg.put("status", Integer.valueOf(200));
//			msg.put("message", "Intra Bank transfer successfully! " + intraResp.get("responseData"));
//		} else {
//			msg.put("status", 400);
//			msg.put("message", "Oops transfer not successful");
//		}
		
		
		
		// Check creditAccount
		if(getCreditAccount(transfer.getCreditAccount()).getOrDefault("account", true) instanceof Boolean) {
			msg.put("status", 404);
			msg.put("message", "Oops transfer not successful - No such recipient exist");
		} else {
			Map<String, String> reqEntity = new HashMap<>();
			reqEntity.put("msgId", "BW"+(new Date()).getTime());
			reqEntity.put("creditAccount", transfer.getCreditAccount());
			reqEntity.put("homeBranch", transfer.getBankCode());
			reqEntity.put("debitAccount", transfer.getDebitAccount());
			reqEntity.put("amount", transfer.getAmount().toString());
			reqEntity.put("narration", transfer.getNarration());
			reqEntity.put("paymentReference", String.valueOf(transfer.hashCode())); //(new Date()).getTime()
			Map<String, Object> fresponse;
			try {
				fresponse = fcubs.doOwnTransfer(reqEntity);
				Map<String, Object> intraResp = (Map<String, Object>) fresponse.get("message");
				if(intraResp.get("responseCode").toString().equals("00")) {
					msg.put("tranId", intraResp.get("reference").toString());
					msg.put("status", Integer.valueOf(200));
					msg.put("message", "Intra Bank transfer successfully! " + intraResp.get("responseData"));
				} else {
					msg.put("status", -1);
					msg.put("message", "Oops transfer not successful - " + intraResp.get("responseMsg"));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return msg;
		
	}
	
	@Override
	public Map<String, Object> transferOwn(Transfer transfer) { // Flexicube
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to transfer    ************/
		// Check if debit account and credit account belong to same person
//		Boolean sameUser =  true;
//		if(Arrays.asList(accountsIntra).contains(transfer.getDebitAccount()) && sameUser) {
//			msg.put("tranId", "NJ"+(new Date()).getTime());
//			msg.put("status", Integer.valueOf(200));
//			msg.put("message", "Own transfer successfully!");
//		} else {
//			msg.put("status", -1);
//			msg.put("message", "Oops own transfer not successful");
//		}
		
		// Check creditAccount
		if(getCreditAccount(transfer.getCreditAccount()).getOrDefault("account", true) instanceof Boolean) {
			msg.put("status", 404);
			msg.put("message", "Oops transfer not successful - No such recipient exist");
		} else {
			Map<String, String> reqEntity = new HashMap<>();
			reqEntity.put("msgId", "BW"+(new Date()).getTime());
			reqEntity.put("creditAccount", transfer.getCreditAccount());
			reqEntity.put("homeBranch", transfer.getBankCode());
			reqEntity.put("debitAccount", transfer.getDebitAccount());
			reqEntity.put("amount", transfer.getAmount().toString());
			reqEntity.put("narration", transfer.getNarration());
			reqEntity.put("paymentReference", String.valueOf(transfer.hashCode())); //(new Date()).getTime()
			Map<String, Object> fresponse;
			try {
				fresponse = fcubs.doOwnTransfer(reqEntity);
				Map<String, Object> intraResp = (Map<String, Object>) fresponse.get("message");
				if(intraResp.get("responseCode").toString().equals("00")) {
					msg.put("tranId", intraResp.get("reference").toString());
					msg.put("status", Integer.valueOf(200));
					msg.put("message", "Own account transfer successfully! " + intraResp.get("responseData"));
				} else {
					msg.put("status", -1);
					msg.put("message", "Oops transfer not successful - " + intraResp.get("responseMsg"));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return msg;
		/*
		 * {
			    "tranId": "099MJRL171658GGO",
			    "message": "Own transfer successfully! Successfully Saved and Authorized",
			    "status": "200"
			}
		 */
	}

	@Override
	public Map<String, Object> transferInterbank(Transfer transfer) { // Nibs doInterTransfer
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to transfer    ************/
//		if(Arrays.asList(accountsInter).contains(transfer.getDebitAccount())) {
//			msg.put("tranId", (new Date()).getTime());
//			msg.put("status", Integer.valueOf(200));
//			msg.put("message", "Inter Bank transfer successfully!");
//		} else {
//			msg.put("status", -1);
//			msg.put("message", "Oops transfer not successful");
//		}
		
		Map<String, Object> cAcctTemp = getCreditAccount(transfer.getCreditAccount(), transfer.getBankCode());
		if(cAcctTemp.getOrDefault("account", true) instanceof Boolean) {
			msg.put("status", 404);
			msg.put("message", "Oops transfer not successful - No such recipient exist");
		} else {
			Map<String, Object> cAcct = (Map<String, Object>) cAcctTemp.get("account");
			Map<String, String> reqEntity = new HashMap<>();
			reqEntity.put("messageId", "BW"+(new Date()).getTime());
			reqEntity.put("moduleName", "BWO");
			reqEntity.put("recipientAccountNumber", transfer.getCreditAccount());
			reqEntity.put("bankcode", transfer.getBankCode());
			reqEntity.put("senderAccountNumber", transfer.getDebitAccount());
			reqEntity.put("paymentReference", String.valueOf((new Date()).getTime()));
			reqEntity.put("amount", transfer.getAmount().toString());
			reqEntity.put("narration", transfer.getNarration());
			reqEntity.put("recipientName", cAcct.get("accountName").toString());
			reqEntity.put("relatedNameEnquiryRef", cAcct.get("sessionId").toString());
			reqEntity.put("senderName", transfer.getCreditAccountName()); // should be sender Account name
			reqEntity.put("recipientBvn", cAcct.get("bvn").toString());
			reqEntity.put("kycLevel", cAcct.get("kycLevel").toString());
			Map<String, Object> fresponse = nipService.doInterTransfer(reqEntity);
			if(fresponse.get("status").equals("200")) {
				msg.put("tranId", fresponse.get("message"));
				msg.put("status", Integer.valueOf(200));
				msg.put("message", "Inter Bank transfer successfully!");
			} else {
				msg.put("status", -1);
				msg.put("message", "Oops transfer not successful");
			}
		}
		return msg;
		/*
		 * Success but returned
		 * {
			    "message": "Oops transfer not successful",
			    "status": "400"
			}
		 */
	
	}

	// Just for validation, to validate the recipient
	@Override
	public Map<String, Object> getCreditAccount(String acc) { // Flexicube

		Map<String, Object> responses = new HashMap<>();
		/************* Call Bank API for intra credit account details  *************/
		try {
//			Map<String, Object> content;
//			content = new HashMap<>();
//			
//			if(Arrays.asList(accountsIntra).contains(acc)) {
//				content.put("accountNumber", acc);
//				content.put("accountName", "ORJI,ELVIS,UKAEF3102");
//				content.put("accountStatus", "REGULAR");
//				content.put("accountBranchName", "DUKE HOUSE BRANCH  ABA");
//				content.put("accountCustomerID", "002243236");
//				content.put("accountCustomerNumber", "002243236");
//				content.put("accountAvailableBalance", "1483.20");
//				content.put("accountBranchCode", "246");
//				responses.put("status", Integer.valueOf(200));
//				responses.put("message", "Intra credit account successfully!");
//			} else {
//				content.put("undefine", "undefine");
//				responses.put("status", Integer.valueOf(400));
//				responses.put("message", "Oops no account found!");
//			}
//			responses.put("account", content);
			
			Map<String, String> reqEntity = new HashMap<>();
			// Returning Arrays
			Map<String, Object> fresponse = fcubs.getCustomerAcctsDetail(acc); // NO RESPONSE
			Map<String, Object> ca = (Map<String, Object>) fresponse.get("message");
			if(ca.get("responseCode").toString().equals("00")) {
				responses.put("status", 200);
				responses.put("message", "Customer account details successful.");
				responses.put("account", (List<Map<String, Object>>) ca.get("customerAcctsDetail"));
//				{
//				    "account": [
//				        {
//				            "accountStatus": "REGULAR",
//				            "accountBranchName": "DUKE HOUSE BRANCH  ABA",
//				            "accountCustomerID": "002243236",
//				            "accountName": "ORJI,ELVIS,UKAEF3102",
//				            "accountCustomerName": "ORJI,ELVIS,UKAEF3102",
//				            "accountCustomerNumber": "002243236",
//				            "accountNumber": "0038487705",
//				            "accountAvailableBalance": "1483.20",
//				            "accountBranchCode": "246"
//				        }
//				    ]
//				}
			} else {
				responses.put("status", 404);
				responses.put("message", "Customer account details could not be retrieved - " + ca.get("responseData"));
			}
			return responses;
			
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return responses;
		
	}

	// Verify recipient transaction
	@Override
	public Map<String, Object> getCreditAccount(String acc, String code) { // NIBs customerName
		
		Map<String, Object> responses = new HashMap<>();
		/************* Call Bank API for inter credit account details  *************/
		try {
//			Map<String, Object> content;
//			content = new HashMap<>();
//			
//			if(Arrays.asList(accountsInter).contains(acc)) {
//				content.put("accountNumber", acc);
//				content.put("accountName", "ADETOKUNBO DOSUNMU");
//				content.put("bankCode", code);
//				content.put("kycLevel", 3);
//				content.put("bvn", "09321769123");
//				content.put("sessionId", "999044170907145526299334300775");
//				content.put("msgId", "BW06092017111");
//				responses.put("status", Integer.valueOf(200));
//				responses.put("message", "Intra credit account successfully!");
//			} else {
//				content.put("undefine", "undefine");
//				responses.put("status", Integer.valueOf(400));
//				responses.put("message", "Oops no account found!");
//			}
//			responses.put("account", content);

			Map<String, String> reqEntity = new HashMap<>();
			reqEntity.put("accountNumber", acc);
			reqEntity.put("bankCode", code);
			reqEntity.put("messageId", "BW"+(new Date()).getTime());
			reqEntity.put("moduleName", "BWO");
			Map<String, Object> fresponse = nipService.getCreditAccount(reqEntity);
			if(fresponse.get("status").toString().equals("200")) {
				responses.put("status", Integer.valueOf(200));
				responses.put("message", "Customer account details retrieved.");
				responses.put("account", (Map<String, Object>) fresponse.get("message"));
			} else {
				responses.put("status", Integer.valueOf(404));
				responses.put("message", "Oops no account found!");
			}
			return responses;
			
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return responses;
	
	}
    
}
