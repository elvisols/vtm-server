package com.najcom.server.service;

import com.najcom.server.model.Vtm;

public interface VtmUserService {

	Vtm loadUserByUsername(String username);
	
}
