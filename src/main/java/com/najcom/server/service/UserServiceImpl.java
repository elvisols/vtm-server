package com.najcom.server.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.CharMatcher;
import com.najcom.access.service.FlexicubeService;
import com.najcom.server.dao.UserDao;
import com.najcom.server.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	final String[] accountsInter = {"10112233443", "10112233445"};
	final String[] accountsIntra = {"22112233443", "22112233445"};
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private FlexicubeService fcubs;

	public List<User> listAllUser() {
		return userDao.listAllUser();
	}

	public Map<String, Object> saveOrUpdate(User user) {
		// Send request to bank
		Map<String, Object> saveOrUpdate = new HashMap<>();
		Map<String, String> reqEntityCa = new HashMap<>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String now = dateFormat.format(new Date());
//		userDao.saveOrUpdate(user);
		// Create customer first
		Map<String, String> reqEntityCc = new HashMap<>();
		reqEntityCc.put("accountHolderMaritalStatus", String.valueOf(user.getMaritalstatus().toUpperCase().charAt(0)));
		reqEntityCc.put("customerFullname", user.getFirstname()+" "+user.getLastname()+" "+user.getMiddlename());
		reqEntityCc.put("dob", dateFormat.format(user.getDateofbirth()));
		reqEntityCc.put("dAddr1", user.getAddress1());
		reqEntityCc.put("dAddr2", user.getAddress2());
		reqEntityCc.put("eAddr1", user.getAddress1());
		reqEntityCc.put("eAddr2", user.getAddress2());
		reqEntityCc.put("pAddr1", user.getAddress1());
		reqEntityCc.put("pAddr2", user.getAddress2());
		reqEntityCc.put("birthCountry", user.getBirthcountry());
		reqEntityCc.put("email", user.getEmail());
		reqEntityCc.put("mName", user.getMiddlename());
		reqEntityCc.put("nationalID", "NG-"+CharMatcher.DIGIT.retainFrom(user.getReference()));
		reqEntityCc.put("iNo", "NG-"+CharMatcher.DIGIT.retainFrom(user.getReference()));
		reqEntityCc.put("tNumber", user.getPhone());
		reqEntityCc.put("homePhone", user.getPhone());
		reqEntityCc.put("mNumber", user.getPhone());
		reqEntityCc.put("firstName", user.getFirstname());
		reqEntityCc.put("lastName", user.getLastname());
		reqEntityCc.put("relValue", user.getReligion());
		reqEntityCc.put("spouseName", user.getSpousename());
		reqEntityCc.put("title", user.getTitle());
		reqEntityCc.put("gender", String.valueOf(user.getSex().toUpperCase().charAt(0)));
		
		Map<String, Object> cresponse = fcubs.createCustomerRequest(reqEntityCc);
		Map<String, Object> msgc = (Map<String, Object>) cresponse.get("message");
		try {
			if(msgc.get("responseCode").toString().equals("00")) {
				reqEntityCa.put("accountName", user.getFirstname()+" "+user.getLastname()+" "+user.getMiddlename());
				reqEntityCa.put("customerNo", msgc.get("custNo").toString());
				reqEntityCa.put("accountOpenDate", "2017-08-29"); // Acceptable test date
				System.out.println("Creating account...");
				Map<String, Object> fresponse = fcubs.createCustAccountRequest(reqEntityCa);
				Map<String, Object> msg = (Map<String, Object>) fresponse.get("message");
				System.out.println("Create account responseCode: " + msg.get("responseCode").toString());
				System.out.println("Create account responseData: " + msg.get("responseData").toString());
				if(msg.get("responseCode").toString().equals("00")) {
					System.out.println("Saving new account...");
					user.setAccountNumber(msg.get("custAcNo").toString());
					user.setLogger(msg.get("responseData").toString());
					user.setCustomerNumber(msgc.get("custNo").toString());
					userDao.saveOrUpdate(user);
					saveOrUpdate.put("status", Integer.valueOf(200));
					saveOrUpdate.put("accountNumber", msg.get("custAcNo").toString());
					saveOrUpdate.put("ticketNumber", msgc.get("custNo").toString());
					saveOrUpdate.put("message", "Account created successfully! " + msg.get("responseData").toString());

				} else {
					saveOrUpdate.put("status", Integer.valueOf(-1));
					saveOrUpdate.put("message", "Account creation fail! " + msg.get("responseData").toString());
				}
				
			} else {
				saveOrUpdate.put("status", Integer.valueOf(-11));
				saveOrUpdate.put("message", "Customer creation fail! " + msgc.get("responseData").toString());
			}
				
		} catch(Exception e) {
			e.printStackTrace();
		}
		return saveOrUpdate;
	}

	public void deleteUser(Integer id) {
		userDao.deleteUser(id);
	}

	public User findUserById(Integer id) {
		return userDao.findUserById(id);
	}
	
	public User findUserByUsername(String username) {
		return userDao.findUserByUsername(username);
	}
	
	public User findUserByUsernameAndPassword(String username, String password) {
		return userDao.findUserByUsernameAndPassword(username, password);
	}

	@Override
	public Map<String, Object> getIntraCreditAccount(String id) {
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to check    ************/
//		if(Arrays.asList(accountsIntra).contains(id)) {
//			msg.put("status", 200);
//			msg.put("message", "Intra credit accoount request successfully!");
//			msg.put("accountName", "Ali Obi");
//			msg.put("accountNumber", 5002332322L);
//			msg.put("bankcode", "FG2343S");
//		} else {
//			msg.put("status", 400);
//			msg.put("message", "Intra credit accoount not found.");
//		}
		return msg;
	}

	@Override
	public Map<String, Object> getInterCreditAccount(String id) {
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to issue card    ************/
//		if(Arrays.asList(accountsIntra).contains(id)) {
//			msg.put("status", 200);
//			msg.put("message", "Inter credit accoount request successfully!");
//			msg.put("accountName", "Amaka Balogun");
//			msg.put("accountNumber", 0012332326L);
//			msg.put("bankcode", "PW2913S");
//		} else {
//			msg.put("status", 400);
//			msg.put("message", "Inter credit account not found.");
//		}
		return msg;
	}

	@Override
	public Map<String, Object> getAccountStatements(String accountNumber) {
		
		Map<String, Object> responses = new HashMap<>();
//		List<Map<String, Object>> allContent = new ArrayList<>(); 
//		
//		Map<String, Object> content = new HashMap<>();
//		content.put("accountNumber", "1293843483");
//		content.put("customerNumber", "1290000483");
////		content.put("tranId", (new Date()).getTime());
//		content.put("currency", "NGN");
//		content.put("tranDate", "2017-05-12");
//		content.put("tranAmount", "5000.05");
//		content.put("drCr", "C");
//		content.put("narration", "Just a little token");
//		content.put("branch", "Ojota Ketu");
//		allContent.add(content);
//		Map<String, Object> content2 = new HashMap<>();
//		content2.put("accountNumber", "1293843111");
//		content2.put("customerNumber", "12333333112");
////		content2.put("tranId", (new Date()).getTime());
//		content2.put("currency", "NGN");
//		content2.put("tranDate", "2017-11-22");
//		content2.put("tranAmount", "19080.08");
//		content2.put("drCr", "D");
//		content2.put("narration", "For a project");
//		content2.put("branch", "Maryland Lagos");
////		content2.put("chequeNumber", "103511129");
//		allContent.add(content2);
//		responses.put("status", 200);
//		responses.put("statements", allContent);
//		responses.put("message", "User " + accountNumber + " statement account successful.");
		
		Map<String, String> req = new HashMap<>();
		req.put("accountNumber", accountNumber);
		req.put("startDate", "2017-08-01");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String endDate = dateFormat.format(new Date());
		req.put("endDate", endDate);
		Map<String, Object> fresponse = fcubs.getAccountSummaryAndTransactions(req);
		if(fresponse.get("status").toString().equals("200")) {
			responses.put("status", 200);
			responses.put("statements", (List<Map<String, Object>>) fresponse.get("message"));
			responses.put("message", "User " + accountNumber + " statement account successful.");
		} else {
			responses.put("status", 404);
			responses.put("message", "Could not retrieve user " + accountNumber + " statement account.");
		}
		
		return responses;
		/* from client
		 * responses.put("account", (List<Map<String, Object>>) fresponse.get("message"));
		{
			"status": 200,
			"message": "Statement account successful"
		    "statements": [
		        {
		            "totalLodgement": "0",
		            "accountName": "ORJI,ELVIS,UKAEF3102",
		            "closingBalance": "1483.20",
		            "branchAddress": "DUKE HOUSE 45 AZIKWE ROAD ABIA STATE, NIGERIA",
		            "hasCOT": "1",
		            "branch": "DUKE HOUSE BRANCH  ABA",
		            "authId": "null",
		            "availableBalance": "1483.20",
		            "postedDate": "06-SEP-17",
		            "unserId": " ",
		            "txnInitiationDate": "6-9-2017",
		            "noofLoggement": "0",
		            "productionCode": "020003",
		            "tranEvent": "null",
		            "trnRefNo": "                ",
		            "tranIndicator": "LODGEMENT",
		            "rowNumber": " ",
		            "accountOpeningBalance": "1483.20",
		            "fcyAmount": "null",
		            "noofWithdrawal": "0",
		            "productionCodeDesc": "PREMIER SAVINGS ",
		            "batchNo": " ",
		            "clearedBalance": "1483.20",
		            "unClearedBalance": "0",
		            "eventSrNo": "null",
		            "ccyCode": "NGN",
		            "module": "null",
		            "totalWithdrawal": "0",
		            "stmtDt": "null",
		            "transactionCode": "OPB",
		            "valueDate": "06-SEP-17",
		            "accountNumber": "0038487705",
		            "customerNumber": "002243236",
		            "externalRefNumber": "null",
		            "instrumentCode": "null",
		            "branchCode": "246",
		            "drCr": "C",
		            "narration": "Opening Balance",
		            "tranDate": "06-SEP-2017",
		            "tranAmount": "0",
		            "acEntrySrNo": "null",
		            "runningBalance": "1483.20",
		            "lcyAmount": "0",
		            "totalTranDescription": "null"
		        }
		    ],
		    "status": "200"
		}
		*/

	}

}
