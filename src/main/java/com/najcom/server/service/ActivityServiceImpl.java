package com.najcom.server.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.najcom.server.dao.ActivityDao;
import com.najcom.server.model.Activity;

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	ActivityDao adao;

	@Override
	@Transactional
	public void saveOrUpdate(Activity a) {
		adao.saveOrUpdate(a);
	}

}
