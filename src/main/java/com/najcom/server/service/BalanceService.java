package com.najcom.server.service;

import java.util.Map;

public interface BalanceService {

	public Map<String, Object> findUserBalance(String id);
	
}
