package com.najcom.server.service;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.najcom.access.service.NipService;
import com.najcom.server.dao.CardDao;
import com.najcom.server.model.Card;

@Service
public class CardServiceImpl implements CardService {

	@Autowired
	CardDao cardao;

	@Override
	@Transactional
	public void saveOrUpdate(Card card) {
		cardao.saveOrUpdate(card);
	}

	@Autowired
	private com.najcom.access.service.CardService cardService;
	
	@Override
	public Map<String, Object> sendCardIssuance(Card card) {
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to issue card    ************/
		// TODO Auto-generated method stub
		msg.put("status", Integer.valueOf(200));
		msg.put("message", "Card issuance request.");
		// Test parameters
//		msg.put("holdID", "653454442");
//		msg.put("metadata", "00 <-> 434023923");
		Map<String, String> idc = new HashMap<>();
		idc.put("accountNumber", card.getAccountno());
		Map<String, Object> resp = cardService.issueDebitCard(idc);
		Map<String, String> cResp = (Map<String, String>) resp.get("message");
		msg.put("holdID", cResp.get("holdID"));
		msg.put("metadata", cResp.get("responseCode") + " <-> " + cResp.get("responseData"));
		return msg;
	}

	// Not used before...
	@Override
	public Map<String, Object> sendCardHotlisting(Card card) {
		Map<String, Object> msg = new HashMap<>();
		/***********  Call bank's API to hotlist card  ************/
		// TODO Auto-generated method stub
		msg.put("status", Integer.valueOf(200));
		Map<String, String> bc = new HashMap<>();
		bc.put("expiryDate", "2020-08-10");
		bc.put("moduleName", card.getNameoncard());
		Map<String, Object> resp = cardService.blockCard(bc);
		msg.put("message", "Card hotlisting request successfully! - " + resp.get("message").toString());
		return msg;
	}
    
}
