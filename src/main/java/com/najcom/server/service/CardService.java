package com.najcom.server.service;

import java.util.Map;

import com.najcom.server.model.Card;

public interface CardService {
	
	public void saveOrUpdate(Card card);

	public Map<String, Object> sendCardIssuance(Card card);
	
	public Map<String, Object> sendCardHotlisting(Card card);
	
}
