package com.najcom.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.najcom.server.dao.VtmUserDao;
import com.najcom.server.model.Vtm;

@Service(value = "vtmUserService")
public class VtmUserServiceImpl implements VtmUserService {

	@Autowired
	VtmUserDao vtmUserDao;

	@Override
	public Vtm loadUserByUsername(String username) {
		return vtmUserDao.loadUserByUsername(username);
	}
	
}
