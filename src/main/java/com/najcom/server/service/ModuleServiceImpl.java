package com.najcom.server.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.najcom.server.dao.ModuleDao;
import com.najcom.server.model.Entity;
import com.najcom.server.model.Module;

@Service
@Transactional
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	private ModuleDao moduleDao;

	public List<Module> listAllModules() {
		return moduleDao.listAllModules();
	}

	public void saveOrUpdate(Module module) {
		moduleDao.saveOrUpdate(module);
	}

	public void deleteModule(Integer id) {
		moduleDao.deleteModule(id);
	}

	public Module findModuleById(Integer id) {
		return moduleDao.findModuleById(id);
	}
	
	public Map<String, Object> findModuleByVtm(Integer id) {
		List<Map<String, Object>> allContent = new ArrayList<>();
		Map<String, Object> content; List<String> ancestors;
		for(Module mdl: moduleDao.findModuleByVtm(id)) {
			content = new HashMap<>();
			ancestors = new ArrayList<>();
			int i = 0;
			for(Entity entity: moduleDao.findModuleAncestor(mdl.getEntity().getId())) {
				if(i == 0) {
					content.put("Node", entity.getName());
					content.put("Visibility", Boolean.parseBoolean(entity.getVisibility().toString()));
				}
				ancestors.add(entity.getName());
				i++;
			}
			content.put("ancestors", ancestors);
			allContent.add(content);
		}
		Map<String, Object> modules = new HashMap<>();
		modules.put("modules", allContent);
		return modules;
//		Map<String, Object> content2 = new HashMap<>();
//		content2.put("address", "Dolphin Estate");
//		content2.put("profession", "Jave Developer");
//		allContent.add(content2);
//		Map<String, Object> testMap = new HashMap<>();
//		testMap.put("firstname", "Elvis");
//		testMap.put("lastname", "Orji");
//		testMap.put("email", "elvisouk@yahoo.com");
//		testMap.put("meta", allContent);
//		return testMap;
	}
	
}
