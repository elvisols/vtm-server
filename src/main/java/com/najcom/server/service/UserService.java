package com.najcom.server.service;

import java.util.List;
import java.util.Map;

import com.najcom.server.model.User;

public interface UserService {

	public List<User> listAllUser();
	
	public Map<String, Object> saveOrUpdate(User user);

	public void deleteUser(Integer id);
	
	public User findUserById(Integer id);
	
	public User findUserByUsername(String username);
	
	public User findUserByUsernameAndPassword(String username, String password);
	
	public Map<String, Object> getIntraCreditAccount(String id);
	
	public Map<String, Object> getInterCreditAccount(String id);
	
	public Map<String, Object> getAccountStatements(String username);
	
}
