package com.najcom.server.service;

import java.util.List;
import java.util.Map;

import com.najcom.server.model.Module;

public interface ModuleService {

	public List<Module> listAllModules();
	
	public void saveOrUpdate(Module module);

	public void deleteModule(Integer id);
	
	public Module findModuleById(Integer id);
	
	public Map<String, Object> findModuleByVtm(Integer id);
	
}
