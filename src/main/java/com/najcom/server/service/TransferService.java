package com.najcom.server.service;

import java.util.Map;

import com.najcom.server.model.Transfer;

public interface TransferService {
	
	public void saveOrUpdate(Transfer transfer);

	public Map<String, Object> transferIntrabank(Transfer transfer);
	
	public Map<String, Object> transferOwn(Transfer transfer);
	
	public Map<String, Object> transferInterbank(Transfer transfer);
	
	public Map<String, Object> getCreditAccount(String acc);
	
	public Map<String, Object> getCreditAccount(String acc, String code);
	
}
