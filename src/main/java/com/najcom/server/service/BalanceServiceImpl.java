package com.najcom.server.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codesnippets4all.json.parsers.JSONParser;
import com.codesnippets4all.json.parsers.JsonParserFactory;
import com.najcom.access.service.FlexicubeService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class BalanceServiceImpl implements BalanceService {

	@Autowired
	private FlexicubeService fcubs;
	
	@Override
	public Map<String, Object> findUserBalance(String id) {
		Map<String, Object> responses = new HashMap<>();
		String account = id;
		/************* Call Bank API for bank details  *************/
		try {
//			List<Map<String, Object>> allContent; 
//			Map<String, Object> content;
//			allContent = new ArrayList<>(); 
//			content = new HashMap<>();
//			
//			content.put("accountNumber", "01293843483");
//			content.put("accountName", "Eze Ali");
//			content.put("accountType", "REGULAR");
//			content.put("workingBalance", 12890.23);
//			content.put("clearedBalance", 12890.23);
//			content.put("unclearedBalance", 0.00);
//			content.put("currency", "NGN");
//			content.put("bankDate", "2017-09-12");
//			allContent.add(content);
//			Map<String, Object> content2 = new HashMap<>();
//			content2.put("accountNumber", "20893230091");
//			content2.put("accountName", "The Johnsons Limited");
//			content2.put("accountType", "Credit");
//			content2.put("workingBalance", 912890.07);
//			content2.put("clearedBalance", 912890.07);
//			content2.put("unclearedBalance", 0.00);
//			content2.put("currency", "NGN");
//			content2.put("bankDate", "2017-09-12");
//			allContent.add(content2);
//			responses.put("status", 200);
//			responses.put("message", "Balance enquiry retrieved");
//			responses.put("account", content);
			
			Map<String, String> req = new HashMap<>();
			req.put("accountNumber", account);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String now = dateFormat.format(new Date());
			req.put("startDate", now);
			req.put("endDate", now);
			Map<String, Object> fresponse = fcubs.getAccountSummaryAndTransactions(req);
			if(fresponse.get("status").toString().equals("200")) {
				responses.put("status", 200);
				responses.put("account", (List<Map<String, Object>>) fresponse.get("message"));
				responses.put("message", "User " + account + " statement account successful.");
			} else {
				responses.put("status", 404);
				responses.put("message", "Could not retrieve user " + account + " statement account.");
			}
			System.out.println(responses.get("message") + " Returning for account... " + account);
			return responses;
			
		  } catch (Exception e) {
			  System.out.println("Exception: " + e.getMessage());
			  e.printStackTrace();
		  }
		return responses;
		/* from client
		{
		    "message": "Success",
		    "account": [
		        {
		            "totalLodgement": "0",
		            "accountName": "ORJI,ELVIS,UKAEF3102",
		            "closingBalance": "1483.20",
		            "branchAddress": "DUKE HOUSE 45 AZIKWE ROAD ABIA STATE, NIGERIA",
		            "hasCOT": "1",
		            "branch": "DUKE HOUSE BRANCH  ABA",
		            "authId": "null",
		            "availableBalance": "1483.20",
		            "postedDate": "06-SEP-17",
		            "unserId": " ",
		            "txnInitiationDate": "6-9-2017", * Is this the bankDate?
		            "noofLoggement": "0",
		            "productionCode": "020003", * Is this the accountType?
		            "tranEvent": "null",
		            "trnRefNo": "                ",
		            "tranIndicator": "LODGEMENT",
		            "rowNumber": " ",
		            "accountOpeningBalance": "1483.20",
		            "fcyAmount": "null",
		            "noofWithdrawal": "0",
		            "productionCodeDesc": "PREMIER SAVINGS ",
		            "batchNo": " ",
		            "clearedBalance": "1483.20",
		            "unClearedBalance": "0",
		            "eventSrNo": "null",
		            "ccyCode": "NGN",
		            "module": "null",
		            "totalWithdrawal": "0",
		            "stmtDt": "null",
		            "transactionCode": "OPB",
		            "valueDate": "06-SEP-17",
		            "accountNumber": "0038487705",
		            "customerNumber": "002243236",
		            "externalRefNumber": "null",
		            "instrumentCode": "null",
		            "branchCode": "246",
		            "drCr": "C",
		            "narration": "Opening Balance",
		            "tranDate": "06-SEP-2017",
		            "tranAmount": "0",
		            "acEntrySrNo": "null",
		            "runningBalance": "1483.20",
		            "lcyAmount": "0",
		            "totalTranDescription": "null"
		        }
		    ],
		    "status": "200"
		}
		*/
	}
    
}
