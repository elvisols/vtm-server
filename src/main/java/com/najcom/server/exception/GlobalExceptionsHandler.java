package com.najcom.server.exception;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionsHandler {
	
	@ExceptionHandler(ConstraintViolationException.class)
	public @ResponseBody Object handleCustomException(ConstraintViolationException cVe, HttpServletRequest request) {
		
		System.out.println("Exception caught: " + cVe.getMessage());
		
		String err = "";
		for(ConstraintViolation cveStr: cVe.getConstraintViolations()) {
			err += cveStr.getPropertyPath()+"{"+cveStr.getInvalidValue()+":"+cveStr.getMessage()+"},";
		}
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", "Oops! Bad or incomplete parameters.");
		errorMsg.put("reason", err);
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", cVe.getMessage());
		errorMsg.put("status", Integer.valueOf(400));
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.BAD_REQUEST);
 
	}
	
	@ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
	public  ResponseEntity<Map<String, Object>> handleAuthentionException(AuthenticationCredentialsNotFoundException ae, HttpServletRequest request) {
		
		System.out.println("Exception caught: " + ae.getMessage());
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", "Oops! something went wrong.");
		errorMsg.put("reason", "Authentication failure !");
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", ae.getMessage());
		errorMsg.put("status", Integer.valueOf(401));
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.UNAUTHORIZED);
		
	}
	
	@ExceptionHandler(UsernameNotFoundException.class)
	public  ResponseEntity<Map<String, Object>> handleUsernameNotFoundException(UsernameNotFoundException ae, HttpServletRequest request) {
		
		System.out.println("Authentication fail: " + ae.getMessage());
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", "Sorry, authentication fail!");
		errorMsg.put("reason", "Authentication failure !");
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", ae.getMessage());
		errorMsg.put("status", Integer.valueOf(404));
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.UNAUTHORIZED);
		
	}
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody Object handleGeneralException(HttpServletRequest request, Exception e) throws Exception {
		
		System.out.println("Exception message: " + e.getMessage());
		e.printStackTrace();
		
		Map<String, Object> errorMsg = new HashMap<>();
		errorMsg.put("message", "Oops! something went wrong.");
		errorMsg.put("reason", "Uncaught internal error !");
		errorMsg.put("path", request.getRequestURI());
		errorMsg.put("error", e.getMessage());
		errorMsg.put("status", Integer.valueOf(500));
		errorMsg.put("timestamp", new Date());
		
		return new ResponseEntity<Map<String, Object>>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
