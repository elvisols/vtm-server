package com.najcom.server.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TransactionInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		System.out.println("Got request to save data : name:"+request.getParameter("name"));
		return true;
	}
	/*
	 *  22:58:31,063 INFO  [stdout] (default task-7) ---Before Method Execution---
		22:58:31,063 INFO  [stdout] (default task-7) Got request to save data : name:null
		22:58:31,080 INFO  [stdout] (default task-7) ---method executed---
		22:58:31,081 INFO  [stdout] (default task-7) ---Request Completed---
	 */
}
