package com.najcom.server.util;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.najcom.server.model.Activity;
import com.najcom.server.security.TokenUtil;
import com.najcom.server.service.ActivityService;

public class ActivityInterceptor implements HandlerInterceptor {

	private Timestamp timeNow;
	
	@Autowired
	ActivityService activityService;
	
	@Autowired
    private TokenUtil tokenUtil;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		System.out.println("---Before Method Execution---");
		return true;
	}
	
	@Override
	public void postHandle(	HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView) throws Exception {
		System.out.println("---method executed---");
		Activity a = new Activity();
		timeNow = new Timestamp(java.util.Calendar.getInstance().getTimeInMillis());
		a.setUser(request.getHeader("Authorization") == null || request.getHeader("Authorization").isEmpty() ? "vtm" : tokenUtil.getUsernameFromToken(request.getHeader("Authorization")));
		a.setVtm(request.getHeader("machine") == null ? 0 : Integer.parseInt(request.getHeader("machine")));
		a.setAction(request.getAttribute("action") == null ? "auth" : request.getAttribute("action").toString());
		a.setCreated(timeNow);
		activityService.saveOrUpdate(a);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
			Object handler, Exception ex) throws Exception {
		System.out.println("---Request Completed---");
	}
	
}
