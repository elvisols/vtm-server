package com.najcom.server.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.najcom.access.service.EntrustService;
import com.najcom.access.service.FlexicubeService;

public class Bank {

	private static String phone, token;
	private static final int SUCCESS_CODE = 200;
	private static final int NOT_FOUND = 404;
	
	public static Map<String, Object> authenticate(String phoneUsername, String tokenPassword, EntrustService ent, FlexicubeService fcub) {
		Map<String, Object> responses = new HashMap<>();
		phone = phoneUsername; token = tokenPassword;
		/************* Call Bank API to authentication details  *************/
		List<Map<String, Object>> allContent; 
		Map<String, Object> content;
		allContent = new ArrayList<>();
		content = new HashMap<>();
//		if(!phoneUsername.equals("00000000000")) {
//			content.put("accountNumber", "01293843483");
//			content.put("accountName", "ADETOKUNBO DOSUNMU");
//			content.put("accountType", "REGULAR");
//			allContent.add(content);
//			Map<String, Object> content2 = new HashMap<>();
//			content2.put("accountNumber", "20893230091");
//			content2.put("accountName", "The Johnsons Limited");
//			content2.put("accountType", "CORPORATE");
//			allContent.add(content2);
//			responses.put("accounts", allContent);
//		}
//		responses.put("status", Integer.valueOf(SUCCESS_CODE));
		Map<String, String> userDetail = new HashMap<>();
		userDetail.put("username", phoneUsername);
		userDetail.put("password", tokenPassword);
		Map<String, Object> fresponse = ent.getResponseOnly(userDetail);
		if(fresponse.get("status").toString().equals("200")) {
			responses.put("status", Integer.valueOf(SUCCESS_CODE));
			responses.put("message", fresponse.get("message"));
			responses.put("accounts", getAccounts(phoneUsername, fcub));
		} else {
			responses.put("status", Integer.valueOf(NOT_FOUND));
			responses.put("message",fresponse.get("message"));
		}
		
		// Impossible Code! If the bank responds with NO_CONTENT
		if(responses.isEmpty()) {
			throw new UsernameNotFoundException(String.format("No User Account found with phone '%s' and token '%s'.", phoneUsername, tokenPassword));
		}

		return responses;
	
	}

	public static String getPhone() {
		return phone;
	}

	public static void setPhone(String phone) {
		Bank.phone = phone;
	}

	public static String getToken() {
		return token;
	}

	public static void setToken(String token) {
		Bank.token = token;
	}
	
	@SuppressWarnings("unchecked")
	private static List<Map<String, Object>> getAccounts(String accountNumber, FlexicubeService fcub) {
		// Returning Arrays
		Map<String, Object> fresponse = fcub.getCustomerAcctsDetail(accountNumber); // NO RESPONSE
		Map<String, Object> ca = (Map<String, Object>) fresponse.get("message");
		return(List<Map<String, Object>>) ca.get("customerAcctsDetail");
//			responses.put("account", (List<Map<String, Object>>) ca.get("customerAcctsDetail"));
			/*{
			    "account": [
			        {
			            "accountStatus": "REGULAR",
			            "accountBranchName": "DUKE HOUSE BRANCH  ABA",
			            "accountCustomerID": "002243236",
			            "accountName": "ORJI,ELVIS,UKAEF3102",
			            "accountCustomerName": "ORJI,ELVIS,UKAEF3102",
			            "accountCustomerNumber": "002243236",
			            "accountNumber": "0038487705",
			            "accountAvailableBalance": "1483.20",
			            "accountBranchCode": "246"
			        }
			    ]
			}*/
	}

}
