package com.najcom.server.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;


/**
 * The persistent class for the modules database table.
 * 
 */
@javax.persistence.Entity
@Table(name="modules")
@NamedQuery(name="Module.findAll", query="SELECT m FROM Module m")
public class Module implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private byte available;

	@Lob
	private String comment;

	private Time duration;

	//bi-directional many-to-one association to Entity
	@ManyToOne
	@JoinColumn(name="entity_id")
	private Entity entity;

	//bi-directional many-to-one association to Vtm
	@ManyToOne
	@JoinColumn(name="vtm_id")
	private Vtm vtm;

	public Module() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getAvailable() {
		return this.available;
	}

	public void setAvailable(byte available) {
		this.available = available;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Time getDuration() {
		return this.duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public Entity getEntity() {
		return this.entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public Vtm getVtm() {
		return this.vtm;
	}

	public void setVtm(Vtm vtm) {
		this.vtm = vtm;
	}

}