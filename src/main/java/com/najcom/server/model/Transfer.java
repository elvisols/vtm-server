package com.najcom.server.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the transfers database table.
 * 
 */
@javax.persistence.Entity
@Table(name="transfers")
@NamedQuery(name="Transfer.findAll", query="SELECT t FROM Transfer t")
public class Transfer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Digits(integer=11, fraction=0, message="Reference ID exceeded!")
	private Integer id;

	@NotNull(message="Transfer amount parameter missing!")
	@Digits(integer=11, fraction=2, message="Transfer amount exceeded!")
	private BigDecimal amount;

	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@NotNull(message="Debit account parameter missing!")
	@Pattern(regexp="[0-9]{10,}", message="Debit account must comprise at least 10 digits!")
	private String debitAccount;

	@NotEmpty(message="Please specify transfer narration!")
	private String narration;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@NotNull(message="Credit account not found!")
	@Pattern(regexp="[0-9]{10,}", message="Credit account must comprise at least 10 digits!")
	private String creditAccount;
	
	@NotEmpty(message="Please specify credit account name!")
	private String creditAccountName;

	private String bankCode;
	
	@Override
	public String toString() {
		return "Amount: " + this.amount +
				" DebitAccountNumber: " + this.debitAccount + 
				" Narration: " + this.narration + " ReceiverAccount: " + this.creditAccount + " ReceiverBank: " + this.bankCode;
	}
	
	public Transfer() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}

	public String getCreditAccountName() {
		return creditAccountName;
	}

	public void setCreditAccountName(String creditAccountName) {
		this.creditAccountName = creditAccountName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

}