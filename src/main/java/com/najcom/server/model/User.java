package com.najcom.server.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;


/**
 * The persistent class for the users database table.
 * 
 */
@javax.persistence.Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
			   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Digits(integer=11, fraction=0, message="Reference ID exceeded!")
	private Integer id;
	
	@NotEmpty(message="Please specify title!")
	private String title;
	
	@NotEmpty(message="Please specify reference!")
	private String reference;

	@NotNull(message="Address1 parameter missing!")
	@Size(min = 10, message = "Address1 must be minimum of 10 characters!")
	private String address1;
	
	@Size(min = 10, message = "Address2 must be minimum of 10 characters!")
	private String address2;
	
	@NotEmpty(message="Please specify address type!")
	private String addresstype;

	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@NotNull(message="Email parameter missing!")
	@Pattern(regexp=EMAIL_PATTERN, message="Please enter your correct email address!")
	private String email;

	@NotEmpty(message="Please specify firstname!")
	private String firstname;

	@NotEmpty(message="Please specify lastname!")
	private String lastname;
	
	@NotEmpty(message="Please specify middlename!")
	private String middlename;
	
	@NotEmpty(message="Please specify city!")
	private String city;
	
	@NotEmpty(message="Please specify state!")
	private String state;
	
	@NotEmpty(message="Please specify marital status!")
	private String maritalstatus;
	
	@NotEmpty(message="Please specify sex!")
	private String sex;
	
	@NotEmpty(message="Please specify state of origin!")
	private String stateoforigin;
	
	@NotEmpty(message="Please specify bvn!")
	private String bvn;
	
	@NotEmpty(message="Please specify tax id number!")
	private String taxidnumber;
	
	@NotEmpty(message="Please specify spousename!")
	private String spousename;
	
	@NotEmpty(message="Please specify birth country!")
	private String birthcountry;
	
	@NotEmpty(message="Please specify resident country!")
	private String residentcountry;
	
	@NotEmpty(message="Please specify religion!")
	private String religion;
	
//	@NotEmpty(message="Please specify utility bill!")
	private String utilitybill;
	
//	@NotEmpty(message="Please specify identification!")
	private String identification;
	
	private String accountNumber;
	
	private String customerNumber;
	
	private String logger;
	
//	@NotEmpty(message="Please specify passport!")
	private String passport;

	@NotNull(message="Date of birth must not be null !")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateofbirth;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@NotNull(message="Phone parameter missing!")
	@Pattern(regexp="[0-9]{11,14}", message="Phone number must comprise 11 digits only!")
	private String phone;

//	@NotEmpty(message="Please specify username!")
//	@Length(min=3, max=20, message="Username must comprise at least 3 characters and max 20 characters!")
//	private String username;

	@Transient
	private String message;
	
	public User() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddresstype() {
		return addresstype;
	}

	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMaritalstatus() {
		return maritalstatus;
	}

	public void setMaritalstatus(String maritalstatus) {
		this.maritalstatus = maritalstatus;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getStateoforigin() {
		return stateoforigin;
	}

	public void setStateoforigin(String stateoforigin) {
		this.stateoforigin = stateoforigin;
	}

	public String getBvn() {
		return bvn;
	}

	public void setBvn(String bvn) {
		this.bvn = bvn;
	}

	public String getTaxidnumber() {
		return taxidnumber;
	}

	public void setTaxidnumber(String taxidnumber) {
		this.taxidnumber = taxidnumber;
	}

	public String getSpousename() {
		return spousename;
	}

	public void setSpousename(String spousename) {
		this.spousename = spousename;
	}

	public String getUtilitybill() {
		return utilitybill;
	}

	public void setUtilitybill(String utilitybill) {
		this.utilitybill = utilitybill;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public Date getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(Date dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getBirthcountry() {
		return birthcountry;
	}

	public void setBirthcountry(String birthcountry) {
		this.birthcountry = birthcountry;
	}

	public String getResidentcountry() {
		return residentcountry;
	}

	public void setResidentcountry(String residentcountry) {
		this.residentcountry = residentcountry;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getLogger() {
		return logger;
	}

	public void setLogger(String logger) {
		this.logger = logger;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

}