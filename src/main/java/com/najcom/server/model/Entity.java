package com.najcom.server.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the entities database table.
 * 
 */
@javax.persistence.Entity
@Table(name="entities")
@NamedQuery(name="Entity.findAll", query="SELECT e FROM Entity e")
public class Entity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String description;

	private String name;

	private String parent;
	
	private Boolean visibility;

	//bi-directional many-to-one association to Module
	@OneToMany(mappedBy="entity")
	private List<Module> modules;

	public Entity() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParent() {
		return this.parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public List<Module> getModules() {
		return this.modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public Module addModule(Module module) {
		getModules().add(module);
		module.setEntity(this);

		return module;
	}

	public Module removeModule(Module module) {
		getModules().remove(module);
		module.setEntity(null);

		return module;
	}

	public Boolean getVisibility() {
		return visibility;
	}

	public void setVisibility(Boolean visibility) {
		this.visibility = visibility;
	}

}