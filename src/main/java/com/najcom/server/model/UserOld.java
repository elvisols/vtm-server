package com.najcom.server.model;
//package com.najcom.server.model;
//
//import java.sql.Timestamp;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//import javax.persistence.Transient;
//import javax.validation.constraints.Digits;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Pattern;
//import javax.validation.constraints.Size;
//
//import org.hibernate.validator.constraints.Length;
//import org.hibernate.validator.constraints.NotEmpty;
//
//@Entity
//@Table(name="users")
////@NamedQuery(name="User.findAll", query="SELECT u FROM users u")
//public class User {
//	
//	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"  
//			   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
//	
//	@Digits(integer=11, fraction=0, message="Reference ID exceeded!")
//	private Integer id;
//	
//	@NotEmpty(message="Please specify firstname!")
//	private String firstname;
//
//	@NotEmpty(message="Please specify lastname!")
//	private String lastname;
//
//	@NotNull(message="Email parameter missing!")
//	@Pattern(regexp=EMAIL_PATTERN, message="Please enter your correct email address!")
//	private String email;
//	
//	@NotNull(message="Phone parameter missing!")
//	@Pattern(regexp="[0-9]{11}", message="Phone number must comprise 11 digits only!")
//	private String phone;
//	
//	@NotEmpty(message="Please specify username!")
//	@Length(min=3, max=20, message="Username must comprise at least 3 characters and max 20 characters!")
//	private String username;
//	
//	@NotEmpty(message="Please specify password!")
//	private String password;
//	
//	@NotNull(message="Address parameter missing!")
//	@Size(min = 10, message = "Address must be minimum of 10 characters!")
//	private String address;
//	
//	@NotEmpty(message="User authorities missing")
//	private String authorities;
//	
//	private String message;
//	
//	private Timestamp lastpasswordresetdate;
//	
//	private Timestamp created;
//	
//	private Timestamp modified;
//	
//	public User() {
//		super();
//	}
//	
//	public User(Integer id) {
//		super();
//		this.id = id;
//	}
//	
//	public User(String message) {
//		this.message = message;
//	}
//	
//	@Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", unique = true, nullable = false)
//	public Integer getId() {
//		return id;
//	}
//	public void setId(Integer id) {
//		this.id = id;
//	}
//	
//	@Column(name = "firstname", unique = false, nullable = false, length = 100)
//	public String getFirstname() {
//		return firstname;
//	}
//	public void setFirstname(String firstname) {
//		this.firstname = firstname;
//	}
//	
//	@Column(name = "lastname", unique = false, nullable = false, length = 100)
//	public String getLastname() {
//		return lastname;
//	}
//	public void setLastname(String lastname) {
//		this.lastname = lastname;
//	}
//	
//	@Column(name = "address", unique = false, nullable = false, length = 250)
//	public String getAddress() {
//		return address;
//	}
//	public void setAddress(String address) {
//		this.address = address;
//	}
//	
//	@Column(name = "email", unique = false, nullable = false, length = 100)
//	public String getEmail() {
//		return email;
//	}
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	
//	@Column(name = "phone", unique = false, nullable = false, length = 50)
//	public String getPhone() {
//		return phone;
//	}
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//	
//	@Column(name = "username", unique = false, nullable = false, length = 100)
//	public String getUsername() {
//		return username;
//	}
//	public void setUsername(String username) {
//		this.username = username;
//	}
//	
//	@Column(name = "password", unique = false, nullable = false, length = 150)
//	public String getPassword() {
//		return password;
//	}
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	
//	@Transient
//	public String getMessage() {
//		return message;
//	}
//	public void setMessage(String message) {
//		this.message = message;
//	}
//	
//	@Column(name = "authorities", unique = false, nullable = false, length = 250)
//	public String getAuthorities() {
//		return authorities;
//	}
//
//	public void setAuthorities(String authorities) {
//		this.authorities = authorities;
//	}
//
//	@Column(name = "lastpasswordresetdate", unique = false, nullable = true)
//	public Timestamp getLastpasswordresetdate() {
//		return lastpasswordresetdate;
//	}
//
//	public void setLastpasswordresetdate(Timestamp lastpasswordresetdate) {
//		this.lastpasswordresetdate = lastpasswordresetdate;
//	}
//	
//	@Column(name = "created", unique = false, nullable = false)
//	public Timestamp getCreated() {
//		return created;
//	}
//	public void setCreated(Timestamp created) {
//		this.created = created;
//	}
//	
//	@Column(name = "modified", unique = false, nullable = false)
//	public Timestamp getModified() {
//		return modified;
//	}
//	public void setModified(Timestamp modified) {
//		this.modified = modified;
//	}
//
//}
