//package com.najcom.server.model;
//
//import java.io.Serializable;
//import javax.persistence.*;
//
//
///**
// * The persistent class for the vtmrootview database table.
// * 
// */
//@javax.persistence.Entity
//@NamedQuery(name="Vtmrootview.findAll", query="SELECT v FROM Vtmrootview v")
//public class Vtmrootview implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	private String lev1;
//
//	private int lev1Id;
//
//	private String lev2;
//
//	private int lev2Id;
//
//	private String lev3;
//
//	private int lev3Id;
//
//	private String lev4;
//
//	private int lev4Id;
//
//	public Vtmrootview() {
//	}
//
//	public String getLev1() {
//		return this.lev1;
//	}
//
//	public void setLev1(String lev1) {
//		this.lev1 = lev1;
//	}
//
//	public int getLev1Id() {
//		return this.lev1Id;
//	}
//
//	public void setLev1Id(int lev1Id) {
//		this.lev1Id = lev1Id;
//	}
//
//	public String getLev2() {
//		return this.lev2;
//	}
//
//	public void setLev2(String lev2) {
//		this.lev2 = lev2;
//	}
//
//	public int getLev2Id() {
//		return this.lev2Id;
//	}
//
//	public void setLev2Id(int lev2Id) {
//		this.lev2Id = lev2Id;
//	}
//
//	public String getLev3() {
//		return this.lev3;
//	}
//
//	public void setLev3(String lev3) {
//		this.lev3 = lev3;
//	}
//
//	public int getLev3Id() {
//		return this.lev3Id;
//	}
//
//	public void setLev3Id(int lev3Id) {
//		this.lev3Id = lev3Id;
//	}
//
//	public String getLev4() {
//		return this.lev4;
//	}
//
//	public void setLev4(String lev4) {
//		this.lev4 = lev4;
//	}
//
//	public int getLev4Id() {
//		return this.lev4Id;
//	}
//
//	public void setLev4Id(int lev4Id) {
//		this.lev4Id = lev4Id;
//	}
//
//}