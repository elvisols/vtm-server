package com.najcom.server.model;
//package com.najcom.server.model;
//
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.persistence.Transient;
//
//@Entity
//@Table(name="vtms")
//public class VtmUser {
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
//	@Column(name = "username", unique = true, nullable = false)
//	private String username;
//	@Column(name = "password", nullable = false)
//	private String password;
//	@Column(name = "authorities", nullable = false)
//	private String authorities;
//	@Column(name = "lastpasswordresetdate", unique = true, nullable = false)
//	private Date lastPasswordResetDate;
//	@Transient
//	private String message;
//
//	public VtmUser() {}
//	
//	public VtmUser(String username, String password) {
//		super();
//		this.username = username;
//		this.password = password;
//	}
//	
//	public VtmUser(String username, String password, String authorities) {
//		super();
//		this.username = username;
//		this.password = password;
//		this.authorities = authorities;
//	}
//	
//	public VtmUser(String message) {
//		this.message = message;
//	}
//
//	public Long getId() {
//		return id;
//	}
//	
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getUsername() {
//		return username;
//	}
//	public void setUsername(String username) {
//		this.username = username;
//	}
//	public String getPassword() {
//		return password;
//	}
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	public Date getLastPasswordResetDate() {
//		return lastPasswordResetDate;
//	}
//	public void setLastPasswordResetDate(Date lastPasswordResetDate) {
//		this.lastPasswordResetDate = lastPasswordResetDate;
//	}
//
//	public String getAuthorities() {
//		return authorities;
//	}
//
//	public void setAuthorities(String authorities) {
//		this.authorities = authorities;
//	}
//	
//	@Override
//	public String toString() {
//		return "id:" + this.id + " username:" + this.username + " password:" + this.password + " authorities:" + this.authorities;
//	}
//
//}
