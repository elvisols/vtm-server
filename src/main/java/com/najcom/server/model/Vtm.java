package com.najcom.server.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the vtms database table.
 * 
 */
@javax.persistence.Entity
@Table(name="vtms")
@NamedQuery(name="Vtm.findAll", query="SELECT v FROM Vtm v")
public class Vtm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String authorities;

	private String bank;

	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastpasswordresetdate;

	private String manufacturer;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	private String password;

	private String username;
	
	@Transient
	private String message;

	//bi-directional many-to-one association to Module
	@OneToMany(mappedBy="vtm")
	private List<Module> modules;

	public Vtm() {
	}
	
	public Vtm(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public Vtm(String username, String password, String authorities) {
		super();
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public String getBank() {
		return this.bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastpasswordresetdate() {
		return this.lastpasswordresetdate;
	}

	public void setLastpasswordresetdate(Date lastpasswordresetdate) {
		this.lastpasswordresetdate = lastpasswordresetdate;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Module> getModules() {
		return this.modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public Module addModule(Module module) {
		getModules().add(module);
		module.setVtm(this);

		return module;
	}

	public Module removeModule(Module module) {
		getModules().remove(module);
		module.setVtm(null);

		return module;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}