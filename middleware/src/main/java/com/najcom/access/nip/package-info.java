@javax.xml.bind.annotation.XmlSchema(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", 
xmlns = { 
	      @javax.xml.bind.annotation.XmlNs(prefix = "out", 
	         namespaceURI="http://api.accessbankplc.com/nip/v2/outgoing")
	   },
elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.najcom.access.nip;
