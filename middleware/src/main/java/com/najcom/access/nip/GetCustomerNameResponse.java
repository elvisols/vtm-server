
package com.najcom.access.nip;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerNameResult" type="{http://api.accessbankplc.com/nip/v2/outgoing}EnquiryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerNameResult"
})
@XmlRootElement(name = "GetCustomerNameResponse")
public class GetCustomerNameResponse {

    @XmlElementRef(name = "GetCustomerNameResult", namespace = "http://api.accessbankplc.com/nip/v2/outgoing", type = JAXBElement.class, required = false)
    protected JAXBElement<EnquiryResponse> getCustomerNameResult;

    /**
     * Gets the value of the getCustomerNameResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EnquiryResponse }{@code >}
     *     
     */
    public JAXBElement<EnquiryResponse> getGetCustomerNameResult() {
        return getCustomerNameResult;
    }

    /**
     * Sets the value of the getCustomerNameResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EnquiryResponse }{@code >}
     *     
     */
    public void setGetCustomerNameResult(JAXBElement<EnquiryResponse> value) {
        this.getCustomerNameResult = value;
    }

}
