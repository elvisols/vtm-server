
package com.najcom.access.nip;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransferRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransferRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BankCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MessageID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModuleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Narration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PaymentReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RecipientAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RecipientBvn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RecipientKycLevel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RecipientName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RelatedNameEnquiryRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SenderAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SenderName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransactionLocation" type="{http://api.accessbankplc.com/nip/v2/outgoing}Location"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransferRequest", propOrder = {
    "amount",
    "bankCode",
    "messageID",
    "moduleName",
    "narration",
    "paymentReference",
    "recipientAccountNumber",
    "recipientBvn",
    "recipientKycLevel",
    "recipientName",
    "relatedNameEnquiryRef",
    "senderAccountNumber",
    "senderName",
    "transactionLocation"
})
public class TransferRequest {

    @XmlElement(name = "Amount", required = true)
    protected BigDecimal amount;
    @XmlElement(name = "BankCode", required = true, nillable = true)
    protected String bankCode;
    @XmlElement(name = "MessageID", required = true, nillable = true)
    protected String messageID;
    @XmlElement(name = "ModuleName", required = true, nillable = true)
    protected String moduleName;
    @XmlElement(name = "Narration", required = true, nillable = true)
    protected String narration;
    @XmlElement(name = "PaymentReference", required = true, nillable = true)
    protected String paymentReference;
    @XmlElement(name = "RecipientAccountNumber", required = true, nillable = true)
    protected String recipientAccountNumber;
    @XmlElement(name = "RecipientBvn", required = true, nillable = true)
    protected String recipientBvn;
    @XmlElement(name = "RecipientKycLevel", required = true, type = Integer.class, nillable = true)
    protected Integer recipientKycLevel;
    @XmlElement(name = "RecipientName", required = true, nillable = true)
    protected String recipientName;
    @XmlElement(name = "RelatedNameEnquiryRef", required = true, nillable = true)
    protected String relatedNameEnquiryRef;
    @XmlElement(name = "SenderAccountNumber", required = true, nillable = true)
    protected String senderAccountNumber;
    @XmlElement(name = "SenderName", required = true, nillable = true)
    protected String senderName;
    @XmlElement(name = "TransactionLocation", required = true, nillable = true)
    protected Location transactionLocation;

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the bankCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the messageID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageID() {
        return messageID;
    }

    /**
     * Sets the value of the messageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageID(String value) {
        this.messageID = value;
    }

    /**
     * Gets the value of the moduleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * Sets the value of the moduleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModuleName(String value) {
        this.moduleName = value;
    }

    /**
     * Gets the value of the narration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets the value of the narration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNarration(String value) {
        this.narration = value;
    }

    /**
     * Gets the value of the paymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * Sets the value of the paymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReference(String value) {
        this.paymentReference = value;
    }

    /**
     * Gets the value of the recipientAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientAccountNumber() {
        return recipientAccountNumber;
    }

    /**
     * Sets the value of the recipientAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientAccountNumber(String value) {
        this.recipientAccountNumber = value;
    }

    /**
     * Gets the value of the recipientBvn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientBvn() {
        return recipientBvn;
    }

    /**
     * Sets the value of the recipientBvn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientBvn(String value) {
        this.recipientBvn = value;
    }

    /**
     * Gets the value of the recipientKycLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRecipientKycLevel() {
        return recipientKycLevel;
    }

    /**
     * Sets the value of the recipientKycLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRecipientKycLevel(Integer value) {
        this.recipientKycLevel = value;
    }

    /**
     * Gets the value of the recipientName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     * Sets the value of the recipientName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientName(String value) {
        this.recipientName = value;
    }

    /**
     * Gets the value of the relatedNameEnquiryRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedNameEnquiryRef() {
        return relatedNameEnquiryRef;
    }

    /**
     * Sets the value of the relatedNameEnquiryRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedNameEnquiryRef(String value) {
        this.relatedNameEnquiryRef = value;
    }

    /**
     * Gets the value of the senderAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderAccountNumber() {
        return senderAccountNumber;
    }

    /**
     * Sets the value of the senderAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderAccountNumber(String value) {
        this.senderAccountNumber = value;
    }

    /**
     * Gets the value of the senderName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * Sets the value of the senderName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderName(String value) {
        this.senderName = value;
    }

    /**
     * Gets the value of the transactionLocation property.
     * 
     * @return
     *     possible object is
     *     {@link Location }
     *     
     */
    public Location getTransactionLocation() {
        return transactionLocation;
    }

    /**
     * Sets the value of the transactionLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Location }
     *     
     */
    public void setTransactionLocation(Location value) {
        this.transactionLocation = value;
    }

}
