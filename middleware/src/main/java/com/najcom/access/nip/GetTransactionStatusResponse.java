
package com.najcom.access.nip;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTransactionStatusResult" type="{http://api.accessbankplc.com/nip/v2/outgoing}StatusResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionStatusResult"
})
@XmlRootElement(name = "GetTransactionStatusResponse")
public class GetTransactionStatusResponse {

    @XmlElementRef(name = "GetTransactionStatusResult", namespace = "http://api.accessbankplc.com/nip/v2/outgoing", type = JAXBElement.class, required = false)
    protected JAXBElement<StatusResponse> getTransactionStatusResult;

    /**
     * Gets the value of the getTransactionStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StatusResponse }{@code >}
     *     
     */
    public JAXBElement<StatusResponse> getGetTransactionStatusResult() {
        return getTransactionStatusResult;
    }

    /**
     * Sets the value of the getTransactionStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StatusResponse }{@code >}
     *     
     */
    public void setGetTransactionStatusResult(JAXBElement<StatusResponse> value) {
        this.getTransactionStatusResult = value;
    }

}
