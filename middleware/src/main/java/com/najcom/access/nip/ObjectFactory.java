
package com.najcom.access.nip;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.najcom.access.nip package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MemberBank_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "MemberBank");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _TransferRequest_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "TransferRequest");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _StatusRequest_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "StatusRequest");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _Banks_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "Banks");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _BankListResponse_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "BankListResponse");
    private final static QName _BankListRequest_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "BankListRequest");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _StatusResponse_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "StatusResponse");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _EnquiryResponse_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "EnquiryResponse");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _EnquiryRequest_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "EnquiryRequest");
    private final static QName _Location_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "Location");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _TransferResponse_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "TransferResponse");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _GetCustomerNameRequest_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "request");
    private final static QName _GetTransactionStatusResponseGetTransactionStatusResult_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "GetTransactionStatusResult");
    private final static QName _GetAllBanksResponseGetAllBanksResult_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "GetAllBanksResult");
    private final static QName _GetCustomerNameResponseGetCustomerNameResult_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "GetCustomerNameResult");
    private final static QName _TransferFundsResponseTransferFundsResult_QNAME = new QName("http://api.accessbankplc.com/nip/v2/outgoing", "TransferFundsResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.najcom.access.nip
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StatusRequest }
     * 
     */
    public StatusRequest createStatusRequest() {
        return new StatusRequest();
    }

    /**
     * Create an instance of {@link EnquiryResponse }
     * 
     */
    public EnquiryResponse createEnquiryResponse() {
        return new EnquiryResponse();
    }

    /**
     * Create an instance of {@link StatusResponse }
     * 
     */
    public StatusResponse createStatusResponse() {
        return new StatusResponse();
    }

    /**
     * Create an instance of {@link BankListRequest }
     * 
     */
    public BankListRequest createBankListRequest() {
        return new BankListRequest();
    }

    /**
     * Create an instance of {@link TransferRequest }
     * 
     */
    public TransferRequest createTransferRequest() {
        return new TransferRequest();
    }

    /**
     * Create an instance of {@link TransferFundsResponse }
     * 
     */
    public TransferFundsResponse createTransferFundsResponse() {
        return new TransferFundsResponse();
    }

    /**
     * Create an instance of {@link TransferResponse }
     * 
     */
    public TransferResponse createTransferResponse() {
        return new TransferResponse();
    }

    /**
     * Create an instance of {@link MemberBank }
     * 
     */
    public MemberBank createMemberBank() {
        return new MemberBank();
    }

    /**
     * Create an instance of {@link GetCustomerNameResponse }
     * 
     */
    public GetCustomerNameResponse createGetCustomerNameResponse() {
        return new GetCustomerNameResponse();
    }

    /**
     * Create an instance of {@link BankListResponse }
     * 
     */
    public BankListResponse createBankListResponse() {
        return new BankListResponse();
    }

    /**
     * Create an instance of {@link GetCustomerName }
     * 
     */
    public GetCustomerName createGetCustomerName() {
        return new GetCustomerName();
    }

    /**
     * Create an instance of {@link EnquiryRequest }
     * 
     */
    public EnquiryRequest createEnquiryRequest() {
        return new EnquiryRequest();
    }

    /**
     * Create an instance of {@link GetTransactionStatus }
     * 
     */
    public GetTransactionStatus createGetTransactionStatus() {
        return new GetTransactionStatus();
    }

    /**
     * Create an instance of {@link TransferFunds }
     * 
     */
    public TransferFunds createTransferFunds() {
        return new TransferFunds();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponse }
     * 
     */
    public GetTransactionStatusResponse createGetTransactionStatusResponse() {
        return new GetTransactionStatusResponse();
    }

    /**
     * Create an instance of {@link Banks }
     * 
     */
    public Banks createBanks() {
        return new Banks();
    }

    /**
     * Create an instance of {@link GetAllBanksResponse }
     * 
     */
    public GetAllBanksResponse createGetAllBanksResponse() {
        return new GetAllBanksResponse();
    }

    /**
     * Create an instance of {@link GetAllBanks }
     * 
     */
    public GetAllBanks createGetAllBanks() {
        return new GetAllBanks();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberBank }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "MemberBank")
    public JAXBElement<MemberBank> createMemberBank(MemberBank value) {
        return new JAXBElement<MemberBank>(_MemberBank_QNAME, MemberBank.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "TransferRequest")
    public JAXBElement<TransferRequest> createTransferRequest(TransferRequest value) {
        return new JAXBElement<TransferRequest>(_TransferRequest_QNAME, TransferRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "StatusRequest")
    public JAXBElement<StatusRequest> createStatusRequest(StatusRequest value) {
        return new JAXBElement<StatusRequest>(_StatusRequest_QNAME, StatusRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Banks }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "Banks")
    public JAXBElement<Banks> createBanks(Banks value) {
        return new JAXBElement<Banks>(_Banks_QNAME, Banks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "BankListResponse")
    public JAXBElement<BankListResponse> createBankListResponse(BankListResponse value) {
        return new JAXBElement<BankListResponse>(_BankListResponse_QNAME, BankListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "BankListRequest")
    public JAXBElement<BankListRequest> createBankListRequest(BankListRequest value) {
        return new JAXBElement<BankListRequest>(_BankListRequest_QNAME, BankListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "StatusResponse")
    public JAXBElement<StatusResponse> createStatusResponse(StatusResponse value) {
        return new JAXBElement<StatusResponse>(_StatusResponse_QNAME, StatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "EnquiryResponse")
    public JAXBElement<EnquiryResponse> createEnquiryResponse(EnquiryResponse value) {
        return new JAXBElement<EnquiryResponse>(_EnquiryResponse_QNAME, EnquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnquiryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "EnquiryRequest")
    public JAXBElement<EnquiryRequest> createEnquiryRequest(EnquiryRequest value) {
        return new JAXBElement<EnquiryRequest>(_EnquiryRequest_QNAME, EnquiryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Location }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "Location")
    public JAXBElement<Location> createLocation(Location value) {
        return new JAXBElement<Location>(_Location_QNAME, Location.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "TransferResponse")
    public JAXBElement<TransferResponse> createTransferResponse(TransferResponse value) {
        return new JAXBElement<TransferResponse>(_TransferResponse_QNAME, TransferResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnquiryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "request", scope = GetCustomerName.class)
    public JAXBElement<EnquiryRequest> createGetCustomerNameRequest(EnquiryRequest value) {
        return new JAXBElement<EnquiryRequest>(_GetCustomerNameRequest_QNAME, EnquiryRequest.class, GetCustomerName.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "GetTransactionStatusResult", scope = GetTransactionStatusResponse.class)
    public JAXBElement<StatusResponse> createGetTransactionStatusResponseGetTransactionStatusResult(StatusResponse value) {
        return new JAXBElement<StatusResponse>(_GetTransactionStatusResponseGetTransactionStatusResult_QNAME, StatusResponse.class, GetTransactionStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "GetAllBanksResult", scope = GetAllBanksResponse.class)
    public JAXBElement<BankListResponse> createGetAllBanksResponseGetAllBanksResult(BankListResponse value) {
        return new JAXBElement<BankListResponse>(_GetAllBanksResponseGetAllBanksResult_QNAME, BankListResponse.class, GetAllBanksResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "request", scope = GetTransactionStatus.class)
    public JAXBElement<StatusRequest> createGetTransactionStatusRequest(StatusRequest value) {
        return new JAXBElement<StatusRequest>(_GetCustomerNameRequest_QNAME, StatusRequest.class, GetTransactionStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "GetCustomerNameResult", scope = GetCustomerNameResponse.class)
    public JAXBElement<EnquiryResponse> createGetCustomerNameResponseGetCustomerNameResult(EnquiryResponse value) {
        return new JAXBElement<EnquiryResponse>(_GetCustomerNameResponseGetCustomerNameResult_QNAME, EnquiryResponse.class, GetCustomerNameResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "request", scope = GetAllBanks.class)
    public JAXBElement<BankListRequest> createGetAllBanksRequest(BankListRequest value) {
        return new JAXBElement<BankListRequest>(_GetCustomerNameRequest_QNAME, BankListRequest.class, GetAllBanks.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "TransferFundsResult", scope = TransferFundsResponse.class)
    public JAXBElement<TransferResponse> createTransferFundsResponseTransferFundsResult(TransferResponse value) {
        return new JAXBElement<TransferResponse>(_TransferFundsResponseTransferFundsResult_QNAME, TransferResponse.class, TransferFundsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/nip/v2/outgoing", name = "request", scope = TransferFunds.class)
    public JAXBElement<TransferRequest> createTransferFundsRequest(TransferRequest value) {
        return new JAXBElement<TransferRequest>(_GetCustomerNameRequest_QNAME, TransferRequest.class, TransferFunds.class, value);
    }

}
