
package com.najcom.access.nip;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransferFundsResult" type="{http://api.accessbankplc.com/nip/v2/outgoing}TransferResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transferFundsResult"
})
@XmlRootElement(name = "TransferFundsResponse")
public class TransferFundsResponse {

    @XmlElementRef(name = "TransferFundsResult", namespace = "http://api.accessbankplc.com/nip/v2/outgoing", type = JAXBElement.class, required = false)
    protected JAXBElement<TransferResponse> transferFundsResult;

    /**
     * Gets the value of the transferFundsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TransferResponse }{@code >}
     *     
     */
    public JAXBElement<TransferResponse> getTransferFundsResult() {
        return transferFundsResult;
    }

    /**
     * Sets the value of the transferFundsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TransferResponse }{@code >}
     *     
     */
    public void setTransferFundsResult(JAXBElement<TransferResponse> value) {
        this.transferFundsResult = value;
    }

}
