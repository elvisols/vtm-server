package com.najcom.access.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;


public class JaxWsLoggingHandler implements SOAPHandler<SOAPMessageContext> {
	 
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
    @Override
    public void close(MessageContext arg0) {
    }
 
    @Override
    public boolean handleFault(SOAPMessageContext arg0) {
        SOAPMessage message = arg0.getMessage();
        try {
            message.writeTo(System.out);
        } catch (SOAPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
 
    @Override
    public boolean handleMessage(SOAPMessageContext arg0) {
        SOAPMessage message = arg0.getMessage();
        boolean isOutboundMessage = (Boolean) arg0.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        
        try {
            Source source = message.getSOAPPart().getContent();
            
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    		String now = dateFormat.format(new Date());
        	File fl = new File("AccessLogger@"+now+".txt");
        	fl.createNewFile(); 
        	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fl, true)));
        	if (isOutboundMessage) {
                out.println("\n----------------------------- OUTBOUND MESSAGE @ " + new Date() + " -----------------------------");
        	} else {
            	out.println("----------------------------- INBOUND MESSAGE @ " + new Date() + " -----------------------------");
            }
            transformer.transform(source, new StreamResult(out));
            
//            transformer.transform(source, new StreamResult(System.out));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
    
    @Override
    public Set<QName> getHeaders() {
        return null;
    }
 
}
