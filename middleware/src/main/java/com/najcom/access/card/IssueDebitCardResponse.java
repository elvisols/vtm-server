
package com.najcom.access.card;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueDebitCardResult" type="{http://api.accessbankplc.com/cards/issuance}CardIssueResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issueDebitCardResult"
})
@XmlRootElement(name = "IssueDebitCardResponse")
public class IssueDebitCardResponse {

    @XmlElementRef(name = "IssueDebitCardResult", namespace = "http://api.accessbankplc.com/cards/issuance", type = JAXBElement.class, required = false)
    protected JAXBElement<CardIssueResponse> issueDebitCardResult;

    /**
     * Gets the value of the issueDebitCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}
     *     
     */
    public JAXBElement<CardIssueResponse> getIssueDebitCardResult() {
        return issueDebitCardResult;
    }

    /**
     * Sets the value of the issueDebitCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}
     *     
     */
    public void setIssueDebitCardResult(JAXBElement<CardIssueResponse> value) {
        this.issueDebitCardResult = value;
    }

}
