
package com.najcom.access.card;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerCardsResult" type="{http://api.accessbankplc.com/cards/enquiry}CardEnquiryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerCardsResult"
})
@XmlRootElement(name = "GetCustomerCardsResponse")
public class GetCustomerCardsResponse {

    @XmlElementRef(name = "GetCustomerCardsResult", namespace = "http://api.accessbankplc.com/cards/issuance", type = JAXBElement.class, required = false)
    protected JAXBElement<CardEnquiryResponse> getCustomerCardsResult;

    /**
     * Gets the value of the getCustomerCardsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CardEnquiryResponse }{@code >}
     *     
     */
    public JAXBElement<CardEnquiryResponse> getGetCustomerCardsResult() {
        return getCustomerCardsResult;
    }

    /**
     * Sets the value of the getCustomerCardsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CardEnquiryResponse }{@code >}
     *     
     */
    public void setGetCustomerCardsResult(JAXBElement<CardEnquiryResponse> value) {
        this.getCustomerCardsResult = value;
    }

}
