
package com.najcom.access.card;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssueVirtualCardResult" type="{http://api.accessbankplc.com/cards/issuance}CardIssueResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "issueVirtualCardResult"
})
@XmlRootElement(name = "IssueVirtualCardResponse")
public class IssueVirtualCardResponse {

    @XmlElementRef(name = "IssueVirtualCardResult", namespace = "http://api.accessbankplc.com/cards/issuance", type = JAXBElement.class, required = false)
    protected JAXBElement<CardIssueResponse> issueVirtualCardResult;

    /**
     * Gets the value of the issueVirtualCardResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}
     *     
     */
    public JAXBElement<CardIssueResponse> getIssueVirtualCardResult() {
        return issueVirtualCardResult;
    }

    /**
     * Sets the value of the issueVirtualCardResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}
     *     
     */
    public void setIssueVirtualCardResult(JAXBElement<CardIssueResponse> value) {
        this.issueVirtualCardResult = value;
    }

}
