
package com.najcom.access.card;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.najcom.access.card package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CardIssueResponse_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardIssueResponse");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedShort");
    private final static QName _CardInfo_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CardInfo");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "duration");
    private final static QName _CardProduct_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardProduct");
    private final static QName _CardProductRequest_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardProductRequest");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _CardEnquiryResponse_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CardEnquiryResponse");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _CardList_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CardList");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "dateTime");
    private final static QName _CardProductList_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardProductList");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyType");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "string");
    private final static QName _IssueRequest_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "IssueRequest");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "decimal");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "boolean");
    private final static QName _HotlistCode_QNAME = new QName("http://api.accessbankplc.com/cards/accesscard", "HotlistCode");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "base64Binary");
    private final static QName _BlockRequest_QNAME = new QName("http://api.accessbankplc.com/cards/accesscard", "BlockRequest");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _CardEnquiryRequest_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CardEnquiryRequest");
    private final static QName _CardProductResponse_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardProductResponse");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "anyURI");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "double");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _CardIssueRequest_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardIssueRequest");
    private final static QName _RetrieveAllCardProductsRequest_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "request");
    private final static QName _CardIssueResponseHoldID_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "HoldID");
    private final static QName _CardIssueResponseResponseCode_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "ResponseCode");
    private final static QName _CardIssueResponseResponseMessage_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "ResponseMessage");
    private final static QName _CardEnquiryRequestAccountNumber_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "AccountNumber");
    private final static QName _IssuePrepaidCardResponseIssuePrepaidCardResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "IssuePrepaidCardResult");
    private final static QName _CardProductResponseCardProducts_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CardProducts");
    private final static QName _IssueDebitCardResponseIssueDebitCardResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "IssueDebitCardResult");
    private final static QName _GetCustomerCardsResponseGetCustomerCardsResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "GetCustomerCardsResult");
    private final static QName _CardInfoCustomerPhone_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CustomerPhone");
    private final static QName _CardInfoCustomerEmail_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CustomerEmail");
    private final static QName _CardInfoPanNumber_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "PanNumber");
    private final static QName _CardInfoCustomerID_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CustomerID");
    private final static QName _CardInfoCustomerName_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CustomerName");
    private final static QName _CardInfoCustomerAddress_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "CustomerAddress");
    private final static QName _BlockCardResponseBlockCardResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "BlockCardResult");
    private final static QName _CardIssueRequestApplicationName_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "ApplicationName");
    private final static QName _CardIssueRequestRequest_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "Request");
    private final static QName _CardEnquiryResponseResponseCode_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "ResponseCode");
    private final static QName _CardEnquiryResponseResponseMessage_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "ResponseMessage");
    private final static QName _CardEnquiryResponseCards_QNAME = new QName("http://api.accessbankplc.com/cards/enquiry", "Cards");
    private final static QName _RetrieveAllCardProductsResponseRetrieveAllCardProductsResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "RetrieveAllCardProductsResult");
    private final static QName _UnBlockCardResponseUnBlockCardResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "UnBlockCardResult");
    private final static QName _CardProductName_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "Name");
    private final static QName _IssueRequestProductCode_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "ProductCode");
    private final static QName _IssueRequestPickupBranch_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "PickupBranch");
    private final static QName _IssueRequestPictureId_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "PictureId");
    private final static QName _IssueRequestUserID_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "UserID");
    private final static QName _IssueRequestCustomerID_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "CustomerID");
    private final static QName _IssueRequestNameOnCard_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "NameOnCard");
    private final static QName _IssueRequestWaiveReason_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "WaiveReason");
    private final static QName _IssueRequestPan_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "Pan");
    private final static QName _IssueRequestAccountNumber_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "AccountNumber");
    private final static QName _BlockRequestModuleName_QNAME = new QName("http://api.accessbankplc.com/cards/accesscard", "ModuleName");
    private final static QName _BlockRequestUserID_QNAME = new QName("http://api.accessbankplc.com/cards/accesscard", "UserID");
    private final static QName _BlockRequestExpiryDate_QNAME = new QName("http://api.accessbankplc.com/cards/accesscard", "ExpiryDate");
    private final static QName _BlockRequestCardNumber_QNAME = new QName("http://api.accessbankplc.com/cards/accesscard", "CardNumber");
    private final static QName _IssueVirtualCardResponseIssueVirtualCardResult_QNAME = new QName("http://api.accessbankplc.com/cards/issuance", "IssueVirtualCardResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.najcom.access.card
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveAllCardProducts }
     * 
     */
    public RetrieveAllCardProducts createRetrieveAllCardProducts() {
        return new RetrieveAllCardProducts();
    }

    /**
     * Create an instance of {@link CardProductRequest }
     * 
     */
    public CardProductRequest createCardProductRequest() {
        return new CardProductRequest();
    }

    /**
     * Create an instance of {@link RetrieveAllCardProductsResponse }
     * 
     */
    public RetrieveAllCardProductsResponse createRetrieveAllCardProductsResponse() {
        return new RetrieveAllCardProductsResponse();
    }

    /**
     * Create an instance of {@link CardProductResponse }
     * 
     */
    public CardProductResponse createCardProductResponse() {
        return new CardProductResponse();
    }

    /**
     * Create an instance of {@link IssueDebitCardResponse }
     * 
     */
    public IssueDebitCardResponse createIssueDebitCardResponse() {
        return new IssueDebitCardResponse();
    }

    /**
     * Create an instance of {@link CardIssueResponse }
     * 
     */
    public CardIssueResponse createCardIssueResponse() {
        return new CardIssueResponse();
    }

    /**
     * Create an instance of {@link GetCustomerCards }
     * 
     */
    public GetCustomerCards createGetCustomerCards() {
        return new GetCustomerCards();
    }

    /**
     * Create an instance of {@link CardEnquiryRequest }
     * 
     */
    public CardEnquiryRequest createCardEnquiryRequest() {
        return new CardEnquiryRequest();
    }

    /**
     * Create an instance of {@link IssueVirtualCard }
     * 
     */
    public IssueVirtualCard createIssueVirtualCard() {
        return new IssueVirtualCard();
    }

    /**
     * Create an instance of {@link CardIssueRequest }
     * 
     */
    public CardIssueRequest createCardIssueRequest() {
        return new CardIssueRequest();
    }

    /**
     * Create an instance of {@link CardProductList }
     * 
     */
    public CardProductList createCardProductList() {
        return new CardProductList();
    }

    /**
     * Create an instance of {@link UnBlockCard }
     * 
     */
    public UnBlockCard createUnBlockCard() {
        return new UnBlockCard();
    }

    /**
     * Create an instance of {@link BlockRequest }
     * 
     */
    public BlockRequest createBlockRequest() {
        return new BlockRequest();
    }

    /**
     * Create an instance of {@link UnBlockCardResponse }
     * 
     */
    public UnBlockCardResponse createUnBlockCardResponse() {
        return new UnBlockCardResponse();
    }

    /**
     * Create an instance of {@link IssuePrepaidCardResponse }
     * 
     */
    public IssuePrepaidCardResponse createIssuePrepaidCardResponse() {
        return new IssuePrepaidCardResponse();
    }

    /**
     * Create an instance of {@link IssueRequest }
     * 
     */
    public IssueRequest createIssueRequest() {
        return new IssueRequest();
    }

    /**
     * Create an instance of {@link GetCustomerCardsResponse }
     * 
     */
    public GetCustomerCardsResponse createGetCustomerCardsResponse() {
        return new GetCustomerCardsResponse();
    }

    /**
     * Create an instance of {@link CardEnquiryResponse }
     * 
     */
    public CardEnquiryResponse createCardEnquiryResponse() {
        return new CardEnquiryResponse();
    }

    /**
     * Create an instance of {@link BlockCard }
     * 
     */
    public BlockCard createBlockCard() {
        return new BlockCard();
    }

    /**
     * Create an instance of {@link BlockCardResponse }
     * 
     */
    public BlockCardResponse createBlockCardResponse() {
        return new BlockCardResponse();
    }

    /**
     * Create an instance of {@link IssuePrepaidCard }
     * 
     */
    public IssuePrepaidCard createIssuePrepaidCard() {
        return new IssuePrepaidCard();
    }

    /**
     * Create an instance of {@link CardProduct }
     * 
     */
    public CardProduct createCardProduct() {
        return new CardProduct();
    }

    /**
     * Create an instance of {@link IssueDebitCard }
     * 
     */
    public IssueDebitCard createIssueDebitCard() {
        return new IssueDebitCard();
    }

    /**
     * Create an instance of {@link IssueVirtualCardResponse }
     * 
     */
    public IssueVirtualCardResponse createIssueVirtualCardResponse() {
        return new IssueVirtualCardResponse();
    }

    /**
     * Create an instance of {@link CardList }
     * 
     */
    public CardList createCardList() {
        return new CardList();
    }

    /**
     * Create an instance of {@link CardInfo }
     * 
     */
    public CardInfo createCardInfo() {
        return new CardInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardIssueResponse")
    public JAXBElement<CardIssueResponse> createCardIssueResponse(CardIssueResponse value) {
        return new JAXBElement<CardIssueResponse>(_CardIssueResponse_QNAME, CardIssueResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CardInfo")
    public JAXBElement<CardInfo> createCardInfo(CardInfo value) {
        return new JAXBElement<CardInfo>(_CardInfo_QNAME, CardInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardProduct")
    public JAXBElement<CardProduct> createCardProduct(CardProduct value) {
        return new JAXBElement<CardProduct>(_CardProduct_QNAME, CardProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProductRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardProductRequest")
    public JAXBElement<CardProductRequest> createCardProductRequest(CardProductRequest value) {
        return new JAXBElement<CardProductRequest>(_CardProductRequest_QNAME, CardProductRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardEnquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CardEnquiryResponse")
    public JAXBElement<CardEnquiryResponse> createCardEnquiryResponse(CardEnquiryResponse value) {
        return new JAXBElement<CardEnquiryResponse>(_CardEnquiryResponse_QNAME, CardEnquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CardList")
    public JAXBElement<CardList> createCardList(CardList value) {
        return new JAXBElement<CardList>(_CardList_QNAME, CardList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardProductList")
    public JAXBElement<CardProductList> createCardProductList(CardProductList value) {
        return new JAXBElement<CardProductList>(_CardProductList_QNAME, CardProductList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "IssueRequest")
    public JAXBElement<IssueRequest> createIssueRequest(IssueRequest value) {
        return new JAXBElement<IssueRequest>(_IssueRequest_QNAME, IssueRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotlistCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/accesscard", name = "HotlistCode")
    public JAXBElement<HotlistCode> createHotlistCode(HotlistCode value) {
        return new JAXBElement<HotlistCode>(_HotlistCode_QNAME, HotlistCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/accesscard", name = "BlockRequest")
    public JAXBElement<BlockRequest> createBlockRequest(BlockRequest value) {
        return new JAXBElement<BlockRequest>(_BlockRequest_QNAME, BlockRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardEnquiryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CardEnquiryRequest")
    public JAXBElement<CardEnquiryRequest> createCardEnquiryRequest(CardEnquiryRequest value) {
        return new JAXBElement<CardEnquiryRequest>(_CardEnquiryRequest_QNAME, CardEnquiryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardProductResponse")
    public JAXBElement<CardProductResponse> createCardProductResponse(CardProductResponse value) {
        return new JAXBElement<CardProductResponse>(_CardProductResponse_QNAME, CardProductResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardIssueRequest")
    public JAXBElement<CardIssueRequest> createCardIssueRequest(CardIssueRequest value) {
        return new JAXBElement<CardIssueRequest>(_CardIssueRequest_QNAME, CardIssueRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProductRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = RetrieveAllCardProducts.class)
    public JAXBElement<CardProductRequest> createRetrieveAllCardProductsRequest(CardProductRequest value) {
        return new JAXBElement<CardProductRequest>(_RetrieveAllCardProductsRequest_QNAME, CardProductRequest.class, RetrieveAllCardProducts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "HoldID", scope = CardIssueResponse.class)
    public JAXBElement<String> createCardIssueResponseHoldID(String value) {
        return new JAXBElement<String>(_CardIssueResponseHoldID_QNAME, String.class, CardIssueResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "ResponseCode", scope = CardIssueResponse.class)
    public JAXBElement<String> createCardIssueResponseResponseCode(String value) {
        return new JAXBElement<String>(_CardIssueResponseResponseCode_QNAME, String.class, CardIssueResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "ResponseMessage", scope = CardIssueResponse.class)
    public JAXBElement<String> createCardIssueResponseResponseMessage(String value) {
        return new JAXBElement<String>(_CardIssueResponseResponseMessage_QNAME, String.class, CardIssueResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = IssueDebitCard.class)
    public JAXBElement<CardIssueRequest> createIssueDebitCardRequest(CardIssueRequest value) {
        return new JAXBElement<CardIssueRequest>(_RetrieveAllCardProductsRequest_QNAME, CardIssueRequest.class, IssueDebitCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "AccountNumber", scope = CardEnquiryRequest.class)
    public JAXBElement<String> createCardEnquiryRequestAccountNumber(String value) {
        return new JAXBElement<String>(_CardEnquiryRequestAccountNumber_QNAME, String.class, CardEnquiryRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "IssuePrepaidCardResult", scope = IssuePrepaidCardResponse.class)
    public JAXBElement<CardIssueResponse> createIssuePrepaidCardResponseIssuePrepaidCardResult(CardIssueResponse value) {
        return new JAXBElement<CardIssueResponse>(_IssuePrepaidCardResponseIssuePrepaidCardResult_QNAME, CardIssueResponse.class, IssuePrepaidCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardEnquiryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = GetCustomerCards.class)
    public JAXBElement<CardEnquiryRequest> createGetCustomerCardsRequest(CardEnquiryRequest value) {
        return new JAXBElement<CardEnquiryRequest>(_RetrieveAllCardProductsRequest_QNAME, CardEnquiryRequest.class, GetCustomerCards.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "ResponseCode", scope = CardProductResponse.class)
    public JAXBElement<String> createCardProductResponseResponseCode(String value) {
        return new JAXBElement<String>(_CardIssueResponseResponseCode_QNAME, String.class, CardProductResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "ResponseMessage", scope = CardProductResponse.class)
    public JAXBElement<String> createCardProductResponseResponseMessage(String value) {
        return new JAXBElement<String>(_CardIssueResponseResponseMessage_QNAME, String.class, CardProductResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProductList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CardProducts", scope = CardProductResponse.class)
    public JAXBElement<CardProductList> createCardProductResponseCardProducts(CardProductList value) {
        return new JAXBElement<CardProductList>(_CardProductResponseCardProducts_QNAME, CardProductList.class, CardProductResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = UnBlockCard.class)
    public JAXBElement<BlockRequest> createUnBlockCardRequest(BlockRequest value) {
        return new JAXBElement<BlockRequest>(_RetrieveAllCardProductsRequest_QNAME, BlockRequest.class, UnBlockCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "IssueDebitCardResult", scope = IssueDebitCardResponse.class)
    public JAXBElement<CardIssueResponse> createIssueDebitCardResponseIssueDebitCardResult(CardIssueResponse value) {
        return new JAXBElement<CardIssueResponse>(_IssueDebitCardResponseIssueDebitCardResult_QNAME, CardIssueResponse.class, IssueDebitCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlockRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = BlockCard.class)
    public JAXBElement<BlockRequest> createBlockCardRequest(BlockRequest value) {
        return new JAXBElement<BlockRequest>(_RetrieveAllCardProductsRequest_QNAME, BlockRequest.class, BlockCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = IssueVirtualCard.class)
    public JAXBElement<CardIssueRequest> createIssueVirtualCardRequest(CardIssueRequest value) {
        return new JAXBElement<CardIssueRequest>(_RetrieveAllCardProductsRequest_QNAME, CardIssueRequest.class, IssueVirtualCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardEnquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "GetCustomerCardsResult", scope = GetCustomerCardsResponse.class)
    public JAXBElement<CardEnquiryResponse> createGetCustomerCardsResponseGetCustomerCardsResult(CardEnquiryResponse value) {
        return new JAXBElement<CardEnquiryResponse>(_GetCustomerCardsResponseGetCustomerCardsResult_QNAME, CardEnquiryResponse.class, GetCustomerCardsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CustomerPhone", scope = CardInfo.class)
    public JAXBElement<String> createCardInfoCustomerPhone(String value) {
        return new JAXBElement<String>(_CardInfoCustomerPhone_QNAME, String.class, CardInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CustomerEmail", scope = CardInfo.class)
    public JAXBElement<String> createCardInfoCustomerEmail(String value) {
        return new JAXBElement<String>(_CardInfoCustomerEmail_QNAME, String.class, CardInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "PanNumber", scope = CardInfo.class)
    public JAXBElement<String> createCardInfoPanNumber(String value) {
        return new JAXBElement<String>(_CardInfoPanNumber_QNAME, String.class, CardInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CustomerID", scope = CardInfo.class)
    public JAXBElement<String> createCardInfoCustomerID(String value) {
        return new JAXBElement<String>(_CardInfoCustomerID_QNAME, String.class, CardInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CustomerName", scope = CardInfo.class)
    public JAXBElement<String> createCardInfoCustomerName(String value) {
        return new JAXBElement<String>(_CardInfoCustomerName_QNAME, String.class, CardInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "CustomerAddress", scope = CardInfo.class)
    public JAXBElement<String> createCardInfoCustomerAddress(String value) {
        return new JAXBElement<String>(_CardInfoCustomerAddress_QNAME, String.class, CardInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "BlockCardResult", scope = BlockCardResponse.class)
    public JAXBElement<String> createBlockCardResponseBlockCardResult(String value) {
        return new JAXBElement<String>(_BlockCardResponseBlockCardResult_QNAME, String.class, BlockCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "ApplicationName", scope = CardIssueRequest.class)
    public JAXBElement<String> createCardIssueRequestApplicationName(String value) {
        return new JAXBElement<String>(_CardIssueRequestApplicationName_QNAME, String.class, CardIssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IssueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "Request", scope = CardIssueRequest.class)
    public JAXBElement<IssueRequest> createCardIssueRequestRequest(IssueRequest value) {
        return new JAXBElement<IssueRequest>(_CardIssueRequestRequest_QNAME, IssueRequest.class, CardIssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "ResponseCode", scope = CardEnquiryResponse.class)
    public JAXBElement<String> createCardEnquiryResponseResponseCode(String value) {
        return new JAXBElement<String>(_CardEnquiryResponseResponseCode_QNAME, String.class, CardEnquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "ResponseMessage", scope = CardEnquiryResponse.class)
    public JAXBElement<String> createCardEnquiryResponseResponseMessage(String value) {
        return new JAXBElement<String>(_CardEnquiryResponseResponseMessage_QNAME, String.class, CardEnquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/enquiry", name = "Cards", scope = CardEnquiryResponse.class)
    public JAXBElement<CardList> createCardEnquiryResponseCards(CardList value) {
        return new JAXBElement<CardList>(_CardEnquiryResponseCards_QNAME, CardList.class, CardEnquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "RetrieveAllCardProductsResult", scope = RetrieveAllCardProductsResponse.class)
    public JAXBElement<CardProductResponse> createRetrieveAllCardProductsResponseRetrieveAllCardProductsResult(CardProductResponse value) {
        return new JAXBElement<CardProductResponse>(_RetrieveAllCardProductsResponseRetrieveAllCardProductsResult_QNAME, CardProductResponse.class, RetrieveAllCardProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "UnBlockCardResult", scope = UnBlockCardResponse.class)
    public JAXBElement<String> createUnBlockCardResponseUnBlockCardResult(String value) {
        return new JAXBElement<String>(_UnBlockCardResponseUnBlockCardResult_QNAME, String.class, UnBlockCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "Name", scope = CardProduct.class)
    public JAXBElement<String> createCardProductName(String value) {
        return new JAXBElement<String>(_CardProductName_QNAME, String.class, CardProduct.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "ProductCode", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestProductCode(String value) {
        return new JAXBElement<String>(_IssueRequestProductCode_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "PickupBranch", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestPickupBranch(String value) {
        return new JAXBElement<String>(_IssueRequestPickupBranch_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "PictureId", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestPictureId(String value) {
        return new JAXBElement<String>(_IssueRequestPictureId_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "UserID", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestUserID(String value) {
        return new JAXBElement<String>(_IssueRequestUserID_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "CustomerID", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestCustomerID(String value) {
        return new JAXBElement<String>(_IssueRequestCustomerID_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "NameOnCard", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestNameOnCard(String value) {
        return new JAXBElement<String>(_IssueRequestNameOnCard_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "WaiveReason", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestWaiveReason(String value) {
        return new JAXBElement<String>(_IssueRequestWaiveReason_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "Pan", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestPan(String value) {
        return new JAXBElement<String>(_IssueRequestPan_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "AccountNumber", scope = IssueRequest.class)
    public JAXBElement<String> createIssueRequestAccountNumber(String value) {
        return new JAXBElement<String>(_IssueRequestAccountNumber_QNAME, String.class, IssueRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/accesscard", name = "ModuleName", scope = BlockRequest.class)
    public JAXBElement<String> createBlockRequestModuleName(String value) {
        return new JAXBElement<String>(_BlockRequestModuleName_QNAME, String.class, BlockRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/accesscard", name = "UserID", scope = BlockRequest.class)
    public JAXBElement<String> createBlockRequestUserID(String value) {
        return new JAXBElement<String>(_BlockRequestUserID_QNAME, String.class, BlockRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/accesscard", name = "ExpiryDate", scope = BlockRequest.class)
    public JAXBElement<String> createBlockRequestExpiryDate(String value) {
        return new JAXBElement<String>(_BlockRequestExpiryDate_QNAME, String.class, BlockRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/accesscard", name = "CardNumber", scope = BlockRequest.class)
    public JAXBElement<String> createBlockRequestCardNumber(String value) {
        return new JAXBElement<String>(_BlockRequestCardNumber_QNAME, String.class, BlockRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "request", scope = IssuePrepaidCard.class)
    public JAXBElement<CardIssueRequest> createIssuePrepaidCardRequest(CardIssueRequest value) {
        return new JAXBElement<CardIssueRequest>(_RetrieveAllCardProductsRequest_QNAME, CardIssueRequest.class, IssuePrepaidCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardIssueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://api.accessbankplc.com/cards/issuance", name = "IssueVirtualCardResult", scope = IssueVirtualCardResponse.class)
    public JAXBElement<CardIssueResponse> createIssueVirtualCardResponseIssueVirtualCardResult(CardIssueResponse value) {
        return new JAXBElement<CardIssueResponse>(_IssueVirtualCardResponseIssueVirtualCardResult_QNAME, CardIssueResponse.class, IssueVirtualCardResponse.class, value);
    }

}
