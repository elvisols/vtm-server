
package com.najcom.access.card;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardEnquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardEnquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cards" type="{http://api.accessbankplc.com/cards/enquiry}CardList" minOccurs="0"/>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardEnquiryResponse", namespace = "http://api.accessbankplc.com/cards/enquiry", propOrder = {
    "cards",
    "responseCode",
    "responseMessage"
})
public class CardEnquiryResponse {

    @XmlElementRef(name = "Cards", namespace = "http://api.accessbankplc.com/cards/enquiry", type = JAXBElement.class, required = false)
    protected JAXBElement<CardList> cards;
    @XmlElementRef(name = "ResponseCode", namespace = "http://api.accessbankplc.com/cards/enquiry", type = JAXBElement.class, required = false)
    protected JAXBElement<String> responseCode;
    @XmlElementRef(name = "ResponseMessage", namespace = "http://api.accessbankplc.com/cards/enquiry", type = JAXBElement.class, required = false)
    protected JAXBElement<String> responseMessage;

    /**
     * Gets the value of the cards property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CardList }{@code >}
     *     
     */
    public JAXBElement<CardList> getCards() {
        return cards;
    }

    /**
     * Sets the value of the cards property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CardList }{@code >}
     *     
     */
    public void setCards(JAXBElement<CardList> value) {
        this.cards = value;
    }

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResponseCode(JAXBElement<String> value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResponseMessage() {
        return responseMessage;
    }

    /**
     * Sets the value of the responseMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResponseMessage(JAXBElement<String> value) {
        this.responseMessage = value;
    }

}
