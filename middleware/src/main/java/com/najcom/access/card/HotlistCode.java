
package com.najcom.access.card;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HotlistCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HotlistCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unspecified"/>
 *     &lt;enumeration value="Lost"/>
 *     &lt;enumeration value="Stolen"/>
 *     &lt;enumeration value="Damaged"/>
 *     &lt;enumeration value="Destroyed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HotlistCode", namespace = "http://api.accessbankplc.com/cards/accesscard")
@XmlEnum
public enum HotlistCode {

    @XmlEnumValue("Unspecified")
    UNSPECIFIED("Unspecified"),
    @XmlEnumValue("Lost")
    LOST("Lost"),
    @XmlEnumValue("Stolen")
    STOLEN("Stolen"),
    @XmlEnumValue("Damaged")
    DAMAGED("Damaged"),
    @XmlEnumValue("Destroyed")
    DESTROYED("Destroyed");
    private final String value;

    HotlistCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HotlistCode fromValue(String v) {
        for (HotlistCode c: HotlistCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
