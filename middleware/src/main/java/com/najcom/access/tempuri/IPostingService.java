
package com.najcom.access.tempuri;

import java.math.BigDecimal;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.najcom.access.datacontract.AccountCotAndVatResponse;
import com.najcom.access.datacontract.AccountRelatedResponse;
import com.najcom.access.datacontract.AccountSearchResponse;
import com.najcom.access.datacontract.AccountSummaryAndTrxResponse;
import com.najcom.access.datacontract.AccountSummaryResponse;
import com.najcom.access.datacontract.AccountTransactionResponseBulk;
import com.najcom.access.datacontract.ActivateChequeBookResponse;
import com.najcom.access.datacontract.ArrayOfCustomerAcctsDetailResponse;
import com.najcom.access.datacontract.ArrayOfImageCollectionDetails;
import com.najcom.access.datacontract.ArrayOfNubanAcctsDetails;
import com.najcom.access.datacontract.ArrayOfSingleTransactionDetails;
import com.najcom.access.datacontract.ArrayOfSweepAcctsDetails;
import com.najcom.access.datacontract.AuthenticateCBAUserResp;
import com.najcom.access.datacontract.AuthorizeWithdrawalRequest;
import com.najcom.access.datacontract.ChequeDetailsResponse;
import com.najcom.access.datacontract.CreateAmtBlockResponse;
import com.najcom.access.datacontract.CreateCustAccountResponse;
import com.najcom.access.datacontract.CreateCustomerResponse;
import com.najcom.access.datacontract.CreateLDContractResponse;
import com.najcom.access.datacontract.CreateMMContractResponse;
import com.najcom.access.datacontract.CreateRTResponse;
import com.najcom.access.datacontract.CreateSIContractResponse;
import com.najcom.access.datacontract.CreateStopPaymentResponse;
import com.najcom.access.datacontract.CreateTDAccountResponse;
import com.najcom.access.datacontract.CurrencyRateResponse;
import com.najcom.access.datacontract.CustomerAcctsDetailResponse;
import com.najcom.access.datacontract.CustomerDetails;
import com.najcom.access.datacontract.CustomerInformationResponse;
import com.najcom.access.datacontract.CustomerODLimitResponse;
import com.najcom.access.datacontract.FCUBSCGServiceCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSCloseCustAccRequest;
import com.najcom.access.datacontract.FCUBSCreateCustAccountRequest;
import com.najcom.access.datacontract.FCUBSCreateCustomerRequest;
import com.najcom.access.datacontract.FCUBSCreateLDContractRequest;
import com.najcom.access.datacontract.FCUBSCreateMMContractRequest;
import com.najcom.access.datacontract.FCUBSCreateSIContractRequest;
import com.najcom.access.datacontract.FCUBSCreateStopPaymentRequest;
import com.najcom.access.datacontract.FCUBSCreateTDAccountRequest;
import com.najcom.access.datacontract.FCUBSCustomerServiceAmtBlkRequest;
import com.najcom.access.datacontract.FCUBSFTCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSRTCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSRTReverseTransactionRequest;
import com.najcom.access.datacontract.FcubsResponse;
import com.najcom.access.datacontract.FixedDepositResponse;
import com.najcom.access.datacontract.GetAccountBranchResp;
import com.najcom.access.datacontract.GetAccountExistResponse;
import com.najcom.access.datacontract.GetAcctMemoInfoResponse;
import com.najcom.access.datacontract.GetChequeValueDateResp;
import com.najcom.access.datacontract.GetCurrencyCodeResp;
import com.najcom.access.datacontract.GetEffectiveBalanceResp;
import com.najcom.access.datacontract.GetUserBranchResp;
import com.najcom.access.datacontract.LoanInfoSummaryResponse;
import com.najcom.access.datacontract.LoanSummaryResponse;
import com.najcom.access.datacontract.LoanSummaryResponseBulk;
import com.najcom.access.datacontract.MultipleJournalMasterTrx;
import com.najcom.access.datacontract.RepaymentScheduleResponse;
import com.najcom.access.datacontract.SampleAccountResponse;
import com.najcom.access.datacontract.ScreenLimitResponse;
import com.najcom.access.datacontract.SingleJournalTrx;
import com.najcom.access.datacontract.TellerLimitResponse;
import com.najcom.access.datacontract.UserLimitResponse;
import com.najcom.access.datacontract.ValidateUserTillResp;
import com.najcom.access.datacontract.VisaCardLimitResponse;
import com.najcom.access.datacontract.webservicewrappers.AccountVerificationResponse;
import com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileAccountDetail;
import com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileTransactionDetail;
import com.najcom.access.datacontract.webservicewrappers.ChequeNoInquiryResponse;
import com.najcom.access.datacontract.webservicewrappers.MessageIDStatusResponse;
import com.najcom.access.datacontract.webservicewrappers.TransactionRefNoDetailsResponse;
import com.najcom.access.datacontract.webservicewrappers.VansoCustomerIdResponse;
import com.najcom.access.microsoft.arrays.ArrayOfstring;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IPostingService", targetNamespace = "http://FCUBS.Webservices")
@XmlSeeAlso({
    com.najcom.access.microsoft.ObjectFactory.class,
    com.najcom.access.microsoft.arrays.ObjectFactory.class,
    com.najcom.access.datacontract.ObjectFactory.class,
    com.najcom.access.datacontract.webservicewrappers.ObjectFactory.class,
    com.najcom.access.webservices.fcubs.ObjectFactory.class
})
public interface IPostingService {


    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "SingleJournalEntry", action = "http://FCUBS.Webservices/IPostingService/SingleJournalEntry")
    @WebResult(name = "SingleJournalEntryResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "SingleJournalEntry", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SingleJournalEntry")
    @ResponseWrapper(localName = "SingleJournalEntryResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SingleJournalEntryResponse")
    public FcubsResponse singleJournalEntry(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        SingleJournalTrx data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param limitsCcy
     * @param userId
     * @return
     *     returns com.najcom.access.datacontract.TellerLimitResponse
     */
    @WebMethod(operationName = "GetTellerLimit", action = "http://FCUBS.Webservices/IPostingService/GetTellerLimit")
    @WebResult(name = "GetTellerLimitResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetTellerLimit", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetTellerLimit")
    @ResponseWrapper(localName = "GetTellerLimitResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetTellerLimitResponse")
    public TellerLimitResponse getTellerLimit(
        @WebParam(name = "user_id", targetNamespace = "http://FCUBS.Webservices")
        String userId,
        @WebParam(name = "limits_ccy", targetNamespace = "http://FCUBS.Webservices")
        String limitsCcy);

    /**
     * 
     * @param branchCode
     * @param userId
     * @return
     *     returns com.najcom.access.datacontract.UserLimitResponse
     */
    @WebMethod(operationName = "GetUserLimit", action = "http://FCUBS.Webservices/IPostingService/GetUserLimit")
    @WebResult(name = "GetUserLimitResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetUserLimit", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetUserLimit")
    @ResponseWrapper(localName = "GetUserLimitResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetUserLimitResponse")
    public UserLimitResponse getUserLimit(
        @WebParam(name = "user_id", targetNamespace = "http://FCUBS.Webservices")
        String userId,
        @WebParam(name = "branch_code", targetNamespace = "http://FCUBS.Webservices")
        String branchCode);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns java.lang.String
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/getAccordGL")
    @WebResult(name = "getAccordGLResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "getAccordGL", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccordGL")
    @ResponseWrapper(localName = "getAccordGLResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccordGLResponse")
    public String getAccordGL(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param ccyCode
     * @param functionNo
     * @return
     *     returns com.najcom.access.datacontract.ScreenLimitResponse
     */
    @WebMethod(operationName = "GetScreenLimit", action = "http://FCUBS.Webservices/IPostingService/GetScreenLimit")
    @WebResult(name = "GetScreenLimitResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetScreenLimit", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetScreenLimit")
    @ResponseWrapper(localName = "GetScreenLimitResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetScreenLimitResponse")
    public ScreenLimitResponse getScreenLimit(
        @WebParam(name = "function_no", targetNamespace = "http://FCUBS.Webservices")
        String functionNo,
        @WebParam(name = "ccy_code", targetNamespace = "http://FCUBS.Webservices")
        String ccyCode);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.SampleAccountResponse
     */
    @WebMethod(operationName = "GetSample", action = "http://FCUBS.Webservices/IPostingService/GetSample")
    @WebResult(name = "GetSampleResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetSample", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetSample")
    @ResponseWrapper(localName = "GetSampleResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetSampleResponse")
    public SampleAccountResponse getSample(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param branchCode
     * @param ccyCode1
     * @param ccyCode2
     * @return
     *     returns com.najcom.access.datacontract.CurrencyRateResponse
     */
    @WebMethod(operationName = "GetCurrencyRate", action = "http://FCUBS.Webservices/IPostingService/GetCurrencyRate")
    @WebResult(name = "GetCurrencyRateResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCurrencyRate", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCurrencyRate")
    @ResponseWrapper(localName = "GetCurrencyRateResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCurrencyRateResponse")
    public CurrencyRateResponse getCurrencyRate(
        @WebParam(name = "BranchCode", targetNamespace = "http://FCUBS.Webservices")
        String branchCode,
        @WebParam(name = "CcyCode1", targetNamespace = "http://FCUBS.Webservices")
        String ccyCode1,
        @WebParam(name = "CcyCode2", targetNamespace = "http://FCUBS.Webservices")
        String ccyCode2);

    /**
     * 
     * @param extSystem
     * @param msgId
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.MessageIDStatusResponse
     */
    @WebMethod(operationName = "GetMessageIDStatus", action = "http://FCUBS.Webservices/IPostingService/GetMessageIDStatus")
    @WebResult(name = "GetMessageIDStatusResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetMessageIDStatus", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetMessageIDStatus")
    @ResponseWrapper(localName = "GetMessageIDStatusResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetMessageIDStatusResponse")
    public MessageIDStatusResponse getMessageIDStatus(
        @WebParam(name = "extSystem", targetNamespace = "http://FCUBS.Webservices")
        String extSystem,
        @WebParam(name = "msgId", targetNamespace = "http://FCUBS.Webservices")
        String msgId);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "MultiJournalEntry", action = "http://FCUBS.Webservices/IPostingService/MultiJournalEntry")
    @WebResult(name = "MultiJournalEntryResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "MultiJournalEntry", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.MultiJournalEntry")
    @ResponseWrapper(localName = "MultiJournalEntryResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.MultiJournalEntryResponse")
    public FcubsResponse multiJournalEntry(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        MultipleJournalMasterTrx data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "FCUBSCGServiceCreateTransaction", action = "http://FCUBS.Webservices/IPostingService/FCUBSCGServiceCreateTransaction")
    @WebResult(name = "FCUBSCGServiceCreateTransactionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCGServiceCreateTransaction", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCGServiceCreateTransaction")
    @ResponseWrapper(localName = "FCUBSCGServiceCreateTransactionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCGServiceCreateTransactionResponse")
    public FcubsResponse fcubscgServiceCreateTransaction(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCGServiceCreateTransactionRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "FCUBSFTCreateTransaction", action = "http://FCUBS.Webservices/IPostingService/FCUBSFTCreateTransaction")
    @WebResult(name = "FCUBSFTCreateTransactionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSFTCreateTransaction", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSFTCreateTransaction")
    @ResponseWrapper(localName = "FCUBSFTCreateTransactionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSFTCreateTransactionResponse")
    public FcubsResponse fcubsftCreateTransaction(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSFTCreateTransactionRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateRTResponse
     */
    @WebMethod(operationName = "FCUBSRTCreateTransaction", action = "http://FCUBS.Webservices/IPostingService/FCUBSRTCreateTransaction")
    @WebResult(name = "FCUBSRTCreateTransactionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSRTCreateTransaction", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSRTCreateTransaction")
    @ResponseWrapper(localName = "FCUBSRTCreateTransactionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSRTCreateTransactionResponse")
    public CreateRTResponse fcubsrtCreateTransaction(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSRTCreateTransactionRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "FCUBSRTReverseTransaction", action = "http://FCUBS.Webservices/IPostingService/FCUBSRTReverseTransaction")
    @WebResult(name = "FCUBSRTReverseTransactionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSRTReverseTransaction", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSRTReverseTransaction")
    @ResponseWrapper(localName = "FCUBSRTReverseTransactionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSRTReverseTransactionResponse")
    public FcubsResponse fcubsrtReverseTransaction(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSRTReverseTransactionRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "FCUBSCustomerServiceCloseAmtBlk", action = "http://FCUBS.Webservices/IPostingService/FCUBSCustomerServiceCloseAmtBlk")
    @WebResult(name = "FCUBSCustomerServiceCloseAmtBlkResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCustomerServiceCloseAmtBlk", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCustomerServiceCloseAmtBlk")
    @ResponseWrapper(localName = "FCUBSCustomerServiceCloseAmtBlkResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCustomerServiceCloseAmtBlkResponse")
    public FcubsResponse fcubsCustomerServiceCloseAmtBlk(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCustomerServiceAmtBlkRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateAmtBlockResponse
     */
    @WebMethod(operationName = "FCUBSCustomerServiceCreateAmtBlk", action = "http://FCUBS.Webservices/IPostingService/FCUBSCustomerServiceCreateAmtBlk")
    @WebResult(name = "FCUBSCustomerServiceCreateAmtBlkResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCustomerServiceCreateAmtBlk", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCustomerServiceCreateAmtBlk")
    @ResponseWrapper(localName = "FCUBSCustomerServiceCreateAmtBlkResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCustomerServiceCreateAmtBlkResponse")
    public CreateAmtBlockResponse fcubsCustomerServiceCreateAmtBlk(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCustomerServiceAmtBlkRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateCustAccountResponse
     */
    @WebMethod(operationName = "FCUBSCreateCustAccount", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateCustAccount")
    @WebResult(name = "FCUBSCreateCustAccountResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateCustAccount", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateCustAccount")
    @ResponseWrapper(localName = "FCUBSCreateCustAccountResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateCustAccountResponse")
    public CreateCustAccountResponse fcubsCreateCustAccount(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateCustAccountRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateCustomerResponse
     */
    @WebMethod(operationName = "FCUBSCreateCustomer", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateCustomer")
    @WebResult(name = "FCUBSCreateCustomerResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateCustomer", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateCustomer")
    @ResponseWrapper(localName = "FCUBSCreateCustomerResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateCustomerResponse")
    public CreateCustomerResponse fcubsCreateCustomer(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateCustomerRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateSIContractResponse
     */
    @WebMethod(operationName = "FCUBSCreateSIContract", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateSIContract")
    @WebResult(name = "FCUBSCreateSIContractResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateSIContract", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateSIContract")
    @ResponseWrapper(localName = "FCUBSCreateSIContractResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateSIContractResponse")
    public CreateSIContractResponse fcubsCreateSIContract(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateSIContractRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateTDAccountResponse
     */
    @WebMethod(operationName = "FCUBSCreateTDAccount", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateTDAccount")
    @WebResult(name = "FCUBSCreateTDAccountResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateTDAccount", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateTDAccount")
    @ResponseWrapper(localName = "FCUBSCreateTDAccountResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateTDAccountResponse")
    public CreateTDAccountResponse fcubsCreateTDAccount(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateTDAccountRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateMMContractResponse
     */
    @WebMethod(operationName = "FCUBSCreateMMContract", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateMMContract")
    @WebResult(name = "FCUBSCreateMMContractResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateMMContract", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateMMContract")
    @ResponseWrapper(localName = "FCUBSCreateMMContractResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateMMContractResponse")
    public CreateMMContractResponse fcubsCreateMMContract(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateMMContractRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateLDContractResponse
     */
    @WebMethod(operationName = "FCUBSCreateLDContract", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateLDContract")
    @WebResult(name = "FCUBSCreateLDContractResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateLDContract", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateLDContract")
    @ResponseWrapper(localName = "FCUBSCreateLDContractResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateLDContractResponse")
    public CreateLDContractResponse fcubsCreateLDContract(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateLDContractRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "FCUBSCloseCustAcc", action = "http://FCUBS.Webservices/IPostingService/FCUBSCloseCustAcc")
    @WebResult(name = "FCUBSCloseCustAccResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCloseCustAcc", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCloseCustAcc")
    @ResponseWrapper(localName = "FCUBSCloseCustAccResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCloseCustAccResponse")
    public FcubsResponse fcubsCloseCustAcc(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCloseCustAccRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.CreateStopPaymentResponse
     */
    @WebMethod(operationName = "FCUBSCreateStopPayment", action = "http://FCUBS.Webservices/IPostingService/FCUBSCreateStopPayment")
    @WebResult(name = "FCUBSCreateStopPaymentResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSCreateStopPayment", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateStopPayment")
    @ResponseWrapper(localName = "FCUBSCreateStopPaymentResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSCreateStopPaymentResponse")
    public CreateStopPaymentResponse fcubsCreateStopPayment(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        FCUBSCreateStopPaymentRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param data
     * @param action
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "FCUBSRTAuthorizeWithdrawal", action = "http://FCUBS.Webservices/IPostingService/FCUBSRTAuthorizeWithdrawal")
    @WebResult(name = "FCUBSRTAuthorizeWithdrawalResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FCUBSRTAuthorizeWithdrawal", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSRTAuthorizeWithdrawal")
    @ResponseWrapper(localName = "FCUBSRTAuthorizeWithdrawalResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FCUBSRTAuthorizeWithdrawalResponse")
    public FcubsResponse fcubsrtAuthorizeWithdrawal(
        @WebParam(name = "data", targetNamespace = "http://FCUBS.Webservices")
        AuthorizeWithdrawalRequest data,
        @WebParam(name = "action", targetNamespace = "http://FCUBS.Webservices")
        String action);

    /**
     * 
     * @param amount
     * @param currencyNumber
     * @param debitAccountNumber
     * @param requestModule
     * @param paymentReference
     * @param msgId
     * @param debitCustomerId
     * @param source
     * @param valueDate
     * @param creditAccountNumber
     * @param userId
     * @param creditCustomerId
     * @param branchCode
     * @param txnCode
     * @param creditBranchCode
     * @param debitBranchCode
     * @param narration
     * @param currencyCode
     * @param batchNumber
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "SingleDebitSingleCredit", action = "http://FCUBS.Webservices/IPostingService/SingleDebitSingleCredit")
    @WebResult(name = "SingleDebitSingleCreditResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "SingleDebitSingleCredit", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SingleDebitSingleCredit")
    @ResponseWrapper(localName = "SingleDebitSingleCreditResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SingleDebitSingleCreditResponse")
    public FcubsResponse singleDebitSingleCredit(
        @WebParam(name = "Source", targetNamespace = "http://FCUBS.Webservices")
        String source,
        @WebParam(name = "MsgId", targetNamespace = "http://FCUBS.Webservices")
        String msgId,
        @WebParam(name = "DebitCustomerId", targetNamespace = "http://FCUBS.Webservices")
        String debitCustomerId,
        @WebParam(name = "CreditCustomerId", targetNamespace = "http://FCUBS.Webservices")
        String creditCustomerId,
        @WebParam(name = "Amount", targetNamespace = "http://FCUBS.Webservices")
        BigDecimal amount,
        @WebParam(name = "CurrencyCode", targetNamespace = "http://FCUBS.Webservices")
        String currencyCode,
        @WebParam(name = "CurrencyNumber", targetNamespace = "http://FCUBS.Webservices")
        String currencyNumber,
        @WebParam(name = "CreditAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String creditAccountNumber,
        @WebParam(name = "DebitAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String debitAccountNumber,
        @WebParam(name = "Narration", targetNamespace = "http://FCUBS.Webservices")
        String narration,
        @WebParam(name = "PaymentReference", targetNamespace = "http://FCUBS.Webservices")
        String paymentReference,
        @WebParam(name = "RequestModule", targetNamespace = "http://FCUBS.Webservices")
        String requestModule,
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId,
        @WebParam(name = "BranchCode", targetNamespace = "http://FCUBS.Webservices")
        String branchCode,
        @WebParam(name = "DebitBranchCode", targetNamespace = "http://FCUBS.Webservices")
        String debitBranchCode,
        @WebParam(name = "CreditBranchCode", targetNamespace = "http://FCUBS.Webservices")
        String creditBranchCode,
        @WebParam(name = "BatchNumber", targetNamespace = "http://FCUBS.Webservices")
        String batchNumber,
        @WebParam(name = "ValueDate", targetNamespace = "http://FCUBS.Webservices")
        String valueDate,
        @WebParam(name = "TxnCode", targetNamespace = "http://FCUBS.Webservices")
        String txnCode);

    /**
     * 
     * @param amount
     * @param localCurrencyAmount
     * @param currencyNumber
     * @param debitAccountNumber
     * @param requestModule
     * @param paymentReference
     * @param msgId
     * @param debitCustomerId
     * @param source
     * @param valueDate
     * @param creditAccountNumber
     * @param userId
     * @param creditCustomerId
     * @param branchCode
     * @param txnCode
     * @param creditBranchCode
     * @param debitBranchCode
     * @param narration
     * @param currencyCode
     * @param batchNumber
     * @return
     *     returns com.najcom.access.datacontract.FcubsResponse
     */
    @WebMethod(operationName = "SingleDebitSingleCreditFX", action = "http://FCUBS.Webservices/IPostingService/SingleDebitSingleCreditFX")
    @WebResult(name = "SingleDebitSingleCreditFXResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "SingleDebitSingleCreditFX", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SingleDebitSingleCreditFX")
    @ResponseWrapper(localName = "SingleDebitSingleCreditFXResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SingleDebitSingleCreditFXResponse")
    public FcubsResponse singleDebitSingleCreditFX(
        @WebParam(name = "Source", targetNamespace = "http://FCUBS.Webservices")
        String source,
        @WebParam(name = "MsgId", targetNamespace = "http://FCUBS.Webservices")
        String msgId,
        @WebParam(name = "DebitCustomerId", targetNamespace = "http://FCUBS.Webservices")
        String debitCustomerId,
        @WebParam(name = "CreditCustomerId", targetNamespace = "http://FCUBS.Webservices")
        String creditCustomerId,
        @WebParam(name = "Amount", targetNamespace = "http://FCUBS.Webservices")
        BigDecimal amount,
        @WebParam(name = "CurrencyCode", targetNamespace = "http://FCUBS.Webservices")
        String currencyCode,
        @WebParam(name = "CurrencyNumber", targetNamespace = "http://FCUBS.Webservices")
        String currencyNumber,
        @WebParam(name = "CreditAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String creditAccountNumber,
        @WebParam(name = "DebitAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String debitAccountNumber,
        @WebParam(name = "LocalCurrencyAmount", targetNamespace = "http://FCUBS.Webservices")
        BigDecimal localCurrencyAmount,
        @WebParam(name = "Narration", targetNamespace = "http://FCUBS.Webservices")
        String narration,
        @WebParam(name = "PaymentReference", targetNamespace = "http://FCUBS.Webservices")
        String paymentReference,
        @WebParam(name = "RequestModule", targetNamespace = "http://FCUBS.Webservices")
        String requestModule,
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId,
        @WebParam(name = "BranchCode", targetNamespace = "http://FCUBS.Webservices")
        String branchCode,
        @WebParam(name = "DebitBranchCode", targetNamespace = "http://FCUBS.Webservices")
        String debitBranchCode,
        @WebParam(name = "CreditBranchCode", targetNamespace = "http://FCUBS.Webservices")
        String creditBranchCode,
        @WebParam(name = "BatchNumber", targetNamespace = "http://FCUBS.Webservices")
        String batchNumber,
        @WebParam(name = "ValueDate", targetNamespace = "http://FCUBS.Webservices")
        String valueDate,
        @WebParam(name = "TxnCode", targetNamespace = "http://FCUBS.Webservices")
        String txnCode);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.CustomerODLimitResponse
     */
    @WebMethod(operationName = "GetCustomerODLimit", action = "http://FCUBS.Webservices/IPostingService/GetCustomerODLimit")
    @WebResult(name = "GetCustomerODLimitResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCustomerODLimit", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerODLimit")
    @ResponseWrapper(localName = "GetCustomerODLimitResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerODLimitResponse")
    public CustomerODLimitResponse getCustomerODLimit(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param trnRefNo
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.TransactionRefNoDetailsResponse
     */
    @WebMethod(operationName = "GetTransactionRefNoDetails", action = "http://FCUBS.Webservices/IPostingService/GetTransactionRefNoDetails")
    @WebResult(name = "GetTransactionRefNoDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetTransactionRefNoDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetTransactionRefNoDetails")
    @ResponseWrapper(localName = "GetTransactionRefNoDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetTransactionRefNoDetailsResponse")
    public TransactionRefNoDetailsResponse getTransactionRefNoDetails(
        @WebParam(name = "trn_ref_no", targetNamespace = "http://FCUBS.Webservices")
        String trnRefNo);

    /**
     * 
     * @param transDate
     * @param accountNumber
     * @param rowID
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfSingleTransactionDetails
     */
    @WebMethod(operationName = "GetSingleTransactionDetails", action = "http://FCUBS.Webservices/IPostingService/GetSingleTransactionDetails")
    @WebResult(name = "GetSingleTransactionDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetSingleTransactionDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetSingleTransactionDetails")
    @ResponseWrapper(localName = "GetSingleTransactionDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetSingleTransactionDetailsResponse")
    public ArrayOfSingleTransactionDetails getSingleTransactionDetails(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "transDate", targetNamespace = "http://FCUBS.Webservices")
        String transDate,
        @WebParam(name = "RowID", targetNamespace = "http://FCUBS.Webservices")
        String rowID);

    /**
     * 
     * @param accountNumber
     * @param chequeNumber
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.ChequeNoInquiryResponse
     */
    @WebMethod(operationName = "ChequeNumberInquiry", action = "http://FCUBS.Webservices/IPostingService/ChequeNumberInquiry")
    @WebResult(name = "ChequeNumberInquiryResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "ChequeNumberInquiry", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ChequeNumberInquiry")
    @ResponseWrapper(localName = "ChequeNumberInquiryResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ChequeNumberInquiryResponse")
    public ChequeNoInquiryResponse chequeNumberInquiry(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "ChequeNumber", targetNamespace = "http://FCUBS.Webservices")
        String chequeNumber);

    /**
     * 
     * @param custID
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfSweepAcctsDetails
     */
    @WebMethod(operationName = "SweepAccts", action = "http://FCUBS.Webservices/IPostingService/SweepAccts")
    @WebResult(name = "SweepAcctsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "SweepAccts", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SweepAccts")
    @ResponseWrapper(localName = "SweepAcctsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SweepAcctsResponse")
    public ArrayOfSweepAcctsDetails sweepAccts(
        @WebParam(name = "CustID", targetNamespace = "http://FCUBS.Webservices")
        String custID);

    /**
     * 
     * @param oldAccountNumber
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfNubanAcctsDetails
     */
    @WebMethod(operationName = "NubanAccts", action = "http://FCUBS.Webservices/IPostingService/NubanAccts")
    @WebResult(name = "NubanAcctsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "NubanAccts", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.NubanAccts")
    @ResponseWrapper(localName = "NubanAcctsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.NubanAcctsResponse")
    public ArrayOfNubanAcctsDetails nubanAccts(
        @WebParam(name = "OldAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String oldAccountNumber);

    /**
     * 
     * @param customerID
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfImageCollectionDetails
     */
    @WebMethod(operationName = "ImageCollection", action = "http://FCUBS.Webservices/IPostingService/ImageCollection")
    @WebResult(name = "ImageCollectionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "ImageCollection", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ImageCollection")
    @ResponseWrapper(localName = "ImageCollectionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ImageCollectionResponse")
    public ArrayOfImageCollectionDetails imageCollection(
        @WebParam(name = "CustomerID", targetNamespace = "http://FCUBS.Webservices")
        String customerID);

    /**
     * 
     * @param accountNo
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfImageCollectionDetails
     */
    @WebMethod(operationName = "AccountImageCollection", action = "http://FCUBS.Webservices/IPostingService/AccountImageCollection")
    @WebResult(name = "AccountImageCollectionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "AccountImageCollection", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.AccountImageCollection")
    @ResponseWrapper(localName = "AccountImageCollectionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.AccountImageCollectionResponse")
    public ArrayOfImageCollectionDetails accountImageCollection(
        @WebParam(name = "AccountNo", targetNamespace = "http://FCUBS.Webservices")
        String accountNo);

    /**
     * 
     * @param accountNo
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfImageCollectionDetails
     */
    @WebMethod(operationName = "SignatureImageCollection", action = "http://FCUBS.Webservices/IPostingService/SignatureImageCollection")
    @WebResult(name = "SignatureImageCollectionResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "SignatureImageCollection", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SignatureImageCollection")
    @ResponseWrapper(localName = "SignatureImageCollectionResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.SignatureImageCollectionResponse")
    public ArrayOfImageCollectionDetails signatureImageCollection(
        @WebParam(name = "AccountNo", targetNamespace = "http://FCUBS.Webservices")
        String accountNo);

    /**
     * 
     * @param dateofBirth
     * @param telephoneNo
     * @param accountClass
     * @param mobileNo
     * @param email
     * @return
     *     returns com.najcom.access.datacontract.AccountSearchResponse
     */
    @WebMethod(operationName = "CheckifCustomerExist", action = "http://FCUBS.Webservices/IPostingService/CheckifCustomerExist")
    @WebResult(name = "CheckifCustomerExistResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "CheckifCustomerExist", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.CheckifCustomerExist")
    @ResponseWrapper(localName = "CheckifCustomerExistResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.CheckifCustomerExistResponse")
    public AccountSearchResponse checkifCustomerExist(
        @WebParam(name = "MobileNo", targetNamespace = "http://FCUBS.Webservices")
        String mobileNo,
        @WebParam(name = "TelephoneNo", targetNamespace = "http://FCUBS.Webservices")
        String telephoneNo,
        @WebParam(name = "DateofBirth", targetNamespace = "http://FCUBS.Webservices")
        String dateofBirth,
        @WebParam(name = "AccountClass", targetNamespace = "http://FCUBS.Webservices")
        String accountClass,
        @WebParam(name = "Email", targetNamespace = "http://FCUBS.Webservices")
        String email);

    /**
     * 
     * @param accountClass
     * @param customerNo
     * @return
     *     returns com.najcom.access.datacontract.AccountSearchResponse
     */
    @WebMethod(operationName = "CheckifAccountClassExist", action = "http://FCUBS.Webservices/IPostingService/CheckifAccountClassExist")
    @WebResult(name = "CheckifAccountClassExistResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "CheckifAccountClassExist", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.CheckifAccountClassExist")
    @ResponseWrapper(localName = "CheckifAccountClassExistResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.CheckifAccountClassExistResponse")
    public AccountSearchResponse checkifAccountClassExist(
        @WebParam(name = "CustomerNo", targetNamespace = "http://FCUBS.Webservices")
        String customerNo,
        @WebParam(name = "AccountClass", targetNamespace = "http://FCUBS.Webservices")
        String accountClass);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.AccountSearchResponse
     */
    @WebMethod(operationName = "GetAccountSearchByAccountNumber", action = "http://FCUBS.Webservices/IPostingService/GetAccountSearchByAccountNumber")
    @WebResult(name = "GetAccountSearchByAccountNumberResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountSearchByAccountNumber", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSearchByAccountNumber")
    @ResponseWrapper(localName = "GetAccountSearchByAccountNumberResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSearchByAccountNumberResponse")
    public AccountSearchResponse getAccountSearchByAccountNumber(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param accountTitle
     * @return
     *     returns com.najcom.access.datacontract.AccountSearchResponse
     */
    @WebMethod(operationName = "GetAccountSearchByAccountTitle", action = "http://FCUBS.Webservices/IPostingService/GetAccountSearchByAccountTitle")
    @WebResult(name = "GetAccountSearchByAccountTitleResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountSearchByAccountTitle", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSearchByAccountTitle")
    @ResponseWrapper(localName = "GetAccountSearchByAccountTitleResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSearchByAccountTitleResponse")
    public AccountSearchResponse getAccountSearchByAccountTitle(
        @WebParam(name = "AccountTitle", targetNamespace = "http://FCUBS.Webservices")
        String accountTitle);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "GetCustomerInfoForIso", action = "http://FCUBS.Webservices/IPostingService/GetCustomerInfoForIso")
    @WebResult(name = "GetCustomerInfoForIsoResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCustomerInfoForIso", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerInfoForIso")
    @ResponseWrapper(localName = "GetCustomerInfoForIsoResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerInfoForIsoResponse")
    public String getCustomerInfoForIso(
        @WebParam(name = "accountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param branchCode
     * @param userId
     * @param tilVltInd
     * @return
     *     returns com.najcom.access.datacontract.ValidateUserTillResp
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/validateUserTill")
    @WebResult(name = "validateUserTillResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "validateUserTill", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ValidateUserTill")
    @ResponseWrapper(localName = "validateUserTillResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ValidateUserTillResponse")
    public ValidateUserTillResp validateUserTill(
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId,
        @WebParam(name = "Til_Vlt_Ind", targetNamespace = "http://FCUBS.Webservices")
        String tilVltInd,
        @WebParam(name = "branchCode", targetNamespace = "http://FCUBS.Webservices")
        String branchCode);

    /**
     * 
     * @param userId
     * @return
     *     returns com.najcom.access.datacontract.AuthenticateCBAUserResp
     */
    @WebMethod(operationName = "AuthenticateCBAUser", action = "http://FCUBS.Webservices/IPostingService/AuthenticateCBAUser")
    @WebResult(name = "AuthenticateCBAUserResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "AuthenticateCBAUser", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.AuthenticateCBAUser")
    @ResponseWrapper(localName = "AuthenticateCBAUserResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.AuthenticateCBAUserResponse")
    public AuthenticateCBAUserResp authenticateCBAUser(
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId);

    /**
     * 
     * @param userId
     * @return
     *     returns com.najcom.access.datacontract.GetUserBranchResp
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/getUserBranch")
    @WebResult(name = "getUserBranchResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "getUserBranch", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetUserBranch")
    @ResponseWrapper(localName = "getUserBranchResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetUserBranchResponse")
    public GetUserBranchResp getUserBranch(
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId);

    /**
     * 
     * @param branchCode
     * @param routingNo
     * @param txnDate
     * @return
     *     returns com.najcom.access.datacontract.GetChequeValueDateResp
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/getChequeValueDate")
    @WebResult(name = "getChequeValueDateResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "getChequeValueDate", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetChequeValueDate")
    @ResponseWrapper(localName = "getChequeValueDateResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetChequeValueDateResponse")
    public GetChequeValueDateResp getChequeValueDate(
        @WebParam(name = "routing_no", targetNamespace = "http://FCUBS.Webservices")
        String routingNo,
        @WebParam(name = "branch_code", targetNamespace = "http://FCUBS.Webservices")
        String branchCode,
        @WebParam(name = "txn_date", targetNamespace = "http://FCUBS.Webservices")
        XMLGregorianCalendar txnDate);

    /**
     * 
     * @param acGlNo
     * @return
     *     returns com.najcom.access.datacontract.GetAccountBranchResp
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/getAccountBranch")
    @WebResult(name = "getAccountBranchResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "getAccountBranch", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountBranch")
    @ResponseWrapper(localName = "getAccountBranchResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountBranchResponse")
    public GetAccountBranchResp getAccountBranch(
        @WebParam(name = "ac_gl_no", targetNamespace = "http://FCUBS.Webservices")
        String acGlNo);

    /**
     * 
     * @param isoNumCcyCode
     * @return
     *     returns com.najcom.access.datacontract.GetCurrencyCodeResp
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/getCurrencyCode")
    @WebResult(name = "getCurrencyCodeResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "getCurrencyCode", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCurrencyCode")
    @ResponseWrapper(localName = "getCurrencyCodeResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCurrencyCodeResponse")
    public GetCurrencyCodeResp getCurrencyCode(
        @WebParam(name = "iso_num_ccy_code", targetNamespace = "http://FCUBS.Webservices")
        String isoNumCcyCode);

    /**
     * 
     * @param custAcNo
     * @return
     *     returns com.najcom.access.datacontract.GetEffectiveBalanceResp
     */
    @WebMethod(action = "http://FCUBS.Webservices/IPostingService/getEffectiveBalance")
    @WebResult(name = "getEffectiveBalanceResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "getEffectiveBalance", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetEffectiveBalance")
    @ResponseWrapper(localName = "getEffectiveBalanceResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetEffectiveBalanceResponse")
    public GetEffectiveBalanceResp getEffectiveBalance(
        @WebParam(name = "cust_ac_no", targetNamespace = "http://FCUBS.Webservices")
        String custAcNo);

    /**
     * 
     * @param endDate
     * @param accountNumber
     * @param startDate
     * @return
     *     returns com.najcom.access.datacontract.AccountSummaryAndTrxResponse
     */
    @WebMethod(operationName = "GetAccountSummaryAndTransactions", action = "http://FCUBS.Webservices/IPostingService/GetAccountSummaryAndTransactions")
    @WebResult(name = "GetAccountSummaryAndTransactionsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountSummaryAndTransactions", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryAndTransactions")
    @ResponseWrapper(localName = "GetAccountSummaryAndTransactionsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryAndTransactionsResponse")
    public AccountSummaryAndTrxResponse getAccountSummaryAndTransactions(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "StartDate", targetNamespace = "http://FCUBS.Webservices")
        String startDate,
        @WebParam(name = "EndDate", targetNamespace = "http://FCUBS.Webservices")
        String endDate);

    /**
     * 
     * @param loanAccountNumber
     * @param reviewDate
     * @return
     *     returns com.najcom.access.datacontract.LoanInfoSummaryResponse
     */
    @WebMethod(operationName = "GetLoanInfoByAccountNumber", action = "http://FCUBS.Webservices/IPostingService/GetLoanInfoByAccountNumber")
    @WebResult(name = "GetLoanInfoByAccountNumberResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetLoanInfoByAccountNumber", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanInfoByAccountNumber")
    @ResponseWrapper(localName = "GetLoanInfoByAccountNumberResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanInfoByAccountNumberResponse")
    public LoanInfoSummaryResponse getLoanInfoByAccountNumber(
        @WebParam(name = "loanAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String loanAccountNumber,
        @WebParam(name = "reviewDate", targetNamespace = "http://FCUBS.Webservices")
        String reviewDate);

    /**
     * 
     * @param endDate
     * @param accountNumber
     * @param startDate
     * @return
     *     returns com.najcom.access.datacontract.AccountSummaryAndTrxResponse
     */
    @WebMethod(operationName = "GetAccountTransactionStatement", action = "http://FCUBS.Webservices/IPostingService/GetAccountTransactionStatement")
    @WebResult(name = "GetAccountTransactionStatementResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountTransactionStatement", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountTransactionStatement")
    @ResponseWrapper(localName = "GetAccountTransactionStatementResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountTransactionStatementResponse")
    public AccountSummaryAndTrxResponse getAccountTransactionStatement(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "StartDate", targetNamespace = "http://FCUBS.Webservices")
        String startDate,
        @WebParam(name = "EndDate", targetNamespace = "http://FCUBS.Webservices")
        String endDate);

    /**
     * 
     * @param endDate
     * @param accountNumber
     * @param startDate
     * @return
     *     returns com.najcom.access.datacontract.AccountSummaryAndTrxResponse
     */
    @WebMethod(operationName = "GetNoTransactionSummary", action = "http://FCUBS.Webservices/IPostingService/GetNoTransactionSummary")
    @WebResult(name = "GetNoTransactionSummaryResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetNoTransactionSummary", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetNoTransactionSummary")
    @ResponseWrapper(localName = "GetNoTransactionSummaryResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetNoTransactionSummaryResponse")
    public AccountSummaryAndTrxResponse getNoTransactionSummary(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "StartDate", targetNamespace = "http://FCUBS.Webservices")
        String startDate,
        @WebParam(name = "EndDate", targetNamespace = "http://FCUBS.Webservices")
        String endDate);

    /**
     * 
     * @param endDate
     * @param accountNumber
     * @param startDate
     * @return
     *     returns com.najcom.access.datacontract.AccountSummaryAndTrxResponse
     */
    @WebMethod(operationName = "GetTransactionSummaryByValueDate", action = "http://FCUBS.Webservices/IPostingService/GetTransactionSummaryByValueDate")
    @WebResult(name = "GetTransactionSummaryByValueDateResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetTransactionSummaryByValueDate", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetTransactionSummaryByValueDate")
    @ResponseWrapper(localName = "GetTransactionSummaryByValueDateResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetTransactionSummaryByValueDateResponse")
    public AccountSummaryAndTrxResponse getTransactionSummaryByValueDate(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "StartDate", targetNamespace = "http://FCUBS.Webservices")
        String startDate,
        @WebParam(name = "EndDate", targetNamespace = "http://FCUBS.Webservices")
        String endDate);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.AccountRelatedResponse
     */
    @WebMethod(operationName = "GetRelatedAccounts", action = "http://FCUBS.Webservices/IPostingService/GetRelatedAccounts")
    @WebResult(name = "GetRelatedAccountsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetRelatedAccounts", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetRelatedAccounts")
    @ResponseWrapper(localName = "GetRelatedAccountsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetRelatedAccountsResponse")
    public AccountRelatedResponse getRelatedAccounts(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.VisaCardLimitResponse
     */
    @WebMethod(operationName = "GetVisaCardLimits", action = "http://FCUBS.Webservices/IPostingService/GetVisaCardLimits")
    @WebResult(name = "GetVisaCardLimitsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetVisaCardLimits", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetVisaCardLimits")
    @ResponseWrapper(localName = "GetVisaCardLimitsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetVisaCardLimitsResponse")
    public VisaCardLimitResponse getVisaCardLimits(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.AccountCotAndVatResponse
     */
    @WebMethod(operationName = "GetCotVatDetails", action = "http://FCUBS.Webservices/IPostingService/GetCotVatDetails")
    @WebResult(name = "GetCotVatDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCotVatDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCotVatDetails")
    @ResponseWrapper(localName = "GetCotVatDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCotVatDetailsResponse")
    public AccountCotAndVatResponse getCotVatDetails(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param customerId
     * @return
     *     returns com.najcom.access.datacontract.AccountSummaryResponse
     */
    @WebMethod(operationName = "GetAccountSummaryByCustomerID", action = "http://FCUBS.Webservices/IPostingService/GetAccountSummaryByCustomerID")
    @WebResult(name = "GetAccountSummaryByCustomerIDResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountSummaryByCustomerID", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryByCustomerID")
    @ResponseWrapper(localName = "GetAccountSummaryByCustomerIDResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryByCustomerIDResponse")
    public AccountSummaryResponse getAccountSummaryByCustomerID(
        @WebParam(name = "CustomerId", targetNamespace = "http://FCUBS.Webservices")
        String customerId);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.AccountSummaryResponse
     */
    @WebMethod(operationName = "GetAccountSummary", action = "http://FCUBS.Webservices/IPostingService/GetAccountSummary")
    @WebResult(name = "GetAccountSummaryResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountSummary", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummary")
    @ResponseWrapper(localName = "GetAccountSummaryResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryResponse")
    public AccountSummaryResponse getAccountSummary(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.ChequeDetailsResponse
     */
    @WebMethod(operationName = "FetchChequeDetails", action = "http://FCUBS.Webservices/IPostingService/FetchChequeDetails")
    @WebResult(name = "FetchChequeDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FetchChequeDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchChequeDetails")
    @ResponseWrapper(localName = "FetchChequeDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchChequeDetailsResponse")
    public ChequeDetailsResponse fetchChequeDetails(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param branchCode
     * @param chequeBookNo
     * @param checkerID
     * @param accountNumber
     * @param makerID
     * @return
     *     returns com.najcom.access.datacontract.ActivateChequeBookResponse
     */
    @WebMethod(operationName = "ActivateChequeBook", action = "http://FCUBS.Webservices/IPostingService/ActivateChequeBook")
    @WebResult(name = "ActivateChequeBookResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "ActivateChequeBook", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ActivateChequeBook")
    @ResponseWrapper(localName = "ActivateChequeBookResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.ActivateChequeBookResponse")
    public ActivateChequeBookResponse activateChequeBook(
        @WebParam(name = "BranchCode", targetNamespace = "http://FCUBS.Webservices")
        String branchCode,
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "ChequeBookNo", targetNamespace = "http://FCUBS.Webservices")
        String chequeBookNo,
        @WebParam(name = "MakerID", targetNamespace = "http://FCUBS.Webservices")
        String makerID,
        @WebParam(name = "CheckerID", targetNamespace = "http://FCUBS.Webservices")
        String checkerID);

    /**
     * 
     * @param loanAccountNumber
     * @return
     *     returns com.najcom.access.datacontract.LoanSummaryResponse
     */
    @WebMethod(operationName = "GetLoanStatement", action = "http://FCUBS.Webservices/IPostingService/GetLoanStatement")
    @WebResult(name = "GetLoanStatementResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetLoanStatement", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanStatement")
    @ResponseWrapper(localName = "GetLoanStatementResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanStatementResponse")
    public LoanSummaryResponse getLoanStatement(
        @WebParam(name = "LoanAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String loanAccountNumber);

    /**
     * 
     * @param loanAccountNumbersRequest
     * @return
     *     returns com.najcom.access.datacontract.LoanSummaryResponseBulk
     */
    @WebMethod(operationName = "GetLoanStatement2", action = "http://FCUBS.Webservices/IPostingService/GetLoanStatement2")
    @WebResult(name = "GetLoanStatement2Result", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetLoanStatement2", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanStatement2")
    @ResponseWrapper(localName = "GetLoanStatement2Response", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanStatement2Response")
    public LoanSummaryResponseBulk getLoanStatement2(
        @WebParam(name = "LoanAccountNumbersRequest", targetNamespace = "http://FCUBS.Webservices")
        ArrayOfstring loanAccountNumbersRequest);

    /**
     * 
     * @param accountNumberList
     * @param endDate
     * @param startDate
     * @return
     *     returns com.najcom.access.datacontract.AccountTransactionResponseBulk
     */
    @WebMethod(operationName = "GetAccountSummaryAndTransactions2", action = "http://FCUBS.Webservices/IPostingService/GetAccountSummaryAndTransactions2")
    @WebResult(name = "GetAccountSummaryAndTransactions2Result", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountSummaryAndTransactions2", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryAndTransactions2")
    @ResponseWrapper(localName = "GetAccountSummaryAndTransactions2Response", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountSummaryAndTransactions2Response")
    public AccountTransactionResponseBulk getAccountSummaryAndTransactions2(
        @WebParam(name = "AccountNumberList", targetNamespace = "http://FCUBS.Webservices")
        ArrayOfstring accountNumberList,
        @WebParam(name = "StartDate", targetNamespace = "http://FCUBS.Webservices")
        String startDate,
        @WebParam(name = "EndDate", targetNamespace = "http://FCUBS.Webservices")
        String endDate);

    /**
     * 
     * @param loanAccountNumberRequest
     * @return
     *     returns com.najcom.access.datacontract.LoanSummaryResponseBulk
     */
    @WebMethod(operationName = "GetLoanDetailByAccountNumber2", action = "http://FCUBS.Webservices/IPostingService/GetLoanDetailByAccountNumber2")
    @WebResult(name = "GetLoanDetailByAccountNumber2Result", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetLoanDetailByAccountNumber2", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanDetailByAccountNumber2")
    @ResponseWrapper(localName = "GetLoanDetailByAccountNumber2Response", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanDetailByAccountNumber2Response")
    public LoanSummaryResponseBulk getLoanDetailByAccountNumber2(
        @WebParam(name = "LoanAccountNumberRequest", targetNamespace = "http://FCUBS.Webservices")
        ArrayOfstring loanAccountNumberRequest);

    /**
     * 
     * @param customerId
     * @return
     *     returns com.najcom.access.datacontract.LoanSummaryResponse
     */
    @WebMethod(operationName = "GetLoanSummaryByCustomerID", action = "http://FCUBS.Webservices/IPostingService/GetLoanSummaryByCustomerID")
    @WebResult(name = "GetLoanSummaryByCustomerIDResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetLoanSummaryByCustomerID", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanSummaryByCustomerID")
    @ResponseWrapper(localName = "GetLoanSummaryByCustomerIDResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanSummaryByCustomerIDResponse")
    public LoanSummaryResponse getLoanSummaryByCustomerID(
        @WebParam(name = "CustomerId", targetNamespace = "http://FCUBS.Webservices")
        String customerId);

    /**
     * 
     * @param loanAccountNumber
     * @return
     *     returns com.najcom.access.datacontract.LoanSummaryResponse
     */
    @WebMethod(operationName = "GetLoanDetailByAccountNumber", action = "http://FCUBS.Webservices/IPostingService/GetLoanDetailByAccountNumber")
    @WebResult(name = "GetLoanDetailByAccountNumberResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetLoanDetailByAccountNumber", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanDetailByAccountNumber")
    @ResponseWrapper(localName = "GetLoanDetailByAccountNumberResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetLoanDetailByAccountNumberResponse")
    public LoanSummaryResponse getLoanDetailByAccountNumber(
        @WebParam(name = "LoanAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String loanAccountNumber);

    /**
     * 
     * @param customerId
     * @return
     *     returns com.najcom.access.datacontract.CustomerDetails
     */
    @WebMethod(operationName = "GetCustomerDetails", action = "http://FCUBS.Webservices/IPostingService/GetCustomerDetails")
    @WebResult(name = "GetCustomerDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCustomerDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerDetails")
    @ResponseWrapper(localName = "GetCustomerDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerDetailsResponse")
    public CustomerDetails getCustomerDetails(
        @WebParam(name = "CustomerId", targetNamespace = "http://FCUBS.Webservices")
        String customerId);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.CustomerInformationResponse
     */
    @WebMethod(operationName = "GetCustomerInformation", action = "http://FCUBS.Webservices/IPostingService/GetCustomerInformation")
    @WebResult(name = "GetCustomerInformationResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCustomerInformation", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerInformation")
    @ResponseWrapper(localName = "GetCustomerInformationResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerInformationResponse")
    public CustomerInformationResponse getCustomerInformation(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.CustomerAcctsDetailResponse
     */
    @WebMethod(operationName = "GetCustomerAcctsDetail", action = "http://FCUBS.Webservices/IPostingService/GetCustomerAcctsDetail")
    @WebResult(name = "GetCustomerAcctsDetailResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCustomerAcctsDetail", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerAcctsDetail")
    @ResponseWrapper(localName = "GetCustomerAcctsDetailResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerAcctsDetailResponse")
    public CustomerAcctsDetailResponse getCustomerAcctsDetail(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param customerNo
     * @return
     *     returns com.najcom.access.datacontract.FixedDepositResponse
     */
    @WebMethod(operationName = "GetFixedDepositInfo", action = "http://FCUBS.Webservices/IPostingService/GetFixedDepositInfo")
    @WebResult(name = "GetFixedDepositInfoResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetFixedDepositInfo", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetFixedDepositInfo")
    @ResponseWrapper(localName = "GetFixedDepositInfoResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetFixedDepositInfoResponse")
    public FixedDepositResponse getFixedDepositInfo(
        @WebParam(name = "customerNo", targetNamespace = "http://FCUBS.Webservices")
        String customerNo);

    /**
     * 
     * @param loanAccountNumber
     * @return
     *     returns com.najcom.access.datacontract.RepaymentScheduleResponse
     */
    @WebMethod(operationName = "GetRepaymentSchedule", action = "http://FCUBS.Webservices/IPostingService/GetRepaymentSchedule")
    @WebResult(name = "GetRepaymentScheduleResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetRepaymentSchedule", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetRepaymentSchedule")
    @ResponseWrapper(localName = "GetRepaymentScheduleResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetRepaymentScheduleResponse")
    public RepaymentScheduleResponse getRepaymentSchedule(
        @WebParam(name = "loanAccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String loanAccountNumber);

    /**
     * 
     * @param accountNumberList
     * @return
     *     returns com.najcom.access.datacontract.ArrayOfCustomerAcctsDetailResponse
     */
    @WebMethod(operationName = "GetCustomerAcctsDetailBulk", action = "http://FCUBS.Webservices/IPostingService/GetCustomerAcctsDetailBulk")
    @WebResult(name = "GetCustomerAcctsDetailBulkResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetCustomerAcctsDetailBulk", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerAcctsDetailBulk")
    @ResponseWrapper(localName = "GetCustomerAcctsDetailBulkResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetCustomerAcctsDetailBulkResponse")
    public ArrayOfCustomerAcctsDetailResponse getCustomerAcctsDetailBulk(
        @WebParam(name = "accountNumberList", targetNamespace = "http://FCUBS.Webservices")
        ArrayOfstring accountNumberList);

    /**
     * 
     * @param accountNo
     * @return
     *     returns com.najcom.access.datacontract.GetAcctMemoInfoResponse
     */
    @WebMethod(operationName = "GetAccountMemoInfo", action = "http://FCUBS.Webservices/IPostingService/GetAccountMemoInfo")
    @WebResult(name = "GetAccountMemoInfoResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "GetAccountMemoInfo", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountMemoInfo")
    @ResponseWrapper(localName = "GetAccountMemoInfoResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.GetAccountMemoInfoResponse")
    public GetAcctMemoInfoResponse getAccountMemoInfo(
        @WebParam(name = "AccountNo", targetNamespace = "http://FCUBS.Webservices")
        String accountNo);

    /**
     * 
     * @param accountNo
     * @return
     *     returns com.najcom.access.datacontract.GetAccountExistResponse
     */
    @WebMethod(operationName = "CheckIfAccountExist", action = "http://FCUBS.Webservices/IPostingService/CheckIfAccountExist")
    @WebResult(name = "CheckIfAccountExistResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "CheckIfAccountExist", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.CheckIfAccountExist")
    @ResponseWrapper(localName = "CheckIfAccountExistResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.CheckIfAccountExistResponse")
    public GetAccountExistResponse checkIfAccountExist(
        @WebParam(name = "AccountNo", targetNamespace = "http://FCUBS.Webservices")
        String accountNo);

    /**
     * 
     * @param accountNumber
     * @param userId
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileAccountDetail
     */
    @WebMethod(operationName = "FetchMobileAccountDetails", action = "http://FCUBS.Webservices/IPostingService/FetchMobileAccountDetails")
    @WebResult(name = "FetchMobileAccountDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FetchMobileAccountDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchMobileAccountDetails")
    @ResponseWrapper(localName = "FetchMobileAccountDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchMobileAccountDetailsResponse")
    public ArrayOfMobileAccountDetail fetchMobileAccountDetails(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber,
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileTransactionDetail
     */
    @WebMethod(operationName = "FetchMobileTransactionDetails", action = "http://FCUBS.Webservices/IPostingService/FetchMobileTransactionDetails")
    @WebResult(name = "FetchMobileTransactionDetailsResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FetchMobileTransactionDetails", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchMobileTransactionDetails")
    @ResponseWrapper(localName = "FetchMobileTransactionDetailsResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchMobileTransactionDetailsResponse")
    public ArrayOfMobileTransactionDetail fetchMobileTransactionDetails(
        @WebParam(name = "AccountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

    /**
     * 
     * @param userId
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.VansoCustomerIdResponse
     */
    @WebMethod(operationName = "FetchVansoCustomerId", action = "http://FCUBS.Webservices/IPostingService/FetchVansoCustomerId")
    @WebResult(name = "FetchVansoCustomerIdResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "FetchVansoCustomerId", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchVansoCustomerId")
    @ResponseWrapper(localName = "FetchVansoCustomerIdResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.FetchVansoCustomerIdResponse")
    public VansoCustomerIdResponse fetchVansoCustomerId(
        @WebParam(name = "UserId", targetNamespace = "http://FCUBS.Webservices")
        String userId);

    /**
     * 
     * @param accountNumber
     * @return
     *     returns com.najcom.access.datacontract.webservicewrappers.AccountVerificationResponse
     */
    @WebMethod(operationName = "PerformAccountVerification", action = "http://FCUBS.Webservices/IPostingService/PerformAccountVerification")
    @WebResult(name = "PerformAccountVerificationResult", targetNamespace = "http://FCUBS.Webservices")
    @RequestWrapper(localName = "PerformAccountVerification", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.PerformAccountVerification")
    @ResponseWrapper(localName = "PerformAccountVerificationResponse", targetNamespace = "http://FCUBS.Webservices", className = "com.najcom.access.webservices.fcubs.PerformAccountVerificationResponse")
    public AccountVerificationResponse performAccountVerification(
        @WebParam(name = "accountNumber", targetNamespace = "http://FCUBS.Webservices")
        String accountNumber);

}
