
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.MessageIDStatusResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetMessageIDStatusResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response}MessageIDStatusResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getMessageIDStatusResult"
})
@XmlRootElement(name = "GetMessageIDStatusResponse")
public class GetMessageIDStatusResponse {

    @XmlElementRef(name = "GetMessageIDStatusResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<MessageIDStatusResponse> getMessageIDStatusResult;

    /**
     * Gets the value of the getMessageIDStatusResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MessageIDStatusResponse }{@code >}
     *     
     */
    public JAXBElement<MessageIDStatusResponse> getGetMessageIDStatusResult() {
        return getMessageIDStatusResult;
    }

    /**
     * Sets the value of the getMessageIDStatusResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MessageIDStatusResponse }{@code >}
     *     
     */
    public void setGetMessageIDStatusResult(JAXBElement<MessageIDStatusResponse> value) {
        this.getMessageIDStatusResult = value;
    }

}
