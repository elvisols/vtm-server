
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.UserLimitResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetUserLimitResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}UserLimitResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserLimitResult"
})
@XmlRootElement(name = "GetUserLimitResponse")
public class GetUserLimitResponse {

    @XmlElementRef(name = "GetUserLimitResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<UserLimitResponse> getUserLimitResult;

    /**
     * Gets the value of the getUserLimitResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UserLimitResponse }{@code >}
     *     
     */
    public JAXBElement<UserLimitResponse> getGetUserLimitResult() {
        return getUserLimitResult;
    }

    /**
     * Sets the value of the getUserLimitResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UserLimitResponse }{@code >}
     *     
     */
    public void setGetUserLimitResult(JAXBElement<UserLimitResponse> value) {
        this.getUserLimitResult = value;
    }

}
