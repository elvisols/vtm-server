
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.AccountVerificationResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PerformAccountVerificationResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO}AccountVerificationResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "performAccountVerificationResult"
})
@XmlRootElement(name = "PerformAccountVerificationResponse")
public class PerformAccountVerificationResponse {

    @XmlElementRef(name = "PerformAccountVerificationResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountVerificationResponse> performAccountVerificationResult;

    /**
     * Gets the value of the performAccountVerificationResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountVerificationResponse }{@code >}
     *     
     */
    public JAXBElement<AccountVerificationResponse> getPerformAccountVerificationResult() {
        return performAccountVerificationResult;
    }

    /**
     * Sets the value of the performAccountVerificationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountVerificationResponse }{@code >}
     *     
     */
    public void setPerformAccountVerificationResult(JAXBElement<AccountVerificationResponse> value) {
        this.performAccountVerificationResult = value;
    }

}
