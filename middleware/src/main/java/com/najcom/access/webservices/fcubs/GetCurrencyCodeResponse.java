
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.GetCurrencyCodeResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getCurrencyCodeResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}GetCurrencyCodeResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCurrencyCodeResult"
})
@XmlRootElement(name = "getCurrencyCodeResponse")
public class GetCurrencyCodeResponse {

    @XmlElementRef(name = "getCurrencyCodeResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<GetCurrencyCodeResp> getCurrencyCodeResult;

    /**
     * Gets the value of the getCurrencyCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCurrencyCodeResp }{@code >}
     *     
     */
    public JAXBElement<GetCurrencyCodeResp> getGetCurrencyCodeResult() {
        return getCurrencyCodeResult;
    }

    /**
     * Sets the value of the getCurrencyCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCurrencyCodeResp }{@code >}
     *     
     */
    public void setGetCurrencyCodeResult(JAXBElement<GetCurrencyCodeResp> value) {
        this.getCurrencyCodeResult = value;
    }

}
