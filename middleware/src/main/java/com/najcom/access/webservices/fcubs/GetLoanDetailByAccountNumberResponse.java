
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.LoanSummaryResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetLoanDetailByAccountNumberResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}LoanSummaryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLoanDetailByAccountNumberResult"
})
@XmlRootElement(name = "GetLoanDetailByAccountNumberResponse")
public class GetLoanDetailByAccountNumberResponse {

    @XmlElementRef(name = "GetLoanDetailByAccountNumberResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<LoanSummaryResponse> getLoanDetailByAccountNumberResult;

    /**
     * Gets the value of the getLoanDetailByAccountNumberResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LoanSummaryResponse }{@code >}
     *     
     */
    public JAXBElement<LoanSummaryResponse> getGetLoanDetailByAccountNumberResult() {
        return getLoanDetailByAccountNumberResult;
    }

    /**
     * Sets the value of the getLoanDetailByAccountNumberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LoanSummaryResponse }{@code >}
     *     
     */
    public void setGetLoanDetailByAccountNumberResult(JAXBElement<LoanSummaryResponse> value) {
        this.getLoanDetailByAccountNumberResult = value;
    }

}
