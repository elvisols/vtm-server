
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileTransactionDetail;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchMobileTransactionDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO}ArrayOfMobileTransactionDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchMobileTransactionDetailsResult"
})
@XmlRootElement(name = "FetchMobileTransactionDetailsResponse")
public class FetchMobileTransactionDetailsResponse {

    @XmlElementRef(name = "FetchMobileTransactionDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfMobileTransactionDetail> fetchMobileTransactionDetailsResult;

    /**
     * Gets the value of the fetchMobileTransactionDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfMobileTransactionDetail }{@code >}
     *     
     */
    public JAXBElement<ArrayOfMobileTransactionDetail> getFetchMobileTransactionDetailsResult() {
        return fetchMobileTransactionDetailsResult;
    }

    /**
     * Sets the value of the fetchMobileTransactionDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfMobileTransactionDetail }{@code >}
     *     
     */
    public void setFetchMobileTransactionDetailsResult(JAXBElement<ArrayOfMobileTransactionDetail> value) {
        this.fetchMobileTransactionDetailsResult = value;
    }

}
