
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.AccountCotAndVatResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCotVatDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountCotAndVatResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCotVatDetailsResult"
})
@XmlRootElement(name = "GetCotVatDetailsResponse")
public class GetCotVatDetailsResponse {

    @XmlElementRef(name = "GetCotVatDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountCotAndVatResponse> getCotVatDetailsResult;

    /**
     * Gets the value of the getCotVatDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountCotAndVatResponse }{@code >}
     *     
     */
    public JAXBElement<AccountCotAndVatResponse> getGetCotVatDetailsResult() {
        return getCotVatDetailsResult;
    }

    /**
     * Sets the value of the getCotVatDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountCotAndVatResponse }{@code >}
     *     
     */
    public void setGetCotVatDetailsResult(JAXBElement<AccountCotAndVatResponse> value) {
        this.getCotVatDetailsResult = value;
    }

}
