@javax.xml.bind.annotation.XmlSchema(namespace = "http://FCUBS.Webservices",
xmlns = { 
		  @javax.xml.bind.annotation.XmlNs(prefix = "fcub", 
		     namespaceURI="http://FCUBS.Webservices"),
		  @javax.xml.bind.annotation.XmlNs(prefix = "fcub1", 
		  namespaceURI="http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans")
		},
elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED
)
package com.najcom.access.webservices.fcubs;
