
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.AccountSearchResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccountSearchByAccountNumberResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountSearchResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountSearchByAccountNumberResult"
})
@XmlRootElement(name = "GetAccountSearchByAccountNumberResponse")
public class GetAccountSearchByAccountNumberResponse {

    @XmlElementRef(name = "GetAccountSearchByAccountNumberResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountSearchResponse> getAccountSearchByAccountNumberResult;

    /**
     * Gets the value of the getAccountSearchByAccountNumberResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}
     *     
     */
    public JAXBElement<AccountSearchResponse> getGetAccountSearchByAccountNumberResult() {
        return getAccountSearchByAccountNumberResult;
    }

    /**
     * Sets the value of the getAccountSearchByAccountNumberResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}
     *     
     */
    public void setGetAccountSearchByAccountNumberResult(JAXBElement<AccountSearchResponse> value) {
        this.getAccountSearchByAccountNumberResult = value;
    }

}
