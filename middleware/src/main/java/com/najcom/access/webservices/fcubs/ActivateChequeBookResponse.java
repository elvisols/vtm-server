
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActivateChequeBookResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ActivateChequeBookResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "activateChequeBookResult"
})
@XmlRootElement(name = "ActivateChequeBookResponse")
public class ActivateChequeBookResponse {

    @XmlElementRef(name = "ActivateChequeBookResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<com.najcom.access.datacontract.ActivateChequeBookResponse> activateChequeBookResult;

    /**
     * Gets the value of the activateChequeBookResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.najcom.access.datacontract.ActivateChequeBookResponse }{@code >}
     *     
     */
    public JAXBElement<com.najcom.access.datacontract.ActivateChequeBookResponse> getActivateChequeBookResult() {
        return activateChequeBookResult;
    }

    /**
     * Sets the value of the activateChequeBookResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.najcom.access.datacontract.ActivateChequeBookResponse }{@code >}
     *     
     */
    public void setActivateChequeBookResult(JAXBElement<com.najcom.access.datacontract.ActivateChequeBookResponse> value) {
        this.activateChequeBookResult = value;
    }

}
