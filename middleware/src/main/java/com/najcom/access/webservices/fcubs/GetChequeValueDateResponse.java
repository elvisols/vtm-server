
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.GetChequeValueDateResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getChequeValueDateResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}GetChequeValueDateResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getChequeValueDateResult"
})
@XmlRootElement(name = "getChequeValueDateResponse")
public class GetChequeValueDateResponse {

    @XmlElementRef(name = "getChequeValueDateResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<GetChequeValueDateResp> getChequeValueDateResult;

    /**
     * Gets the value of the getChequeValueDateResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetChequeValueDateResp }{@code >}
     *     
     */
    public JAXBElement<GetChequeValueDateResp> getGetChequeValueDateResult() {
        return getChequeValueDateResult;
    }

    /**
     * Sets the value of the getChequeValueDateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetChequeValueDateResp }{@code >}
     *     
     */
    public void setGetChequeValueDateResult(JAXBElement<GetChequeValueDateResp> value) {
        this.getChequeValueDateResult = value;
    }

}
