
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CustomerDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CustomerDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerDetailsResult"
})
@XmlRootElement(name = "GetCustomerDetailsResponse")
public class GetCustomerDetailsResponse {

    @XmlElementRef(name = "GetCustomerDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerDetails> getCustomerDetailsResult;

    /**
     * Gets the value of the getCustomerDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerDetails }{@code >}
     *     
     */
    public JAXBElement<CustomerDetails> getGetCustomerDetailsResult() {
        return getCustomerDetailsResult;
    }

    /**
     * Sets the value of the getCustomerDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerDetails }{@code >}
     *     
     */
    public void setGetCustomerDetailsResult(JAXBElement<CustomerDetails> value) {
        this.getCustomerDetailsResult = value;
    }

}
