
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.AccountSummaryResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccountSummaryByCustomerIDResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountSummaryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountSummaryByCustomerIDResult"
})
@XmlRootElement(name = "GetAccountSummaryByCustomerIDResponse")
public class GetAccountSummaryByCustomerIDResponse {

    @XmlElementRef(name = "GetAccountSummaryByCustomerIDResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountSummaryResponse> getAccountSummaryByCustomerIDResult;

    /**
     * Gets the value of the getAccountSummaryByCustomerIDResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountSummaryResponse }{@code >}
     *     
     */
    public JAXBElement<AccountSummaryResponse> getGetAccountSummaryByCustomerIDResult() {
        return getAccountSummaryByCustomerIDResult;
    }

    /**
     * Sets the value of the getAccountSummaryByCustomerIDResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountSummaryResponse }{@code >}
     *     
     */
    public void setGetAccountSummaryByCustomerIDResult(JAXBElement<AccountSummaryResponse> value) {
        this.getAccountSummaryByCustomerIDResult = value;
    }

}
