
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ScreenLimitResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetScreenLimitResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ScreenLimitResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getScreenLimitResult"
})
@XmlRootElement(name = "GetScreenLimitResponse")
public class GetScreenLimitResponse {

    @XmlElementRef(name = "GetScreenLimitResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ScreenLimitResponse> getScreenLimitResult;

    /**
     * Gets the value of the getScreenLimitResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ScreenLimitResponse }{@code >}
     *     
     */
    public JAXBElement<ScreenLimitResponse> getGetScreenLimitResult() {
        return getScreenLimitResult;
    }

    /**
     * Sets the value of the getScreenLimitResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ScreenLimitResponse }{@code >}
     *     
     */
    public void setGetScreenLimitResult(JAXBElement<ScreenLimitResponse> value) {
        this.getScreenLimitResult = value;
    }

}
