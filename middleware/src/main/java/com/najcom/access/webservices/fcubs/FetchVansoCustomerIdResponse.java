
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.VansoCustomerIdResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchVansoCustomerIdResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO}VansoCustomerIdResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchVansoCustomerIdResult"
})
@XmlRootElement(name = "FetchVansoCustomerIdResponse")
public class FetchVansoCustomerIdResponse {

    @XmlElementRef(name = "FetchVansoCustomerIdResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<VansoCustomerIdResponse> fetchVansoCustomerIdResult;

    /**
     * Gets the value of the fetchVansoCustomerIdResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VansoCustomerIdResponse }{@code >}
     *     
     */
    public JAXBElement<VansoCustomerIdResponse> getFetchVansoCustomerIdResult() {
        return fetchVansoCustomerIdResult;
    }

    /**
     * Sets the value of the fetchVansoCustomerIdResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VansoCustomerIdResponse }{@code >}
     *     
     */
    public void setFetchVansoCustomerIdResult(JAXBElement<VansoCustomerIdResponse> value) {
        this.fetchVansoCustomerIdResult = value;
    }

}
