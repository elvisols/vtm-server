
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateAmtBlockResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCustomerServiceCreateAmtBlkResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateAmtBlockResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCustomerServiceCreateAmtBlkResult"
})
@XmlRootElement(name = "FCUBSCustomerServiceCreateAmtBlkResponse")
public class FCUBSCustomerServiceCreateAmtBlkResponse {

    @XmlElementRef(name = "FCUBSCustomerServiceCreateAmtBlkResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateAmtBlockResponse> fcubsCustomerServiceCreateAmtBlkResult;

    /**
     * Gets the value of the fcubsCustomerServiceCreateAmtBlkResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateAmtBlockResponse }{@code >}
     *     
     */
    public JAXBElement<CreateAmtBlockResponse> getFCUBSCustomerServiceCreateAmtBlkResult() {
        return fcubsCustomerServiceCreateAmtBlkResult;
    }

    /**
     * Sets the value of the fcubsCustomerServiceCreateAmtBlkResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateAmtBlockResponse }{@code >}
     *     
     */
    public void setFCUBSCustomerServiceCreateAmtBlkResult(JAXBElement<CreateAmtBlockResponse> value) {
        this.fcubsCustomerServiceCreateAmtBlkResult = value;
    }

}
