
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateStopPaymentResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCreateStopPaymentResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateStopPaymentResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCreateStopPaymentResult"
})
@XmlRootElement(name = "FCUBSCreateStopPaymentResponse")
public class FCUBSCreateStopPaymentResponse {

    @XmlElementRef(name = "FCUBSCreateStopPaymentResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateStopPaymentResponse> fcubsCreateStopPaymentResult;

    /**
     * Gets the value of the fcubsCreateStopPaymentResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateStopPaymentResponse }{@code >}
     *     
     */
    public JAXBElement<CreateStopPaymentResponse> getFCUBSCreateStopPaymentResult() {
        return fcubsCreateStopPaymentResult;
    }

    /**
     * Sets the value of the fcubsCreateStopPaymentResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateStopPaymentResponse }{@code >}
     *     
     */
    public void setFCUBSCreateStopPaymentResult(JAXBElement<CreateStopPaymentResponse> value) {
        this.fcubsCreateStopPaymentResult = value;
    }

}
