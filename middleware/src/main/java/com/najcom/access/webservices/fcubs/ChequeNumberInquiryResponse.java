
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.ChequeNoInquiryResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChequeNumberInquiryResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO}ChequeNoInquiryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "chequeNumberInquiryResult"
})
@XmlRootElement(name = "ChequeNumberInquiryResponse")
public class ChequeNumberInquiryResponse {

    @XmlElementRef(name = "ChequeNumberInquiryResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ChequeNoInquiryResponse> chequeNumberInquiryResult;

    /**
     * Gets the value of the chequeNumberInquiryResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ChequeNoInquiryResponse }{@code >}
     *     
     */
    public JAXBElement<ChequeNoInquiryResponse> getChequeNumberInquiryResult() {
        return chequeNumberInquiryResult;
    }

    /**
     * Sets the value of the chequeNumberInquiryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ChequeNoInquiryResponse }{@code >}
     *     
     */
    public void setChequeNumberInquiryResult(JAXBElement<ChequeNoInquiryResponse> value) {
        this.chequeNumberInquiryResult = value;
    }

}
