
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfSingleTransactionDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSingleTransactionDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfSingleTransactionDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSingleTransactionDetailsResult"
})
@XmlRootElement(name = "GetSingleTransactionDetailsResponse")
public class GetSingleTransactionDetailsResponse {

    @XmlElementRef(name = "GetSingleTransactionDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSingleTransactionDetails> getSingleTransactionDetailsResult;

    /**
     * Gets the value of the getSingleTransactionDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSingleTransactionDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSingleTransactionDetails> getGetSingleTransactionDetailsResult() {
        return getSingleTransactionDetailsResult;
    }

    /**
     * Sets the value of the getSingleTransactionDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSingleTransactionDetails }{@code >}
     *     
     */
    public void setGetSingleTransactionDetailsResult(JAXBElement<ArrayOfSingleTransactionDetails> value) {
        this.getSingleTransactionDetailsResult = value;
    }

}
