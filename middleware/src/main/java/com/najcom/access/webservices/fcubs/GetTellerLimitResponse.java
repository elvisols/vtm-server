
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.TellerLimitResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTellerLimitResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}TellerLimitResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTellerLimitResult"
})
@XmlRootElement(name = "GetTellerLimitResponse")
public class GetTellerLimitResponse {

    @XmlElementRef(name = "GetTellerLimitResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<TellerLimitResponse> getTellerLimitResult;

    /**
     * Gets the value of the getTellerLimitResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TellerLimitResponse }{@code >}
     *     
     */
    public JAXBElement<TellerLimitResponse> getGetTellerLimitResult() {
        return getTellerLimitResult;
    }

    /**
     * Sets the value of the getTellerLimitResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TellerLimitResponse }{@code >}
     *     
     */
    public void setGetTellerLimitResult(JAXBElement<TellerLimitResponse> value) {
        this.getTellerLimitResult = value;
    }

}
