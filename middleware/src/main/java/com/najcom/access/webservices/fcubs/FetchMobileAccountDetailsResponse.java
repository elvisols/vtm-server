
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileAccountDetail;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchMobileAccountDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO}ArrayOfMobileAccountDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchMobileAccountDetailsResult"
})
@XmlRootElement(name = "FetchMobileAccountDetailsResponse")
public class FetchMobileAccountDetailsResponse {

    @XmlElementRef(name = "FetchMobileAccountDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfMobileAccountDetail> fetchMobileAccountDetailsResult;

    /**
     * Gets the value of the fetchMobileAccountDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfMobileAccountDetail }{@code >}
     *     
     */
    public JAXBElement<ArrayOfMobileAccountDetail> getFetchMobileAccountDetailsResult() {
        return fetchMobileAccountDetailsResult;
    }

    /**
     * Sets the value of the fetchMobileAccountDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfMobileAccountDetail }{@code >}
     *     
     */
    public void setFetchMobileAccountDetailsResult(JAXBElement<ArrayOfMobileAccountDetail> value) {
        this.fetchMobileAccountDetailsResult = value;
    }

}
