
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.AccountSummaryAndTrxResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTransactionSummaryByValueDateResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountSummaryAndTrxResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionSummaryByValueDateResult"
})
@XmlRootElement(name = "GetTransactionSummaryByValueDateResponse")
public class GetTransactionSummaryByValueDateResponse {

    @XmlElementRef(name = "GetTransactionSummaryByValueDateResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountSummaryAndTrxResponse> getTransactionSummaryByValueDateResult;

    /**
     * Gets the value of the getTransactionSummaryByValueDateResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}
     *     
     */
    public JAXBElement<AccountSummaryAndTrxResponse> getGetTransactionSummaryByValueDateResult() {
        return getTransactionSummaryByValueDateResult;
    }

    /**
     * Sets the value of the getTransactionSummaryByValueDateResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}
     *     
     */
    public void setGetTransactionSummaryByValueDateResult(JAXBElement<AccountSummaryAndTrxResponse> value) {
        this.getTransactionSummaryByValueDateResult = value;
    }

}
