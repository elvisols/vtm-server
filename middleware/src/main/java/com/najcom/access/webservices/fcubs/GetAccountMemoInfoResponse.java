
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.GetAcctMemoInfoResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccountMemoInfoResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}GetAcctMemoInfoResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountMemoInfoResult"
})
@XmlRootElement(name = "GetAccountMemoInfoResponse")
public class GetAccountMemoInfoResponse {

    @XmlElementRef(name = "GetAccountMemoInfoResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<GetAcctMemoInfoResponse> getAccountMemoInfoResult;

    /**
     * Gets the value of the getAccountMemoInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetAcctMemoInfoResponse }{@code >}
     *     
     */
    public JAXBElement<GetAcctMemoInfoResponse> getGetAccountMemoInfoResult() {
        return getAccountMemoInfoResult;
    }

    /**
     * Sets the value of the getAccountMemoInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetAcctMemoInfoResponse }{@code >}
     *     
     */
    public void setGetAccountMemoInfoResult(JAXBElement<GetAcctMemoInfoResponse> value) {
        this.getAccountMemoInfoResult = value;
    }

}
