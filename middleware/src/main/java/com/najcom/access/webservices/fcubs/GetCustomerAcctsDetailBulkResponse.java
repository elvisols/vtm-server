
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfCustomerAcctsDetailResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerAcctsDetailBulkResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfCustomerAcctsDetailResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerAcctsDetailBulkResult"
})
@XmlRootElement(name = "GetCustomerAcctsDetailBulkResponse")
public class GetCustomerAcctsDetailBulkResponse {

    @XmlElementRef(name = "GetCustomerAcctsDetailBulkResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomerAcctsDetailResponse> getCustomerAcctsDetailBulkResult;

    /**
     * Gets the value of the getCustomerAcctsDetailBulkResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetailResponse }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomerAcctsDetailResponse> getGetCustomerAcctsDetailBulkResult() {
        return getCustomerAcctsDetailBulkResult;
    }

    /**
     * Sets the value of the getCustomerAcctsDetailBulkResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetailResponse }{@code >}
     *     
     */
    public void setGetCustomerAcctsDetailBulkResult(JAXBElement<ArrayOfCustomerAcctsDetailResponse> value) {
        this.getCustomerAcctsDetailBulkResult = value;
    }

}
