
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateSIContractResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCreateSIContractResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateSIContractResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCreateSIContractResult"
})
@XmlRootElement(name = "FCUBSCreateSIContractResponse")
public class FCUBSCreateSIContractResponse {

    @XmlElementRef(name = "FCUBSCreateSIContractResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateSIContractResponse> fcubsCreateSIContractResult;

    /**
     * Gets the value of the fcubsCreateSIContractResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateSIContractResponse }{@code >}
     *     
     */
    public JAXBElement<CreateSIContractResponse> getFCUBSCreateSIContractResult() {
        return fcubsCreateSIContractResult;
    }

    /**
     * Sets the value of the fcubsCreateSIContractResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateSIContractResponse }{@code >}
     *     
     */
    public void setFCUBSCreateSIContractResult(JAXBElement<CreateSIContractResponse> value) {
        this.fcubsCreateSIContractResult = value;
    }

}
