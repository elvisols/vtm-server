
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.GetAccountBranchResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getAccountBranchResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}GetAccountBranchResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountBranchResult"
})
@XmlRootElement(name = "getAccountBranchResponse")
public class GetAccountBranchResponse {

    @XmlElementRef(name = "getAccountBranchResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<GetAccountBranchResp> getAccountBranchResult;

    /**
     * Gets the value of the getAccountBranchResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetAccountBranchResp }{@code >}
     *     
     */
    public JAXBElement<GetAccountBranchResp> getGetAccountBranchResult() {
        return getAccountBranchResult;
    }

    /**
     * Sets the value of the getAccountBranchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetAccountBranchResp }{@code >}
     *     
     */
    public void setGetAccountBranchResult(JAXBElement<GetAccountBranchResp> value) {
        this.getAccountBranchResult = value;
    }

}
