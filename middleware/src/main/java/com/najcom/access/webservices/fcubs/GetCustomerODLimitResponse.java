
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CustomerODLimitResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerODLimitResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CustomerODLimitResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerODLimitResult"
})
@XmlRootElement(name = "GetCustomerODLimitResponse")
public class GetCustomerODLimitResponse {

    @XmlElementRef(name = "GetCustomerODLimitResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerODLimitResponse> getCustomerODLimitResult;

    /**
     * Gets the value of the getCustomerODLimitResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerODLimitResponse }{@code >}
     *     
     */
    public JAXBElement<CustomerODLimitResponse> getGetCustomerODLimitResult() {
        return getCustomerODLimitResult;
    }

    /**
     * Sets the value of the getCustomerODLimitResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerODLimitResponse }{@code >}
     *     
     */
    public void setGetCustomerODLimitResult(JAXBElement<CustomerODLimitResponse> value) {
        this.getCustomerODLimitResult = value;
    }

}
