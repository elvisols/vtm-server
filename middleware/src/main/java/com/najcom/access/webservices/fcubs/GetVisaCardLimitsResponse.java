
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.VisaCardLimitResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetVisaCardLimitsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}VisaCardLimitResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getVisaCardLimitsResult"
})
@XmlRootElement(name = "GetVisaCardLimitsResponse")
public class GetVisaCardLimitsResponse {

    @XmlElementRef(name = "GetVisaCardLimitsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<VisaCardLimitResponse> getVisaCardLimitsResult;

    /**
     * Gets the value of the getVisaCardLimitsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VisaCardLimitResponse }{@code >}
     *     
     */
    public JAXBElement<VisaCardLimitResponse> getGetVisaCardLimitsResult() {
        return getVisaCardLimitsResult;
    }

    /**
     * Sets the value of the getVisaCardLimitsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VisaCardLimitResponse }{@code >}
     *     
     */
    public void setGetVisaCardLimitsResult(JAXBElement<VisaCardLimitResponse> value) {
        this.getVisaCardLimitsResult = value;
    }

}
