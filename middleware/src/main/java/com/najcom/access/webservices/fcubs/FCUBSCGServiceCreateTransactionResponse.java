
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.FcubsResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCGServiceCreateTransactionResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FcubsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubscgServiceCreateTransactionResult"
})
@XmlRootElement(name = "FCUBSCGServiceCreateTransactionResponse")
public class FCUBSCGServiceCreateTransactionResponse {

    @XmlElementRef(name = "FCUBSCGServiceCreateTransactionResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<FcubsResponse> fcubscgServiceCreateTransactionResult;

    /**
     * Gets the value of the fcubscgServiceCreateTransactionResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}
     *     
     */
    public JAXBElement<FcubsResponse> getFCUBSCGServiceCreateTransactionResult() {
        return fcubscgServiceCreateTransactionResult;
    }

    /**
     * Sets the value of the fcubscgServiceCreateTransactionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}
     *     
     */
    public void setFCUBSCGServiceCreateTransactionResult(JAXBElement<FcubsResponse> value) {
        this.fcubscgServiceCreateTransactionResult = value;
    }

}
