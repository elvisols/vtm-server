
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CustomerInformationResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerInformationResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CustomerInformationResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerInformationResult"
})
@XmlRootElement(name = "GetCustomerInformationResponse")
public class GetCustomerInformationResponse {

    @XmlElementRef(name = "GetCustomerInformationResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerInformationResponse> getCustomerInformationResult;

    /**
     * Gets the value of the getCustomerInformationResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerInformationResponse }{@code >}
     *     
     */
    public JAXBElement<CustomerInformationResponse> getGetCustomerInformationResult() {
        return getCustomerInformationResult;
    }

    /**
     * Sets the value of the getCustomerInformationResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerInformationResponse }{@code >}
     *     
     */
    public void setGetCustomerInformationResult(JAXBElement<CustomerInformationResponse> value) {
        this.getCustomerInformationResult = value;
    }

}
