
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateLDContractResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCreateLDContractResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateLDContractResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCreateLDContractResult"
})
@XmlRootElement(name = "FCUBSCreateLDContractResponse")
public class FCUBSCreateLDContractResponse {

    @XmlElementRef(name = "FCUBSCreateLDContractResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateLDContractResponse> fcubsCreateLDContractResult;

    /**
     * Gets the value of the fcubsCreateLDContractResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateLDContractResponse }{@code >}
     *     
     */
    public JAXBElement<CreateLDContractResponse> getFCUBSCreateLDContractResult() {
        return fcubsCreateLDContractResult;
    }

    /**
     * Sets the value of the fcubsCreateLDContractResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateLDContractResponse }{@code >}
     *     
     */
    public void setFCUBSCreateLDContractResult(JAXBElement<CreateLDContractResponse> value) {
        this.fcubsCreateLDContractResult = value;
    }

}
