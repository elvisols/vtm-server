
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ValidateUserTillResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validateUserTillResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ValidateUserTillResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validateUserTillResult"
})
@XmlRootElement(name = "validateUserTillResponse")
public class ValidateUserTillResponse {

    @XmlElementRef(name = "validateUserTillResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ValidateUserTillResp> validateUserTillResult;

    /**
     * Gets the value of the validateUserTillResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ValidateUserTillResp }{@code >}
     *     
     */
    public JAXBElement<ValidateUserTillResp> getValidateUserTillResult() {
        return validateUserTillResult;
    }

    /**
     * Sets the value of the validateUserTillResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ValidateUserTillResp }{@code >}
     *     
     */
    public void setValidateUserTillResult(JAXBElement<ValidateUserTillResp> value) {
        this.validateUserTillResult = value;
    }

}
