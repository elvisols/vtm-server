
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfSweepAcctsDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SweepAcctsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfSweepAcctsDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sweepAcctsResult"
})
@XmlRootElement(name = "SweepAcctsResponse")
public class SweepAcctsResponse {

    @XmlElementRef(name = "SweepAcctsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfSweepAcctsDetails> sweepAcctsResult;

    /**
     * Gets the value of the sweepAcctsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSweepAcctsDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfSweepAcctsDetails> getSweepAcctsResult() {
        return sweepAcctsResult;
    }

    /**
     * Sets the value of the sweepAcctsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfSweepAcctsDetails }{@code >}
     *     
     */
    public void setSweepAcctsResult(JAXBElement<ArrayOfSweepAcctsDetails> value) {
        this.sweepAcctsResult = value;
    }

}
