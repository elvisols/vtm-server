
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateCustAccountResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCreateCustAccountResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateCustAccountResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCreateCustAccountResult"
})
@XmlRootElement(name = "FCUBSCreateCustAccountResponse")
public class FCUBSCreateCustAccountResponse {

    @XmlElementRef(name = "FCUBSCreateCustAccountResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateCustAccountResponse> fcubsCreateCustAccountResult;

    /**
     * Gets the value of the fcubsCreateCustAccountResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateCustAccountResponse }{@code >}
     *     
     */
    public JAXBElement<CreateCustAccountResponse> getFCUBSCreateCustAccountResult() {
        return fcubsCreateCustAccountResult;
    }

    /**
     * Sets the value of the fcubsCreateCustAccountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateCustAccountResponse }{@code >}
     *     
     */
    public void setFCUBSCreateCustAccountResult(JAXBElement<CreateCustAccountResponse> value) {
        this.fcubsCreateCustAccountResult = value;
    }

}
