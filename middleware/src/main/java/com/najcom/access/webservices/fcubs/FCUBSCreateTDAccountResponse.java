
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateTDAccountResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCreateTDAccountResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateTDAccountResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCreateTDAccountResult"
})
@XmlRootElement(name = "FCUBSCreateTDAccountResponse")
public class FCUBSCreateTDAccountResponse {

    @XmlElementRef(name = "FCUBSCreateTDAccountResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateTDAccountResponse> fcubsCreateTDAccountResult;

    /**
     * Gets the value of the fcubsCreateTDAccountResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateTDAccountResponse }{@code >}
     *     
     */
    public JAXBElement<CreateTDAccountResponse> getFCUBSCreateTDAccountResult() {
        return fcubsCreateTDAccountResult;
    }

    /**
     * Sets the value of the fcubsCreateTDAccountResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateTDAccountResponse }{@code >}
     *     
     */
    public void setFCUBSCreateTDAccountResult(JAXBElement<CreateTDAccountResponse> value) {
        this.fcubsCreateTDAccountResult = value;
    }

}
