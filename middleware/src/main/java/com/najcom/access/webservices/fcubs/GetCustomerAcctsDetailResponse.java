
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CustomerAcctsDetailResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerAcctsDetailResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CustomerAcctsDetailResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerAcctsDetailResult"
})
@XmlRootElement(name = "GetCustomerAcctsDetailResponse")
public class GetCustomerAcctsDetailResponse {

    @XmlElementRef(name = "GetCustomerAcctsDetailResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerAcctsDetailResponse> getCustomerAcctsDetailResult;

    /**
     * Gets the value of the getCustomerAcctsDetailResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerAcctsDetailResponse }{@code >}
     *     
     */
    public JAXBElement<CustomerAcctsDetailResponse> getGetCustomerAcctsDetailResult() {
        return getCustomerAcctsDetailResult;
    }

    /**
     * Sets the value of the getCustomerAcctsDetailResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerAcctsDetailResponse }{@code >}
     *     
     */
    public void setGetCustomerAcctsDetailResult(JAXBElement<CustomerAcctsDetailResponse> value) {
        this.getCustomerAcctsDetailResult = value;
    }

}
