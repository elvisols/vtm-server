
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.AccountTransactionResponseBulk;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccountSummaryAndTransactions2Result" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountTransactionResponseBulk" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountSummaryAndTransactions2Result"
})
@XmlRootElement(name = "GetAccountSummaryAndTransactions2Response")
public class GetAccountSummaryAndTransactions2Response {

    @XmlElementRef(name = "GetAccountSummaryAndTransactions2Result", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountTransactionResponseBulk> getAccountSummaryAndTransactions2Result;

    /**
     * Gets the value of the getAccountSummaryAndTransactions2Result property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountTransactionResponseBulk }{@code >}
     *     
     */
    public JAXBElement<AccountTransactionResponseBulk> getGetAccountSummaryAndTransactions2Result() {
        return getAccountSummaryAndTransactions2Result;
    }

    /**
     * Sets the value of the getAccountSummaryAndTransactions2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountTransactionResponseBulk }{@code >}
     *     
     */
    public void setGetAccountSummaryAndTransactions2Result(JAXBElement<AccountTransactionResponseBulk> value) {
        this.getAccountSummaryAndTransactions2Result = value;
    }

}
