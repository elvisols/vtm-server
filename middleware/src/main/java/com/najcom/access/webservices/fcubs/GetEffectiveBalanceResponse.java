
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.GetEffectiveBalanceResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getEffectiveBalanceResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}GetEffectiveBalanceResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getEffectiveBalanceResult"
})
@XmlRootElement(name = "getEffectiveBalanceResponse")
public class GetEffectiveBalanceResponse {

    @XmlElementRef(name = "getEffectiveBalanceResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<GetEffectiveBalanceResp> getEffectiveBalanceResult;

    /**
     * Gets the value of the getEffectiveBalanceResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetEffectiveBalanceResp }{@code >}
     *     
     */
    public JAXBElement<GetEffectiveBalanceResp> getGetEffectiveBalanceResult() {
        return getEffectiveBalanceResult;
    }

    /**
     * Sets the value of the getEffectiveBalanceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetEffectiveBalanceResp }{@code >}
     *     
     */
    public void setGetEffectiveBalanceResult(JAXBElement<GetEffectiveBalanceResp> value) {
        this.getEffectiveBalanceResult = value;
    }

}
