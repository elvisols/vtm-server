
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfNubanAcctsDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NubanAcctsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfNubanAcctsDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nubanAcctsResult"
})
@XmlRootElement(name = "NubanAcctsResponse")
public class NubanAcctsResponse {

    @XmlElementRef(name = "NubanAcctsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfNubanAcctsDetails> nubanAcctsResult;

    /**
     * Gets the value of the nubanAcctsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfNubanAcctsDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfNubanAcctsDetails> getNubanAcctsResult() {
        return nubanAcctsResult;
    }

    /**
     * Sets the value of the nubanAcctsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfNubanAcctsDetails }{@code >}
     *     
     */
    public void setNubanAcctsResult(JAXBElement<ArrayOfNubanAcctsDetails> value) {
        this.nubanAcctsResult = value;
    }

}
