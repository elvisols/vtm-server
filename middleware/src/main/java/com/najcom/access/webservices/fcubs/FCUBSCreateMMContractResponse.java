
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.CreateMMContractResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FCUBSCreateMMContractResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CreateMMContractResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fcubsCreateMMContractResult"
})
@XmlRootElement(name = "FCUBSCreateMMContractResponse")
public class FCUBSCreateMMContractResponse {

    @XmlElementRef(name = "FCUBSCreateMMContractResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<CreateMMContractResponse> fcubsCreateMMContractResult;

    /**
     * Gets the value of the fcubsCreateMMContractResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateMMContractResponse }{@code >}
     *     
     */
    public JAXBElement<CreateMMContractResponse> getFCUBSCreateMMContractResult() {
        return fcubsCreateMMContractResult;
    }

    /**
     * Sets the value of the fcubsCreateMMContractResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateMMContractResponse }{@code >}
     *     
     */
    public void setFCUBSCreateMMContractResult(JAXBElement<CreateMMContractResponse> value) {
        this.fcubsCreateMMContractResult = value;
    }

}
