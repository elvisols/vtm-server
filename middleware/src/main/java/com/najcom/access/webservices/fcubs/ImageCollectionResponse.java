
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfImageCollectionDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImageCollectionResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfImageCollectionDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "imageCollectionResult"
})
@XmlRootElement(name = "ImageCollectionResponse")
public class ImageCollectionResponse {

    @XmlElementRef(name = "ImageCollectionResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfImageCollectionDetails> imageCollectionResult;

    /**
     * Gets the value of the imageCollectionResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfImageCollectionDetails> getImageCollectionResult() {
        return imageCollectionResult;
    }

    /**
     * Sets the value of the imageCollectionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}
     *     
     */
    public void setImageCollectionResult(JAXBElement<ArrayOfImageCollectionDetails> value) {
        this.imageCollectionResult = value;
    }

}
