
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.SampleAccountResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSampleResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}SampleAccountResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSampleResult"
})
@XmlRootElement(name = "GetSampleResponse")
public class GetSampleResponse {

    @XmlElementRef(name = "GetSampleResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<SampleAccountResponse> getSampleResult;

    /**
     * Gets the value of the getSampleResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SampleAccountResponse }{@code >}
     *     
     */
    public JAXBElement<SampleAccountResponse> getGetSampleResult() {
        return getSampleResult;
    }

    /**
     * Sets the value of the getSampleResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SampleAccountResponse }{@code >}
     *     
     */
    public void setGetSampleResult(JAXBElement<SampleAccountResponse> value) {
        this.getSampleResult = value;
    }

}
