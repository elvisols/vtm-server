
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import com.najcom.access.datacontract.AccountCotAndVatResponse;
import com.najcom.access.datacontract.AccountRelatedResponse;
import com.najcom.access.datacontract.AccountSearchResponse;
import com.najcom.access.datacontract.AccountSummaryAndTrxResponse;
import com.najcom.access.datacontract.AccountSummaryResponse;
import com.najcom.access.datacontract.AccountTransactionResponseBulk;
import com.najcom.access.datacontract.ArrayOfCustomerAcctsDetailResponse;
import com.najcom.access.datacontract.ArrayOfImageCollectionDetails;
import com.najcom.access.datacontract.ArrayOfNubanAcctsDetails;
import com.najcom.access.datacontract.ArrayOfSingleTransactionDetails;
import com.najcom.access.datacontract.ArrayOfSweepAcctsDetails;
import com.najcom.access.datacontract.AuthenticateCBAUserResp;
import com.najcom.access.datacontract.AuthorizeWithdrawalRequest;
import com.najcom.access.datacontract.ChequeDetailsResponse;
import com.najcom.access.datacontract.CreateAmtBlockResponse;
import com.najcom.access.datacontract.CreateCustAccountResponse;
import com.najcom.access.datacontract.CreateCustomerResponse;
import com.najcom.access.datacontract.CreateLDContractResponse;
import com.najcom.access.datacontract.CreateMMContractResponse;
import com.najcom.access.datacontract.CreateRTResponse;
import com.najcom.access.datacontract.CreateSIContractResponse;
import com.najcom.access.datacontract.CreateStopPaymentResponse;
import com.najcom.access.datacontract.CreateTDAccountResponse;
import com.najcom.access.datacontract.CurrencyRateResponse;
import com.najcom.access.datacontract.CustomerAcctsDetailResponse;
import com.najcom.access.datacontract.CustomerDetails;
import com.najcom.access.datacontract.CustomerInformationResponse;
import com.najcom.access.datacontract.CustomerODLimitResponse;
import com.najcom.access.datacontract.FCUBSCGServiceCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSCloseCustAccRequest;
import com.najcom.access.datacontract.FCUBSCreateCustAccountRequest;
import com.najcom.access.datacontract.FCUBSCreateCustomerRequest;
import com.najcom.access.datacontract.FCUBSCreateLDContractRequest;
import com.najcom.access.datacontract.FCUBSCreateMMContractRequest;
import com.najcom.access.datacontract.FCUBSCreateSIContractRequest;
import com.najcom.access.datacontract.FCUBSCreateStopPaymentRequest;
import com.najcom.access.datacontract.FCUBSCreateTDAccountRequest;
import com.najcom.access.datacontract.FCUBSCustomerServiceAmtBlkRequest;
import com.najcom.access.datacontract.FCUBSFTCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSRTCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSRTReverseTransactionRequest;
import com.najcom.access.datacontract.FcubsResponse;
import com.najcom.access.datacontract.FixedDepositResponse;
import com.najcom.access.datacontract.GetAccountBranchResp;
import com.najcom.access.datacontract.GetAccountExistResponse;
import com.najcom.access.datacontract.GetAcctMemoInfoResponse;
import com.najcom.access.datacontract.GetChequeValueDateResp;
import com.najcom.access.datacontract.GetCurrencyCodeResp;
import com.najcom.access.datacontract.GetEffectiveBalanceResp;
import com.najcom.access.datacontract.GetUserBranchResp;
import com.najcom.access.datacontract.LoanInfoSummaryResponse;
import com.najcom.access.datacontract.LoanSummaryResponse;
import com.najcom.access.datacontract.LoanSummaryResponseBulk;
import com.najcom.access.datacontract.MultipleJournalMasterTrx;
import com.najcom.access.datacontract.RepaymentScheduleResponse;
import com.najcom.access.datacontract.SampleAccountResponse;
import com.najcom.access.datacontract.ScreenLimitResponse;
import com.najcom.access.datacontract.SingleJournalTrx;
import com.najcom.access.datacontract.TellerLimitResponse;
import com.najcom.access.datacontract.UserLimitResponse;
import com.najcom.access.datacontract.ValidateUserTillResp;
import com.najcom.access.datacontract.VisaCardLimitResponse;
import com.najcom.access.datacontract.webservicewrappers.AccountVerificationResponse;
import com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileAccountDetail;
import com.najcom.access.datacontract.webservicewrappers.ArrayOfMobileTransactionDetail;
import com.najcom.access.datacontract.webservicewrappers.ChequeNoInquiryResponse;
import com.najcom.access.datacontract.webservicewrappers.MessageIDStatusResponse;
import com.najcom.access.datacontract.webservicewrappers.TransactionRefNoDetailsResponse;
import com.najcom.access.datacontract.webservicewrappers.VansoCustomerIdResponse;
import com.najcom.access.microsoft.arrays.ArrayOfstring;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.najcom.access.webservices.fcubs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FCUBSCreateSIContractAction_QNAME = new QName("http://FCUBS.Webservices", "action");
    private final static QName _FCUBSCreateSIContractData_QNAME = new QName("http://FCUBS.Webservices", "data");
    private final static QName _GetLoanSummaryByCustomerIDResponseGetLoanSummaryByCustomerIDResult_QNAME = new QName("http://FCUBS.Webservices", "GetLoanSummaryByCustomerIDResult");
    private final static QName _ImageCollectionResponseImageCollectionResult_QNAME = new QName("http://FCUBS.Webservices", "ImageCollectionResult");
    private final static QName _FetchVansoCustomerIdUserId_QNAME = new QName("http://FCUBS.Webservices", "UserId");
    private final static QName _CheckifCustomerExistResponseCheckifCustomerExistResult_QNAME = new QName("http://FCUBS.Webservices", "CheckifCustomerExistResult");
    private final static QName _GetAccountSummaryAndTransactionsEndDate_QNAME = new QName("http://FCUBS.Webservices", "EndDate");
    private final static QName _GetAccountSummaryAndTransactionsAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "AccountNumber");
    private final static QName _GetAccountSummaryAndTransactionsStartDate_QNAME = new QName("http://FCUBS.Webservices", "StartDate");
    private final static QName _GetAccountMemoInfoAccountNo_QNAME = new QName("http://FCUBS.Webservices", "AccountNo");
    private final static QName _ActivateChequeBookResponseActivateChequeBookResult_QNAME = new QName("http://FCUBS.Webservices", "ActivateChequeBookResult");
    private final static QName _GetAccountSummaryAndTransactions2ResponseGetAccountSummaryAndTransactions2Result_QNAME = new QName("http://FCUBS.Webservices", "GetAccountSummaryAndTransactions2Result");
    private final static QName _GetVisaCardLimitsResponseGetVisaCardLimitsResult_QNAME = new QName("http://FCUBS.Webservices", "GetVisaCardLimitsResult");
    private final static QName _GetAccountSummaryResponseGetAccountSummaryResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountSummaryResult");
    private final static QName _PerformAccountVerificationResponsePerformAccountVerificationResult_QNAME = new QName("http://FCUBS.Webservices", "PerformAccountVerificationResult");
    private final static QName _GetChequeValueDateResponseGetChequeValueDateResult_QNAME = new QName("http://FCUBS.Webservices", "getChequeValueDateResult");
    private final static QName _GetLoanInfoByAccountNumberLoanAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "loanAccountNumber");
    private final static QName _GetLoanInfoByAccountNumberReviewDate_QNAME = new QName("http://FCUBS.Webservices", "reviewDate");
    private final static QName _SingleDebitSingleCreditDebitAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "DebitAccountNumber");
    private final static QName _SingleDebitSingleCreditCreditAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "CreditAccountNumber");
    private final static QName _SingleDebitSingleCreditValueDate_QNAME = new QName("http://FCUBS.Webservices", "ValueDate");
    private final static QName _SingleDebitSingleCreditDebitBranchCode_QNAME = new QName("http://FCUBS.Webservices", "DebitBranchCode");
    private final static QName _SingleDebitSingleCreditCurrencyNumber_QNAME = new QName("http://FCUBS.Webservices", "CurrencyNumber");
    private final static QName _SingleDebitSingleCreditBranchCode_QNAME = new QName("http://FCUBS.Webservices", "BranchCode");
    private final static QName _SingleDebitSingleCreditCreditCustomerId_QNAME = new QName("http://FCUBS.Webservices", "CreditCustomerId");
    private final static QName _SingleDebitSingleCreditCurrencyCode_QNAME = new QName("http://FCUBS.Webservices", "CurrencyCode");
    private final static QName _SingleDebitSingleCreditCreditBranchCode_QNAME = new QName("http://FCUBS.Webservices", "CreditBranchCode");
    private final static QName _SingleDebitSingleCreditTxnCode_QNAME = new QName("http://FCUBS.Webservices", "TxnCode");
    private final static QName _SingleDebitSingleCreditSource_QNAME = new QName("http://FCUBS.Webservices", "Source");
    private final static QName _SingleDebitSingleCreditMsgId_QNAME = new QName("http://FCUBS.Webservices", "MsgId");
    private final static QName _SingleDebitSingleCreditPaymentReference_QNAME = new QName("http://FCUBS.Webservices", "PaymentReference");
    private final static QName _SingleDebitSingleCreditDebitCustomerId_QNAME = new QName("http://FCUBS.Webservices", "DebitCustomerId");
    private final static QName _SingleDebitSingleCreditNarration_QNAME = new QName("http://FCUBS.Webservices", "Narration");
    private final static QName _SingleDebitSingleCreditBatchNumber_QNAME = new QName("http://FCUBS.Webservices", "BatchNumber");
    private final static QName _SingleDebitSingleCreditRequestModule_QNAME = new QName("http://FCUBS.Webservices", "RequestModule");
    private final static QName _GetCurrencyRateResponseGetCurrencyRateResult_QNAME = new QName("http://FCUBS.Webservices", "GetCurrencyRateResult");
    private final static QName _GetLoanInfoByAccountNumberResponseGetLoanInfoByAccountNumberResult_QNAME = new QName("http://FCUBS.Webservices", "GetLoanInfoByAccountNumberResult");
    private final static QName _FCUBSCreateCustAccountResponseFCUBSCreateCustAccountResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateCustAccountResult");
    private final static QName _GetAccountSummaryByCustomerIDCustomerId_QNAME = new QName("http://FCUBS.Webservices", "CustomerId");
    private final static QName _GetTellerLimitResponseGetTellerLimitResult_QNAME = new QName("http://FCUBS.Webservices", "GetTellerLimitResult");
    private final static QName _GetAccountSearchByAccountTitleResponseGetAccountSearchByAccountTitleResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountSearchByAccountTitleResult");
    private final static QName _FCUBSCustomerServiceCloseAmtBlkResponseFCUBSCustomerServiceCloseAmtBlkResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCustomerServiceCloseAmtBlkResult");
    private final static QName _PerformAccountVerificationAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "accountNumber");
    private final static QName _GetCustomerAcctsDetailResponseGetCustomerAcctsDetailResult_QNAME = new QName("http://FCUBS.Webservices", "GetCustomerAcctsDetailResult");
    private final static QName _GetCustomerAcctsDetailBulkResponseGetCustomerAcctsDetailBulkResult_QNAME = new QName("http://FCUBS.Webservices", "GetCustomerAcctsDetailBulkResult");
    private final static QName _CheckifAccountClassExistResponseCheckifAccountClassExistResult_QNAME = new QName("http://FCUBS.Webservices", "CheckifAccountClassExistResult");
    private final static QName _NubanAcctsOldAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "OldAccountNumber");
    private final static QName _GetTellerLimitLimitsCcy_QNAME = new QName("http://FCUBS.Webservices", "limits_ccy");
    private final static QName _GetTellerLimitUserId_QNAME = new QName("http://FCUBS.Webservices", "user_id");
    private final static QName _FCUBSCGServiceCreateTransactionResponseFCUBSCGServiceCreateTransactionResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCGServiceCreateTransactionResult");
    private final static QName _CheckifAccountClassExistCustomerNo_QNAME = new QName("http://FCUBS.Webservices", "CustomerNo");
    private final static QName _CheckifAccountClassExistAccountClass_QNAME = new QName("http://FCUBS.Webservices", "AccountClass");
    private final static QName _GetCustomerInformationResponseGetCustomerInformationResult_QNAME = new QName("http://FCUBS.Webservices", "GetCustomerInformationResult");
    private final static QName _GetLoanDetailByAccountNumber2LoanAccountNumberRequest_QNAME = new QName("http://FCUBS.Webservices", "LoanAccountNumberRequest");
    private final static QName _GetAccountSummaryByCustomerIDResponseGetAccountSummaryByCustomerIDResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountSummaryByCustomerIDResult");
    private final static QName _GetNoTransactionSummaryResponseGetNoTransactionSummaryResult_QNAME = new QName("http://FCUBS.Webservices", "GetNoTransactionSummaryResult");
    private final static QName _GetLoanStatementResponseGetLoanStatementResult_QNAME = new QName("http://FCUBS.Webservices", "GetLoanStatementResult");
    private final static QName _GetRelatedAccountsResponseGetRelatedAccountsResult_QNAME = new QName("http://FCUBS.Webservices", "GetRelatedAccountsResult");
    private final static QName _GetLoanStatement2ResponseGetLoanStatement2Result_QNAME = new QName("http://FCUBS.Webservices", "GetLoanStatement2Result");
    private final static QName _ChequeNumberInquiryChequeNumber_QNAME = new QName("http://FCUBS.Webservices", "ChequeNumber");
    private final static QName _GetUserLimitBranchCode_QNAME = new QName("http://FCUBS.Webservices", "branch_code");
    private final static QName _GetTransactionRefNoDetailsResponseGetTransactionRefNoDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "GetTransactionRefNoDetailsResult");
    private final static QName _FetchMobileAccountDetailsResponseFetchMobileAccountDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "FetchMobileAccountDetailsResult");
    private final static QName _GetLoanStatementLoanAccountNumber_QNAME = new QName("http://FCUBS.Webservices", "LoanAccountNumber");
    private final static QName _CheckIfAccountExistResponseCheckIfAccountExistResult_QNAME = new QName("http://FCUBS.Webservices", "CheckIfAccountExistResult");
    private final static QName _GetTransactionSummaryByValueDateResponseGetTransactionSummaryByValueDateResult_QNAME = new QName("http://FCUBS.Webservices", "GetTransactionSummaryByValueDateResult");
    private final static QName _SweepAcctsResponseSweepAcctsResult_QNAME = new QName("http://FCUBS.Webservices", "SweepAcctsResult");
    private final static QName _GetEffectiveBalanceCustAcNo_QNAME = new QName("http://FCUBS.Webservices", "cust_ac_no");
    private final static QName _GetFixedDepositInfoResponseGetFixedDepositInfoResult_QNAME = new QName("http://FCUBS.Webservices", "GetFixedDepositInfoResult");
    private final static QName _GetUserLimitResponseGetUserLimitResult_QNAME = new QName("http://FCUBS.Webservices", "GetUserLimitResult");
    private final static QName _GetScreenLimitResponseGetScreenLimitResult_QNAME = new QName("http://FCUBS.Webservices", "GetScreenLimitResult");
    private final static QName _GetFixedDepositInfoCustomerNo_QNAME = new QName("http://FCUBS.Webservices", "customerNo");
    private final static QName _NubanAcctsResponseNubanAcctsResult_QNAME = new QName("http://FCUBS.Webservices", "NubanAcctsResult");
    private final static QName _SweepAcctsCustID_QNAME = new QName("http://FCUBS.Webservices", "CustID");
    private final static QName _GetAccountMemoInfoResponseGetAccountMemoInfoResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountMemoInfoResult");
    private final static QName _GetAccountSummaryAndTransactions2AccountNumberList_QNAME = new QName("http://FCUBS.Webservices", "AccountNumberList");
    private final static QName _FCUBSCreateStopPaymentResponseFCUBSCreateStopPaymentResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateStopPaymentResult");
    private final static QName _GetUserBranchResponseGetUserBranchResult_QNAME = new QName("http://FCUBS.Webservices", "getUserBranchResult");
    private final static QName _GetCustomerDetailsResponseGetCustomerDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "GetCustomerDetailsResult");
    private final static QName _GetSingleTransactionDetailsResponseGetSingleTransactionDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "GetSingleTransactionDetailsResult");
    private final static QName _GetCurrencyCodeResponseGetCurrencyCodeResult_QNAME = new QName("http://FCUBS.Webservices", "getCurrencyCodeResult");
    private final static QName _GetAccountBranchResponseGetAccountBranchResult_QNAME = new QName("http://FCUBS.Webservices", "getAccountBranchResult");
    private final static QName _FetchVansoCustomerIdResponseFetchVansoCustomerIdResult_QNAME = new QName("http://FCUBS.Webservices", "FetchVansoCustomerIdResult");
    private final static QName _FetchChequeDetailsResponseFetchChequeDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "FetchChequeDetailsResult");
    private final static QName _SingleJournalEntryResponseSingleJournalEntryResult_QNAME = new QName("http://FCUBS.Webservices", "SingleJournalEntryResult");
    private final static QName _SingleDebitSingleCreditResponseSingleDebitSingleCreditResult_QNAME = new QName("http://FCUBS.Webservices", "SingleDebitSingleCreditResult");
    private final static QName _GetChequeValueDateTxnDate_QNAME = new QName("http://FCUBS.Webservices", "txn_date");
    private final static QName _GetChequeValueDateRoutingNo_QNAME = new QName("http://FCUBS.Webservices", "routing_no");
    private final static QName _GetCurrencyCodeIsoNumCcyCode_QNAME = new QName("http://FCUBS.Webservices", "iso_num_ccy_code");
    private final static QName _GetAccountSearchByAccountNumberResponseGetAccountSearchByAccountNumberResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountSearchByAccountNumberResult");
    private final static QName _SingleDebitSingleCreditFXResponseSingleDebitSingleCreditFXResult_QNAME = new QName("http://FCUBS.Webservices", "SingleDebitSingleCreditFXResult");
    private final static QName _GetLoanStatement2LoanAccountNumbersRequest_QNAME = new QName("http://FCUBS.Webservices", "LoanAccountNumbersRequest");
    private final static QName _ImageCollectionCustomerID_QNAME = new QName("http://FCUBS.Webservices", "CustomerID");
    private final static QName _GetScreenLimitCcyCode_QNAME = new QName("http://FCUBS.Webservices", "ccy_code");
    private final static QName _GetScreenLimitFunctionNo_QNAME = new QName("http://FCUBS.Webservices", "function_no");
    private final static QName _FCUBSCreateSIContractResponseFCUBSCreateSIContractResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateSIContractResult");
    private final static QName _GetSingleTransactionDetailsTransDate_QNAME = new QName("http://FCUBS.Webservices", "transDate");
    private final static QName _GetSingleTransactionDetailsRowID_QNAME = new QName("http://FCUBS.Webservices", "RowID");
    private final static QName _GetAccountBranchAcGlNo_QNAME = new QName("http://FCUBS.Webservices", "ac_gl_no");
    private final static QName _GetLoanDetailByAccountNumber2ResponseGetLoanDetailByAccountNumber2Result_QNAME = new QName("http://FCUBS.Webservices", "GetLoanDetailByAccountNumber2Result");
    private final static QName _FCUBSRTReverseTransactionResponseFCUBSRTReverseTransactionResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSRTReverseTransactionResult");
    private final static QName _FCUBSCreateLDContractResponseFCUBSCreateLDContractResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateLDContractResult");
    private final static QName _FCUBSRTAuthorizeWithdrawalResponseFCUBSRTAuthorizeWithdrawalResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSRTAuthorizeWithdrawalResult");
    private final static QName _GetCustomerAcctsDetailBulkAccountNumberList_QNAME = new QName("http://FCUBS.Webservices", "accountNumberList");
    private final static QName _GetAccountSummaryAndTransactionsResponseGetAccountSummaryAndTransactionsResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountSummaryAndTransactionsResult");
    private final static QName _GetMessageIDStatusMsgId_QNAME = new QName("http://FCUBS.Webservices", "msgId");
    private final static QName _GetMessageIDStatusExtSystem_QNAME = new QName("http://FCUBS.Webservices", "extSystem");
    private final static QName _GetLoanDetailByAccountNumberResponseGetLoanDetailByAccountNumberResult_QNAME = new QName("http://FCUBS.Webservices", "GetLoanDetailByAccountNumberResult");
    private final static QName _AuthenticateCBAUserResponseAuthenticateCBAUserResult_QNAME = new QName("http://FCUBS.Webservices", "AuthenticateCBAUserResult");
    private final static QName _FCUBSFTCreateTransactionResponseFCUBSFTCreateTransactionResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSFTCreateTransactionResult");
    private final static QName _CheckifCustomerExistDateofBirth_QNAME = new QName("http://FCUBS.Webservices", "DateofBirth");
    private final static QName _CheckifCustomerExistTelephoneNo_QNAME = new QName("http://FCUBS.Webservices", "TelephoneNo");
    private final static QName _CheckifCustomerExistMobileNo_QNAME = new QName("http://FCUBS.Webservices", "MobileNo");
    private final static QName _CheckifCustomerExistEmail_QNAME = new QName("http://FCUBS.Webservices", "Email");
    private final static QName _ValidateUserTillTilVltInd_QNAME = new QName("http://FCUBS.Webservices", "Til_Vlt_Ind");
    private final static QName _ValidateUserTillBranchCode_QNAME = new QName("http://FCUBS.Webservices", "branchCode");
    private final static QName _GetCotVatDetailsResponseGetCotVatDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "GetCotVatDetailsResult");
    private final static QName _ValidateUserTillResponseValidateUserTillResult_QNAME = new QName("http://FCUBS.Webservices", "validateUserTillResult");
    private final static QName _GetCustomerODLimitResponseGetCustomerODLimitResult_QNAME = new QName("http://FCUBS.Webservices", "GetCustomerODLimitResult");
    private final static QName _GetAccountSearchByAccountTitleAccountTitle_QNAME = new QName("http://FCUBS.Webservices", "AccountTitle");
    private final static QName _FCUBSRTCreateTransactionResponseFCUBSRTCreateTransactionResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSRTCreateTransactionResult");
    private final static QName _FCUBSCreateTDAccountResponseFCUBSCreateTDAccountResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateTDAccountResult");
    private final static QName _FCUBSCustomerServiceCreateAmtBlkResponseFCUBSCustomerServiceCreateAmtBlkResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCustomerServiceCreateAmtBlkResult");
    private final static QName _SignatureImageCollectionResponseSignatureImageCollectionResult_QNAME = new QName("http://FCUBS.Webservices", "SignatureImageCollectionResult");
    private final static QName _GetEffectiveBalanceResponseGetEffectiveBalanceResult_QNAME = new QName("http://FCUBS.Webservices", "getEffectiveBalanceResult");
    private final static QName _GetCustomerInfoForIsoResponseGetCustomerInfoForIsoResult_QNAME = new QName("http://FCUBS.Webservices", "GetCustomerInfoForIsoResult");
    private final static QName _FCUBSCreateMMContractResponseFCUBSCreateMMContractResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateMMContractResult");
    private final static QName _GetAccountTransactionStatementResponseGetAccountTransactionStatementResult_QNAME = new QName("http://FCUBS.Webservices", "GetAccountTransactionStatementResult");
    private final static QName _FCUBSCloseCustAccResponseFCUBSCloseCustAccResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCloseCustAccResult");
    private final static QName _FCUBSCreateCustomerResponseFCUBSCreateCustomerResult_QNAME = new QName("http://FCUBS.Webservices", "FCUBSCreateCustomerResult");
    private final static QName _GetTransactionRefNoDetailsTrnRefNo_QNAME = new QName("http://FCUBS.Webservices", "trn_ref_no");
    private final static QName _GetMessageIDStatusResponseGetMessageIDStatusResult_QNAME = new QName("http://FCUBS.Webservices", "GetMessageIDStatusResult");
    private final static QName _ChequeNumberInquiryResponseChequeNumberInquiryResult_QNAME = new QName("http://FCUBS.Webservices", "ChequeNumberInquiryResult");
    private final static QName _MultiJournalEntryResponseMultiJournalEntryResult_QNAME = new QName("http://FCUBS.Webservices", "MultiJournalEntryResult");
    private final static QName _FetchMobileTransactionDetailsResponseFetchMobileTransactionDetailsResult_QNAME = new QName("http://FCUBS.Webservices", "FetchMobileTransactionDetailsResult");
    private final static QName _GetRepaymentScheduleResponseGetRepaymentScheduleResult_QNAME = new QName("http://FCUBS.Webservices", "GetRepaymentScheduleResult");
    private final static QName _ActivateChequeBookCheckerID_QNAME = new QName("http://FCUBS.Webservices", "CheckerID");
    private final static QName _ActivateChequeBookChequeBookNo_QNAME = new QName("http://FCUBS.Webservices", "ChequeBookNo");
    private final static QName _ActivateChequeBookMakerID_QNAME = new QName("http://FCUBS.Webservices", "MakerID");
    private final static QName _GetCurrencyRateCcyCode2_QNAME = new QName("http://FCUBS.Webservices", "CcyCode2");
    private final static QName _GetCurrencyRateCcyCode1_QNAME = new QName("http://FCUBS.Webservices", "CcyCode1");
    private final static QName _GetAccordGLResponseGetAccordGLResult_QNAME = new QName("http://FCUBS.Webservices", "getAccordGLResult");
    private final static QName _GetSampleResponseGetSampleResult_QNAME = new QName("http://FCUBS.Webservices", "GetSampleResult");
    private final static QName _AccountImageCollectionResponseAccountImageCollectionResult_QNAME = new QName("http://FCUBS.Webservices", "AccountImageCollectionResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.najcom.access.webservices.fcubs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSampleResponse }
     * 
     */
    public GetSampleResponse createGetSampleResponse() {
        return new GetSampleResponse();
    }

    /**
     * Create an instance of {@link ImageCollectionResponse }
     * 
     */
    public ImageCollectionResponse createImageCollectionResponse() {
        return new ImageCollectionResponse();
    }

    /**
     * Create an instance of {@link SingleDebitSingleCreditResponse }
     * 
     */
    public SingleDebitSingleCreditResponse createSingleDebitSingleCreditResponse() {
        return new SingleDebitSingleCreditResponse();
    }

    /**
     * Create an instance of {@link SingleJournalEntry }
     * 
     */
    public SingleJournalEntry createSingleJournalEntry() {
        return new SingleJournalEntry();
    }

    /**
     * Create an instance of {@link GetTransactionSummaryByValueDateResponse }
     * 
     */
    public GetTransactionSummaryByValueDateResponse createGetTransactionSummaryByValueDateResponse() {
        return new GetTransactionSummaryByValueDateResponse();
    }

    /**
     * Create an instance of {@link GetCustomerODLimitResponse }
     * 
     */
    public GetCustomerODLimitResponse createGetCustomerODLimitResponse() {
        return new GetCustomerODLimitResponse();
    }

    /**
     * Create an instance of {@link AuthenticateCBAUserResponse }
     * 
     */
    public AuthenticateCBAUserResponse createAuthenticateCBAUserResponse() {
        return new AuthenticateCBAUserResponse();
    }

    /**
     * Create an instance of {@link GetAccountBranch }
     * 
     */
    public GetAccountBranch createGetAccountBranch() {
        return new GetAccountBranch();
    }

    /**
     * Create an instance of {@link GetTellerLimitResponse }
     * 
     */
    public GetTellerLimitResponse createGetTellerLimitResponse() {
        return new GetTellerLimitResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummaryByCustomerIDResponse }
     * 
     */
    public GetAccountSummaryByCustomerIDResponse createGetAccountSummaryByCustomerIDResponse() {
        return new GetAccountSummaryByCustomerIDResponse();
    }

    /**
     * Create an instance of {@link CheckifCustomerExistResponse }
     * 
     */
    public CheckifCustomerExistResponse createCheckifCustomerExistResponse() {
        return new CheckifCustomerExistResponse();
    }

    /**
     * Create an instance of {@link GetCustomerDetails }
     * 
     */
    public GetCustomerDetails createGetCustomerDetails() {
        return new GetCustomerDetails();
    }

    /**
     * Create an instance of {@link GetLoanDetailByAccountNumber }
     * 
     */
    public GetLoanDetailByAccountNumber createGetLoanDetailByAccountNumber() {
        return new GetLoanDetailByAccountNumber();
    }

    /**
     * Create an instance of {@link FCUBSRTAuthorizeWithdrawalResponse }
     * 
     */
    public FCUBSRTAuthorizeWithdrawalResponse createFCUBSRTAuthorizeWithdrawalResponse() {
        return new FCUBSRTAuthorizeWithdrawalResponse();
    }

    /**
     * Create an instance of {@link CheckifAccountClassExistResponse }
     * 
     */
    public CheckifAccountClassExistResponse createCheckifAccountClassExistResponse() {
        return new CheckifAccountClassExistResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateStopPaymentResponse }
     * 
     */
    public FCUBSCreateStopPaymentResponse createFCUBSCreateStopPaymentResponse() {
        return new FCUBSCreateStopPaymentResponse();
    }

    /**
     * Create an instance of {@link GetLoanStatement }
     * 
     */
    public GetLoanStatement createGetLoanStatement() {
        return new GetLoanStatement();
    }

    /**
     * Create an instance of {@link GetEffectiveBalance }
     * 
     */
    public GetEffectiveBalance createGetEffectiveBalance() {
        return new GetEffectiveBalance();
    }

    /**
     * Create an instance of {@link FetchChequeDetailsResponse }
     * 
     */
    public FetchChequeDetailsResponse createFetchChequeDetailsResponse() {
        return new FetchChequeDetailsResponse();
    }

    /**
     * Create an instance of {@link FCUBSCustomerServiceCreateAmtBlk }
     * 
     */
    public FCUBSCustomerServiceCreateAmtBlk createFCUBSCustomerServiceCreateAmtBlk() {
        return new FCUBSCustomerServiceCreateAmtBlk();
    }

    /**
     * Create an instance of {@link FCUBSFTCreateTransactionResponse }
     * 
     */
    public FCUBSFTCreateTransactionResponse createFCUBSFTCreateTransactionResponse() {
        return new FCUBSFTCreateTransactionResponse();
    }

    /**
     * Create an instance of {@link GetAccountBranchResponse }
     * 
     */
    public GetAccountBranchResponse createGetAccountBranchResponse() {
        return new GetAccountBranchResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateTDAccount }
     * 
     */
    public FCUBSCreateTDAccount createFCUBSCreateTDAccount() {
        return new FCUBSCreateTDAccount();
    }

    /**
     * Create an instance of {@link ActivateChequeBook }
     * 
     */
    public ActivateChequeBook createActivateChequeBook() {
        return new ActivateChequeBook();
    }

    /**
     * Create an instance of {@link GetLoanInfoByAccountNumber }
     * 
     */
    public GetLoanInfoByAccountNumber createGetLoanInfoByAccountNumber() {
        return new GetLoanInfoByAccountNumber();
    }

    /**
     * Create an instance of {@link CheckifCustomerExist }
     * 
     */
    public CheckifCustomerExist createCheckifCustomerExist() {
        return new CheckifCustomerExist();
    }

    /**
     * Create an instance of {@link SingleDebitSingleCreditFX }
     * 
     */
    public SingleDebitSingleCreditFX createSingleDebitSingleCreditFX() {
        return new SingleDebitSingleCreditFX();
    }

    /**
     * Create an instance of {@link MultiJournalEntryResponse }
     * 
     */
    public MultiJournalEntryResponse createMultiJournalEntryResponse() {
        return new MultiJournalEntryResponse();
    }

    /**
     * Create an instance of {@link GetTransactionRefNoDetailsResponse }
     * 
     */
    public GetTransactionRefNoDetailsResponse createGetTransactionRefNoDetailsResponse() {
        return new GetTransactionRefNoDetailsResponse();
    }

    /**
     * Create an instance of {@link ChequeNumberInquiryResponse }
     * 
     */
    public ChequeNumberInquiryResponse createChequeNumberInquiryResponse() {
        return new ChequeNumberInquiryResponse();
    }

    /**
     * Create an instance of {@link GetLoanSummaryByCustomerID }
     * 
     */
    public GetLoanSummaryByCustomerID createGetLoanSummaryByCustomerID() {
        return new GetLoanSummaryByCustomerID();
    }

    /**
     * Create an instance of {@link GetUserLimitResponse }
     * 
     */
    public GetUserLimitResponse createGetUserLimitResponse() {
        return new GetUserLimitResponse();
    }

    /**
     * Create an instance of {@link GetLoanStatement2Response }
     * 
     */
    public GetLoanStatement2Response createGetLoanStatement2Response() {
        return new GetLoanStatement2Response();
    }

    /**
     * Create an instance of {@link AccountImageCollection }
     * 
     */
    public AccountImageCollection createAccountImageCollection() {
        return new AccountImageCollection();
    }

    /**
     * Create an instance of {@link GetCurrencyCodeResponse }
     * 
     */
    public GetCurrencyCodeResponse createGetCurrencyCodeResponse() {
        return new GetCurrencyCodeResponse();
    }

    /**
     * Create an instance of {@link GetAccordGLResponse }
     * 
     */
    public GetAccordGLResponse createGetAccordGLResponse() {
        return new GetAccordGLResponse();
    }

    /**
     * Create an instance of {@link GetUserBranch }
     * 
     */
    public GetUserBranch createGetUserBranch() {
        return new GetUserBranch();
    }

    /**
     * Create an instance of {@link FCUBSFTCreateTransaction }
     * 
     */
    public FCUBSFTCreateTransaction createFCUBSFTCreateTransaction() {
        return new FCUBSFTCreateTransaction();
    }

    /**
     * Create an instance of {@link GetCotVatDetailsResponse }
     * 
     */
    public GetCotVatDetailsResponse createGetCotVatDetailsResponse() {
        return new GetCotVatDetailsResponse();
    }

    /**
     * Create an instance of {@link GetRepaymentSchedule }
     * 
     */
    public GetRepaymentSchedule createGetRepaymentSchedule() {
        return new GetRepaymentSchedule();
    }

    /**
     * Create an instance of {@link FCUBSCreateMMContractResponse }
     * 
     */
    public FCUBSCreateMMContractResponse createFCUBSCreateMMContractResponse() {
        return new FCUBSCreateMMContractResponse();
    }

    /**
     * Create an instance of {@link FCUBSRTCreateTransactionResponse }
     * 
     */
    public FCUBSRTCreateTransactionResponse createFCUBSRTCreateTransactionResponse() {
        return new FCUBSRTCreateTransactionResponse();
    }

    /**
     * Create an instance of {@link PerformAccountVerificationResponse }
     * 
     */
    public PerformAccountVerificationResponse createPerformAccountVerificationResponse() {
        return new PerformAccountVerificationResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateCustomerResponse }
     * 
     */
    public FCUBSCreateCustomerResponse createFCUBSCreateCustomerResponse() {
        return new FCUBSCreateCustomerResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummaryAndTransactions }
     * 
     */
    public GetAccountSummaryAndTransactions createGetAccountSummaryAndTransactions() {
        return new GetAccountSummaryAndTransactions();
    }

    /**
     * Create an instance of {@link MultiJournalEntry }
     * 
     */
    public MultiJournalEntry createMultiJournalEntry() {
        return new MultiJournalEntry();
    }

    /**
     * Create an instance of {@link CheckIfAccountExist }
     * 
     */
    public CheckIfAccountExist createCheckIfAccountExist() {
        return new CheckIfAccountExist();
    }

    /**
     * Create an instance of {@link FetchMobileAccountDetailsResponse }
     * 
     */
    public FetchMobileAccountDetailsResponse createFetchMobileAccountDetailsResponse() {
        return new FetchMobileAccountDetailsResponse();
    }

    /**
     * Create an instance of {@link NubanAccts }
     * 
     */
    public NubanAccts createNubanAccts() {
        return new NubanAccts();
    }

    /**
     * Create an instance of {@link GetCustomerInfoForIso }
     * 
     */
    public GetCustomerInfoForIso createGetCustomerInfoForIso() {
        return new GetCustomerInfoForIso();
    }

    /**
     * Create an instance of {@link GetLoanInfoByAccountNumberResponse }
     * 
     */
    public GetLoanInfoByAccountNumberResponse createGetLoanInfoByAccountNumberResponse() {
        return new GetLoanInfoByAccountNumberResponse();
    }

    /**
     * Create an instance of {@link GetRelatedAccountsResponse }
     * 
     */
    public GetRelatedAccountsResponse createGetRelatedAccountsResponse() {
        return new GetRelatedAccountsResponse();
    }

    /**
     * Create an instance of {@link GetLoanDetailByAccountNumber2Response }
     * 
     */
    public GetLoanDetailByAccountNumber2Response createGetLoanDetailByAccountNumber2Response() {
        return new GetLoanDetailByAccountNumber2Response();
    }

    /**
     * Create an instance of {@link ImageCollection }
     * 
     */
    public ImageCollection createImageCollection() {
        return new ImageCollection();
    }

    /**
     * Create an instance of {@link FetchChequeDetails }
     * 
     */
    public FetchChequeDetails createFetchChequeDetails() {
        return new FetchChequeDetails();
    }

    /**
     * Create an instance of {@link FetchMobileTransactionDetailsResponse }
     * 
     */
    public FetchMobileTransactionDetailsResponse createFetchMobileTransactionDetailsResponse() {
        return new FetchMobileTransactionDetailsResponse();
    }

    /**
     * Create an instance of {@link FCUBSCGServiceCreateTransactionResponse }
     * 
     */
    public FCUBSCGServiceCreateTransactionResponse createFCUBSCGServiceCreateTransactionResponse() {
        return new FCUBSCGServiceCreateTransactionResponse();
    }

    /**
     * Create an instance of {@link FCUBSRTCreateTransaction }
     * 
     */
    public FCUBSRTCreateTransaction createFCUBSRTCreateTransaction() {
        return new FCUBSRTCreateTransaction();
    }

    /**
     * Create an instance of {@link ValidateUserTillResponse }
     * 
     */
    public ValidateUserTillResponse createValidateUserTillResponse() {
        return new ValidateUserTillResponse();
    }

    /**
     * Create an instance of {@link GetCustomerAcctsDetail }
     * 
     */
    public GetCustomerAcctsDetail createGetCustomerAcctsDetail() {
        return new GetCustomerAcctsDetail();
    }

    /**
     * Create an instance of {@link GetMessageIDStatusResponse }
     * 
     */
    public GetMessageIDStatusResponse createGetMessageIDStatusResponse() {
        return new GetMessageIDStatusResponse();
    }

    /**
     * Create an instance of {@link NubanAcctsResponse }
     * 
     */
    public NubanAcctsResponse createNubanAcctsResponse() {
        return new NubanAcctsResponse();
    }

    /**
     * Create an instance of {@link GetNoTransactionSummary }
     * 
     */
    public GetNoTransactionSummary createGetNoTransactionSummary() {
        return new GetNoTransactionSummary();
    }

    /**
     * Create an instance of {@link FetchVansoCustomerIdResponse }
     * 
     */
    public FetchVansoCustomerIdResponse createFetchVansoCustomerIdResponse() {
        return new FetchVansoCustomerIdResponse();
    }

    /**
     * Create an instance of {@link GetAccordGL }
     * 
     */
    public GetAccordGL createGetAccordGL() {
        return new GetAccordGL();
    }

    /**
     * Create an instance of {@link FetchMobileAccountDetails }
     * 
     */
    public FetchMobileAccountDetails createFetchMobileAccountDetails() {
        return new FetchMobileAccountDetails();
    }

    /**
     * Create an instance of {@link FCUBSCreateCustAccount }
     * 
     */
    public FCUBSCreateCustAccount createFCUBSCreateCustAccount() {
        return new FCUBSCreateCustAccount();
    }

    /**
     * Create an instance of {@link FCUBSCreateLDContract }
     * 
     */
    public FCUBSCreateLDContract createFCUBSCreateLDContract() {
        return new FCUBSCreateLDContract();
    }

    /**
     * Create an instance of {@link GetAccountSummaryAndTransactionsResponse }
     * 
     */
    public GetAccountSummaryAndTransactionsResponse createGetAccountSummaryAndTransactionsResponse() {
        return new GetAccountSummaryAndTransactionsResponse();
    }

    /**
     * Create an instance of {@link GetTellerLimit }
     * 
     */
    public GetTellerLimit createGetTellerLimit() {
        return new GetTellerLimit();
    }

    /**
     * Create an instance of {@link SweepAccts }
     * 
     */
    public SweepAccts createSweepAccts() {
        return new SweepAccts();
    }

    /**
     * Create an instance of {@link GetAccountSummaryByCustomerID }
     * 
     */
    public GetAccountSummaryByCustomerID createGetAccountSummaryByCustomerID() {
        return new GetAccountSummaryByCustomerID();
    }

    /**
     * Create an instance of {@link FCUBSCreateTDAccountResponse }
     * 
     */
    public FCUBSCreateTDAccountResponse createFCUBSCreateTDAccountResponse() {
        return new FCUBSCreateTDAccountResponse();
    }

    /**
     * Create an instance of {@link CheckifAccountClassExist }
     * 
     */
    public CheckifAccountClassExist createCheckifAccountClassExist() {
        return new CheckifAccountClassExist();
    }

    /**
     * Create an instance of {@link GetCustomerDetailsResponse }
     * 
     */
    public GetCustomerDetailsResponse createGetCustomerDetailsResponse() {
        return new GetCustomerDetailsResponse();
    }

    /**
     * Create an instance of {@link GetUserLimit }
     * 
     */
    public GetUserLimit createGetUserLimit() {
        return new GetUserLimit();
    }

    /**
     * Create an instance of {@link GetCustomerAcctsDetailBulk }
     * 
     */
    public GetCustomerAcctsDetailBulk createGetCustomerAcctsDetailBulk() {
        return new GetCustomerAcctsDetailBulk();
    }

    /**
     * Create an instance of {@link FCUBSCloseCustAccResponse }
     * 
     */
    public FCUBSCloseCustAccResponse createFCUBSCloseCustAccResponse() {
        return new FCUBSCloseCustAccResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateStopPayment }
     * 
     */
    public FCUBSCreateStopPayment createFCUBSCreateStopPayment() {
        return new FCUBSCreateStopPayment();
    }

    /**
     * Create an instance of {@link SignatureImageCollection }
     * 
     */
    public SignatureImageCollection createSignatureImageCollection() {
        return new SignatureImageCollection();
    }

    /**
     * Create an instance of {@link GetCurrencyRate }
     * 
     */
    public GetCurrencyRate createGetCurrencyRate() {
        return new GetCurrencyRate();
    }

    /**
     * Create an instance of {@link FCUBSCreateCustAccountResponse }
     * 
     */
    public FCUBSCreateCustAccountResponse createFCUBSCreateCustAccountResponse() {
        return new FCUBSCreateCustAccountResponse();
    }

    /**
     * Create an instance of {@link GetTransactionRefNoDetails }
     * 
     */
    public GetTransactionRefNoDetails createGetTransactionRefNoDetails() {
        return new GetTransactionRefNoDetails();
    }

    /**
     * Create an instance of {@link SingleJournalEntryResponse }
     * 
     */
    public SingleJournalEntryResponse createSingleJournalEntryResponse() {
        return new SingleJournalEntryResponse();
    }

    /**
     * Create an instance of {@link GetFixedDepositInfo }
     * 
     */
    public GetFixedDepositInfo createGetFixedDepositInfo() {
        return new GetFixedDepositInfo();
    }

    /**
     * Create an instance of {@link GetAccountSearchByAccountNumberResponse }
     * 
     */
    public GetAccountSearchByAccountNumberResponse createGetAccountSearchByAccountNumberResponse() {
        return new GetAccountSearchByAccountNumberResponse();
    }

    /**
     * Create an instance of {@link FCUBSCGServiceCreateTransaction }
     * 
     */
    public FCUBSCGServiceCreateTransaction createFCUBSCGServiceCreateTransaction() {
        return new FCUBSCGServiceCreateTransaction();
    }

    /**
     * Create an instance of {@link GetChequeValueDateResponse }
     * 
     */
    public GetChequeValueDateResponse createGetChequeValueDateResponse() {
        return new GetChequeValueDateResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateMMContract }
     * 
     */
    public FCUBSCreateMMContract createFCUBSCreateMMContract() {
        return new FCUBSCreateMMContract();
    }

    /**
     * Create an instance of {@link GetVisaCardLimitsResponse }
     * 
     */
    public GetVisaCardLimitsResponse createGetVisaCardLimitsResponse() {
        return new GetVisaCardLimitsResponse();
    }

    /**
     * Create an instance of {@link GetAccountMemoInfo }
     * 
     */
    public GetAccountMemoInfo createGetAccountMemoInfo() {
        return new GetAccountMemoInfo();
    }

    /**
     * Create an instance of {@link SingleDebitSingleCreditFXResponse }
     * 
     */
    public SingleDebitSingleCreditFXResponse createSingleDebitSingleCreditFXResponse() {
        return new SingleDebitSingleCreditFXResponse();
    }

    /**
     * Create an instance of {@link CheckIfAccountExistResponse }
     * 
     */
    public CheckIfAccountExistResponse createCheckIfAccountExistResponse() {
        return new CheckIfAccountExistResponse();
    }

    /**
     * Create an instance of {@link FCUBSCustomerServiceCreateAmtBlkResponse }
     * 
     */
    public FCUBSCustomerServiceCreateAmtBlkResponse createFCUBSCustomerServiceCreateAmtBlkResponse() {
        return new FCUBSCustomerServiceCreateAmtBlkResponse();
    }

    /**
     * Create an instance of {@link GetMessageIDStatus }
     * 
     */
    public GetMessageIDStatus createGetMessageIDStatus() {
        return new GetMessageIDStatus();
    }

    /**
     * Create an instance of {@link FetchVansoCustomerId }
     * 
     */
    public FetchVansoCustomerId createFetchVansoCustomerId() {
        return new FetchVansoCustomerId();
    }

    /**
     * Create an instance of {@link FCUBSCreateLDContractResponse }
     * 
     */
    public FCUBSCreateLDContractResponse createFCUBSCreateLDContractResponse() {
        return new FCUBSCreateLDContractResponse();
    }

    /**
     * Create an instance of {@link GetRelatedAccounts }
     * 
     */
    public GetRelatedAccounts createGetRelatedAccounts() {
        return new GetRelatedAccounts();
    }

    /**
     * Create an instance of {@link GetAccountSummaryAndTransactions2 }
     * 
     */
    public GetAccountSummaryAndTransactions2 createGetAccountSummaryAndTransactions2() {
        return new GetAccountSummaryAndTransactions2();
    }

    /**
     * Create an instance of {@link GetAccountSearchByAccountTitle }
     * 
     */
    public GetAccountSearchByAccountTitle createGetAccountSearchByAccountTitle() {
        return new GetAccountSearchByAccountTitle();
    }

    /**
     * Create an instance of {@link GetRepaymentScheduleResponse }
     * 
     */
    public GetRepaymentScheduleResponse createGetRepaymentScheduleResponse() {
        return new GetRepaymentScheduleResponse();
    }

    /**
     * Create an instance of {@link GetScreenLimitResponse }
     * 
     */
    public GetScreenLimitResponse createGetScreenLimitResponse() {
        return new GetScreenLimitResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateCustomer }
     * 
     */
    public FCUBSCreateCustomer createFCUBSCreateCustomer() {
        return new FCUBSCreateCustomer();
    }

    /**
     * Create an instance of {@link ValidateUserTill }
     * 
     */
    public ValidateUserTill createValidateUserTill() {
        return new ValidateUserTill();
    }

    /**
     * Create an instance of {@link GetAccountSearchByAccountNumber }
     * 
     */
    public GetAccountSearchByAccountNumber createGetAccountSearchByAccountNumber() {
        return new GetAccountSearchByAccountNumber();
    }

    /**
     * Create an instance of {@link SingleDebitSingleCredit }
     * 
     */
    public SingleDebitSingleCredit createSingleDebitSingleCredit() {
        return new SingleDebitSingleCredit();
    }

    /**
     * Create an instance of {@link FCUBSRTReverseTransaction }
     * 
     */
    public FCUBSRTReverseTransaction createFCUBSRTReverseTransaction() {
        return new FCUBSRTReverseTransaction();
    }

    /**
     * Create an instance of {@link GetLoanDetailByAccountNumber2 }
     * 
     */
    public GetLoanDetailByAccountNumber2 createGetLoanDetailByAccountNumber2() {
        return new GetLoanDetailByAccountNumber2();
    }

    /**
     * Create an instance of {@link PerformAccountVerification }
     * 
     */
    public PerformAccountVerification createPerformAccountVerification() {
        return new PerformAccountVerification();
    }

    /**
     * Create an instance of {@link FCUBSCreateSIContract }
     * 
     */
    public FCUBSCreateSIContract createFCUBSCreateSIContract() {
        return new FCUBSCreateSIContract();
    }

    /**
     * Create an instance of {@link GetAccountTransactionStatement }
     * 
     */
    public GetAccountTransactionStatement createGetAccountTransactionStatement() {
        return new GetAccountTransactionStatement();
    }

    /**
     * Create an instance of {@link GetAccountTransactionStatementResponse }
     * 
     */
    public GetAccountTransactionStatementResponse createGetAccountTransactionStatementResponse() {
        return new GetAccountTransactionStatementResponse();
    }

    /**
     * Create an instance of {@link GetCustomerInformationResponse }
     * 
     */
    public GetCustomerInformationResponse createGetCustomerInformationResponse() {
        return new GetCustomerInformationResponse();
    }

    /**
     * Create an instance of {@link FCUBSCustomerServiceCloseAmtBlk }
     * 
     */
    public FCUBSCustomerServiceCloseAmtBlk createFCUBSCustomerServiceCloseAmtBlk() {
        return new FCUBSCustomerServiceCloseAmtBlk();
    }

    /**
     * Create an instance of {@link GetCotVatDetails }
     * 
     */
    public GetCotVatDetails createGetCotVatDetails() {
        return new GetCotVatDetails();
    }

    /**
     * Create an instance of {@link GetLoanStatementResponse }
     * 
     */
    public GetLoanStatementResponse createGetLoanStatementResponse() {
        return new GetLoanStatementResponse();
    }

    /**
     * Create an instance of {@link GetCurrencyCode }
     * 
     */
    public GetCurrencyCode createGetCurrencyCode() {
        return new GetCurrencyCode();
    }

    /**
     * Create an instance of {@link FCUBSCloseCustAcc }
     * 
     */
    public FCUBSCloseCustAcc createFCUBSCloseCustAcc() {
        return new FCUBSCloseCustAcc();
    }

    /**
     * Create an instance of {@link GetTransactionSummaryByValueDate }
     * 
     */
    public GetTransactionSummaryByValueDate createGetTransactionSummaryByValueDate() {
        return new GetTransactionSummaryByValueDate();
    }

    /**
     * Create an instance of {@link FCUBSRTAuthorizeWithdrawal }
     * 
     */
    public FCUBSRTAuthorizeWithdrawal createFCUBSRTAuthorizeWithdrawal() {
        return new FCUBSRTAuthorizeWithdrawal();
    }

    /**
     * Create an instance of {@link GetCustomerAcctsDetailBulkResponse }
     * 
     */
    public GetCustomerAcctsDetailBulkResponse createGetCustomerAcctsDetailBulkResponse() {
        return new GetCustomerAcctsDetailBulkResponse();
    }

    /**
     * Create an instance of {@link ChequeNumberInquiry }
     * 
     */
    public ChequeNumberInquiry createChequeNumberInquiry() {
        return new ChequeNumberInquiry();
    }

    /**
     * Create an instance of {@link GetSample }
     * 
     */
    public GetSample createGetSample() {
        return new GetSample();
    }

    /**
     * Create an instance of {@link GetCustomerODLimit }
     * 
     */
    public GetCustomerODLimit createGetCustomerODLimit() {
        return new GetCustomerODLimit();
    }

    /**
     * Create an instance of {@link com.najcom.access.webservices.fcubs.ActivateChequeBookResponse }
     * 
     */
    public com.najcom.access.webservices.fcubs.ActivateChequeBookResponse createActivateChequeBookResponse() {
        return new com.najcom.access.webservices.fcubs.ActivateChequeBookResponse();
    }

    /**
     * Create an instance of {@link SweepAcctsResponse }
     * 
     */
    public SweepAcctsResponse createSweepAcctsResponse() {
        return new SweepAcctsResponse();
    }

    /**
     * Create an instance of {@link GetCustomerInfoForIsoResponse }
     * 
     */
    public GetCustomerInfoForIsoResponse createGetCustomerInfoForIsoResponse() {
        return new GetCustomerInfoForIsoResponse();
    }

    /**
     * Create an instance of {@link FCUBSRTReverseTransactionResponse }
     * 
     */
    public FCUBSRTReverseTransactionResponse createFCUBSRTReverseTransactionResponse() {
        return new FCUBSRTReverseTransactionResponse();
    }

    /**
     * Create an instance of {@link GetCustomerInformation }
     * 
     */
    public GetCustomerInformation createGetCustomerInformation() {
        return new GetCustomerInformation();
    }

    /**
     * Create an instance of {@link GetLoanStatement2 }
     * 
     */
    public GetLoanStatement2 createGetLoanStatement2() {
        return new GetLoanStatement2();
    }

    /**
     * Create an instance of {@link GetFixedDepositInfoResponse }
     * 
     */
    public GetFixedDepositInfoResponse createGetFixedDepositInfoResponse() {
        return new GetFixedDepositInfoResponse();
    }

    /**
     * Create an instance of {@link GetLoanSummaryByCustomerIDResponse }
     * 
     */
    public GetLoanSummaryByCustomerIDResponse createGetLoanSummaryByCustomerIDResponse() {
        return new GetLoanSummaryByCustomerIDResponse();
    }

    /**
     * Create an instance of {@link FCUBSCustomerServiceCloseAmtBlkResponse }
     * 
     */
    public FCUBSCustomerServiceCloseAmtBlkResponse createFCUBSCustomerServiceCloseAmtBlkResponse() {
        return new FCUBSCustomerServiceCloseAmtBlkResponse();
    }

    /**
     * Create an instance of {@link GetUserBranchResponse }
     * 
     */
    public GetUserBranchResponse createGetUserBranchResponse() {
        return new GetUserBranchResponse();
    }

    /**
     * Create an instance of {@link GetCurrencyRateResponse }
     * 
     */
    public GetCurrencyRateResponse createGetCurrencyRateResponse() {
        return new GetCurrencyRateResponse();
    }

    /**
     * Create an instance of {@link GetChequeValueDate }
     * 
     */
    public GetChequeValueDate createGetChequeValueDate() {
        return new GetChequeValueDate();
    }

    /**
     * Create an instance of {@link GetAccountSearchByAccountTitleResponse }
     * 
     */
    public GetAccountSearchByAccountTitleResponse createGetAccountSearchByAccountTitleResponse() {
        return new GetAccountSearchByAccountTitleResponse();
    }

    /**
     * Create an instance of {@link GetScreenLimit }
     * 
     */
    public GetScreenLimit createGetScreenLimit() {
        return new GetScreenLimit();
    }

    /**
     * Create an instance of {@link GetNoTransactionSummaryResponse }
     * 
     */
    public GetNoTransactionSummaryResponse createGetNoTransactionSummaryResponse() {
        return new GetNoTransactionSummaryResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummary }
     * 
     */
    public GetAccountSummary createGetAccountSummary() {
        return new GetAccountSummary();
    }

    /**
     * Create an instance of {@link SignatureImageCollectionResponse }
     * 
     */
    public SignatureImageCollectionResponse createSignatureImageCollectionResponse() {
        return new SignatureImageCollectionResponse();
    }

    /**
     * Create an instance of {@link AuthenticateCBAUser }
     * 
     */
    public AuthenticateCBAUser createAuthenticateCBAUser() {
        return new AuthenticateCBAUser();
    }

    /**
     * Create an instance of {@link FetchMobileTransactionDetails }
     * 
     */
    public FetchMobileTransactionDetails createFetchMobileTransactionDetails() {
        return new FetchMobileTransactionDetails();
    }

    /**
     * Create an instance of {@link GetSingleTransactionDetailsResponse }
     * 
     */
    public GetSingleTransactionDetailsResponse createGetSingleTransactionDetailsResponse() {
        return new GetSingleTransactionDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAccountSummaryAndTransactions2Response }
     * 
     */
    public GetAccountSummaryAndTransactions2Response createGetAccountSummaryAndTransactions2Response() {
        return new GetAccountSummaryAndTransactions2Response();
    }

    /**
     * Create an instance of {@link GetLoanDetailByAccountNumberResponse }
     * 
     */
    public GetLoanDetailByAccountNumberResponse createGetLoanDetailByAccountNumberResponse() {
        return new GetLoanDetailByAccountNumberResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateSIContractResponse }
     * 
     */
    public FCUBSCreateSIContractResponse createFCUBSCreateSIContractResponse() {
        return new FCUBSCreateSIContractResponse();
    }

    /**
     * Create an instance of {@link GetAccountMemoInfoResponse }
     * 
     */
    public GetAccountMemoInfoResponse createGetAccountMemoInfoResponse() {
        return new GetAccountMemoInfoResponse();
    }

    /**
     * Create an instance of {@link GetVisaCardLimits }
     * 
     */
    public GetVisaCardLimits createGetVisaCardLimits() {
        return new GetVisaCardLimits();
    }

    /**
     * Create an instance of {@link GetAccountSummaryResponse }
     * 
     */
    public GetAccountSummaryResponse createGetAccountSummaryResponse() {
        return new GetAccountSummaryResponse();
    }

    /**
     * Create an instance of {@link AccountImageCollectionResponse }
     * 
     */
    public AccountImageCollectionResponse createAccountImageCollectionResponse() {
        return new AccountImageCollectionResponse();
    }

    /**
     * Create an instance of {@link GetCustomerAcctsDetailResponse }
     * 
     */
    public GetCustomerAcctsDetailResponse createGetCustomerAcctsDetailResponse() {
        return new GetCustomerAcctsDetailResponse();
    }

    /**
     * Create an instance of {@link GetEffectiveBalanceResponse }
     * 
     */
    public GetEffectiveBalanceResponse createGetEffectiveBalanceResponse() {
        return new GetEffectiveBalanceResponse();
    }

    /**
     * Create an instance of {@link GetSingleTransactionDetails }
     * 
     */
    public GetSingleTransactionDetails createGetSingleTransactionDetails() {
        return new GetSingleTransactionDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateSIContract.class)
    public JAXBElement<String> createFCUBSCreateSIContractAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateSIContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateSIContractRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateSIContract.class)
    public JAXBElement<FCUBSCreateSIContractRequest> createFCUBSCreateSIContractData(FCUBSCreateSIContractRequest value) {
        return new JAXBElement<FCUBSCreateSIContractRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateSIContractRequest.class, FCUBSCreateSIContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetLoanSummaryByCustomerIDResult", scope = GetLoanSummaryByCustomerIDResponse.class)
    public JAXBElement<LoanSummaryResponse> createGetLoanSummaryByCustomerIDResponseGetLoanSummaryByCustomerIDResult(LoanSummaryResponse value) {
        return new JAXBElement<LoanSummaryResponse>(_GetLoanSummaryByCustomerIDResponseGetLoanSummaryByCustomerIDResult_QNAME, LoanSummaryResponse.class, GetLoanSummaryByCustomerIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ImageCollectionResult", scope = ImageCollectionResponse.class)
    public JAXBElement<ArrayOfImageCollectionDetails> createImageCollectionResponseImageCollectionResult(ArrayOfImageCollectionDetails value) {
        return new JAXBElement<ArrayOfImageCollectionDetails>(_ImageCollectionResponseImageCollectionResult_QNAME, ArrayOfImageCollectionDetails.class, ImageCollectionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = FetchVansoCustomerId.class)
    public JAXBElement<String> createFetchVansoCustomerIdUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, FetchVansoCustomerId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CheckifCustomerExistResult", scope = CheckifCustomerExistResponse.class)
    public JAXBElement<AccountSearchResponse> createCheckifCustomerExistResponseCheckifCustomerExistResult(AccountSearchResponse value) {
        return new JAXBElement<AccountSearchResponse>(_CheckifCustomerExistResponseCheckifCustomerExistResult_QNAME, AccountSearchResponse.class, CheckifCustomerExistResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "EndDate", scope = GetAccountSummaryAndTransactions.class)
    public JAXBElement<String> createGetAccountSummaryAndTransactionsEndDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsEndDate_QNAME, String.class, GetAccountSummaryAndTransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetAccountSummaryAndTransactions.class)
    public JAXBElement<String> createGetAccountSummaryAndTransactionsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetAccountSummaryAndTransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "StartDate", scope = GetAccountSummaryAndTransactions.class)
    public JAXBElement<String> createGetAccountSummaryAndTransactionsStartDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsStartDate_QNAME, String.class, GetAccountSummaryAndTransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNo", scope = GetAccountMemoInfo.class)
    public JAXBElement<String> createGetAccountMemoInfoAccountNo(String value) {
        return new JAXBElement<String>(_GetAccountMemoInfoAccountNo_QNAME, String.class, GetAccountMemoInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.najcom.access.datacontract.ActivateChequeBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ActivateChequeBookResult", scope = com.najcom.access.webservices.fcubs.ActivateChequeBookResponse.class)
    public JAXBElement<com.najcom.access.datacontract.ActivateChequeBookResponse> createActivateChequeBookResponseActivateChequeBookResult(com.najcom.access.datacontract.ActivateChequeBookResponse value) {
        return new JAXBElement<com.najcom.access.datacontract.ActivateChequeBookResponse>(_ActivateChequeBookResponseActivateChequeBookResult_QNAME, com.najcom.access.datacontract.ActivateChequeBookResponse.class, com.najcom.access.webservices.fcubs.ActivateChequeBookResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = MultiJournalEntry.class)
    public JAXBElement<String> createMultiJournalEntryAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, MultiJournalEntry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultipleJournalMasterTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = MultiJournalEntry.class)
    public JAXBElement<MultipleJournalMasterTrx> createMultiJournalEntryData(MultipleJournalMasterTrx value) {
        return new JAXBElement<MultipleJournalMasterTrx>(_FCUBSCreateSIContractData_QNAME, MultipleJournalMasterTrx.class, MultiJournalEntry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountTransactionResponseBulk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountSummaryAndTransactions2Result", scope = GetAccountSummaryAndTransactions2Response.class)
    public JAXBElement<AccountTransactionResponseBulk> createGetAccountSummaryAndTransactions2ResponseGetAccountSummaryAndTransactions2Result(AccountTransactionResponseBulk value) {
        return new JAXBElement<AccountTransactionResponseBulk>(_GetAccountSummaryAndTransactions2ResponseGetAccountSummaryAndTransactions2Result_QNAME, AccountTransactionResponseBulk.class, GetAccountSummaryAndTransactions2Response.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VisaCardLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetVisaCardLimitsResult", scope = GetVisaCardLimitsResponse.class)
    public JAXBElement<VisaCardLimitResponse> createGetVisaCardLimitsResponseGetVisaCardLimitsResult(VisaCardLimitResponse value) {
        return new JAXBElement<VisaCardLimitResponse>(_GetVisaCardLimitsResponseGetVisaCardLimitsResult_QNAME, VisaCardLimitResponse.class, GetVisaCardLimitsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountSummaryResult", scope = GetAccountSummaryResponse.class)
    public JAXBElement<AccountSummaryResponse> createGetAccountSummaryResponseGetAccountSummaryResult(AccountSummaryResponse value) {
        return new JAXBElement<AccountSummaryResponse>(_GetAccountSummaryResponseGetAccountSummaryResult_QNAME, AccountSummaryResponse.class, GetAccountSummaryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountVerificationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "PerformAccountVerificationResult", scope = PerformAccountVerificationResponse.class)
    public JAXBElement<AccountVerificationResponse> createPerformAccountVerificationResponsePerformAccountVerificationResult(AccountVerificationResponse value) {
        return new JAXBElement<AccountVerificationResponse>(_PerformAccountVerificationResponsePerformAccountVerificationResult_QNAME, AccountVerificationResponse.class, PerformAccountVerificationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = FetchMobileAccountDetails.class)
    public JAXBElement<String> createFetchMobileAccountDetailsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, FetchMobileAccountDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = FetchMobileAccountDetails.class)
    public JAXBElement<String> createFetchMobileAccountDetailsUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, FetchMobileAccountDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChequeValueDateResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "getChequeValueDateResult", scope = GetChequeValueDateResponse.class)
    public JAXBElement<GetChequeValueDateResp> createGetChequeValueDateResponseGetChequeValueDateResult(GetChequeValueDateResp value) {
        return new JAXBElement<GetChequeValueDateResp>(_GetChequeValueDateResponseGetChequeValueDateResult_QNAME, GetChequeValueDateResp.class, GetChequeValueDateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSRTReverseTransaction.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSRTReverseTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSRTReverseTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSRTReverseTransaction.class)
    public JAXBElement<FCUBSRTReverseTransactionRequest> createFCUBSRTReverseTransactionData(FCUBSRTReverseTransactionRequest value) {
        return new JAXBElement<FCUBSRTReverseTransactionRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSRTReverseTransactionRequest.class, FCUBSRTReverseTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "loanAccountNumber", scope = GetLoanInfoByAccountNumber.class)
    public JAXBElement<String> createGetLoanInfoByAccountNumberLoanAccountNumber(String value) {
        return new JAXBElement<String>(_GetLoanInfoByAccountNumberLoanAccountNumber_QNAME, String.class, GetLoanInfoByAccountNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "reviewDate", scope = GetLoanInfoByAccountNumber.class)
    public JAXBElement<String> createGetLoanInfoByAccountNumberReviewDate(String value) {
        return new JAXBElement<String>(_GetLoanInfoByAccountNumberReviewDate_QNAME, String.class, GetLoanInfoByAccountNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DebitAccountNumber", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditDebitAccountNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditDebitAccountNumber_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CreditAccountNumber", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditCreditAccountNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCreditAccountNumber_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ValueDate", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditValueDate(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditValueDate_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DebitBranchCode", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditDebitBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditDebitBranchCode_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CurrencyNumber", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditCurrencyNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCurrencyNumber_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "BranchCode", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditBranchCode_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CreditCustomerId", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditCreditCustomerId(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCreditCustomerId_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CurrencyCode", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditCurrencyCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCurrencyCode_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CreditBranchCode", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditCreditBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCreditBranchCode_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "TxnCode", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditTxnCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditTxnCode_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "Source", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditSource(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditSource_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "MsgId", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditMsgId(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditMsgId_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "PaymentReference", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditPaymentReference(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditPaymentReference_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DebitCustomerId", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditDebitCustomerId(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditDebitCustomerId_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "Narration", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditNarration(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditNarration_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "BatchNumber", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditBatchNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditBatchNumber_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "RequestModule", scope = SingleDebitSingleCredit.class)
    public JAXBElement<String> createSingleDebitSingleCreditRequestModule(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditRequestModule_QNAME, String.class, SingleDebitSingleCredit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CurrencyRateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCurrencyRateResult", scope = GetCurrencyRateResponse.class)
    public JAXBElement<CurrencyRateResponse> createGetCurrencyRateResponseGetCurrencyRateResult(CurrencyRateResponse value) {
        return new JAXBElement<CurrencyRateResponse>(_GetCurrencyRateResponseGetCurrencyRateResult_QNAME, CurrencyRateResponse.class, GetCurrencyRateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanInfoSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetLoanInfoByAccountNumberResult", scope = GetLoanInfoByAccountNumberResponse.class)
    public JAXBElement<LoanInfoSummaryResponse> createGetLoanInfoByAccountNumberResponseGetLoanInfoByAccountNumberResult(LoanInfoSummaryResponse value) {
        return new JAXBElement<LoanInfoSummaryResponse>(_GetLoanInfoByAccountNumberResponseGetLoanInfoByAccountNumberResult_QNAME, LoanInfoSummaryResponse.class, GetLoanInfoByAccountNumberResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateCustAccountResult", scope = FCUBSCreateCustAccountResponse.class)
    public JAXBElement<CreateCustAccountResponse> createFCUBSCreateCustAccountResponseFCUBSCreateCustAccountResult(CreateCustAccountResponse value) {
        return new JAXBElement<CreateCustAccountResponse>(_FCUBSCreateCustAccountResponseFCUBSCreateCustAccountResult_QNAME, CreateCustAccountResponse.class, FCUBSCreateCustAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CustomerId", scope = GetAccountSummaryByCustomerID.class)
    public JAXBElement<String> createGetAccountSummaryByCustomerIDCustomerId(String value) {
        return new JAXBElement<String>(_GetAccountSummaryByCustomerIDCustomerId_QNAME, String.class, GetAccountSummaryByCustomerID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TellerLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetTellerLimitResult", scope = GetTellerLimitResponse.class)
    public JAXBElement<TellerLimitResponse> createGetTellerLimitResponseGetTellerLimitResult(TellerLimitResponse value) {
        return new JAXBElement<TellerLimitResponse>(_GetTellerLimitResponseGetTellerLimitResult_QNAME, TellerLimitResponse.class, GetTellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountSearchByAccountTitleResult", scope = GetAccountSearchByAccountTitleResponse.class)
    public JAXBElement<AccountSearchResponse> createGetAccountSearchByAccountTitleResponseGetAccountSearchByAccountTitleResult(AccountSearchResponse value) {
        return new JAXBElement<AccountSearchResponse>(_GetAccountSearchByAccountTitleResponseGetAccountSearchByAccountTitleResult_QNAME, AccountSearchResponse.class, GetAccountSearchByAccountTitleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetCustomerInformation.class)
    public JAXBElement<String> createGetCustomerInformationAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetCustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCustomerServiceCloseAmtBlkResult", scope = FCUBSCustomerServiceCloseAmtBlkResponse.class)
    public JAXBElement<FcubsResponse> createFCUBSCustomerServiceCloseAmtBlkResponseFCUBSCustomerServiceCloseAmtBlkResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FCUBSCustomerServiceCloseAmtBlkResponseFCUBSCustomerServiceCloseAmtBlkResult_QNAME, FcubsResponse.class, FCUBSCustomerServiceCloseAmtBlkResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "EndDate", scope = GetNoTransactionSummary.class)
    public JAXBElement<String> createGetNoTransactionSummaryEndDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsEndDate_QNAME, String.class, GetNoTransactionSummary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetNoTransactionSummary.class)
    public JAXBElement<String> createGetNoTransactionSummaryAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetNoTransactionSummary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "StartDate", scope = GetNoTransactionSummary.class)
    public JAXBElement<String> createGetNoTransactionSummaryStartDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsStartDate_QNAME, String.class, GetNoTransactionSummary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCloseCustAcc.class)
    public JAXBElement<String> createFCUBSCloseCustAccAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCloseCustAcc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCloseCustAccRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCloseCustAcc.class)
    public JAXBElement<FCUBSCloseCustAccRequest> createFCUBSCloseCustAccData(FCUBSCloseCustAccRequest value) {
        return new JAXBElement<FCUBSCloseCustAccRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCloseCustAccRequest.class, FCUBSCloseCustAcc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "accountNumber", scope = PerformAccountVerification.class)
    public JAXBElement<String> createPerformAccountVerificationAccountNumber(String value) {
        return new JAXBElement<String>(_PerformAccountVerificationAccountNumber_QNAME, String.class, PerformAccountVerification.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerAcctsDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCustomerAcctsDetailResult", scope = GetCustomerAcctsDetailResponse.class)
    public JAXBElement<CustomerAcctsDetailResponse> createGetCustomerAcctsDetailResponseGetCustomerAcctsDetailResult(CustomerAcctsDetailResponse value) {
        return new JAXBElement<CustomerAcctsDetailResponse>(_GetCustomerAcctsDetailResponseGetCustomerAcctsDetailResult_QNAME, CustomerAcctsDetailResponse.class, GetCustomerAcctsDetailResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCustomerAcctsDetailBulkResult", scope = GetCustomerAcctsDetailBulkResponse.class)
    public JAXBElement<ArrayOfCustomerAcctsDetailResponse> createGetCustomerAcctsDetailBulkResponseGetCustomerAcctsDetailBulkResult(ArrayOfCustomerAcctsDetailResponse value) {
        return new JAXBElement<ArrayOfCustomerAcctsDetailResponse>(_GetCustomerAcctsDetailBulkResponseGetCustomerAcctsDetailBulkResult_QNAME, ArrayOfCustomerAcctsDetailResponse.class, GetCustomerAcctsDetailBulkResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CheckifAccountClassExistResult", scope = CheckifAccountClassExistResponse.class)
    public JAXBElement<AccountSearchResponse> createCheckifAccountClassExistResponseCheckifAccountClassExistResult(AccountSearchResponse value) {
        return new JAXBElement<AccountSearchResponse>(_CheckifAccountClassExistResponseCheckifAccountClassExistResult_QNAME, AccountSearchResponse.class, CheckifAccountClassExistResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "OldAccountNumber", scope = NubanAccts.class)
    public JAXBElement<String> createNubanAcctsOldAccountNumber(String value) {
        return new JAXBElement<String>(_NubanAcctsOldAccountNumber_QNAME, String.class, NubanAccts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "limits_ccy", scope = GetTellerLimit.class)
    public JAXBElement<String> createGetTellerLimitLimitsCcy(String value) {
        return new JAXBElement<String>(_GetTellerLimitLimitsCcy_QNAME, String.class, GetTellerLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "user_id", scope = GetTellerLimit.class)
    public JAXBElement<String> createGetTellerLimitUserId(String value) {
        return new JAXBElement<String>(_GetTellerLimitUserId_QNAME, String.class, GetTellerLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCGServiceCreateTransactionResult", scope = FCUBSCGServiceCreateTransactionResponse.class)
    public JAXBElement<FcubsResponse> createFCUBSCGServiceCreateTransactionResponseFCUBSCGServiceCreateTransactionResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FCUBSCGServiceCreateTransactionResponseFCUBSCGServiceCreateTransactionResult_QNAME, FcubsResponse.class, FCUBSCGServiceCreateTransactionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CustomerNo", scope = CheckifAccountClassExist.class)
    public JAXBElement<String> createCheckifAccountClassExistCustomerNo(String value) {
        return new JAXBElement<String>(_CheckifAccountClassExistCustomerNo_QNAME, String.class, CheckifAccountClassExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountClass", scope = CheckifAccountClassExist.class)
    public JAXBElement<String> createCheckifAccountClassExistAccountClass(String value) {
        return new JAXBElement<String>(_CheckifAccountClassExistAccountClass_QNAME, String.class, CheckifAccountClassExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNo", scope = CheckIfAccountExist.class)
    public JAXBElement<String> createCheckIfAccountExistAccountNo(String value) {
        return new JAXBElement<String>(_GetAccountMemoInfoAccountNo_QNAME, String.class, CheckIfAccountExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateStopPayment.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateStopPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateStopPaymentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateStopPayment.class)
    public JAXBElement<FCUBSCreateStopPaymentRequest> createFCUBSCreateStopPaymentData(FCUBSCreateStopPaymentRequest value) {
        return new JAXBElement<FCUBSCreateStopPaymentRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateStopPaymentRequest.class, FCUBSCreateStopPayment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCustomerInformationResult", scope = GetCustomerInformationResponse.class)
    public JAXBElement<CustomerInformationResponse> createGetCustomerInformationResponseGetCustomerInformationResult(CustomerInformationResponse value) {
        return new JAXBElement<CustomerInformationResponse>(_GetCustomerInformationResponseGetCustomerInformationResult_QNAME, CustomerInformationResponse.class, GetCustomerInformationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CustomerId", scope = GetLoanSummaryByCustomerID.class)
    public JAXBElement<String> createGetLoanSummaryByCustomerIDCustomerId(String value) {
        return new JAXBElement<String>(_GetAccountSummaryByCustomerIDCustomerId_QNAME, String.class, GetLoanSummaryByCustomerID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSRTAuthorizeWithdrawal.class)
    public JAXBElement<String> createFCUBSRTAuthorizeWithdrawalAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSRTAuthorizeWithdrawal.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeWithdrawalRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSRTAuthorizeWithdrawal.class)
    public JAXBElement<AuthorizeWithdrawalRequest> createFCUBSRTAuthorizeWithdrawalData(AuthorizeWithdrawalRequest value) {
        return new JAXBElement<AuthorizeWithdrawalRequest>(_FCUBSCreateSIContractData_QNAME, AuthorizeWithdrawalRequest.class, FCUBSRTAuthorizeWithdrawal.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNo", scope = AccountImageCollection.class)
    public JAXBElement<String> createAccountImageCollectionAccountNo(String value) {
        return new JAXBElement<String>(_GetAccountMemoInfoAccountNo_QNAME, String.class, AccountImageCollection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNo", scope = SignatureImageCollection.class)
    public JAXBElement<String> createSignatureImageCollectionAccountNo(String value) {
        return new JAXBElement<String>(_GetAccountMemoInfoAccountNo_QNAME, String.class, SignatureImageCollection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "LoanAccountNumberRequest", scope = GetLoanDetailByAccountNumber2 .class)
    public JAXBElement<ArrayOfstring> createGetLoanDetailByAccountNumber2LoanAccountNumberRequest(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetLoanDetailByAccountNumber2LoanAccountNumberRequest_QNAME, ArrayOfstring.class, GetLoanDetailByAccountNumber2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountSummaryByCustomerIDResult", scope = GetAccountSummaryByCustomerIDResponse.class)
    public JAXBElement<AccountSummaryResponse> createGetAccountSummaryByCustomerIDResponseGetAccountSummaryByCustomerIDResult(AccountSummaryResponse value) {
        return new JAXBElement<AccountSummaryResponse>(_GetAccountSummaryByCustomerIDResponseGetAccountSummaryByCustomerIDResult_QNAME, AccountSummaryResponse.class, GetAccountSummaryByCustomerIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetNoTransactionSummaryResult", scope = GetNoTransactionSummaryResponse.class)
    public JAXBElement<AccountSummaryAndTrxResponse> createGetNoTransactionSummaryResponseGetNoTransactionSummaryResult(AccountSummaryAndTrxResponse value) {
        return new JAXBElement<AccountSummaryAndTrxResponse>(_GetNoTransactionSummaryResponseGetNoTransactionSummaryResult_QNAME, AccountSummaryAndTrxResponse.class, GetNoTransactionSummaryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetAccountSummary.class)
    public JAXBElement<String> createGetAccountSummaryAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetAccountSummary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetLoanStatementResult", scope = GetLoanStatementResponse.class)
    public JAXBElement<LoanSummaryResponse> createGetLoanStatementResponseGetLoanStatementResult(LoanSummaryResponse value) {
        return new JAXBElement<LoanSummaryResponse>(_GetLoanStatementResponseGetLoanStatementResult_QNAME, LoanSummaryResponse.class, GetLoanStatementResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountRelatedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetRelatedAccountsResult", scope = GetRelatedAccountsResponse.class)
    public JAXBElement<AccountRelatedResponse> createGetRelatedAccountsResponseGetRelatedAccountsResult(AccountRelatedResponse value) {
        return new JAXBElement<AccountRelatedResponse>(_GetRelatedAccountsResponseGetRelatedAccountsResult_QNAME, AccountRelatedResponse.class, GetRelatedAccountsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "EndDate", scope = GetAccountTransactionStatement.class)
    public JAXBElement<String> createGetAccountTransactionStatementEndDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsEndDate_QNAME, String.class, GetAccountTransactionStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetAccountTransactionStatement.class)
    public JAXBElement<String> createGetAccountTransactionStatementAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetAccountTransactionStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "StartDate", scope = GetAccountTransactionStatement.class)
    public JAXBElement<String> createGetAccountTransactionStatementStartDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsStartDate_QNAME, String.class, GetAccountTransactionStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponseBulk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetLoanStatement2Result", scope = GetLoanStatement2Response.class)
    public JAXBElement<LoanSummaryResponseBulk> createGetLoanStatement2ResponseGetLoanStatement2Result(LoanSummaryResponseBulk value) {
        return new JAXBElement<LoanSummaryResponseBulk>(_GetLoanStatement2ResponseGetLoanStatement2Result_QNAME, LoanSummaryResponseBulk.class, GetLoanStatement2Response.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateMMContract.class)
    public JAXBElement<String> createFCUBSCreateMMContractAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateMMContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateMMContractRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateMMContract.class)
    public JAXBElement<FCUBSCreateMMContractRequest> createFCUBSCreateMMContractData(FCUBSCreateMMContractRequest value) {
        return new JAXBElement<FCUBSCreateMMContractRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateMMContractRequest.class, FCUBSCreateMMContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ChequeNumber", scope = ChequeNumberInquiry.class)
    public JAXBElement<String> createChequeNumberInquiryChequeNumber(String value) {
        return new JAXBElement<String>(_ChequeNumberInquiryChequeNumber_QNAME, String.class, ChequeNumberInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = ChequeNumberInquiry.class)
    public JAXBElement<String> createChequeNumberInquiryAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, ChequeNumberInquiry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "EndDate", scope = GetTransactionSummaryByValueDate.class)
    public JAXBElement<String> createGetTransactionSummaryByValueDateEndDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsEndDate_QNAME, String.class, GetTransactionSummaryByValueDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetTransactionSummaryByValueDate.class)
    public JAXBElement<String> createGetTransactionSummaryByValueDateAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetTransactionSummaryByValueDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "StartDate", scope = GetTransactionSummaryByValueDate.class)
    public JAXBElement<String> createGetTransactionSummaryByValueDateStartDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsStartDate_QNAME, String.class, GetTransactionSummaryByValueDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "accountNumber", scope = GetCustomerInfoForIso.class)
    public JAXBElement<String> createGetCustomerInfoForIsoAccountNumber(String value) {
        return new JAXBElement<String>(_PerformAccountVerificationAccountNumber_QNAME, String.class, GetCustomerInfoForIso.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCGServiceCreateTransaction.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCGServiceCreateTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCGServiceCreateTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCGServiceCreateTransaction.class)
    public JAXBElement<FCUBSCGServiceCreateTransactionRequest> createFCUBSCGServiceCreateTransactionData(FCUBSCGServiceCreateTransactionRequest value) {
        return new JAXBElement<FCUBSCGServiceCreateTransactionRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCGServiceCreateTransactionRequest.class, FCUBSCGServiceCreateTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "branch_code", scope = GetUserLimit.class)
    public JAXBElement<String> createGetUserLimitBranchCode(String value) {
        return new JAXBElement<String>(_GetUserLimitBranchCode_QNAME, String.class, GetUserLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "user_id", scope = GetUserLimit.class)
    public JAXBElement<String> createGetUserLimitUserId(String value) {
        return new JAXBElement<String>(_GetTellerLimitUserId_QNAME, String.class, GetUserLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CustomerId", scope = GetCustomerDetails.class)
    public JAXBElement<String> createGetCustomerDetailsCustomerId(String value) {
        return new JAXBElement<String>(_GetAccountSummaryByCustomerIDCustomerId_QNAME, String.class, GetCustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionRefNoDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetTransactionRefNoDetailsResult", scope = GetTransactionRefNoDetailsResponse.class)
    public JAXBElement<TransactionRefNoDetailsResponse> createGetTransactionRefNoDetailsResponseGetTransactionRefNoDetailsResult(TransactionRefNoDetailsResponse value) {
        return new JAXBElement<TransactionRefNoDetailsResponse>(_GetTransactionRefNoDetailsResponseGetTransactionRefNoDetailsResult_QNAME, TransactionRefNoDetailsResponse.class, GetTransactionRefNoDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "loanAccountNumber", scope = GetRepaymentSchedule.class)
    public JAXBElement<String> createGetRepaymentScheduleLoanAccountNumber(String value) {
        return new JAXBElement<String>(_GetLoanInfoByAccountNumberLoanAccountNumber_QNAME, String.class, GetRepaymentSchedule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMobileAccountDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FetchMobileAccountDetailsResult", scope = FetchMobileAccountDetailsResponse.class)
    public JAXBElement<ArrayOfMobileAccountDetail> createFetchMobileAccountDetailsResponseFetchMobileAccountDetailsResult(ArrayOfMobileAccountDetail value) {
        return new JAXBElement<ArrayOfMobileAccountDetail>(_FetchMobileAccountDetailsResponseFetchMobileAccountDetailsResult_QNAME, ArrayOfMobileAccountDetail.class, FetchMobileAccountDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "LoanAccountNumber", scope = GetLoanStatement.class)
    public JAXBElement<String> createGetLoanStatementLoanAccountNumber(String value) {
        return new JAXBElement<String>(_GetLoanStatementLoanAccountNumber_QNAME, String.class, GetLoanStatement.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountExistResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CheckIfAccountExistResult", scope = CheckIfAccountExistResponse.class)
    public JAXBElement<GetAccountExistResponse> createCheckIfAccountExistResponseCheckIfAccountExistResult(GetAccountExistResponse value) {
        return new JAXBElement<GetAccountExistResponse>(_CheckIfAccountExistResponseCheckIfAccountExistResult_QNAME, GetAccountExistResponse.class, CheckIfAccountExistResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetTransactionSummaryByValueDateResult", scope = GetTransactionSummaryByValueDateResponse.class)
    public JAXBElement<AccountSummaryAndTrxResponse> createGetTransactionSummaryByValueDateResponseGetTransactionSummaryByValueDateResult(AccountSummaryAndTrxResponse value) {
        return new JAXBElement<AccountSummaryAndTrxResponse>(_GetTransactionSummaryByValueDateResponseGetTransactionSummaryByValueDateResult_QNAME, AccountSummaryAndTrxResponse.class, GetTransactionSummaryByValueDateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSweepAcctsDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "SweepAcctsResult", scope = SweepAcctsResponse.class)
    public JAXBElement<ArrayOfSweepAcctsDetails> createSweepAcctsResponseSweepAcctsResult(ArrayOfSweepAcctsDetails value) {
        return new JAXBElement<ArrayOfSweepAcctsDetails>(_SweepAcctsResponseSweepAcctsResult_QNAME, ArrayOfSweepAcctsDetails.class, SweepAcctsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "cust_ac_no", scope = GetEffectiveBalance.class)
    public JAXBElement<String> createGetEffectiveBalanceCustAcNo(String value) {
        return new JAXBElement<String>(_GetEffectiveBalanceCustAcNo_QNAME, String.class, GetEffectiveBalance.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DebitAccountNumber", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXDebitAccountNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditDebitAccountNumber_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CreditAccountNumber", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXCreditAccountNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCreditAccountNumber_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ValueDate", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXValueDate(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditValueDate_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DebitBranchCode", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXDebitBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditDebitBranchCode_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CurrencyNumber", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXCurrencyNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCurrencyNumber_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "BranchCode", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditBranchCode_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CreditCustomerId", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXCreditCustomerId(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCreditCustomerId_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CurrencyCode", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXCurrencyCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCurrencyCode_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CreditBranchCode", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXCreditBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditCreditBranchCode_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "TxnCode", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXTxnCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditTxnCode_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "Source", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXSource(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditSource_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "MsgId", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXMsgId(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditMsgId_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "PaymentReference", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXPaymentReference(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditPaymentReference_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DebitCustomerId", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXDebitCustomerId(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditDebitCustomerId_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "Narration", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXNarration(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditNarration_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "BatchNumber", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXBatchNumber(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditBatchNumber_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "RequestModule", scope = SingleDebitSingleCreditFX.class)
    public JAXBElement<String> createSingleDebitSingleCreditFXRequestModule(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditRequestModule_QNAME, String.class, SingleDebitSingleCreditFX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FixedDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetFixedDepositInfoResult", scope = GetFixedDepositInfoResponse.class)
    public JAXBElement<FixedDepositResponse> createGetFixedDepositInfoResponseGetFixedDepositInfoResult(FixedDepositResponse value) {
        return new JAXBElement<FixedDepositResponse>(_GetFixedDepositInfoResponseGetFixedDepositInfoResult_QNAME, FixedDepositResponse.class, GetFixedDepositInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetUserLimitResult", scope = GetUserLimitResponse.class)
    public JAXBElement<UserLimitResponse> createGetUserLimitResponseGetUserLimitResult(UserLimitResponse value) {
        return new JAXBElement<UserLimitResponse>(_GetUserLimitResponseGetUserLimitResult_QNAME, UserLimitResponse.class, GetUserLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScreenLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetScreenLimitResult", scope = GetScreenLimitResponse.class)
    public JAXBElement<ScreenLimitResponse> createGetScreenLimitResponseGetScreenLimitResult(ScreenLimitResponse value) {
        return new JAXBElement<ScreenLimitResponse>(_GetScreenLimitResponseGetScreenLimitResult_QNAME, ScreenLimitResponse.class, GetScreenLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "customerNo", scope = GetFixedDepositInfo.class)
    public JAXBElement<String> createGetFixedDepositInfoCustomerNo(String value) {
        return new JAXBElement<String>(_GetFixedDepositInfoCustomerNo_QNAME, String.class, GetFixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfNubanAcctsDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "NubanAcctsResult", scope = NubanAcctsResponse.class)
    public JAXBElement<ArrayOfNubanAcctsDetails> createNubanAcctsResponseNubanAcctsResult(ArrayOfNubanAcctsDetails value) {
        return new JAXBElement<ArrayOfNubanAcctsDetails>(_NubanAcctsResponseNubanAcctsResult_QNAME, ArrayOfNubanAcctsDetails.class, NubanAcctsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateLDContract.class)
    public JAXBElement<String> createFCUBSCreateLDContractAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateLDContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateLDContractRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateLDContract.class)
    public JAXBElement<FCUBSCreateLDContractRequest> createFCUBSCreateLDContractData(FCUBSCreateLDContractRequest value) {
        return new JAXBElement<FCUBSCreateLDContractRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateLDContractRequest.class, FCUBSCreateLDContract.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CustID", scope = SweepAccts.class)
    public JAXBElement<String> createSweepAcctsCustID(String value) {
        return new JAXBElement<String>(_SweepAcctsCustID_QNAME, String.class, SweepAccts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetCotVatDetails.class)
    public JAXBElement<String> createGetCotVatDetailsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetCotVatDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAcctMemoInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountMemoInfoResult", scope = GetAccountMemoInfoResponse.class)
    public JAXBElement<GetAcctMemoInfoResponse> createGetAccountMemoInfoResponseGetAccountMemoInfoResult(GetAcctMemoInfoResponse value) {
        return new JAXBElement<GetAcctMemoInfoResponse>(_GetAccountMemoInfoResponseGetAccountMemoInfoResult_QNAME, GetAcctMemoInfoResponse.class, GetAccountMemoInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "EndDate", scope = GetAccountSummaryAndTransactions2 .class)
    public JAXBElement<String> createGetAccountSummaryAndTransactions2EndDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsEndDate_QNAME, String.class, GetAccountSummaryAndTransactions2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumberList", scope = GetAccountSummaryAndTransactions2 .class)
    public JAXBElement<ArrayOfstring> createGetAccountSummaryAndTransactions2AccountNumberList(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetAccountSummaryAndTransactions2AccountNumberList_QNAME, ArrayOfstring.class, GetAccountSummaryAndTransactions2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "StartDate", scope = GetAccountSummaryAndTransactions2 .class)
    public JAXBElement<String> createGetAccountSummaryAndTransactions2StartDate(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsStartDate_QNAME, String.class, GetAccountSummaryAndTransactions2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateTDAccount.class)
    public JAXBElement<String> createFCUBSCreateTDAccountAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateTDAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateTDAccountRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateTDAccount.class)
    public JAXBElement<FCUBSCreateTDAccountRequest> createFCUBSCreateTDAccountData(FCUBSCreateTDAccountRequest value) {
        return new JAXBElement<FCUBSCreateTDAccountRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateTDAccountRequest.class, FCUBSCreateTDAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSFTCreateTransaction.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSFTCreateTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSFTCreateTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSFTCreateTransaction.class)
    public JAXBElement<FCUBSFTCreateTransactionRequest> createFCUBSFTCreateTransactionData(FCUBSFTCreateTransactionRequest value) {
        return new JAXBElement<FCUBSFTCreateTransactionRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSFTCreateTransactionRequest.class, FCUBSFTCreateTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStopPaymentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateStopPaymentResult", scope = FCUBSCreateStopPaymentResponse.class)
    public JAXBElement<CreateStopPaymentResponse> createFCUBSCreateStopPaymentResponseFCUBSCreateStopPaymentResult(CreateStopPaymentResponse value) {
        return new JAXBElement<CreateStopPaymentResponse>(_FCUBSCreateStopPaymentResponseFCUBSCreateStopPaymentResult_QNAME, CreateStopPaymentResponse.class, FCUBSCreateStopPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetRelatedAccounts.class)
    public JAXBElement<String> createGetRelatedAccountsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetRelatedAccounts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserBranchResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "getUserBranchResult", scope = GetUserBranchResponse.class)
    public JAXBElement<GetUserBranchResp> createGetUserBranchResponseGetUserBranchResult(GetUserBranchResp value) {
        return new JAXBElement<GetUserBranchResp>(_GetUserBranchResponseGetUserBranchResult_QNAME, GetUserBranchResp.class, GetUserBranchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCustomerDetailsResult", scope = GetCustomerDetailsResponse.class)
    public JAXBElement<CustomerDetails> createGetCustomerDetailsResponseGetCustomerDetailsResult(CustomerDetails value) {
        return new JAXBElement<CustomerDetails>(_GetCustomerDetailsResponseGetCustomerDetailsResult_QNAME, CustomerDetails.class, GetCustomerDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSingleTransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetSingleTransactionDetailsResult", scope = GetSingleTransactionDetailsResponse.class)
    public JAXBElement<ArrayOfSingleTransactionDetails> createGetSingleTransactionDetailsResponseGetSingleTransactionDetailsResult(ArrayOfSingleTransactionDetails value) {
        return new JAXBElement<ArrayOfSingleTransactionDetails>(_GetSingleTransactionDetailsResponseGetSingleTransactionDetailsResult_QNAME, ArrayOfSingleTransactionDetails.class, GetSingleTransactionDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetVisaCardLimits.class)
    public JAXBElement<String> createGetVisaCardLimitsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetVisaCardLimits.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyCodeResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "getCurrencyCodeResult", scope = GetCurrencyCodeResponse.class)
    public JAXBElement<GetCurrencyCodeResp> createGetCurrencyCodeResponseGetCurrencyCodeResult(GetCurrencyCodeResp value) {
        return new JAXBElement<GetCurrencyCodeResp>(_GetCurrencyCodeResponseGetCurrencyCodeResult_QNAME, GetCurrencyCodeResp.class, GetCurrencyCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountBranchResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "getAccountBranchResult", scope = GetAccountBranchResponse.class)
    public JAXBElement<GetAccountBranchResp> createGetAccountBranchResponseGetAccountBranchResult(GetAccountBranchResp value) {
        return new JAXBElement<GetAccountBranchResp>(_GetAccountBranchResponseGetAccountBranchResult_QNAME, GetAccountBranchResp.class, GetAccountBranchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VansoCustomerIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FetchVansoCustomerIdResult", scope = FetchVansoCustomerIdResponse.class)
    public JAXBElement<VansoCustomerIdResponse> createFetchVansoCustomerIdResponseFetchVansoCustomerIdResult(VansoCustomerIdResponse value) {
        return new JAXBElement<VansoCustomerIdResponse>(_FetchVansoCustomerIdResponseFetchVansoCustomerIdResult_QNAME, VansoCustomerIdResponse.class, FetchVansoCustomerIdResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FetchChequeDetailsResult", scope = FetchChequeDetailsResponse.class)
    public JAXBElement<ChequeDetailsResponse> createFetchChequeDetailsResponseFetchChequeDetailsResult(ChequeDetailsResponse value) {
        return new JAXBElement<ChequeDetailsResponse>(_FetchChequeDetailsResponseFetchChequeDetailsResult_QNAME, ChequeDetailsResponse.class, FetchChequeDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "SingleJournalEntryResult", scope = SingleJournalEntryResponse.class)
    public JAXBElement<FcubsResponse> createSingleJournalEntryResponseSingleJournalEntryResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_SingleJournalEntryResponseSingleJournalEntryResult_QNAME, FcubsResponse.class, SingleJournalEntryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "SingleDebitSingleCreditResult", scope = SingleDebitSingleCreditResponse.class)
    public JAXBElement<FcubsResponse> createSingleDebitSingleCreditResponseSingleDebitSingleCreditResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_SingleDebitSingleCreditResponseSingleDebitSingleCreditResult_QNAME, FcubsResponse.class, SingleDebitSingleCreditResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "branch_code", scope = GetChequeValueDate.class)
    public JAXBElement<String> createGetChequeValueDateBranchCode(String value) {
        return new JAXBElement<String>(_GetUserLimitBranchCode_QNAME, String.class, GetChequeValueDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "txn_date", scope = GetChequeValueDate.class)
    public JAXBElement<XMLGregorianCalendar> createGetChequeValueDateTxnDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetChequeValueDateTxnDate_QNAME, XMLGregorianCalendar.class, GetChequeValueDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "routing_no", scope = GetChequeValueDate.class)
    public JAXBElement<String> createGetChequeValueDateRoutingNo(String value) {
        return new JAXBElement<String>(_GetChequeValueDateRoutingNo_QNAME, String.class, GetChequeValueDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "iso_num_ccy_code", scope = GetCurrencyCode.class)
    public JAXBElement<String> createGetCurrencyCodeIsoNumCcyCode(String value) {
        return new JAXBElement<String>(_GetCurrencyCodeIsoNumCcyCode_QNAME, String.class, GetCurrencyCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountSearchByAccountNumberResult", scope = GetAccountSearchByAccountNumberResponse.class)
    public JAXBElement<AccountSearchResponse> createGetAccountSearchByAccountNumberResponseGetAccountSearchByAccountNumberResult(AccountSearchResponse value) {
        return new JAXBElement<AccountSearchResponse>(_GetAccountSearchByAccountNumberResponseGetAccountSearchByAccountNumberResult_QNAME, AccountSearchResponse.class, GetAccountSearchByAccountNumberResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "SingleDebitSingleCreditFXResult", scope = SingleDebitSingleCreditFXResponse.class)
    public JAXBElement<FcubsResponse> createSingleDebitSingleCreditFXResponseSingleDebitSingleCreditFXResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_SingleDebitSingleCreditFXResponseSingleDebitSingleCreditFXResult_QNAME, FcubsResponse.class, SingleDebitSingleCreditFXResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetAccountSearchByAccountNumber.class)
    public JAXBElement<String> createGetAccountSearchByAccountNumberAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetAccountSearchByAccountNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "LoanAccountNumbersRequest", scope = GetLoanStatement2 .class)
    public JAXBElement<ArrayOfstring> createGetLoanStatement2LoanAccountNumbersRequest(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetLoanStatement2LoanAccountNumbersRequest_QNAME, ArrayOfstring.class, GetLoanStatement2 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CustomerID", scope = ImageCollection.class)
    public JAXBElement<String> createImageCollectionCustomerID(String value) {
        return new JAXBElement<String>(_ImageCollectionCustomerID_QNAME, String.class, ImageCollection.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ccy_code", scope = GetScreenLimit.class)
    public JAXBElement<String> createGetScreenLimitCcyCode(String value) {
        return new JAXBElement<String>(_GetScreenLimitCcyCode_QNAME, String.class, GetScreenLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "function_no", scope = GetScreenLimit.class)
    public JAXBElement<String> createGetScreenLimitFunctionNo(String value) {
        return new JAXBElement<String>(_GetScreenLimitFunctionNo_QNAME, String.class, GetScreenLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSIContractResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateSIContractResult", scope = FCUBSCreateSIContractResponse.class)
    public JAXBElement<CreateSIContractResponse> createFCUBSCreateSIContractResponseFCUBSCreateSIContractResult(CreateSIContractResponse value) {
        return new JAXBElement<CreateSIContractResponse>(_FCUBSCreateSIContractResponseFCUBSCreateSIContractResult_QNAME, CreateSIContractResponse.class, FCUBSCreateSIContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = AuthenticateCBAUser.class)
    public JAXBElement<String> createAuthenticateCBAUserUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, AuthenticateCBAUser.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "transDate", scope = GetSingleTransactionDetails.class)
    public JAXBElement<String> createGetSingleTransactionDetailsTransDate(String value) {
        return new JAXBElement<String>(_GetSingleTransactionDetailsTransDate_QNAME, String.class, GetSingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetSingleTransactionDetails.class)
    public JAXBElement<String> createGetSingleTransactionDetailsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetSingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "RowID", scope = GetSingleTransactionDetails.class)
    public JAXBElement<String> createGetSingleTransactionDetailsRowID(String value) {
        return new JAXBElement<String>(_GetSingleTransactionDetailsRowID_QNAME, String.class, GetSingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = SingleJournalEntry.class)
    public JAXBElement<String> createSingleJournalEntryAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, SingleJournalEntry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SingleJournalTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = SingleJournalEntry.class)
    public JAXBElement<SingleJournalTrx> createSingleJournalEntryData(SingleJournalTrx value) {
        return new JAXBElement<SingleJournalTrx>(_FCUBSCreateSIContractData_QNAME, SingleJournalTrx.class, SingleJournalEntry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = GetUserBranch.class)
    public JAXBElement<String> createGetUserBranchUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, GetUserBranch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ac_gl_no", scope = GetAccountBranch.class)
    public JAXBElement<String> createGetAccountBranchAcGlNo(String value) {
        return new JAXBElement<String>(_GetAccountBranchAcGlNo_QNAME, String.class, GetAccountBranch.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponseBulk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetLoanDetailByAccountNumber2Result", scope = GetLoanDetailByAccountNumber2Response.class)
    public JAXBElement<LoanSummaryResponseBulk> createGetLoanDetailByAccountNumber2ResponseGetLoanDetailByAccountNumber2Result(LoanSummaryResponseBulk value) {
        return new JAXBElement<LoanSummaryResponseBulk>(_GetLoanDetailByAccountNumber2ResponseGetLoanDetailByAccountNumber2Result_QNAME, LoanSummaryResponseBulk.class, GetLoanDetailByAccountNumber2Response.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetAccordGL.class)
    public JAXBElement<String> createGetAccordGLAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetAccordGL.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateCustomer.class)
    public JAXBElement<String> createFCUBSCreateCustomerAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateCustomerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateCustomer.class)
    public JAXBElement<FCUBSCreateCustomerRequest> createFCUBSCreateCustomerData(FCUBSCreateCustomerRequest value) {
        return new JAXBElement<FCUBSCreateCustomerRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateCustomerRequest.class, FCUBSCreateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSRTReverseTransactionResult", scope = FCUBSRTReverseTransactionResponse.class)
    public JAXBElement<FcubsResponse> createFCUBSRTReverseTransactionResponseFCUBSRTReverseTransactionResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FCUBSRTReverseTransactionResponseFCUBSRTReverseTransactionResult_QNAME, FcubsResponse.class, FCUBSRTReverseTransactionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLDContractResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateLDContractResult", scope = FCUBSCreateLDContractResponse.class)
    public JAXBElement<CreateLDContractResponse> createFCUBSCreateLDContractResponseFCUBSCreateLDContractResult(CreateLDContractResponse value) {
        return new JAXBElement<CreateLDContractResponse>(_FCUBSCreateLDContractResponseFCUBSCreateLDContractResult_QNAME, CreateLDContractResponse.class, FCUBSCreateLDContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSRTAuthorizeWithdrawalResult", scope = FCUBSRTAuthorizeWithdrawalResponse.class)
    public JAXBElement<FcubsResponse> createFCUBSRTAuthorizeWithdrawalResponseFCUBSRTAuthorizeWithdrawalResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FCUBSRTAuthorizeWithdrawalResponseFCUBSRTAuthorizeWithdrawalResult_QNAME, FcubsResponse.class, FCUBSRTAuthorizeWithdrawalResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "accountNumberList", scope = GetCustomerAcctsDetailBulk.class)
    public JAXBElement<ArrayOfstring> createGetCustomerAcctsDetailBulkAccountNumberList(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetCustomerAcctsDetailBulkAccountNumberList_QNAME, ArrayOfstring.class, GetCustomerAcctsDetailBulk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountSummaryAndTransactionsResult", scope = GetAccountSummaryAndTransactionsResponse.class)
    public JAXBElement<AccountSummaryAndTrxResponse> createGetAccountSummaryAndTransactionsResponseGetAccountSummaryAndTransactionsResult(AccountSummaryAndTrxResponse value) {
        return new JAXBElement<AccountSummaryAndTrxResponse>(_GetAccountSummaryAndTransactionsResponseGetAccountSummaryAndTransactionsResult_QNAME, AccountSummaryAndTrxResponse.class, GetAccountSummaryAndTransactionsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "msgId", scope = GetMessageIDStatus.class)
    public JAXBElement<String> createGetMessageIDStatusMsgId(String value) {
        return new JAXBElement<String>(_GetMessageIDStatusMsgId_QNAME, String.class, GetMessageIDStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "extSystem", scope = GetMessageIDStatus.class)
    public JAXBElement<String> createGetMessageIDStatusExtSystem(String value) {
        return new JAXBElement<String>(_GetMessageIDStatusExtSystem_QNAME, String.class, GetMessageIDStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetCustomerODLimit.class)
    public JAXBElement<String> createGetCustomerODLimitAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetCustomerODLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetLoanDetailByAccountNumberResult", scope = GetLoanDetailByAccountNumberResponse.class)
    public JAXBElement<LoanSummaryResponse> createGetLoanDetailByAccountNumberResponseGetLoanDetailByAccountNumberResult(LoanSummaryResponse value) {
        return new JAXBElement<LoanSummaryResponse>(_GetLoanDetailByAccountNumberResponseGetLoanDetailByAccountNumberResult_QNAME, LoanSummaryResponse.class, GetLoanDetailByAccountNumberResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "LoanAccountNumber", scope = GetLoanDetailByAccountNumber.class)
    public JAXBElement<String> createGetLoanDetailByAccountNumberLoanAccountNumber(String value) {
        return new JAXBElement<String>(_GetLoanStatementLoanAccountNumber_QNAME, String.class, GetLoanDetailByAccountNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticateCBAUserResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AuthenticateCBAUserResult", scope = AuthenticateCBAUserResponse.class)
    public JAXBElement<AuthenticateCBAUserResp> createAuthenticateCBAUserResponseAuthenticateCBAUserResult(AuthenticateCBAUserResp value) {
        return new JAXBElement<AuthenticateCBAUserResp>(_AuthenticateCBAUserResponseAuthenticateCBAUserResult_QNAME, AuthenticateCBAUserResp.class, AuthenticateCBAUserResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSFTCreateTransactionResult", scope = FCUBSFTCreateTransactionResponse.class)
    public JAXBElement<FcubsResponse> createFCUBSFTCreateTransactionResponseFCUBSFTCreateTransactionResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FCUBSFTCreateTransactionResponseFCUBSFTCreateTransactionResult_QNAME, FcubsResponse.class, FCUBSFTCreateTransactionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "DateofBirth", scope = CheckifCustomerExist.class)
    public JAXBElement<String> createCheckifCustomerExistDateofBirth(String value) {
        return new JAXBElement<String>(_CheckifCustomerExistDateofBirth_QNAME, String.class, CheckifCustomerExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "TelephoneNo", scope = CheckifCustomerExist.class)
    public JAXBElement<String> createCheckifCustomerExistTelephoneNo(String value) {
        return new JAXBElement<String>(_CheckifCustomerExistTelephoneNo_QNAME, String.class, CheckifCustomerExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "MobileNo", scope = CheckifCustomerExist.class)
    public JAXBElement<String> createCheckifCustomerExistMobileNo(String value) {
        return new JAXBElement<String>(_CheckifCustomerExistMobileNo_QNAME, String.class, CheckifCustomerExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountClass", scope = CheckifCustomerExist.class)
    public JAXBElement<String> createCheckifCustomerExistAccountClass(String value) {
        return new JAXBElement<String>(_CheckifAccountClassExistAccountClass_QNAME, String.class, CheckifCustomerExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "Email", scope = CheckifCustomerExist.class)
    public JAXBElement<String> createCheckifCustomerExistEmail(String value) {
        return new JAXBElement<String>(_CheckifCustomerExistEmail_QNAME, String.class, CheckifCustomerExist.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "Til_Vlt_Ind", scope = ValidateUserTill.class)
    public JAXBElement<String> createValidateUserTillTilVltInd(String value) {
        return new JAXBElement<String>(_ValidateUserTillTilVltInd_QNAME, String.class, ValidateUserTill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "branchCode", scope = ValidateUserTill.class)
    public JAXBElement<String> createValidateUserTillBranchCode(String value) {
        return new JAXBElement<String>(_ValidateUserTillBranchCode_QNAME, String.class, ValidateUserTill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "UserId", scope = ValidateUserTill.class)
    public JAXBElement<String> createValidateUserTillUserId(String value) {
        return new JAXBElement<String>(_FetchVansoCustomerIdUserId_QNAME, String.class, ValidateUserTill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountCotAndVatResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCotVatDetailsResult", scope = GetCotVatDetailsResponse.class)
    public JAXBElement<AccountCotAndVatResponse> createGetCotVatDetailsResponseGetCotVatDetailsResult(AccountCotAndVatResponse value) {
        return new JAXBElement<AccountCotAndVatResponse>(_GetCotVatDetailsResponseGetCotVatDetailsResult_QNAME, AccountCotAndVatResponse.class, GetCotVatDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateUserTillResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "validateUserTillResult", scope = ValidateUserTillResponse.class)
    public JAXBElement<ValidateUserTillResp> createValidateUserTillResponseValidateUserTillResult(ValidateUserTillResp value) {
        return new JAXBElement<ValidateUserTillResp>(_ValidateUserTillResponseValidateUserTillResult_QNAME, ValidateUserTillResp.class, ValidateUserTillResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerODLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCustomerODLimitResult", scope = GetCustomerODLimitResponse.class)
    public JAXBElement<CustomerODLimitResponse> createGetCustomerODLimitResponseGetCustomerODLimitResult(CustomerODLimitResponse value) {
        return new JAXBElement<CustomerODLimitResponse>(_GetCustomerODLimitResponseGetCustomerODLimitResult_QNAME, CustomerODLimitResponse.class, GetCustomerODLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountTitle", scope = GetAccountSearchByAccountTitle.class)
    public JAXBElement<String> createGetAccountSearchByAccountTitleAccountTitle(String value) {
        return new JAXBElement<String>(_GetAccountSearchByAccountTitleAccountTitle_QNAME, String.class, GetAccountSearchByAccountTitle.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateRTResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSRTCreateTransactionResult", scope = FCUBSRTCreateTransactionResponse.class)
    public JAXBElement<CreateRTResponse> createFCUBSRTCreateTransactionResponseFCUBSRTCreateTransactionResult(CreateRTResponse value) {
        return new JAXBElement<CreateRTResponse>(_FCUBSRTCreateTransactionResponseFCUBSRTCreateTransactionResult_QNAME, CreateRTResponse.class, FCUBSRTCreateTransactionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTDAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateTDAccountResult", scope = FCUBSCreateTDAccountResponse.class)
    public JAXBElement<CreateTDAccountResponse> createFCUBSCreateTDAccountResponseFCUBSCreateTDAccountResult(CreateTDAccountResponse value) {
        return new JAXBElement<CreateTDAccountResponse>(_FCUBSCreateTDAccountResponseFCUBSCreateTDAccountResult_QNAME, CreateTDAccountResponse.class, FCUBSCreateTDAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAmtBlockResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCustomerServiceCreateAmtBlkResult", scope = FCUBSCustomerServiceCreateAmtBlkResponse.class)
    public JAXBElement<CreateAmtBlockResponse> createFCUBSCustomerServiceCreateAmtBlkResponseFCUBSCustomerServiceCreateAmtBlkResult(CreateAmtBlockResponse value) {
        return new JAXBElement<CreateAmtBlockResponse>(_FCUBSCustomerServiceCreateAmtBlkResponseFCUBSCustomerServiceCreateAmtBlkResult_QNAME, CreateAmtBlockResponse.class, FCUBSCustomerServiceCreateAmtBlkResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "SignatureImageCollectionResult", scope = SignatureImageCollectionResponse.class)
    public JAXBElement<ArrayOfImageCollectionDetails> createSignatureImageCollectionResponseSignatureImageCollectionResult(ArrayOfImageCollectionDetails value) {
        return new JAXBElement<ArrayOfImageCollectionDetails>(_SignatureImageCollectionResponseSignatureImageCollectionResult_QNAME, ArrayOfImageCollectionDetails.class, SignatureImageCollectionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEffectiveBalanceResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "getEffectiveBalanceResult", scope = GetEffectiveBalanceResponse.class)
    public JAXBElement<GetEffectiveBalanceResp> createGetEffectiveBalanceResponseGetEffectiveBalanceResult(GetEffectiveBalanceResp value) {
        return new JAXBElement<GetEffectiveBalanceResp>(_GetEffectiveBalanceResponseGetEffectiveBalanceResult_QNAME, GetEffectiveBalanceResp.class, GetEffectiveBalanceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetCustomerInfoForIsoResult", scope = GetCustomerInfoForIsoResponse.class)
    public JAXBElement<String> createGetCustomerInfoForIsoResponseGetCustomerInfoForIsoResult(String value) {
        return new JAXBElement<String>(_GetCustomerInfoForIsoResponseGetCustomerInfoForIsoResult_QNAME, String.class, GetCustomerInfoForIsoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMMContractResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateMMContractResult", scope = FCUBSCreateMMContractResponse.class)
    public JAXBElement<CreateMMContractResponse> createFCUBSCreateMMContractResponseFCUBSCreateMMContractResult(CreateMMContractResponse value) {
        return new JAXBElement<CreateMMContractResponse>(_FCUBSCreateMMContractResponseFCUBSCreateMMContractResult_QNAME, CreateMMContractResponse.class, FCUBSCreateMMContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetAccountTransactionStatementResult", scope = GetAccountTransactionStatementResponse.class)
    public JAXBElement<AccountSummaryAndTrxResponse> createGetAccountTransactionStatementResponseGetAccountTransactionStatementResult(AccountSummaryAndTrxResponse value) {
        return new JAXBElement<AccountSummaryAndTrxResponse>(_GetAccountTransactionStatementResponseGetAccountTransactionStatementResult_QNAME, AccountSummaryAndTrxResponse.class, GetAccountTransactionStatementResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCloseCustAccResult", scope = FCUBSCloseCustAccResponse.class)
    public JAXBElement<FcubsResponse> createFCUBSCloseCustAccResponseFCUBSCloseCustAccResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FCUBSCloseCustAccResponseFCUBSCloseCustAccResult_QNAME, FcubsResponse.class, FCUBSCloseCustAccResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FCUBSCreateCustomerResult", scope = FCUBSCreateCustomerResponse.class)
    public JAXBElement<CreateCustomerResponse> createFCUBSCreateCustomerResponseFCUBSCreateCustomerResult(CreateCustomerResponse value) {
        return new JAXBElement<CreateCustomerResponse>(_FCUBSCreateCustomerResponseFCUBSCreateCustomerResult_QNAME, CreateCustomerResponse.class, FCUBSCreateCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetSample.class)
    public JAXBElement<String> createGetSampleAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetSample.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "trn_ref_no", scope = GetTransactionRefNoDetails.class)
    public JAXBElement<String> createGetTransactionRefNoDetailsTrnRefNo(String value) {
        return new JAXBElement<String>(_GetTransactionRefNoDetailsTrnRefNo_QNAME, String.class, GetTransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSRTCreateTransaction.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSRTCreateTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSRTCreateTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSRTCreateTransaction.class)
    public JAXBElement<FCUBSRTCreateTransactionRequest> createFCUBSRTCreateTransactionData(FCUBSRTCreateTransactionRequest value) {
        return new JAXBElement<FCUBSRTCreateTransactionRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSRTCreateTransactionRequest.class, FCUBSRTCreateTransaction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = GetCustomerAcctsDetail.class)
    public JAXBElement<String> createGetCustomerAcctsDetailAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, GetCustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageIDStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetMessageIDStatusResult", scope = GetMessageIDStatusResponse.class)
    public JAXBElement<MessageIDStatusResponse> createGetMessageIDStatusResponseGetMessageIDStatusResult(MessageIDStatusResponse value) {
        return new JAXBElement<MessageIDStatusResponse>(_GetMessageIDStatusResponseGetMessageIDStatusResult_QNAME, MessageIDStatusResponse.class, GetMessageIDStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeNoInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ChequeNumberInquiryResult", scope = ChequeNumberInquiryResponse.class)
    public JAXBElement<ChequeNoInquiryResponse> createChequeNumberInquiryResponseChequeNumberInquiryResult(ChequeNoInquiryResponse value) {
        return new JAXBElement<ChequeNoInquiryResponse>(_ChequeNumberInquiryResponseChequeNumberInquiryResult_QNAME, ChequeNoInquiryResponse.class, ChequeNumberInquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCustomerServiceCloseAmtBlk.class)
    public JAXBElement<String> createFCUBSCustomerServiceCloseAmtBlkAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCustomerServiceCloseAmtBlk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCustomerServiceAmtBlkRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCustomerServiceCloseAmtBlk.class)
    public JAXBElement<FCUBSCustomerServiceAmtBlkRequest> createFCUBSCustomerServiceCloseAmtBlkData(FCUBSCustomerServiceAmtBlkRequest value) {
        return new JAXBElement<FCUBSCustomerServiceAmtBlkRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCustomerServiceAmtBlkRequest.class, FCUBSCustomerServiceCloseAmtBlk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "MultiJournalEntryResult", scope = MultiJournalEntryResponse.class)
    public JAXBElement<FcubsResponse> createMultiJournalEntryResponseMultiJournalEntryResult(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_MultiJournalEntryResponseMultiJournalEntryResult_QNAME, FcubsResponse.class, MultiJournalEntryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMobileTransactionDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "FetchMobileTransactionDetailsResult", scope = FetchMobileTransactionDetailsResponse.class)
    public JAXBElement<ArrayOfMobileTransactionDetail> createFetchMobileTransactionDetailsResponseFetchMobileTransactionDetailsResult(ArrayOfMobileTransactionDetail value) {
        return new JAXBElement<ArrayOfMobileTransactionDetail>(_FetchMobileTransactionDetailsResponseFetchMobileTransactionDetailsResult_QNAME, ArrayOfMobileTransactionDetail.class, FetchMobileTransactionDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepaymentScheduleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetRepaymentScheduleResult", scope = GetRepaymentScheduleResponse.class)
    public JAXBElement<RepaymentScheduleResponse> createGetRepaymentScheduleResponseGetRepaymentScheduleResult(RepaymentScheduleResponse value) {
        return new JAXBElement<RepaymentScheduleResponse>(_GetRepaymentScheduleResponseGetRepaymentScheduleResult_QNAME, RepaymentScheduleResponse.class, GetRepaymentScheduleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CheckerID", scope = ActivateChequeBook.class)
    public JAXBElement<String> createActivateChequeBookCheckerID(String value) {
        return new JAXBElement<String>(_ActivateChequeBookCheckerID_QNAME, String.class, ActivateChequeBook.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "BranchCode", scope = ActivateChequeBook.class)
    public JAXBElement<String> createActivateChequeBookBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditBranchCode_QNAME, String.class, ActivateChequeBook.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = ActivateChequeBook.class)
    public JAXBElement<String> createActivateChequeBookAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, ActivateChequeBook.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "ChequeBookNo", scope = ActivateChequeBook.class)
    public JAXBElement<String> createActivateChequeBookChequeBookNo(String value) {
        return new JAXBElement<String>(_ActivateChequeBookChequeBookNo_QNAME, String.class, ActivateChequeBook.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "MakerID", scope = ActivateChequeBook.class)
    public JAXBElement<String> createActivateChequeBookMakerID(String value) {
        return new JAXBElement<String>(_ActivateChequeBookMakerID_QNAME, String.class, ActivateChequeBook.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CcyCode2", scope = GetCurrencyRate.class)
    public JAXBElement<String> createGetCurrencyRateCcyCode2(String value) {
        return new JAXBElement<String>(_GetCurrencyRateCcyCode2_QNAME, String.class, GetCurrencyRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "CcyCode1", scope = GetCurrencyRate.class)
    public JAXBElement<String> createGetCurrencyRateCcyCode1(String value) {
        return new JAXBElement<String>(_GetCurrencyRateCcyCode1_QNAME, String.class, GetCurrencyRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "BranchCode", scope = GetCurrencyRate.class)
    public JAXBElement<String> createGetCurrencyRateBranchCode(String value) {
        return new JAXBElement<String>(_SingleDebitSingleCreditBranchCode_QNAME, String.class, GetCurrencyRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCustomerServiceCreateAmtBlk.class)
    public JAXBElement<String> createFCUBSCustomerServiceCreateAmtBlkAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCustomerServiceCreateAmtBlk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCustomerServiceAmtBlkRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCustomerServiceCreateAmtBlk.class)
    public JAXBElement<FCUBSCustomerServiceAmtBlkRequest> createFCUBSCustomerServiceCreateAmtBlkData(FCUBSCustomerServiceAmtBlkRequest value) {
        return new JAXBElement<FCUBSCustomerServiceAmtBlkRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCustomerServiceAmtBlkRequest.class, FCUBSCustomerServiceCreateAmtBlk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "getAccordGLResult", scope = GetAccordGLResponse.class)
    public JAXBElement<String> createGetAccordGLResponseGetAccordGLResult(String value) {
        return new JAXBElement<String>(_GetAccordGLResponseGetAccordGLResult_QNAME, String.class, GetAccordGLResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SampleAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "GetSampleResult", scope = GetSampleResponse.class)
    public JAXBElement<SampleAccountResponse> createGetSampleResponseGetSampleResult(SampleAccountResponse value) {
        return new JAXBElement<SampleAccountResponse>(_GetSampleResponseGetSampleResult_QNAME, SampleAccountResponse.class, GetSampleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "action", scope = FCUBSCreateCustAccount.class)
    public JAXBElement<String> createFCUBSCreateCustAccountAction(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractAction_QNAME, String.class, FCUBSCreateCustAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateCustAccountRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "data", scope = FCUBSCreateCustAccount.class)
    public JAXBElement<FCUBSCreateCustAccountRequest> createFCUBSCreateCustAccountData(FCUBSCreateCustAccountRequest value) {
        return new JAXBElement<FCUBSCreateCustAccountRequest>(_FCUBSCreateSIContractData_QNAME, FCUBSCreateCustAccountRequest.class, FCUBSCreateCustAccount.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountImageCollectionResult", scope = AccountImageCollectionResponse.class)
    public JAXBElement<ArrayOfImageCollectionDetails> createAccountImageCollectionResponseAccountImageCollectionResult(ArrayOfImageCollectionDetails value) {
        return new JAXBElement<ArrayOfImageCollectionDetails>(_AccountImageCollectionResponseAccountImageCollectionResult_QNAME, ArrayOfImageCollectionDetails.class, AccountImageCollectionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = FetchChequeDetails.class)
    public JAXBElement<String> createFetchChequeDetailsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, FetchChequeDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://FCUBS.Webservices", name = "AccountNumber", scope = FetchMobileTransactionDetails.class)
    public JAXBElement<String> createFetchMobileTransactionDetailsAccountNumber(String value) {
        return new JAXBElement<String>(_GetAccountSummaryAndTransactionsAccountNumber_QNAME, String.class, FetchMobileTransactionDetails.class, value);
    }

}
