
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfImageCollectionDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountImageCollectionResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfImageCollectionDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accountImageCollectionResult"
})
@XmlRootElement(name = "AccountImageCollectionResponse")
public class AccountImageCollectionResponse {

    @XmlElementRef(name = "AccountImageCollectionResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfImageCollectionDetails> accountImageCollectionResult;

    /**
     * Gets the value of the accountImageCollectionResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfImageCollectionDetails> getAccountImageCollectionResult() {
        return accountImageCollectionResult;
    }

    /**
     * Sets the value of the accountImageCollectionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}
     *     
     */
    public void setAccountImageCollectionResult(JAXBElement<ArrayOfImageCollectionDetails> value) {
        this.accountImageCollectionResult = value;
    }

}
