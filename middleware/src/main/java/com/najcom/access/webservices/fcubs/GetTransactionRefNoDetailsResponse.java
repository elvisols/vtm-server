
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.TransactionRefNoDetailsResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetTransactionRefNoDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response}TransactionRefNoDetailsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getTransactionRefNoDetailsResult"
})
@XmlRootElement(name = "GetTransactionRefNoDetailsResponse")
public class GetTransactionRefNoDetailsResponse {

    @XmlElementRef(name = "GetTransactionRefNoDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<TransactionRefNoDetailsResponse> getTransactionRefNoDetailsResult;

    /**
     * Gets the value of the getTransactionRefNoDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TransactionRefNoDetailsResponse }{@code >}
     *     
     */
    public JAXBElement<TransactionRefNoDetailsResponse> getGetTransactionRefNoDetailsResult() {
        return getTransactionRefNoDetailsResult;
    }

    /**
     * Sets the value of the getTransactionRefNoDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TransactionRefNoDetailsResponse }{@code >}
     *     
     */
    public void setGetTransactionRefNoDetailsResult(JAXBElement<TransactionRefNoDetailsResponse> value) {
        this.getTransactionRefNoDetailsResult = value;
    }

}
