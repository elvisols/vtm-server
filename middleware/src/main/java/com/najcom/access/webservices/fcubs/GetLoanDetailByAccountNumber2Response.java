
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.LoanSummaryResponseBulk;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetLoanDetailByAccountNumber2Result" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}LoanSummaryResponseBulk" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLoanDetailByAccountNumber2Result"
})
@XmlRootElement(name = "GetLoanDetailByAccountNumber2Response")
public class GetLoanDetailByAccountNumber2Response {

    @XmlElementRef(name = "GetLoanDetailByAccountNumber2Result", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<LoanSummaryResponseBulk> getLoanDetailByAccountNumber2Result;

    /**
     * Gets the value of the getLoanDetailByAccountNumber2Result property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LoanSummaryResponseBulk }{@code >}
     *     
     */
    public JAXBElement<LoanSummaryResponseBulk> getGetLoanDetailByAccountNumber2Result() {
        return getLoanDetailByAccountNumber2Result;
    }

    /**
     * Sets the value of the getLoanDetailByAccountNumber2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LoanSummaryResponseBulk }{@code >}
     *     
     */
    public void setGetLoanDetailByAccountNumber2Result(JAXBElement<LoanSummaryResponseBulk> value) {
        this.getLoanDetailByAccountNumber2Result = value;
    }

}
