
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.AccountSearchResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetAccountSearchByAccountTitleResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountSearchResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getAccountSearchByAccountTitleResult"
})
@XmlRootElement(name = "GetAccountSearchByAccountTitleResponse")
public class GetAccountSearchByAccountTitleResponse {

    @XmlElementRef(name = "GetAccountSearchByAccountTitleResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<AccountSearchResponse> getAccountSearchByAccountTitleResult;

    /**
     * Gets the value of the getAccountSearchByAccountTitleResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}
     *     
     */
    public JAXBElement<AccountSearchResponse> getGetAccountSearchByAccountTitleResult() {
        return getAccountSearchByAccountTitleResult;
    }

    /**
     * Sets the value of the getAccountSearchByAccountTitleResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}
     *     
     */
    public void setGetAccountSearchByAccountTitleResult(JAXBElement<AccountSearchResponse> value) {
        this.getAccountSearchByAccountTitleResult = value;
    }

}
