
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfImageCollectionDetails;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SignatureImageCollectionResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfImageCollectionDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "signatureImageCollectionResult"
})
@XmlRootElement(name = "SignatureImageCollectionResponse")
public class SignatureImageCollectionResponse {

    @XmlElementRef(name = "SignatureImageCollectionResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfImageCollectionDetails> signatureImageCollectionResult;

    /**
     * Gets the value of the signatureImageCollectionResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfImageCollectionDetails> getSignatureImageCollectionResult() {
        return signatureImageCollectionResult;
    }

    /**
     * Sets the value of the signatureImageCollectionResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}
     *     
     */
    public void setSignatureImageCollectionResult(JAXBElement<ArrayOfImageCollectionDetails> value) {
        this.signatureImageCollectionResult = value;
    }

}
