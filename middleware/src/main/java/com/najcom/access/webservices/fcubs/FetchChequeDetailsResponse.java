
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ChequeDetailsResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FetchChequeDetailsResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ChequeDetailsResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fetchChequeDetailsResult"
})
@XmlRootElement(name = "FetchChequeDetailsResponse")
public class FetchChequeDetailsResponse {

    @XmlElementRef(name = "FetchChequeDetailsResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<ChequeDetailsResponse> fetchChequeDetailsResult;

    /**
     * Gets the value of the fetchChequeDetailsResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ChequeDetailsResponse }{@code >}
     *     
     */
    public JAXBElement<ChequeDetailsResponse> getFetchChequeDetailsResult() {
        return fetchChequeDetailsResult;
    }

    /**
     * Sets the value of the fetchChequeDetailsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ChequeDetailsResponse }{@code >}
     *     
     */
    public void setFetchChequeDetailsResult(JAXBElement<ChequeDetailsResponse> value) {
        this.fetchChequeDetailsResult = value;
    }

}
