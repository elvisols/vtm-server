
package com.najcom.access.webservices.fcubs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.GetUserBranchResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getUserBranchResult" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}GetUserBranchResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUserBranchResult"
})
@XmlRootElement(name = "getUserBranchResponse")
public class GetUserBranchResponse {

    @XmlElementRef(name = "getUserBranchResult", namespace = "http://FCUBS.Webservices", type = JAXBElement.class, required = false)
    protected JAXBElement<GetUserBranchResp> getUserBranchResult;

    /**
     * Gets the value of the getUserBranchResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetUserBranchResp }{@code >}
     *     
     */
    public JAXBElement<GetUserBranchResp> getGetUserBranchResult() {
        return getUserBranchResult;
    }

    /**
     * Sets the value of the getUserBranchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetUserBranchResp }{@code >}
     *     
     */
    public void setGetUserBranchResult(JAXBElement<GetUserBranchResp> value) {
        this.getUserBranchResult = value;
    }

}
