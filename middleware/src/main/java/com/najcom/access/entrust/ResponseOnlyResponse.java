
package com.najcom.access.entrust;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseOnlyResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseOnlyResult"
})
@XmlRootElement(name = "ResponseOnlyResponse")
public class ResponseOnlyResponse {

    @XmlElement(name = "ResponseOnlyResult")
    protected String responseOnlyResult;

    /**
     * Gets the value of the responseOnlyResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseOnlyResult() {
        return responseOnlyResult;
    }

    /**
     * Sets the value of the responseOnlyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseOnlyResult(String value) {
        this.responseOnlyResult = value;
    }

}
