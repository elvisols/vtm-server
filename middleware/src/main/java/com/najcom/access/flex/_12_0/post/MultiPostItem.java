
package com.najcom.access.flex._12_0.post;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.najcom.access.flex._12.Account;


/**
 * <p>Java class for MultiPostItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiPostItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContraAccount" type="{http://api.accessbankplc.com/core/flex/12.0}Account"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Narration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PaymentReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NoCOT" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TransactionMisCode1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransactionMisCode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompMis" type="{http://api.accessbankplc.com/core/flex/12.0/post}CompMisGroup"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiPostItem", propOrder = {
    "contraAccount",
    "amount",
    "narration",
    "paymentReference",
    "noCOT",
    "transactionMisCode1",
    "transactionMisCode2",
    "compMis"
})
public class MultiPostItem {

    @XmlElement(name = "ContraAccount", required = true)
    protected Account contraAccount;
    @XmlElement(name = "Amount", required = true)
    protected BigDecimal amount;
    @XmlElement(name = "Narration", required = true, nillable = true)
    protected String narration;
    @XmlElement(name = "PaymentReference", required = true, nillable = true)
    protected String paymentReference;
    @XmlElement(name = "NoCOT")
    protected boolean noCOT;
    @XmlElement(name = "TransactionMisCode1", required = true, nillable = true)
    protected String transactionMisCode1;
    @XmlElement(name = "TransactionMisCode2", required = true, nillable = true)
    protected String transactionMisCode2;
    @XmlElement(name = "CompMis", required = true, nillable = true)
    protected CompMisGroup compMis;

    /**
     * Gets the value of the contraAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Account }
     *     
     */
    public Account getContraAccount() {
        return contraAccount;
    }

    /**
     * Sets the value of the contraAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Account }
     *     
     */
    public void setContraAccount(Account value) {
        this.contraAccount = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the narration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets the value of the narration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNarration(String value) {
        this.narration = value;
    }

    /**
     * Gets the value of the paymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * Sets the value of the paymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReference(String value) {
        this.paymentReference = value;
    }

    /**
     * Gets the value of the noCOT property.
     * 
     */
    public boolean isNoCOT() {
        return noCOT;
    }

    /**
     * Sets the value of the noCOT property.
     * 
     */
    public void setNoCOT(boolean value) {
        this.noCOT = value;
    }

    /**
     * Gets the value of the transactionMisCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionMisCode1() {
        return transactionMisCode1;
    }

    /**
     * Sets the value of the transactionMisCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionMisCode1(String value) {
        this.transactionMisCode1 = value;
    }

    /**
     * Gets the value of the transactionMisCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionMisCode2() {
        return transactionMisCode2;
    }

    /**
     * Sets the value of the transactionMisCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionMisCode2(String value) {
        this.transactionMisCode2 = value;
    }

    /**
     * Gets the value of the compMis property.
     * 
     * @return
     *     possible object is
     *     {@link CompMisGroup }
     *     
     */
    public CompMisGroup getCompMis() {
        return compMis;
    }

    /**
     * Sets the value of the compMis property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompMisGroup }
     *     
     */
    public void setCompMis(CompMisGroup value) {
        this.compMis = value;
    }

}
