
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.accessbankplc.api.core.flex._12_0.post package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.accessbankplc.api.core.flex._12_0.post
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MultiPostWithMIS }
     * 
     */
    public MultiPostWithMIS createMultiPostWithMIS() {
        return new MultiPostWithMIS();
    }

    /**
     * Create an instance of {@link MultiPostWithMISReq }
     * 
     */
    public MultiPostWithMISReq createMultiPostWithMISReq() {
        return new MultiPostWithMISReq();
    }

    /**
     * Create an instance of {@link BatchPostResponse }
     * 
     */
    public BatchPostResponse createBatchPostResponse() {
        return new BatchPostResponse();
    }

    /**
     * Create an instance of {@link FlexPostRsp }
     * 
     */
    public FlexPostRsp createFlexPostRsp() {
        return new FlexPostRsp();
    }

    /**
     * Create an instance of {@link SinglePostWithMIS }
     * 
     */
    public SinglePostWithMIS createSinglePostWithMIS() {
        return new SinglePostWithMIS();
    }

    /**
     * Create an instance of {@link SinglePostWithMISReq }
     * 
     */
    public SinglePostWithMISReq createSinglePostWithMISReq() {
        return new SinglePostWithMISReq();
    }

    /**
     * Create an instance of {@link BatchPost }
     * 
     */
    public BatchPost createBatchPost() {
        return new BatchPost();
    }

    /**
     * Create an instance of {@link BatchPostReq }
     * 
     */
    public BatchPostReq createBatchPostReq() {
        return new BatchPostReq();
    }

    /**
     * Create an instance of {@link ReversalPost }
     * 
     */
    public ReversalPost createReversalPost() {
        return new ReversalPost();
    }

    /**
     * Create an instance of {@link PostReq }
     * 
     */
    public PostReq createPostReq() {
        return new PostReq();
    }

    /**
     * Create an instance of {@link MultiPostResponse }
     * 
     */
    public MultiPostResponse createMultiPostResponse() {
        return new MultiPostResponse();
    }

    /**
     * Create an instance of {@link MultiPost }
     * 
     */
    public MultiPost createMultiPost() {
        return new MultiPost();
    }

    /**
     * Create an instance of {@link MultiPostReq }
     * 
     */
    public MultiPostReq createMultiPostReq() {
        return new MultiPostReq();
    }

    /**
     * Create an instance of {@link SinglePostResponse }
     * 
     */
    public SinglePostResponse createSinglePostResponse() {
        return new SinglePostResponse();
    }

    /**
     * Create an instance of {@link ReversalPostResponse }
     * 
     */
    public ReversalPostResponse createReversalPostResponse() {
        return new ReversalPostResponse();
    }

    /**
     * Create an instance of {@link SinglePostWithMISResponse }
     * 
     */
    public SinglePostWithMISResponse createSinglePostWithMISResponse() {
        return new SinglePostWithMISResponse();
    }

    /**
     * Create an instance of {@link SinglePost }
     * 
     */
    public SinglePost createSinglePost() {
        return new SinglePost();
    }

    /**
     * Create an instance of {@link SinglePostReq }
     * 
     */
    public SinglePostReq createSinglePostReq() {
        return new SinglePostReq();
    }

    /**
     * Create an instance of {@link MultiPostWithMISResponse }
     * 
     */
    public MultiPostWithMISResponse createMultiPostWithMISResponse() {
        return new MultiPostWithMISResponse();
    }

    /**
     * Create an instance of {@link TranBase }
     * 
     */
    public TranBase createTranBase() {
        return new TranBase();
    }

    /**
     * Create an instance of {@link SingleEntryWithMIS }
     * 
     */
    public SingleEntryWithMIS createSingleEntryWithMIS() {
        return new SingleEntryWithMIS();
    }

    /**
     * Create an instance of {@link MultiEntry }
     * 
     */
    public MultiEntry createMultiEntry() {
        return new MultiEntry();
    }

    /**
     * Create an instance of {@link SingleEntry }
     * 
     */
    public SingleEntry createSingleEntry() {
        return new SingleEntry();
    }

    /**
     * Create an instance of {@link BatchPostItem }
     * 
     */
    public BatchPostItem createBatchPostItem() {
        return new BatchPostItem();
    }

    /**
     * Create an instance of {@link MultiPostItem }
     * 
     */
    public MultiPostItem createMultiPostItem() {
        return new MultiPostItem();
    }

    /**
     * Create an instance of {@link MultiPostItems }
     * 
     */
    public MultiPostItems createMultiPostItems() {
        return new MultiPostItems();
    }

    /**
     * Create an instance of {@link CompMisGroup }
     * 
     */
    public CompMisGroup createCompMisGroup() {
        return new CompMisGroup();
    }

    /**
     * Create an instance of {@link MultiEntryWithMIS }
     * 
     */
    public MultiEntryWithMIS createMultiEntryWithMIS() {
        return new MultiEntryWithMIS();
    }

    /**
     * Create an instance of {@link BatchEntry }
     * 
     */
    public BatchEntry createBatchEntry() {
        return new BatchEntry();
    }

    /**
     * Create an instance of {@link BatchPostItems }
     * 
     */
    public BatchPostItems createBatchPostItems() {
        return new BatchPostItems();
    }

}
