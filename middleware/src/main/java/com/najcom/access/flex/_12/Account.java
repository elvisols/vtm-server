
package com.najcom.access.flex._12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Account complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Account">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AcctType" type="{http://api.accessbankplc.com/core/flex/12.0}AccountType"/>
 *         &lt;element name="HomeBranch" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://api.accessbankplc.com/core/flex/12.0}ISOCurrency" minOccurs="0"/>
 *         &lt;element name="InstrumentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Account", propOrder = {
    "acctNo",
    "acctType",
    "homeBranch",
    "currency",
    "instrumentCode"
})
public class Account {

    @XmlElement(name = "AcctNo", required = true, nillable = true)
    protected String acctNo;
    @XmlElement(name = "AcctType", required = true)
    @XmlSchemaType(name = "string")
    protected AccountType acctType;
    @XmlElement(name = "HomeBranch")
    protected Integer homeBranch;
    @XmlElement(name = "Currency")
    @XmlSchemaType(name = "string")
    protected ISOCurrency currency;
    @XmlElement(name = "InstrumentCode")
    protected String instrumentCode;

    /**
     * Gets the value of the acctNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctNo() {
        return acctNo;
    }

    /**
     * Sets the value of the acctNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctNo(String value) {
        this.acctNo = value;
    }

    /**
     * Gets the value of the acctType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountType }
     *     
     */
    public AccountType getAcctType() {
        return acctType;
    }

    /**
     * Sets the value of the acctType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountType }
     *     
     */
    public void setAcctType(AccountType value) {
        this.acctType = value;
    }

    /**
     * Gets the value of the homeBranch property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHomeBranch() {
        return homeBranch;
    }

    /**
     * Sets the value of the homeBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHomeBranch(Integer value) {
        this.homeBranch = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link ISOCurrency }
     *     
     */
    public ISOCurrency getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISOCurrency }
     *     
     */
    public void setCurrency(ISOCurrency value) {
        this.currency = value;
    }

    /**
     * Gets the value of the instrumentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstrumentCode() {
        return instrumentCode;
    }

    /**
     * Sets the value of the instrumentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstrumentCode(String value) {
        this.instrumentCode = value;
    }

}
