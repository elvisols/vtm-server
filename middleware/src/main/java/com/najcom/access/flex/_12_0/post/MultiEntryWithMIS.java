
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MultiEntryWithMIS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiEntryWithMIS">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.accessbankplc.com/core/flex/12.0/post}MultiEntry">
 *       &lt;sequence>
 *         &lt;element name="TransactionMisCode1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransactionMisCode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompMis" type="{http://api.accessbankplc.com/core/flex/12.0/post}CompMisGroup"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiEntryWithMIS", propOrder = {
    "transactionMisCode1",
    "transactionMisCode2",
    "compMis"
})
public class MultiEntryWithMIS
    extends MultiEntry
{

    @XmlElement(name = "TransactionMisCode1", required = true)
    protected String transactionMisCode1;
    @XmlElement(name = "TransactionMisCode2", required = true)
    protected String transactionMisCode2;
    @XmlElement(name = "CompMis", required = true)
    protected CompMisGroup compMis;

    /**
     * Gets the value of the transactionMisCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionMisCode1() {
        return transactionMisCode1;
    }

    /**
     * Sets the value of the transactionMisCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionMisCode1(String value) {
        this.transactionMisCode1 = value;
    }

    /**
     * Gets the value of the transactionMisCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionMisCode2() {
        return transactionMisCode2;
    }

    /**
     * Sets the value of the transactionMisCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionMisCode2(String value) {
        this.transactionMisCode2 = value;
    }

    /**
     * Gets the value of the compMis property.
     * 
     * @return
     *     possible object is
     *     {@link CompMisGroup }
     *     
     */
    public CompMisGroup getCompMis() {
        return compMis;
    }

    /**
     * Sets the value of the compMis property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompMisGroup }
     *     
     */
    public void setCompMis(CompMisGroup value) {
        this.compMis = value;
    }

}
