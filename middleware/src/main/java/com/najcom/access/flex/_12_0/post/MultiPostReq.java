
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MultiPostReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiPostReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.accessbankplc.com/core/flex/12.0/post}PostReq">
 *       &lt;sequence>
 *         &lt;element name="Posting" type="{http://api.accessbankplc.com/core/flex/12.0/post}MultiEntry"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiPostReq", propOrder = {
    "posting"
})
public class MultiPostReq
    extends PostReq
{

    @XmlElement(name = "Posting", required = true)
    protected MultiEntry posting;

    /**
     * Gets the value of the posting property.
     * 
     * @return
     *     possible object is
     *     {@link MultiEntry }
     *     
     */
    public MultiEntry getPosting() {
        return posting;
    }

    /**
     * Sets the value of the posting property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiEntry }
     *     
     */
    public void setPosting(MultiEntry value) {
        this.posting = value;
    }

}
