
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SinglePostReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SinglePostReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.accessbankplc.com/core/flex/12.0/post}PostReq">
 *       &lt;sequence>
 *         &lt;element name="Posting" type="{http://api.accessbankplc.com/core/flex/12.0/post}SingleEntry"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SinglePostReq", propOrder = {
    "posting"
})
public class SinglePostReq
    extends PostReq
{

    @XmlElement(name = "Posting", required = true)
    protected SingleEntry posting;

    /**
     * Gets the value of the posting property.
     * 
     * @return
     *     possible object is
     *     {@link SingleEntry }
     *     
     */
    public SingleEntry getPosting() {
        return posting;
    }

    /**
     * Sets the value of the posting property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleEntry }
     *     
     */
    public void setPosting(SingleEntry value) {
        this.posting = value;
    }

}
