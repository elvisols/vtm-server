
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MultiPostType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MultiPostType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MultiCredit"/>
 *     &lt;enumeration value="MultiDebit"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MultiPostType")
@XmlEnum
public enum MultiPostType {

    @XmlEnumValue("MultiCredit")
    MULTI_CREDIT("MultiCredit"),
    @XmlEnumValue("MultiDebit")
    MULTI_DEBIT("MultiDebit");
    private final String value;

    MultiPostType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MultiPostType fromValue(String v) {
        for (MultiPostType c: MultiPostType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
