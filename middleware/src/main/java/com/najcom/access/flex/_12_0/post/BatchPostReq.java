
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BatchPostReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BatchPostReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.accessbankplc.com/core/flex/12.0/post}PostReq">
 *       &lt;sequence>
 *         &lt;element name="Posting" type="{http://api.accessbankplc.com/core/flex/12.0/post}BatchEntry"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchPostReq", propOrder = {
    "posting"
})
public class BatchPostReq
    extends PostReq
{

    @XmlElement(name = "Posting", required = true)
    protected BatchEntry posting;

    /**
     * Gets the value of the posting property.
     * 
     * @return
     *     possible object is
     *     {@link BatchEntry }
     *     
     */
    public BatchEntry getPosting() {
        return posting;
    }

    /**
     * Sets the value of the posting property.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchEntry }
     *     
     */
    public void setPosting(BatchEntry value) {
        this.posting = value;
    }

}
