
package com.najcom.access.flex._12_0.post;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.najcom.access.flex._12.Account;
import com.najcom.access.flex._12.ISOCurrency;


/**
 * <p>Java class for SingleEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SingleEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditAccount" type="{http://api.accessbankplc.com/core/flex/12.0}Account"/>
 *         &lt;element name="DebitAccount" type="{http://api.accessbankplc.com/core/flex/12.0}Account"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Currency" type="{http://api.accessbankplc.com/core/flex/12.0}ISOCurrency"/>
 *         &lt;element name="Narration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PaymentReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NoCOT" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="PostingBranch" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BatchNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SingleEntry", propOrder = {
    "creditAccount",
    "debitAccount",
    "amount",
    "currency",
    "narration",
    "paymentReference",
    "noCOT",
    "valueDate",
    "postingBranch",
    "batchNumber"
})
@XmlSeeAlso({
    SingleEntryWithMIS.class
})
public class SingleEntry {

    @XmlElement(name = "CreditAccount", required = true)
    protected Account creditAccount;
    @XmlElement(name = "DebitAccount", required = true)
    protected Account debitAccount;
    @XmlElement(name = "Amount", required = true)
    protected BigDecimal amount;
    @XmlElement(name = "Currency", required = true)
    @XmlSchemaType(name = "string")
    protected ISOCurrency currency;
    @XmlElement(name = "Narration", required = true, nillable = true)
    protected String narration;
    @XmlElement(name = "PaymentReference", required = true, nillable = true)
    protected String paymentReference;
    @XmlElement(name = "NoCOT")
    protected boolean noCOT;
    @XmlElement(name = "ValueDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valueDate;
    @XmlElement(name = "PostingBranch")
    protected int postingBranch;
    @XmlElement(name = "BatchNumber")
    protected int batchNumber;

    /**
     * Gets the value of the creditAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Account }
     *     
     */
    public Account getCreditAccount() {
        return creditAccount;
    }

    /**
     * Sets the value of the creditAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Account }
     *     
     */
    public void setCreditAccount(Account value) {
        this.creditAccount = value;
    }

    /**
     * Gets the value of the debitAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Account }
     *     
     */
    public Account getDebitAccount() {
        return debitAccount;
    }

    /**
     * Sets the value of the debitAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Account }
     *     
     */
    public void setDebitAccount(Account value) {
        this.debitAccount = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link ISOCurrency }
     *     
     */
    public ISOCurrency getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISOCurrency }
     *     
     */
    public void setCurrency(ISOCurrency value) {
        this.currency = value;
    }

    /**
     * Gets the value of the narration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets the value of the narration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNarration(String value) {
        this.narration = value;
    }

    /**
     * Gets the value of the paymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * Sets the value of the paymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReference(String value) {
        this.paymentReference = value;
    }

    /**
     * Gets the value of the noCOT property.
     * 
     */
    public boolean isNoCOT() {
        return noCOT;
    }

    /**
     * Sets the value of the noCOT property.
     * 
     */
    public void setNoCOT(boolean value) {
        this.noCOT = value;
    }

    /**
     * Gets the value of the valueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValueDate() {
        return valueDate;
    }

    /**
     * Sets the value of the valueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValueDate(XMLGregorianCalendar value) {
        this.valueDate = value;
    }

    /**
     * Gets the value of the postingBranch property.
     * 
     */
    public int getPostingBranch() {
        return postingBranch;
    }

    /**
     * Sets the value of the postingBranch property.
     * 
     */
    public void setPostingBranch(int value) {
        this.postingBranch = value;
    }

    /**
     * Gets the value of the batchNumber property.
     * 
     */
    public int getBatchNumber() {
        return batchNumber;
    }

    /**
     * Sets the value of the batchNumber property.
     * 
     */
    public void setBatchNumber(int value) {
        this.batchNumber = value;
    }

}
