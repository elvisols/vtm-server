
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MultiPostWithMISReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiPostWithMISReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.accessbankplc.com/core/flex/12.0/post}PostReq">
 *       &lt;sequence>
 *         &lt;element name="Posting" type="{http://api.accessbankplc.com/core/flex/12.0/post}MultiEntryWithMIS"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiPostWithMISReq", propOrder = {
    "posting"
})
public class MultiPostWithMISReq
    extends PostReq
{

    @XmlElement(name = "Posting", required = true)
    protected MultiEntryWithMIS posting;

    /**
     * Gets the value of the posting property.
     * 
     * @return
     *     possible object is
     *     {@link MultiEntryWithMIS }
     *     
     */
    public MultiEntryWithMIS getPosting() {
        return posting;
    }

    /**
     * Sets the value of the posting property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiEntryWithMIS }
     *     
     */
    public void setPosting(MultiEntryWithMIS value) {
        this.posting = value;
    }

}
