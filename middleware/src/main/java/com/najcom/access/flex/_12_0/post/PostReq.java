
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.najcom.access.flex._12.FlexcubeUser;


/**
 * <p>Java class for PostReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PostReq">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MsgId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModuleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Initiator" type="{http://api.accessbankplc.com/core/flex/12.0}FlexcubeUser" minOccurs="0"/>
 *         &lt;element name="Approver" type="{http://api.accessbankplc.com/core/flex/12.0}FlexcubeUser" minOccurs="0"/>
 *         &lt;element name="AuditDetails" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostReq", propOrder = {
    "msgId",
    "moduleName",
    "initiator",
    "approver",
    "auditDetails"
})
@XmlSeeAlso({
    MultiPostWithMISReq.class,
    SinglePostWithMISReq.class,
    BatchPostReq.class,
    MultiPostReq.class,
    SinglePostReq.class
})
public class PostReq {

    @XmlElement(name = "MsgId", required = true, nillable = true)
    protected String msgId;
    @XmlElement(name = "ModuleName", required = true, nillable = true)
    protected String moduleName;
    @XmlElement(name = "Initiator")
    protected FlexcubeUser initiator;
    @XmlElement(name = "Approver")
    protected FlexcubeUser approver;
    @XmlElement(name = "AuditDetails", required = true, nillable = true)
    protected String auditDetails;

    /**
     * Gets the value of the msgId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgId() {
        return msgId;
    }

    /**
     * Sets the value of the msgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgId(String value) {
        this.msgId = value;
    }

    /**
     * Gets the value of the moduleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * Sets the value of the moduleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModuleName(String value) {
        this.moduleName = value;
    }

    /**
     * Gets the value of the initiator property.
     * 
     * @return
     *     possible object is
     *     {@link FlexcubeUser }
     *     
     */
    public FlexcubeUser getInitiator() {
        return initiator;
    }

    /**
     * Sets the value of the initiator property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexcubeUser }
     *     
     */
    public void setInitiator(FlexcubeUser value) {
        this.initiator = value;
    }

    /**
     * Gets the value of the approver property.
     * 
     * @return
     *     possible object is
     *     {@link FlexcubeUser }
     *     
     */
    public FlexcubeUser getApprover() {
        return approver;
    }

    /**
     * Sets the value of the approver property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexcubeUser }
     *     
     */
    public void setApprover(FlexcubeUser value) {
        this.approver = value;
    }

    /**
     * Gets the value of the auditDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuditDetails() {
        return auditDetails;
    }

    /**
     * Sets the value of the auditDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditDetails(String value) {
        this.auditDetails = value;
    }

}
