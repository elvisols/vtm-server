
package com.najcom.access.flex._12;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISOCurrency.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ISOCurrency">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NGN"/>
 *     &lt;enumeration value="CNY"/>
 *     &lt;enumeration value="USD"/>
 *     &lt;enumeration value="EUR"/>
 *     &lt;enumeration value="GBP"/>
 *     &lt;enumeration value="DEM"/>
 *     &lt;enumeration value="JPY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ISOCurrency")
@XmlEnum
public enum ISOCurrency {

    NGN,
    CNY,
    USD,
    EUR,
    GBP,
    DEM,
    JPY;

    public String value() {
        return name();
    }

    public static ISOCurrency fromValue(String v) {
        return valueOf(v);
    }

}
