
package com.najcom.access.flex._12;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItemTranType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ItemTranType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CR"/>
 *     &lt;enumeration value="DR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ItemTranType")
@XmlEnum
public enum ItemTranType {

    CR,
    DR;

    public String value() {
        return name();
    }

    public static ItemTranType fromValue(String v) {
        return valueOf(v);
    }

}
