
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.najcom.access.flex._12.Account;
import com.najcom.access.flex._12.ISOCurrency;


/**
 * <p>Java class for MultiEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultiEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Items" type="{http://api.accessbankplc.com/core/flex/12.0/post}MultiPostItems"/>
 *         &lt;element name="ActionAccount" type="{http://api.accessbankplc.com/core/flex/12.0}Account"/>
 *         &lt;element name="PaymentReference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Narration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MultiPostType" type="{http://api.accessbankplc.com/core/flex/12.0/post}MultiPostType"/>
 *         &lt;element name="Currency" type="{http://api.accessbankplc.com/core/flex/12.0}ISOCurrency"/>
 *         &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="PostingBranch" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BatchNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultiEntry", propOrder = {
    "items",
    "actionAccount",
    "paymentReference",
    "narration",
    "multiPostType",
    "currency",
    "valueDate",
    "postingBranch",
    "batchNumber"
})
@XmlSeeAlso({
    MultiEntryWithMIS.class
})
public class MultiEntry {

    @XmlElement(name = "Items", required = true)
    protected MultiPostItems items;
    @XmlElement(name = "ActionAccount", required = true)
    protected Account actionAccount;
    @XmlElement(name = "PaymentReference", required = true, nillable = true)
    protected String paymentReference;
    @XmlElement(name = "Narration", required = true, nillable = true)
    protected String narration;
    @XmlElement(name = "MultiPostType", required = true)
    @XmlSchemaType(name = "string")
    protected MultiPostType multiPostType;
    @XmlElement(name = "Currency", required = true)
    @XmlSchemaType(name = "string")
    protected ISOCurrency currency;
    @XmlElement(name = "ValueDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar valueDate;
    @XmlElement(name = "PostingBranch")
    protected int postingBranch;
    @XmlElement(name = "BatchNumber")
    protected int batchNumber;

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link MultiPostItems }
     *     
     */
    public MultiPostItems getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiPostItems }
     *     
     */
    public void setItems(MultiPostItems value) {
        this.items = value;
    }

    /**
     * Gets the value of the actionAccount property.
     * 
     * @return
     *     possible object is
     *     {@link Account }
     *     
     */
    public Account getActionAccount() {
        return actionAccount;
    }

    /**
     * Sets the value of the actionAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Account }
     *     
     */
    public void setActionAccount(Account value) {
        this.actionAccount = value;
    }

    /**
     * Gets the value of the paymentReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentReference() {
        return paymentReference;
    }

    /**
     * Sets the value of the paymentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentReference(String value) {
        this.paymentReference = value;
    }

    /**
     * Gets the value of the narration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNarration() {
        return narration;
    }

    /**
     * Sets the value of the narration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNarration(String value) {
        this.narration = value;
    }

    /**
     * Gets the value of the multiPostType property.
     * 
     * @return
     *     possible object is
     *     {@link MultiPostType }
     *     
     */
    public MultiPostType getMultiPostType() {
        return multiPostType;
    }

    /**
     * Sets the value of the multiPostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultiPostType }
     *     
     */
    public void setMultiPostType(MultiPostType value) {
        this.multiPostType = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link ISOCurrency }
     *     
     */
    public ISOCurrency getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link ISOCurrency }
     *     
     */
    public void setCurrency(ISOCurrency value) {
        this.currency = value;
    }

    /**
     * Gets the value of the valueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getValueDate() {
        return valueDate;
    }

    /**
     * Sets the value of the valueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setValueDate(XMLGregorianCalendar value) {
        this.valueDate = value;
    }

    /**
     * Gets the value of the postingBranch property.
     * 
     */
    public int getPostingBranch() {
        return postingBranch;
    }

    /**
     * Sets the value of the postingBranch property.
     * 
     */
    public void setPostingBranch(int value) {
        this.postingBranch = value;
    }

    /**
     * Gets the value of the batchNumber property.
     * 
     */
    public int getBatchNumber() {
        return batchNumber;
    }

    /**
     * Sets the value of the batchNumber property.
     * 
     */
    public void setBatchNumber(int value) {
        this.batchNumber = value;
    }

}
