/**
 * Service: FlexPost Service
 *     Version: 3.0
 *     Owner: Access Bank Plc
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://api.accessbankplc.com/core/flex/12.0/post", 
xmlns = { 
@javax.xml.bind.annotation.XmlNs(prefix = "post", namespaceURI="http://api.accessbankplc.com/core/flex/12.0/post")
},
elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.najcom.access.flex._12_0.post;
