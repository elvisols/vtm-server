
package com.najcom.access.flex._12_0.post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SinglePostWithMISReq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SinglePostWithMISReq">
 *   &lt;complexContent>
 *     &lt;extension base="{http://api.accessbankplc.com/core/flex/12.0/post}PostReq">
 *       &lt;sequence>
 *         &lt;element name="Posting" type="{http://api.accessbankplc.com/core/flex/12.0/post}SingleEntryWithMIS"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SinglePostWithMISReq", propOrder = {
    "posting"
})
public class SinglePostWithMISReq
    extends PostReq
{

    @XmlElement(name = "Posting", required = true)
    protected SingleEntryWithMIS posting;

    /**
     * Gets the value of the posting property.
     * 
     * @return
     *     possible object is
     *     {@link SingleEntryWithMIS }
     *     
     */
    public SingleEntryWithMIS getPosting() {
        return posting;
    }

    /**
     * Sets the value of the posting property.
     * 
     * @param value
     *     allowed object is
     *     {@link SingleEntryWithMIS }
     *     
     */
    public void setPosting(SingleEntryWithMIS value) {
        this.posting = value;
    }

}
