
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FixedDepositResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FixedDepositResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FcubsResponse">
 *       &lt;sequence>
 *         &lt;element name="FixedDepositInfo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfFixedDepositInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FixedDepositResponse", propOrder = {
    "fixedDepositInfo"
})
public class FixedDepositResponse
    extends FcubsResponse
{

    @XmlElementRef(name = "FixedDepositInfo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFixedDepositInfo> fixedDepositInfo;

    /**
     * Gets the value of the fixedDepositInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFixedDepositInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFixedDepositInfo> getFixedDepositInfo() {
        return fixedDepositInfo;
    }

    /**
     * Sets the value of the fixedDepositInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFixedDepositInfo }{@code >}
     *     
     */
    public void setFixedDepositInfo(JAXBElement<ArrayOfFixedDepositInfo> value) {
        this.fixedDepositInfo = value;
    }

}
