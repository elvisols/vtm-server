
package com.najcom.access.datacontract.webservicewrappers;

import java.math.BigDecimal;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import com.najcom.access.datacontract.ArrayOfChequeNumberDetails;
import com.najcom.access.datacontract.ArrayOfTransactionRefNoDetails;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.fcubs_webservices_shared_webservicewrappers package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfMobileTransactionDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "ArrayOfMobileTransactionDetail");
    private final static QName _ChequeNoInquiryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "ChequeNoInquiryResponse");
    private final static QName _VansoCustomerIdResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "VansoCustomerIdResponse");
    private final static QName _AccountVerificationResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "AccountVerificationResponse");
    private final static QName _MobileTransactionDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "MobileTransactionDetail");
    private final static QName _ArrayOfMobileAccountDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "ArrayOfMobileAccountDetail");
    private final static QName _MessageIDStatusResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", "MessageIDStatusResponse");
    private final static QName _TransactionRefNoDetailsResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", "TransactionRefNoDetailsResponse");
    private final static QName _MobileAccountDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "MobileAccountDetail");
    private final static QName _MobileAccountDetailAccountType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "accountType");
    private final static QName _MobileAccountDetailAccountStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "accountStatus");
    private final static QName _MobileAccountDetailAvailableBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "availableBalance");
    private final static QName _MobileAccountDetailAccountNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "accountNumber");
    private final static QName _MobileAccountDetailBookBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "bookBalance");
    private final static QName _MobileAccountDetailAccountCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "accountCurrency");
    private final static QName _AccountVerificationResponseCustNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "cust_no");
    private final static QName _AccountVerificationResponseEMail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "e_mail");
    private final static QName _AccountVerificationResponseTELEPHONE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "TELEPHONE");
    private final static QName _AccountVerificationResponseCustomerName1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "customer_name1");
    private final static QName _MobileTransactionDetailNarration_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "narration");
    private final static QName _MobileTransactionDetailTransactionAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "transactionAmount");
    private final static QName _MobileTransactionDetailTransactionCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "transactionCurrency");
    private final static QName _MobileTransactionDetailTransactionType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "transactionType");
    private final static QName _MobileTransactionDetailTransactionDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "transactionDate");
    private final static QName _MessageIDStatusResponseErrorMessage_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", "ErrorMessage");
    private final static QName _MessageIDStatusResponseStatusFlag_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", "StatusFlag");
    private final static QName _MessageIDStatusResponseTransactionStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", "TransactionStatus");
    private final static QName _ChequeNoInquiryResponseChequeNumberDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", "chequeNumberDetails");
    private final static QName _TransactionRefNoDetailsResponseTransactiondetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", "transactiondetail");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.fcubs_webservices_shared_webservicewrappers
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionRefNoDetailsResponse }
     * 
     */
    public TransactionRefNoDetailsResponse createTransactionRefNoDetailsResponse() {
        return new TransactionRefNoDetailsResponse();
    }

    /**
     * Create an instance of {@link ChequeNoInquiryResponse }
     * 
     */
    public ChequeNoInquiryResponse createChequeNoInquiryResponse() {
        return new ChequeNoInquiryResponse();
    }

    /**
     * Create an instance of {@link AccountVerificationResponse }
     * 
     */
    public AccountVerificationResponse createAccountVerificationResponse() {
        return new AccountVerificationResponse();
    }

    /**
     * Create an instance of {@link ArrayOfMobileAccountDetail }
     * 
     */
    public ArrayOfMobileAccountDetail createArrayOfMobileAccountDetail() {
        return new ArrayOfMobileAccountDetail();
    }

    /**
     * Create an instance of {@link ArrayOfMobileTransactionDetail }
     * 
     */
    public ArrayOfMobileTransactionDetail createArrayOfMobileTransactionDetail() {
        return new ArrayOfMobileTransactionDetail();
    }

    /**
     * Create an instance of {@link MessageIDStatusResponse }
     * 
     */
    public MessageIDStatusResponse createMessageIDStatusResponse() {
        return new MessageIDStatusResponse();
    }

    /**
     * Create an instance of {@link VansoCustomerIdResponse }
     * 
     */
    public VansoCustomerIdResponse createVansoCustomerIdResponse() {
        return new VansoCustomerIdResponse();
    }

    /**
     * Create an instance of {@link MobileTransactionDetail }
     * 
     */
    public MobileTransactionDetail createMobileTransactionDetail() {
        return new MobileTransactionDetail();
    }

    /**
     * Create an instance of {@link MobileAccountDetail }
     * 
     */
    public MobileAccountDetail createMobileAccountDetail() {
        return new MobileAccountDetail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMobileTransactionDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "ArrayOfMobileTransactionDetail")
    public JAXBElement<ArrayOfMobileTransactionDetail> createArrayOfMobileTransactionDetail(ArrayOfMobileTransactionDetail value) {
        return new JAXBElement<ArrayOfMobileTransactionDetail>(_ArrayOfMobileTransactionDetail_QNAME, ArrayOfMobileTransactionDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeNoInquiryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "ChequeNoInquiryResponse")
    public JAXBElement<ChequeNoInquiryResponse> createChequeNoInquiryResponse(ChequeNoInquiryResponse value) {
        return new JAXBElement<ChequeNoInquiryResponse>(_ChequeNoInquiryResponse_QNAME, ChequeNoInquiryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VansoCustomerIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "VansoCustomerIdResponse")
    public JAXBElement<VansoCustomerIdResponse> createVansoCustomerIdResponse(VansoCustomerIdResponse value) {
        return new JAXBElement<VansoCustomerIdResponse>(_VansoCustomerIdResponse_QNAME, VansoCustomerIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountVerificationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "AccountVerificationResponse")
    public JAXBElement<AccountVerificationResponse> createAccountVerificationResponse(AccountVerificationResponse value) {
        return new JAXBElement<AccountVerificationResponse>(_AccountVerificationResponse_QNAME, AccountVerificationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileTransactionDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "MobileTransactionDetail")
    public JAXBElement<MobileTransactionDetail> createMobileTransactionDetail(MobileTransactionDetail value) {
        return new JAXBElement<MobileTransactionDetail>(_MobileTransactionDetail_QNAME, MobileTransactionDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMobileAccountDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "ArrayOfMobileAccountDetail")
    public JAXBElement<ArrayOfMobileAccountDetail> createArrayOfMobileAccountDetail(ArrayOfMobileAccountDetail value) {
        return new JAXBElement<ArrayOfMobileAccountDetail>(_ArrayOfMobileAccountDetail_QNAME, ArrayOfMobileAccountDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessageIDStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", name = "MessageIDStatusResponse")
    public JAXBElement<MessageIDStatusResponse> createMessageIDStatusResponse(MessageIDStatusResponse value) {
        return new JAXBElement<MessageIDStatusResponse>(_MessageIDStatusResponse_QNAME, MessageIDStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionRefNoDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", name = "TransactionRefNoDetailsResponse")
    public JAXBElement<TransactionRefNoDetailsResponse> createTransactionRefNoDetailsResponse(TransactionRefNoDetailsResponse value) {
        return new JAXBElement<TransactionRefNoDetailsResponse>(_TransactionRefNoDetailsResponse_QNAME, TransactionRefNoDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileAccountDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "MobileAccountDetail")
    public JAXBElement<MobileAccountDetail> createMobileAccountDetail(MobileAccountDetail value) {
        return new JAXBElement<MobileAccountDetail>(_MobileAccountDetail_QNAME, MobileAccountDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "accountType", scope = MobileAccountDetail.class)
    public JAXBElement<String> createMobileAccountDetailAccountType(String value) {
        return new JAXBElement<String>(_MobileAccountDetailAccountType_QNAME, String.class, MobileAccountDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "accountStatus", scope = MobileAccountDetail.class)
    public JAXBElement<String> createMobileAccountDetailAccountStatus(String value) {
        return new JAXBElement<String>(_MobileAccountDetailAccountStatus_QNAME, String.class, MobileAccountDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "availableBalance", scope = MobileAccountDetail.class)
    public JAXBElement<BigDecimal> createMobileAccountDetailAvailableBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_MobileAccountDetailAvailableBalance_QNAME, BigDecimal.class, MobileAccountDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "accountNumber", scope = MobileAccountDetail.class)
    public JAXBElement<String> createMobileAccountDetailAccountNumber(String value) {
        return new JAXBElement<String>(_MobileAccountDetailAccountNumber_QNAME, String.class, MobileAccountDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "bookBalance", scope = MobileAccountDetail.class)
    public JAXBElement<BigDecimal> createMobileAccountDetailBookBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_MobileAccountDetailBookBalance_QNAME, BigDecimal.class, MobileAccountDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "accountCurrency", scope = MobileAccountDetail.class)
    public JAXBElement<String> createMobileAccountDetailAccountCurrency(String value) {
        return new JAXBElement<String>(_MobileAccountDetailAccountCurrency_QNAME, String.class, MobileAccountDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "cust_no", scope = AccountVerificationResponse.class)
    public JAXBElement<String> createAccountVerificationResponseCustNo(String value) {
        return new JAXBElement<String>(_AccountVerificationResponseCustNo_QNAME, String.class, AccountVerificationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "e_mail", scope = AccountVerificationResponse.class)
    public JAXBElement<String> createAccountVerificationResponseEMail(String value) {
        return new JAXBElement<String>(_AccountVerificationResponseEMail_QNAME, String.class, AccountVerificationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "TELEPHONE", scope = AccountVerificationResponse.class)
    public JAXBElement<String> createAccountVerificationResponseTELEPHONE(String value) {
        return new JAXBElement<String>(_AccountVerificationResponseTELEPHONE_QNAME, String.class, AccountVerificationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "customer_name1", scope = AccountVerificationResponse.class)
    public JAXBElement<String> createAccountVerificationResponseCustomerName1(String value) {
        return new JAXBElement<String>(_AccountVerificationResponseCustomerName1_QNAME, String.class, AccountVerificationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "narration", scope = MobileTransactionDetail.class)
    public JAXBElement<String> createMobileTransactionDetailNarration(String value) {
        return new JAXBElement<String>(_MobileTransactionDetailNarration_QNAME, String.class, MobileTransactionDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "transactionAmount", scope = MobileTransactionDetail.class)
    public JAXBElement<BigDecimal> createMobileTransactionDetailTransactionAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_MobileTransactionDetailTransactionAmount_QNAME, BigDecimal.class, MobileTransactionDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "transactionCurrency", scope = MobileTransactionDetail.class)
    public JAXBElement<String> createMobileTransactionDetailTransactionCurrency(String value) {
        return new JAXBElement<String>(_MobileTransactionDetailTransactionCurrency_QNAME, String.class, MobileTransactionDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "transactionType", scope = MobileTransactionDetail.class)
    public JAXBElement<String> createMobileTransactionDetailTransactionType(String value) {
        return new JAXBElement<String>(_MobileTransactionDetailTransactionType_QNAME, String.class, MobileTransactionDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "transactionDate", scope = MobileTransactionDetail.class)
    public JAXBElement<XMLGregorianCalendar> createMobileTransactionDetailTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MobileTransactionDetailTransactionDate_QNAME, XMLGregorianCalendar.class, MobileTransactionDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", name = "ErrorMessage", scope = MessageIDStatusResponse.class)
    public JAXBElement<String> createMessageIDStatusResponseErrorMessage(String value) {
        return new JAXBElement<String>(_MessageIDStatusResponseErrorMessage_QNAME, String.class, MessageIDStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", name = "StatusFlag", scope = MessageIDStatusResponse.class)
    public JAXBElement<String> createMessageIDStatusResponseStatusFlag(String value) {
        return new JAXBElement<String>(_MessageIDStatusResponseStatusFlag_QNAME, String.class, MessageIDStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", name = "TransactionStatus", scope = MessageIDStatusResponse.class)
    public JAXBElement<String> createMessageIDStatusResponseTransactionStatus(String value) {
        return new JAXBElement<String>(_MessageIDStatusResponseTransactionStatus_QNAME, String.class, MessageIDStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "cust_no", scope = VansoCustomerIdResponse.class)
    public JAXBElement<String> createVansoCustomerIdResponseCustNo(String value) {
        return new JAXBElement<String>(_AccountVerificationResponseCustNo_QNAME, String.class, VansoCustomerIdResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChequeNumberDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", name = "chequeNumberDetails", scope = ChequeNoInquiryResponse.class)
    public JAXBElement<ArrayOfChequeNumberDetails> createChequeNoInquiryResponseChequeNumberDetails(ArrayOfChequeNumberDetails value) {
        return new JAXBElement<ArrayOfChequeNumberDetails>(_ChequeNoInquiryResponseChequeNumberDetails_QNAME, ArrayOfChequeNumberDetails.class, ChequeNoInquiryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransactionRefNoDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.Response", name = "transactiondetail", scope = TransactionRefNoDetailsResponse.class)
    public JAXBElement<ArrayOfTransactionRefNoDetails> createTransactionRefNoDetailsResponseTransactiondetail(ArrayOfTransactionRefNoDetails value) {
        return new JAXBElement<ArrayOfTransactionRefNoDetails>(_TransactionRefNoDetailsResponseTransactiondetail_QNAME, ArrayOfTransactionRefNoDetails.class, TransactionRefNoDetailsResponse.class, value);
    }

}
