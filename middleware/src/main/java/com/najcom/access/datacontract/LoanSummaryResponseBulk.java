
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanSummaryResponseBulk complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanSummaryResponseBulk">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanSummaryResponses" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfLoanSummaryResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanSummaryResponseBulk", propOrder = {
    "loanSummaryResponses"
})
public class LoanSummaryResponseBulk {

    @XmlElementRef(name = "LoanSummaryResponses", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLoanSummaryResponse> loanSummaryResponses;

    /**
     * Gets the value of the loanSummaryResponses property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryResponse }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLoanSummaryResponse> getLoanSummaryResponses() {
        return loanSummaryResponses;
    }

    /**
     * Sets the value of the loanSummaryResponses property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryResponse }{@code >}
     *     
     */
    public void setLoanSummaryResponses(JAXBElement<ArrayOfLoanSummaryResponse> value) {
        this.loanSummaryResponses = value;
    }

}
