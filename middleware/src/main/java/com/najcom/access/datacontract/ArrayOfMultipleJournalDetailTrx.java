
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMultipleJournalDetailTrx complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMultipleJournalDetailTrx">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MultipleJournalDetailTrx" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans}MultipleJournalDetailTrx" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMultipleJournalDetailTrx", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", propOrder = {
    "multipleJournalDetailTrx"
})
public class ArrayOfMultipleJournalDetailTrx {

    @XmlElement(name = "MultipleJournalDetailTrx", nillable = true)
    protected List<MultipleJournalDetailTrx> multipleJournalDetailTrx;

    /**
     * Gets the value of the multipleJournalDetailTrx property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the multipleJournalDetailTrx property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMultipleJournalDetailTrx().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MultipleJournalDetailTrx }
     * 
     * 
     */
    public List<MultipleJournalDetailTrx> getMultipleJournalDetailTrx() {
        if (multipleJournalDetailTrx == null) {
            multipleJournalDetailTrx = new ArrayList<MultipleJournalDetailTrx>();
        }
        return this.multipleJournalDetailTrx;
    }

}
