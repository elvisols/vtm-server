
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FCUBSRTReverseTransactionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FCUBSRTReverseTransactionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AUTHSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BOOKDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CALCMETH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHEQUE_ISSUE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EXPDT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FCCREF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSTRDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MsgId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PRD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="REJECT_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIMERECV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRACKREVERSE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TXNDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TXNDRCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VALDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VDATECR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VDATEDR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FCUBSRTReverseTransactionRequest", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", propOrder = {
    "authstat",
    "bookdate",
    "branchCode",
    "calcmeth",
    "chequeissuedate",
    "expdt",
    "fccref",
    "instrdate",
    "msgId",
    "prd",
    "rejectcode",
    "source",
    "timerecv",
    "trackreverse",
    "txndate",
    "txndrcr",
    "userId",
    "valdate",
    "vdatecr",
    "vdatedr"
})
public class FCUBSRTReverseTransactionRequest {

    @XmlElementRef(name = "AUTHSTAT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> authstat;
    @XmlElementRef(name = "BOOKDATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> bookdate;
    @XmlElementRef(name = "BranchCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> branchCode;
    @XmlElementRef(name = "CALCMETH", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> calcmeth;
    @XmlElementRef(name = "CHEQUE_ISSUE_DATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> chequeissuedate;
    @XmlElementRef(name = "EXPDT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> expdt;
    @XmlElementRef(name = "FCCREF", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fccref;
    @XmlElementRef(name = "INSTRDATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> instrdate;
    @XmlElementRef(name = "MsgId", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgId;
    @XmlElementRef(name = "PRD", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prd;
    @XmlElementRef(name = "REJECT_CODE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rejectcode;
    @XmlElementRef(name = "Source", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> source;
    @XmlElementRef(name = "TIMERECV", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> timerecv;
    @XmlElementRef(name = "TRACKREVERSE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> trackreverse;
    @XmlElementRef(name = "TXNDATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> txndate;
    @XmlElementRef(name = "TXNDRCR", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> txndrcr;
    @XmlElementRef(name = "UserId", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userId;
    @XmlElementRef(name = "VALDATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> valdate;
    @XmlElementRef(name = "VDATECR", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vdatecr;
    @XmlElementRef(name = "VDATEDR", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vdatedr;

    /**
     * Gets the value of the authstat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAUTHSTAT() {
        return authstat;
    }

    /**
     * Sets the value of the authstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAUTHSTAT(JAXBElement<String> value) {
        this.authstat = value;
    }

    /**
     * Gets the value of the bookdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBOOKDATE() {
        return bookdate;
    }

    /**
     * Sets the value of the bookdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBOOKDATE(JAXBElement<String> value) {
        this.bookdate = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBranchCode(JAXBElement<String> value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the calcmeth property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCALCMETH() {
        return calcmeth;
    }

    /**
     * Sets the value of the calcmeth property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCALCMETH(JAXBElement<String> value) {
        this.calcmeth = value;
    }

    /**
     * Gets the value of the chequeissuedate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCHEQUEISSUEDATE() {
        return chequeissuedate;
    }

    /**
     * Sets the value of the chequeissuedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCHEQUEISSUEDATE(JAXBElement<String> value) {
        this.chequeissuedate = value;
    }

    /**
     * Gets the value of the expdt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEXPDT() {
        return expdt;
    }

    /**
     * Sets the value of the expdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEXPDT(JAXBElement<String> value) {
        this.expdt = value;
    }

    /**
     * Gets the value of the fccref property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFCCREF() {
        return fccref;
    }

    /**
     * Sets the value of the fccref property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFCCREF(JAXBElement<String> value) {
        this.fccref = value;
    }

    /**
     * Gets the value of the instrdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getINSTRDATE() {
        return instrdate;
    }

    /**
     * Sets the value of the instrdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setINSTRDATE(JAXBElement<String> value) {
        this.instrdate = value;
    }

    /**
     * Gets the value of the msgId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMsgId() {
        return msgId;
    }

    /**
     * Sets the value of the msgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMsgId(JAXBElement<String> value) {
        this.msgId = value;
    }

    /**
     * Gets the value of the prd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPRD() {
        return prd;
    }

    /**
     * Sets the value of the prd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPRD(JAXBElement<String> value) {
        this.prd = value;
    }

    /**
     * Gets the value of the rejectcode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getREJECTCODE() {
        return rejectcode;
    }

    /**
     * Sets the value of the rejectcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setREJECTCODE(JAXBElement<String> value) {
        this.rejectcode = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSource(JAXBElement<String> value) {
        this.source = value;
    }

    /**
     * Gets the value of the timerecv property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTIMERECV() {
        return timerecv;
    }

    /**
     * Sets the value of the timerecv property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTIMERECV(JAXBElement<String> value) {
        this.timerecv = value;
    }

    /**
     * Gets the value of the trackreverse property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTRACKREVERSE() {
        return trackreverse;
    }

    /**
     * Sets the value of the trackreverse property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTRACKREVERSE(JAXBElement<String> value) {
        this.trackreverse = value;
    }

    /**
     * Gets the value of the txndate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTXNDATE() {
        return txndate;
    }

    /**
     * Sets the value of the txndate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTXNDATE(JAXBElement<String> value) {
        this.txndate = value;
    }

    /**
     * Gets the value of the txndrcr property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTXNDRCR() {
        return txndrcr;
    }

    /**
     * Sets the value of the txndrcr property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTXNDRCR(JAXBElement<String> value) {
        this.txndrcr = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserId(JAXBElement<String> value) {
        this.userId = value;
    }

    /**
     * Gets the value of the valdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVALDATE() {
        return valdate;
    }

    /**
     * Sets the value of the valdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVALDATE(JAXBElement<String> value) {
        this.valdate = value;
    }

    /**
     * Gets the value of the vdatecr property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVDATECR() {
        return vdatecr;
    }

    /**
     * Sets the value of the vdatecr property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVDATECR(JAXBElement<String> value) {
        this.vdatecr = value;
    }

    /**
     * Gets the value of the vdatedr property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVDATEDR() {
        return vdatedr;
    }

    /**
     * Sets the value of the vdatedr property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVDATEDR(JAXBElement<String> value) {
        this.vdatedr = value;
    }

}
