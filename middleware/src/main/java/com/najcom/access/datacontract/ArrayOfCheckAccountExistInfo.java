
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCheckAccountExistInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCheckAccountExistInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckAccountExistInfo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}CheckAccountExistInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCheckAccountExistInfo", propOrder = {
    "checkAccountExistInfo"
})
public class ArrayOfCheckAccountExistInfo {

    @XmlElement(name = "CheckAccountExistInfo", nillable = true)
    protected List<CheckAccountExistInfo> checkAccountExistInfo;

    /**
     * Gets the value of the checkAccountExistInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the checkAccountExistInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCheckAccountExistInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CheckAccountExistInfo }
     * 
     * 
     */
    public List<CheckAccountExistInfo> getCheckAccountExistInfo() {
        if (checkAccountExistInfo == null) {
            checkAccountExistInfo = new ArrayList<CheckAccountExistInfo>();
        }
        return this.checkAccountExistInfo;
    }

}
