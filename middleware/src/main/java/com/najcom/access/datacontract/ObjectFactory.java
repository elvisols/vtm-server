
package com.najcom.access.datacontract;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.fcubs_webservices_shared package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfSingleTransactionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfSingleTransactionDetails");
    private final static QName _VisaCardLimitResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "VisaCardLimitResponse");
    private final static QName _FCUBSRTCreateTransactionRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSRTCreateTransactionRequest");
    private final static QName _CreateRTResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateRTResponse");
    private final static QName _ArrayOfSweepAcctsDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfSweepAcctsDetails");
    private final static QName _FCUBSCreateCustomerRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateCustomerRequest");
    private final static QName _CreateCustomerResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateCustomerResponse");
    private final static QName _AuthenticateCBAUserResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AuthenticateCBAUserResp");
    private final static QName _CustomerODLimitLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerODLimitLines");
    private final static QName _ImageCollectionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ImageCollectionDetails");
    private final static QName _UserLimitResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "UserLimitResponse");
    private final static QName _ArrayOfAccountCotAndVatLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfAccountCotAndVatLines");
    private final static QName _CurrencyRateResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CurrencyRateResponse");
    private final static QName _AccountSummaryLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSummaryLine");
    private final static QName _ActivateChequeBookResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ActivateChequeBookResponse");
    private final static QName _LoanInfoSummaryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanInfoSummaryResponse");
    private final static QName _ArrayOfCheckAccountExistInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfCheckAccountExistInfo");
    private final static QName _GetAccountExistResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetAccountExistResponse");
    private final static QName _FCUBSCustomerServiceAmtBlkRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCustomerServiceAmtBlkRequest");
    private final static QName _CreateCustAccountResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateCustAccountResponse");
    private final static QName _LoanSummaryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanSummaryResponse");
    private final static QName _ArrayOfLoanSummaryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfLoanSummaryResponse");
    private final static QName _CreateLDContractResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateLDContractResponse");
    private final static QName _ArrayOfAccountSummaryLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfAccountSummaryLine");
    private final static QName _ArrayOfCustomerODLimitLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfCustomerODLimitLines");
    private final static QName _ArrayOfAccountSummaryAndTrxResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfAccountSummaryAndTrxResponse");
    private final static QName _ArrayOfImageCollectionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfImageCollectionDetails");
    private final static QName _GetEffectiveBalanceResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetEffectiveBalanceResp");
    private final static QName _AccountSummaryResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSummaryResponse");
    private final static QName _GetAcctMemoInfoResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetAcctMemoInfoResponse");
    private final static QName _ArrayOfAccountSearchResponseLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfAccountSearchResponseLines");
    private final static QName _AccountSearchResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSearchResponse");
    private final static QName _NubanAcctsDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "NubanAcctsDetails");
    private final static QName _SingleTransactionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SingleTransactionDetails");
    private final static QName _AccountSummaryAndTrxLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSummaryAndTrxLines");
    private final static QName _TellerLimitResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TellerLimitResponse");
    private final static QName _RelatedAccountSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RelatedAccountSummaryLines");
    private final static QName _FcubsResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "FcubsResponse");
    private final static QName _ArrayOfVisaCardLimitSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfVisaCardLimitSummaryLines");
    private final static QName _CustomerAcctsDetailResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerAcctsDetailResponse");
    private final static QName _FCUBSFTCreateTransactionRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSFTCreateTransactionRequest");
    private final static QName _GetChequeValueDateResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetChequeValueDateResp");
    private final static QName _AccountSearchResponseLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSearchResponseLines");
    private final static QName _TransactionRefNoDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TransactionRefNoDetails");
    private final static QName _RepaymentScheduleResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RepaymentScheduleResponse");
    private final static QName _ArrayOfCustomerInformation_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfCustomerInformation");
    private final static QName _CustomerInformation_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerInformation");
    private final static QName _FCUBSCreateMMContractRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateMMContractRequest");
    private final static QName _LoanInfoSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanInfoSummaryLines");
    private final static QName _RepaymentScheduleInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RepaymentScheduleInfo");
    private final static QName _CustomerDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerDetails");
    private final static QName _ChequeDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ChequeDetails");
    private final static QName _GetCurrencyCodeResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetCurrencyCodeResp");
    private final static QName _FCUBSRTReverseTransactionRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSRTReverseTransactionRequest");
    private final static QName _ArrayOfLoanInfoSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfLoanInfoSummaryLines");
    private final static QName _ScreenLimitResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ScreenLimitResponse");
    private final static QName _FCUBSCreateSIContractRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateSIContractRequest");
    private final static QName _MultipleJournalMasterTrx_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MultipleJournalMasterTrx");
    private final static QName _LoanSummaryResponseBulk_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanSummaryResponseBulk");
    private final static QName _FCUBSCreateCustAccountRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateCustAccountRequest");
    private final static QName _SingleJournalTrx_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SingleJournalTrx");
    private final static QName _CheckAccountExistInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CheckAccountExistInfo");
    private final static QName _CustomerAcctsDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerAcctsDetail");
    private final static QName _GetUserBranchResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetUserBranchResp");
    private final static QName _AccountTransactionResponseBulk_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountTransactionResponseBulk");
    private final static QName _FCUBSCGServiceCreateTransactionRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCGServiceCreateTransactionRequest");
    private final static QName _ArrayOfRelatedAccountSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfRelatedAccountSummaryLines");
    private final static QName _AccountSummaryAndTrxResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSummaryAndTrxResponse");
    private final static QName _FCUBSCreateStopPaymentRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateStopPaymentRequest");
    private final static QName _CreateStopPaymentResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateStopPaymentResponse");
    private final static QName _ArrayOfLoanSummaryLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfLoanSummaryLine");
    private final static QName _ArrayOfNubanAcctsDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfNubanAcctsDetails");
    private final static QName _FCUBSCreateTDAccountRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateTDAccountRequest");
    private final static QName _ArrayOfFixedDepositInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfFixedDepositInfo");
    private final static QName _ArrayOfCustomerAcctsDetailResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfCustomerAcctsDetailResponse");
    private final static QName _MultipleJournalDetailTrx_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MultipleJournalDetailTrx");
    private final static QName _CreateAmtBlockResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateAmtBlockResponse");
    private final static QName _AuthorizeWithdrawalRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AuthorizeWithdrawalRequest");
    private final static QName _CustomerInformationResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerInformationResponse");
    private final static QName _ArrayOfChequeDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfChequeDetails");
    private final static QName _ValidateUserTillResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ValidateUserTillResp");
    private final static QName _SampleAccountResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SampleAccountResponse");
    private final static QName _ArrayOfAccountSummaryAndTrxLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfAccountSummaryAndTrxLines");
    private final static QName _SweepAcctsDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SweepAcctsDetails");
    private final static QName _CreateSIContractResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateSIContractResponse");
    private final static QName _CreateMMContractResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateMMContractResponse");
    private final static QName _AccountRelatedResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountRelatedResponse");
    private final static QName _CreateTDAccountResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CreateTDAccountResponse");
    private final static QName _AccountCotAndVatLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountCotAndVatLines");
    private final static QName _AccountMemoInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountMemoInfo");
    private final static QName _FixedDepositInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "FixedDepositInfo");
    private final static QName _ArrayOfCustomerAcctsDetail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfCustomerAcctsDetail");
    private final static QName _ArrayOfTransactionRefNoDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfTransactionRefNoDetails");
    private final static QName _FCUBSCloseCustAccRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCloseCustAccRequest");
    private final static QName _GetAccountBranchResp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "GetAccountBranchResp");
    private final static QName _FCUBSCreateLDContractRequest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCUBSCreateLDContractRequest");
    private final static QName _LoanSummaryLine_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanSummaryLine");
    private final static QName _FixedDepositResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "FixedDepositResponse");
    private final static QName _CustomerODLimitResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerODLimitResponse");
    private final static QName _ArrayOfChequeNumberDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfChequeNumberDetails");
    private final static QName _VisaCardLimitSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "VisaCardLimitSummaryLines");
    private final static QName _ChequeNumberDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ChequeNumberDetails");
    private final static QName _ArrayOfAccountMemoInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfAccountMemoInfo");
    private final static QName _ChequeDetailsResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ChequeDetailsResponse");
    private final static QName _ArrayOfRepaymentScheduleInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ArrayOfRepaymentScheduleInfo");
    private final static QName _AccountCotAndVatResponse_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountCotAndVatResponse");
    private final static QName _ArrayOfMultipleJournalDetailTrx_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ArrayOfMultipleJournalDetailTrx");
    private final static QName _ChequeNumberDetailsBeneficiary_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "beneficiary");
    private final static QName _ChequeNumberDetailsPresentationDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "presentation_date");
    private final static QName _ChequeNumberDetailsCheckNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "check_no");
    private final static QName _ChequeNumberDetailsAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amount");
    private final static QName _ChequeNumberDetailsBranch_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "branch");
    private final static QName _ChequeNumberDetailsCheckBookNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "check_book_no");
    private final static QName _ChequeNumberDetailsStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "status");
    private final static QName _ChequeNumberDetailsValueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "value_date");
    private final static QName _ChequeNumberDetailsAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "account");
    private final static QName _ChequeNumberDetailsAcDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ac_desc");
    private final static QName _ChequeNumberDetailsCustAcNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cust_ac_no");
    private final static QName _CreateSIContractResponseCONREFNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CONREFNO");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BranchCode");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestREFERENCENO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "REFERENCE_NO");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestHOLDDESC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "HOLDDESC");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestCHECKERID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHECKER_ID");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestREMARK_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "REMARK");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestABLKTYPE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ABLKTYPE");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestEFFDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EFF_DATE");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestCHECKERSTAMP_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHECKER_STAMP");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestHPCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "HPCODE");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestUserId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UserId");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestAMTBLKNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AMTBLKNO");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MsgId");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestSource_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Source");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestAMT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AMT");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestMAKERID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MAKER_ID");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestMAKERSTAMP_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MAKER_STAMP");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UserBranchCode");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestCORRELID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CORRELID");
    private final static QName _FCUBSCustomerServiceAmtBlkRequestACC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ACC");
    private final static QName _CustomerDetailsCustEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "custEmail");
    private final static QName _CustomerDetailsMaritalStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "MaritalStatus");
    private final static QName _CustomerDetailsCustomerCategory_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerCategory");
    private final static QName _CustomerDetailsCustFax_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "custFax");
    private final static QName _CustomerDetailsCustomerAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerAddress");
    private final static QName _CustomerDetailsCustomerNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "customerNo");
    private final static QName _CustomerDetailsCustid_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "custid");
    private final static QName _CustomerDetailsDirectorName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "director_name");
    private final static QName _CustomerDetailsBusinessGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BusinessGroup");
    private final static QName _CustomerDetailsProfitCenter_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ProfitCenter");
    private final static QName _CustomerDetailsCompanyRC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "companyRC");
    private final static QName _CustomerDetailsCustomerCategoryDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerCategoryDesc");
    private final static QName _CustomerDetailsCustNationality_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "custNationality");
    private final static QName _CustomerDetailsDatCustOpen_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "datCustOpen");
    private final static QName _CustomerDetailsUNIQUEIDVALUE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "UNIQUE_ID_VALUE");
    private final static QName _CustomerDetailsCustomerName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "customerName");
    private final static QName _CustomerDetailsBranchName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "branchName");
    private final static QName _CustomerDetailsAccountOfficer_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountOfficer");
    private final static QName _CustomerDetailsMaillingAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "maillingAddress");
    private final static QName _CustomerDetailsDOB_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DOB");
    private final static QName _CustomerDetailsAccountStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountStatus");
    private final static QName _CustomerDetailsMISCODE3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "mISCODE3");
    private final static QName _CustomerDetailsPermanentAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "permanentAddress");
    private final static QName _CustomerDetailsIsStaff_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "IsStaff");
    private final static QName _CustomerDetailsGender_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "gender");
    private final static QName _CustomerDetailsDateIncoporated_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "dateIncoporated");
    private final static QName _CustomerDetailsCustPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "custPhone");
    private final static QName _CustomerDetailsPhoneNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "phoneNo");
    private final static QName _CustomerDetailsBranchAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BranchAddress");
    private final static QName _CustomerDetailsBusinessDivision_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BusinessDivision");
    private final static QName _CustomerDetailsSPOUSENAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SPOUSE_NAME");
    private final static QName _AccountSummaryResponseAccountSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSummaryLines");
    private final static QName _CreateRTResponseTRNREFNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TRNREFNO");
    private final static QName _CustomerAcctsDetailInterestPaidYTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "interestPaidYTD");
    private final static QName _CustomerAcctsDetailAmountLastDebit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountLastDebit");
    private final static QName _CustomerAcctsDetailProductCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "productCode");
    private final static QName _CustomerAcctsDetailCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "currencyCode");
    private final static QName _CustomerAcctsDetailCOMPMIS8_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "COMP_MIS_8");
    private final static QName _CustomerAcctsDetailStrCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strCity");
    private final static QName _CustomerAcctsDetailBVN_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BVN");
    private final static QName _CustomerAcctsDetailStrZip_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strZip");
    private final static QName _CustomerAcctsDetailClearedBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "clearedBalance");
    private final static QName _CustomerAcctsDetailLastDebitInterestAccrued_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastDebitInterestAccrued");
    private final static QName _CustomerAcctsDetailLastMaintainedBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastMaintainedBy");
    private final static QName _CustomerAcctsDetailATMStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "aTMStatus");
    private final static QName _CustomerAcctsDetailDaueLimit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "daueLimit");
    private final static QName _CustomerAcctsDetailNetBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "netBalance");
    private final static QName _CustomerAcctsDetailStaff_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "staff");
    private final static QName _CustomerAcctsDetailAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountNo");
    private final static QName _CustomerAcctsDetailAccountClassType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountClassType");
    private final static QName _CustomerAcctsDetailAmountDebitYTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountDebitYTD");
    private final static QName _CustomerAcctsDetailCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "currency");
    private final static QName _CustomerAcctsDetailAccountName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountName");
    private final static QName _CustomerAcctsDetailStrAdd1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strAdd1");
    private final static QName _CustomerAcctsDetailAmountDebitMTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountDebitMTD");
    private final static QName _CustomerAcctsDetailStrAdd3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strAdd3");
    private final static QName _CustomerAcctsDetailStrAdd2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strAdd2");
    private final static QName _CustomerAcctsDetailMaintenanceAuthorizedBy_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "maintenanceAuthorizedBy");
    private final static QName _CustomerAcctsDetailAvailableBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "availableBalance");
    private final static QName _CustomerAcctsDetailInterestReceivedYTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "interestReceivedYTD");
    private final static QName _CustomerAcctsDetailProfitCenter_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "profitCenter");
    private final static QName _CustomerAcctsDetailPhone_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "phone");
    private final static QName _CustomerAcctsDetailLastDebitDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastDebitDate");
    private final static QName _CustomerAcctsDetailTaxAccrued_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "taxAccrued");
    private final static QName _CustomerAcctsDetailLastCreditDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastCreditDate");
    private final static QName _CustomerAcctsDetailBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "branchCode");
    private final static QName _CustomerAcctsDetailStrCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strCountry");
    private final static QName _CustomerAcctsDetailAmountCreditYTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountCreditYTD");
    private final static QName _CustomerAcctsDetailCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "custID");
    private final static QName _CustomerAcctsDetailAmountHold_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountHold");
    private final static QName _CustomerAcctsDetailODLimit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "oDLimit");
    private final static QName _CustomerAcctsDetailSignatory_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "signatory");
    private final static QName _CustomerAcctsDetailDateofbirth_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "dateofbirth");
    private final static QName _CustomerAcctsDetailLastSerialofCheque_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastSerialofCheque");
    private final static QName _CustomerAcctsDetailAmountCreditMTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountCreditMTD");
    private final static QName _CustomerAcctsDetailServiceChargeYTD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "serviceChargeYTD");
    private final static QName _CustomerAcctsDetailStrState_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "strState");
    private final static QName _CustomerAcctsDetailUnavailableBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "unavailableBalance");
    private final static QName _CustomerAcctsDetailAmountLastCredit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amountLastCredit");
    private final static QName _CustomerAcctsDetailEMail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "e_mail");
    private final static QName _CustomerAcctsDetailDateOpened_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "dateOpened");
    private final static QName _CustomerAcctsDetailOpeningBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "openingBalance");
    private final static QName _CustomerAcctsDetailCOMPMIS2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "COMP_MIS_2");
    private final static QName _CustomerAcctsDetailDAUEStartDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "dAUEStartDate");
    private final static QName _CustomerAcctsDetailCOMPMIS4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "COMP_MIS_4");
    private final static QName _CustomerAcctsDetailLastUsedChequeNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastUsedChequeNo");
    private final static QName _CustomerAcctsDetailUnclearedBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "unclearedBalance");
    private final static QName _CustomerAcctsDetailProductName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "productName");
    private final static QName _CustomerAcctsDetailLastCreditInterestAccrued_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "lastCreditInterestAccrued");
    private final static QName _CustomerAcctsDetailClosingBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "closingBalance");
    private final static QName _CreateMMContractResponseUSERREFNO1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "USERREF_NO1");
    private final static QName _FCUBSCreateSIContractRequestCompMIS4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMIS4");
    private final static QName _FCUBSCreateSIContractRequestCrAccountCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CrAccountCcyCode");
    private final static QName _FCUBSCreateSIContractRequestCompMIS2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMIS2");
    private final static QName _FCUBSCreateSIContractRequestSIAmountCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SIAmountCcyCode");
    private final static QName _FCUBSCreateSIContractRequestExecDayInterval_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ExecDayInterval");
    private final static QName _FCUBSCreateSIContractRequestFirstExecutionDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FirstExecutionDate");
    private final static QName _FCUBSCreateSIContractRequestExecMonthInterval_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ExecMonthInterval");
    private final static QName _FCUBSCreateSIContractRequestMakerDateStamp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MakerDateStamp");
    private final static QName _FCUBSCreateSIContractRequestCheckerID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CheckerID");
    private final static QName _FCUBSCreateSIContractRequestCrAccountBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CrAccountBranchCode");
    private final static QName _FCUBSCreateSIContractRequestExecYearInterval_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ExecYearInterval");
    private final static QName _FCUBSCreateSIContractRequestSIAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SIAmount");
    private final static QName _FCUBSCreateSIContractRequestSIExpiryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SIExpiryDate");
    private final static QName _FCUBSCreateSIContractRequestDrAccountCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DrAccountCcyCode");
    private final static QName _FCUBSCreateSIContractRequestCompMIS8_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMIS8");
    private final static QName _FCUBSCreateSIContractRequestCheckerDateStamp_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CheckerDateStamp");
    private final static QName _FCUBSCreateSIContractRequestDrAccountBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DrAccountBranchCode");
    private final static QName _FCUBSCreateSIContractRequestDrAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DrAccountNo");
    private final static QName _FCUBSCreateSIContractRequestDrAccountName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DrAccountName");
    private final static QName _FCUBSCreateSIContractRequestInternalRemarks_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "InternalRemarks");
    private final static QName _FCUBSCreateSIContractRequestMakerID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MakerID");
    private final static QName _FCUBSCreateSIContractRequestNextExecutionDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "NextExecutionDate");
    private final static QName _FCUBSCreateSIContractRequestCrAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CrAccountNo");
    private final static QName _FCUBSCreateSIContractRequestProductCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ProductCode");
    private final static QName _RepaymentScheduleInfoAmountFinanced_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AmountFinanced");
    private final static QName _RepaymentScheduleInfoCRPRODAC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CR_PROD_AC");
    private final static QName _RepaymentScheduleInfoCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CurrencyCode");
    private final static QName _RepaymentScheduleInfoLoanAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanAccountNo");
    private final static QName _RepaymentScheduleInfoOverDueAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "OverDueAmount");
    private final static QName _RepaymentScheduleInfoBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BranchCode");
    private final static QName _RepaymentScheduleInfoValueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ValueDate");
    private final static QName _RepaymentScheduleInfoBookDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BookDate");
    private final static QName _RepaymentScheduleInfoProductCategory_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ProductCategory");
    private final static QName _RepaymentScheduleInfoScheduleDueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ScheduleDueDate");
    private final static QName _RepaymentScheduleInfoProductDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ProductDescription");
    private final static QName _RepaymentScheduleInfoComponentName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ComponentName");
    private final static QName _RepaymentScheduleInfoAmountDisbursed_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AmountDisbursed");
    private final static QName _RepaymentScheduleInfoAmountDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AmountDue");
    private final static QName _RepaymentScheduleInfoCustomerNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerNo");
    private final static QName _RepaymentScheduleInfoDRACCBRN_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DR_ACC_BRN");
    private final static QName _RepaymentScheduleInfoDRPRODAC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DR_PROD_AC");
    private final static QName _RepaymentScheduleInfoOriginalStartDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "OriginalStartDate");
    private final static QName _RepaymentScheduleInfoCRACCBRN_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CR_ACC_BRN");
    private final static QName _RepaymentScheduleInfoAmountSettled_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AmountSettled");
    private final static QName _RepaymentScheduleInfoApplicantName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ApplicantName");
    private final static QName _RepaymentScheduleInfoMaturityDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "MaturityDate");
    private final static QName _RepaymentScheduleInfoScheduleStartDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ScheduleStartDate");
    private final static QName _MultipleJournalMasterTrxCompMis7_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis7");
    private final static QName _MultipleJournalMasterTrxCompMis6_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis6");
    private final static QName _MultipleJournalMasterTrxTotalCredit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TotalCredit");
    private final static QName _MultipleJournalMasterTrxCompMis5_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis5");
    private final static QName _MultipleJournalMasterTrxCompMis4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis4");
    private final static QName _MultipleJournalMasterTrxCompMis3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis3");
    private final static QName _MultipleJournalMasterTrxCompMis2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis2");
    private final static QName _MultipleJournalMasterTrxCompMis1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis1");
    private final static QName _MultipleJournalMasterTrxFUNCTIONID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FUNCTIONID");
    private final static QName _MultipleJournalMasterTrxValueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ValueDate");
    private final static QName _MultipleJournalMasterTrxCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CurrencyCode");
    private final static QName _MultipleJournalMasterTrxMAKDTTIME_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MAKDTTIME");
    private final static QName _MultipleJournalMasterTrxCHECHKERID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHECHKERID");
    private final static QName _MultipleJournalMasterTrxMSGSTAT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MSGSTAT");
    private final static QName _MultipleJournalMasterTrxCompMis9_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis9");
    private final static QName _MultipleJournalMasterTrxCompMis8_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis8");
    private final static QName _MultipleJournalMasterTrxMAKER_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MAKER");
    private final static QName _MultipleJournalMasterTrxCurrencyNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CurrencyNumber");
    private final static QName _MultipleJournalMasterTrxACTION_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ACTION");
    private final static QName _MultipleJournalMasterTrxTransactionMisCode1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TransactionMisCode1");
    private final static QName _MultipleJournalMasterTrxRelativeAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "RelativeAccount");
    private final static QName _MultipleJournalMasterTrxTransactionDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TransactionDetails");
    private final static QName _MultipleJournalMasterTrxTotalDebit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TotalDebit");
    private final static QName _MultipleJournalMasterTrxTransactionMisCode2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TransactionMisCode2");
    private final static QName _MultipleJournalMasterTrxCompMis10_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMis10");
    private final static QName _MultipleJournalMasterTrxBatchNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BatchNumber");
    private final static QName _MultipleJournalMasterTrxCHKDTTIME_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHKDTTIME");
    private final static QName _LoanInfoSummaryLinesTotalPrincipalOutstanding_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TotalPrincipalOutstanding");
    private final static QName _LoanInfoSummaryLinesNextPrincipalDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "NextPrincipalDue");
    private final static QName _LoanInfoSummaryLinesCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CcyCode");
    private final static QName _LoanInfoSummaryLinesProductCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ProductCode");
    private final static QName _LoanInfoSummaryLinesInterestPaid_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "InterestPaid");
    private final static QName _LoanInfoSummaryLinesInterestPenaltyPaid_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "InterestPenaltyPaid");
    private final static QName _LoanInfoSummaryLinesPrincipalPaid_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PrincipalPaid");
    private final static QName _LoanInfoSummaryLinesInterestNotDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "InterestNotDue");
    private final static QName _LoanInfoSummaryLinesLoanAccountNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanAccountNumber");
    private final static QName _LoanInfoSummaryLinesDomiciliaryAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DomiciliaryAddress");
    private final static QName _LoanInfoSummaryLinesPrincipalOverDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PrincipalOverDue");
    private final static QName _LoanInfoSummaryLinesCustomerName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerName");
    private final static QName _LoanInfoSummaryLinesNextInterestDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "NextInterestDue");
    private final static QName _LoanInfoSummaryLinesInterestOverDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "InterestOverDue");
    private final static QName _LoanInfoSummaryLinesPrincipalNotDue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PrincipalNotDue");
    private final static QName _FixedDepositInfoMaturityAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "MaturityAmount");
    private final static QName _FixedDepositInfoReferenceNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ReferenceNo");
    private final static QName _FixedDepositInfoPaymentMethod_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PaymentMethod");
    private final static QName _FixedDepositInfoSettlementAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SettlementAccount");
    private final static QName _FixedDepositInfoUserReferenceNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "UserReferenceNo");
    private final static QName _FixedDepositInfoBookingDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BookingDate");
    private final static QName _FixedDepositInfoRolloverAllowed_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RolloverAllowed");
    private final static QName _FixedDepositInfoInitialDeposit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "InitialDeposit");
    private final static QName _FixedDepositInfoTenor_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Tenor");
    private final static QName _FixedDepositInfoSettlementAccCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SettlementAccCcyCode");
    private final static QName _FixedDepositInfoProduct_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Product");
    private final static QName _FixedDepositInfoSettlementAccBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SettlementAccBranchCode");
    private final static QName _FixedDepositInfoContractStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ContractStatus");
    private final static QName _FixedDepositInfoDepositCurrencyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DepositCurrencyCode");
    private final static QName _CreateCustomerResponseCUSTNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CUSTNO");
    private final static QName _FCUBSFTCreateTransactionRequestBENEFICIARYADDRESS1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENEFICIARY_ADDRESS1");
    private final static QName _FCUBSFTCreateTransactionRequestCREDITACCOUNT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CREDIT_ACCOUNT");
    private final static QName _FCUBSFTCreateTransactionRequestMSGID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MSGID");
    private final static QName _FCUBSFTCreateTransactionRequestCREDITBRANCH_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CREDIT_BRANCH");
    private final static QName _FCUBSFTCreateTransactionRequestSOURCE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SOURCE");
    private final static QName _FCUBSFTCreateTransactionRequestUSERID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "USERID");
    private final static QName _FCUBSFTCreateTransactionRequestUSERBRANCHCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "USER_BRANCH_CODE");
    private final static QName _FCUBSFTCreateTransactionRequestBENEFICIARYNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENEFICIARY_NAME");
    private final static QName _FCUBSFTCreateTransactionRequestBENEFICIARYADDRESS3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENEFICIARY_ADDRESS3");
    private final static QName _FCUBSFTCreateTransactionRequestBENEFICIARYADDRESS2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENEFICIARY_ADDRESS2");
    private final static QName _FCUBSFTCreateTransactionRequestDEBITCCY_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DEBIT_CCY");
    private final static QName _FCUBSFTCreateTransactionRequestDEBITBRANCH_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DEBIT_BRANCH");
    private final static QName _FCUBSFTCreateTransactionRequestDEBITAMOUNT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DEBIT_AMOUNT");
    private final static QName _FCUBSFTCreateTransactionRequestDEBITVALUEDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DEBIT_VALUEDATE");
    private final static QName _FCUBSFTCreateTransactionRequestCREDITCCY_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CREDIT_CCY");
    private final static QName _FCUBSFTCreateTransactionRequestCREDITAMOUNT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CREDIT_AMOUNT");
    private final static QName _FCUBSFTCreateTransactionRequestDEBITACCOUNT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DEBIT_ACCOUNT");
    private final static QName _FCUBSFTCreateTransactionRequestCREDITVALUEDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CREDIT_VALUEDATE");
    private final static QName _ScreenLimitResponseFunctionName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Function_Name");
    private final static QName _ScreenLimitResponseFunctionGroup_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "function_group");
    private final static QName _ScreenLimitResponseBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "branch_code");
    private final static QName _ScreenLimitResponseCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ccy_code");
    private final static QName _ValidateUserTillRespTillCount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Till_Count");
    private final static QName _GetUserBranchRespHomeBranch_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "home_branch");
    private final static QName _AuthenticateCBAUserRespUSERSTATUS_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "USER_STATUS");
    private final static QName _AuthenticateCBAUserRespUSERID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "USER_ID");
    private final static QName _FCUBSCreateCustAccountRequestAccountPostNoDebit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountPostNoDebit");
    private final static QName _FCUBSCreateCustAccountRequestIsSalaryAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "IsSalaryAccount");
    private final static QName _FCUBSCreateCustAccountRequestAccountClassType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountClassType");
    private final static QName _FCUBSCreateCustAccountRequestAccountCompMIS3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountCompMIS3");
    private final static QName _FCUBSCreateCustAccountRequestAccountCompMIS4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountCompMIS4");
    private final static QName _FCUBSCreateCustAccountRequestCustomerNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CustomerNo");
    private final static QName _FCUBSCreateCustAccountRequestAccountCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountCcyCode");
    private final static QName _FCUBSCreateCustAccountRequestDormant_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Dormant");
    private final static QName _FCUBSCreateCustAccountRequestAccountCompMIS8_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountCompMIS8");
    private final static QName _FCUBSCreateCustAccountRequestChequeBookAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ChequeBookAccount");
    private final static QName _FCUBSCreateCustAccountRequestAccountOpenDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountOpenDate");
    private final static QName _FCUBSCreateCustAccountRequestAccountBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountBranchCode");
    private final static QName _FCUBSCreateCustAccountRequestAccountPoolCD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountPoolCD");
    private final static QName _FCUBSCreateCustAccountRequestAccountCompMIS2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountCompMIS2");
    private final static QName _FCUBSCreateCustAccountRequestAccountName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountName");
    private final static QName _FCUBSCreateCustAccountRequestAuthStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AuthStatus");
    private final static QName _FCUBSCreateCustAccountRequestATMAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ATMAccount");
    private final static QName _FCUBSCreateCustAccountRequestAccountPostNoCredit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountPostNoCredit");
    private final static QName _FCUBSCreateCustAccountRequestFrozen_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Frozen");
    private final static QName _FCUBSCreateCustAccountRequestMinBalanceReqd_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MinBalanceReqd");
    private final static QName _FCUBSCreateCustAccountRequestAccountClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountClass");
    private final static QName _AuthorizeWithdrawalRequestBOOKDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BOOKDATE");
    private final static QName _AuthorizeWithdrawalRequestVALDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "VALDATE");
    private final static QName _AuthorizeWithdrawalRequestOFFSETCCY_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OFFSETCCY");
    private final static QName _AuthorizeWithdrawalRequestTXNCCY_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TXNCCY");
    private final static QName _AuthorizeWithdrawalRequestTRACKREVERSE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TRACKREVERSE");
    private final static QName _AuthorizeWithdrawalRequestTXNDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TXNDATE");
    private final static QName _AuthorizeWithdrawalRequestOFFSETACC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OFFSETACC");
    private final static QName _AuthorizeWithdrawalRequestTIMERECV_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TIMERECV");
    private final static QName _AuthorizeWithdrawalRequestXref_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Xref");
    private final static QName _AuthorizeWithdrawalRequestAUTHSTAT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AUTHSTAT");
    private final static QName _AuthorizeWithdrawalRequestFCCREF_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FCCREF");
    private final static QName _AuthorizeWithdrawalRequestLCYAMT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LCYAMT");
    private final static QName _AuthorizeWithdrawalRequestOFFSETAMT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OFFSETAMT");
    private final static QName _AuthorizeWithdrawalRequestTXNAMT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TXNAMT");
    private final static QName _AuthorizeWithdrawalRequestCHEQUEISSUEDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHEQUE_ISSUE_DATE");
    private final static QName _AuthorizeWithdrawalRequestTXNDRCR_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TXNDRCR");
    private final static QName _AuthorizeWithdrawalRequestTXNACC_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TXNACC");
    private final static QName _AuthorizeWithdrawalRequestINSTRDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "INSTRDATE");
    private final static QName _AuthorizeWithdrawalRequestPRD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PRD");
    private final static QName _AccountTransactionResponseBulkAccountTransactionResponses_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountTransactionResponses");
    private final static QName _FCUBSCreateMMContractRequestReferenceNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ReferenceNo");
    private final static QName _FCUBSCreateMMContractRequestCompMIS5_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMIS5");
    private final static QName _FCUBSCreateMMContractRequestCompMIS3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMIS3");
    private final static QName _FCUBSCreateMMContractRequestDebitAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DebitAccount");
    private final static QName _FCUBSCreateMMContractRequestCrAccBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CrAccBranchCode");
    private final static QName _FCUBSCreateMMContractRequestAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Amount");
    private final static QName _FCUBSCreateMMContractRequestDrAccBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DrAccBranchCode");
    private final static QName _FCUBSCreateMMContractRequestCreditAccount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CreditAccount");
    private final static QName _CustomerODLimitLinesLimitAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LimitAmount");
    private final static QName _CustomerODLimitLinesOdIntRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "OdIntRate");
    private final static QName _CustomerODLimitLinesLineExpiryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LineExpiryDate");
    private final static QName _CustomerODLimitLinesLineStartDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LineStartDate");
    private final static QName _CustomerODLimitLinesAccountNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountNumber");
    private final static QName _AccountCotAndVatResponseAccountCotAndVatTransactions_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountCotAndVatTransactions");
    private final static QName _CreateLDContractResponseLDCONTRACTREFNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LDCONTRACTREFNO");
    private final static QName _AccountSummaryAndTrxResponseAccountSummaryAndTransactions_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSummaryAndTransactions");
    private final static QName _AccountMemoInfoCodLastMntMakerid_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_last_mnt_makerid");
    private final static QName _AccountMemoInfoCodLastMntChkrid_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_last_mnt_chkrid");
    private final static QName _AccountMemoInfoMemotext_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "memotext");
    private final static QName _AccountMemoInfoDatLastMntx_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "dat_last_mntx");
    private final static QName _FCUBSCloseCustAccRequestAcountBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AcountBranchCode");
    private final static QName _FCUBSCloseCustAccRequestAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountNo");
    private final static QName _CreateStopPaymentResponseSTOPPAYMENTNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "STOPPAYMENTNO");
    private final static QName _AccountRelatedResponseRelatedAccountSummary_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RelatedAccountSummary");
    private final static QName _CustomerODLimitResponseCustomerODLimitTransactions_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerODLimitTransactions");
    private final static QName _CreateTDAccountResponseCUSTACNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CUST_AC_NO");
    private final static QName _FcubsResponseResponseData_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ResponseData");
    private final static QName _FcubsResponseResponseCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ResponseCode");
    private final static QName _SweepAcctsDetailsCodAcctNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_acct_no");
    private final static QName _AccountSummaryAndTrxLinesDrCrInd_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DrCrInd");
    private final static QName _AccountSummaryAndTrxLinesInstrumentCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "InstrumentCode");
    private final static QName _AccountSummaryAndTrxLinesUserId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "UserId");
    private final static QName _AccountSummaryAndTrxLinesClosingBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ClosingBalance");
    private final static QName _AccountSummaryAndTrxLinesExternalRefNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ExternalRefNumber");
    private final static QName _AccountSummaryAndTrxLinesRowNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RowNumber");
    private final static QName _AccountSummaryAndTrxLinesTranAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranAmount");
    private final static QName _AccountSummaryAndTrxLinesEventSrNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "event_sr_no");
    private final static QName _AccountSummaryAndTrxLinesModule_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Module");
    private final static QName _AccountSummaryAndTrxLinesTxnInitiationDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TxnInitiationDate");
    private final static QName _AccountSummaryAndTrxLinesPostedDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PostedDate");
    private final static QName _AccountSummaryAndTrxLinesTotalWithdrawal_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TotalWithdrawal");
    private final static QName _AccountSummaryAndTrxLinesAcctOpeningBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AcctOpeningBalance");
    private final static QName _AccountSummaryAndTrxLinesTranEvent_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranEvent");
    private final static QName _AccountSummaryAndTrxLinesTranDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranDescription");
    private final static QName _AccountSummaryAndTrxLinesNoofLodgement_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "noofLodgement");
    private final static QName _AccountSummaryAndTrxLinesLcyAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LcyAmount");
    private final static QName _AccountSummaryAndTrxLinesStmtDt_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Stmt_Dt");
    private final static QName _AccountSummaryAndTrxLinesTranIndicator_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranIndicator");
    private final static QName _AccountSummaryAndTrxLinesTrnRefNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TrnRefNo");
    private final static QName _AccountSummaryAndTrxLinesAuthId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AuthId");
    private final static QName _AccountSummaryAndTrxLinesAvailableBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AvailableBalance");
    private final static QName _AccountSummaryAndTrxLinesAcEntrySrNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ac_entry_sr_no");
    private final static QName _AccountSummaryAndTrxLinesTransactionDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TransactionDate");
    private final static QName _AccountSummaryAndTrxLinesNoofWithdrawal_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "noofWithdrawal");
    private final static QName _AccountSummaryAndTrxLinesTotalLodgement_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TotalLodgement");
    private final static QName _AccountSummaryAndTrxLinesTransactionCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TransactionCode");
    private final static QName _AccountSummaryAndTrxLinesUnClearedBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "UnClearedBalance");
    private final static QName _AccountSummaryAndTrxLinesBranchName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BranchName");
    private final static QName _AccountSummaryAndTrxLinesRunningBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "RunningBalance");
    private final static QName _AccountSummaryAndTrxLinesProductCodeDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ProductCodeDesc");
    private final static QName _AccountSummaryAndTrxLinesFcyAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "FcyAmount");
    private final static QName _AccountSummaryAndTrxLinesTranNarration_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranNarration");
    private final static QName _AccountSummaryAndTrxLinesBatchNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BatchNo");
    private final static QName _AccountSummaryAndTrxLinesHasCOT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "hasCOT");
    private final static QName _VisaCardLimitResponseVisaCardLimitSummary_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "VisaCardLimitSummary");
    private final static QName _FCUBSCGServiceCreateTransactionRequestTRANDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TRAN_DATE");
    private final static QName _FCUBSCGServiceCreateTransactionRequestACCCCY_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ACC_CCY");
    private final static QName _FCUBSCGServiceCreateTransactionRequestBRANCHCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BRANCH_CODE");
    private final static QName _FCUBSCGServiceCreateTransactionRequestBENEFICIARYACCTNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENEFICIARY_ACCTNO");
    private final static QName _FCUBSCGServiceCreateTransactionRequestINSTRAMT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "INSTRAMT");
    private final static QName _FCUBSCGServiceCreateTransactionRequestPAYERACCOUNT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PAYERACCOUNT");
    private final static QName _FCUBSCGServiceCreateTransactionRequestXREF_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "XREF");
    private final static QName _FCUBSCGServiceCreateTransactionRequestPAYERINSTRNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PAYERINSTRNO");
    private final static QName _FCUBSCGServiceCreateTransactionRequestVALUEDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "VALUE_DATE");
    private final static QName _FCUBSCGServiceCreateTransactionRequestREMARKS_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "REMARKS");
    private final static QName _FCUBSCGServiceCreateTransactionRequestROUTINGNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ROUTING_NO");
    private final static QName _FCUBSCGServiceCreateTransactionRequestINSTRCCY_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "INSTRCCY");
    private final static QName _ActivateChequeBookResponseCheckbookstatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "checkbookstatus");
    private final static QName _ActivateChequeBookResponseErrCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "err_code");
    private final static QName _LoanSummaryResponseBulkLoanSummaryResponses_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanSummaryResponses");
    private final static QName _GetAcctMemoInfoResponseAccountMemo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountMemo");
    private final static QName _LoanSummaryResponseLoanSummaryLines_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanSummaryLines");
    private final static QName _FCUBSRTCreateTransactionRequestCHQNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHQNO");
    private final static QName _FCUBSRTCreateTransactionRequestNARRATIVE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "NARRATIVE");
    private final static QName _FCUBSRTCreateTransactionRequestBENFNAME_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENFNAME");
    private final static QName _FCUBSRTCreateTransactionRequestOFFSETBRN_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OFFSETBRN");
    private final static QName _FCUBSRTCreateTransactionRequestBENFADDR4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENFADDR4");
    private final static QName _FCUBSRTCreateTransactionRequestBENFADDR1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENFADDR1");
    private final static QName _FCUBSRTCreateTransactionRequestTXNBRN_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TXNBRN");
    private final static QName _FCUBSRTCreateTransactionRequestBENFADDR2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENFADDR2");
    private final static QName _FCUBSRTCreateTransactionRequestBENFADDR3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BENFADDR3");
    private final static QName _ChequeDetailsAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountNo");
    private final static QName _ChequeDetailsSectorCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SectorCode");
    private final static QName _ChequeDetailsTotalChequeLeafNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TotalChequeLeafNo");
    private final static QName _ChequeDetailsChequeBookNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ChequeBookNo");
    private final static QName _FCUBSRTReverseTransactionRequestEXPDT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EXPDT");
    private final static QName _FCUBSRTReverseTransactionRequestREJECTCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "REJECT_CODE");
    private final static QName _FCUBSRTReverseTransactionRequestVDATEDR_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "VDATEDR");
    private final static QName _FCUBSRTReverseTransactionRequestVDATECR_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "VDATECR");
    private final static QName _FCUBSRTReverseTransactionRequestCALCMETH_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CALCMETH");
    private final static QName _CheckAccountExistInfoAccountName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountName");
    private final static QName _MultipleJournalDetailTrxTransactionCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TransactionCode");
    private final static QName _MultipleJournalDetailTrxACCORGL_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ACCORGL");
    private final static QName _MultipleJournalDetailTrxSerialNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SerialNumber");
    private final static QName _MultipleJournalDetailTrxLocalCurrencyAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LocalCurrencyAmount");
    private final static QName _MultipleJournalDetailTrxAccountNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountNumber");
    private final static QName _MultipleJournalDetailTrxAdditionalText_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AdditionalText");
    private final static QName _MultipleJournalDetailTrxCustomerId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CustomerId");
    private final static QName _MultipleJournalDetailTrxInstrumentNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "InstrumentNumber");
    private final static QName _MultipleJournalDetailTrxExchangeRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ExchangeRate");
    private final static QName _MultipleJournalDetailTrxTransactionType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TransactionType");
    private final static QName _MultipleJournalDetailTrxUserReferenceNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UserReferenceNumber");
    private final static QName _FCUBSCreateTDAccountRequestRolloverType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "RolloverType");
    private final static QName _FCUBSCreateTDAccountRequestTDAccountName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TDAccountName");
    private final static QName _FCUBSCreateTDAccountRequestOffsetAccountNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OffsetAccountNo");
    private final static QName _FCUBSCreateTDAccountRequestAutoRollover_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AutoRollover");
    private final static QName _FCUBSCreateTDAccountRequestTDTenor_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TDTenor");
    private final static QName _FCUBSCreateTDAccountRequestTDInterestRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TDInterestRate");
    private final static QName _FCUBSCreateTDAccountRequestOffsetBranchCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OffsetBranchCode");
    private final static QName _FCUBSCreateTDAccountRequestPoolCD_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PoolCD");
    private final static QName _FCUBSCreateTDAccountRequestTDCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TDCcyCode");
    private final static QName _FCUBSCreateTDAccountRequestTDAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TDAmount");
    private final static QName _SingleJournalTrxPeriodCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PeriodCode");
    private final static QName _SingleJournalTrxCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Currency");
    private final static QName _SingleJournalTrxFinancialCycle_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FinancialCycle");
    private final static QName _SingleJournalTrxTaxationCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TaxationCode");
    private final static QName _SingleJournalTrxCompMisCode8_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMisCode8");
    private final static QName _SingleJournalTrxDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Description");
    private final static QName _SingleJournalTrxCompMisCode2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMisCode2");
    private final static QName _SingleJournalTrxCompMisCode1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMisCode1");
    private final static QName _SingleJournalTrxCompMisCode4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMisCode4");
    private final static QName _SingleJournalTrxCompMisCode3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CompMisCode3");
    private final static QName _SingleJournalTrxBatchDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BatchDescription");
    private final static QName _SingleJournalTrxAccountDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountDescription");
    private final static QName _AccountCotAndVatLinesCotDetails_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CotDetails");
    private final static QName _AccountCotAndVatLinesTranDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranDate");
    private final static QName _VisaCardLimitSummaryLinesCardType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CardType");
    private final static QName _VisaCardLimitSummaryLinesCardCurrency_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CardCurrency");
    private final static QName _VisaCardLimitSummaryLinesCardStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CardStatus");
    private final static QName _VisaCardLimitSummaryLinesNewCreditLimit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "NewCreditLimit");
    private final static QName _VisaCardLimitSummaryLinesLimitUtilization_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LimitUtilization");
    private final static QName _VisaCardLimitSummaryLinesSignStat_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "SignStat");
    private final static QName _FCUBSCreateCustomerRequestBUZSECTORCustomerMISClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BUZSECTOR_CustomerMISClass");
    private final static QName _FCUBSCreateCustomerRequestUDFIncomeBandFieldName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UDF_IncomeBandFieldName");
    private final static QName _FCUBSCreateCustomerRequestGuardian_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Guardian");
    private final static QName _FCUBSCreateCustomerRequestUDFProfessionFieldName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UDF_ProfessionFieldName");
    private final static QName _FCUBSCreateCustomerRequestMothersMaidenName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MothersMaidenName");
    private final static QName _FCUBSCreateCustomerRequestHomePhoneNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "HomePhoneNo");
    private final static QName _FCUBSCreateCustomerRequestUDFReligionFieldName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UDF_ReligionFieldName");
    private final static QName _FCUBSCreateCustomerRequestHomeTelISDNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "HomeTelISDNo");
    private final static QName _FCUBSCreateCustomerRequestTelephoneNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TelephoneNumber");
    private final static QName _FCUBSCreateCustomerRequestShortName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ShortName");
    private final static QName _FCUBSCreateCustomerRequestGender_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Gender");
    private final static QName _FCUBSCreateCustomerRequestNationalID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "NationalID");
    private final static QName _FCUBSCreateCustomerRequestAcctOfficerCompositeMISCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AcctOfficer_CompositeMISCode");
    private final static QName _FCUBSCreateCustomerRequestProfitCenterCompositeMISCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ProfitCenter_CompositeMISCode");
    private final static QName _FCUBSCreateCustomerRequestEmailID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmailID");
    private final static QName _FCUBSCreateCustomerRequestIdentityExpiryDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "IdentityExpiryDate");
    private final static QName _FCUBSCreateCustomerRequestMinorYesorNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MinorYesorNo");
    private final static QName _FCUBSCreateCustomerRequestPermanentAddressLine4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PermanentAddressLine4");
    private final static QName _FCUBSCreateCustomerRequestPermanentAddressLine3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PermanentAddressLine3");
    private final static QName _FCUBSCreateCustomerRequestPermanentAddressLine2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PermanentAddressLine2");
    private final static QName _FCUBSCreateCustomerRequestDateofBirth_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DateofBirth");
    private final static QName _FCUBSCreateCustomerRequestLastName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LastName");
    private final static QName _FCUBSCreateCustomerRequestOJCAcctCompositeMISClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OJCAcct_CompositeMISClass");
    private final static QName _FCUBSCreateCustomerRequestLiabilityCcy_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LiabilityCcy");
    private final static QName _FCUBSCreateCustomerRequestAUCCustomerMISClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AUC_CustomerMISClass");
    private final static QName _FCUBSCreateCustomerRequestCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Country");
    private final static QName _FCUBSCreateCustomerRequestPlaceofBirth_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PlaceofBirth");
    private final static QName _FCUBSCreateCustomerRequestUDFIncomeBandFieldValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UDF_IncomeBandFieldValue");
    private final static QName _FCUBSCreateCustomerRequestPermanentAddressLine1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PermanentAddressLine1");
    private final static QName _FCUBSCreateCustomerRequestCustomerFullname_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CustomerFullname");
    private final static QName _FCUBSCreateCustomerRequestCustomerCat_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CustomerCat");
    private final static QName _FCUBSCreateCustomerRequestCustomerType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CustomerType");
    private final static QName _FCUBSCreateCustomerRequestEmployerFax_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerFax");
    private final static QName _FCUBSCreateCustomerRequestEmploymentStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmploymentStatus");
    private final static QName _FCUBSCreateCustomerRequestSamePermanentAddress_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SamePermanentAddress");
    private final static QName _FCUBSCreateCustomerRequestIdentityIssueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "IdentityIssueDate");
    private final static QName _FCUBSCreateCustomerRequestUDFProfessionFieldValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UDF_ProfessionFieldValue");
    private final static QName _FCUBSCreateCustomerRequestIdentityNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "IdentityNo");
    private final static QName _FCUBSCreateCustomerRequestLiabilityName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LiabilityName");
    private final static QName _FCUBSCreateCustomerRequestCustomerCommMode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CustomerCommMode");
    private final static QName _FCUBSCreateCustomerRequestProfitCenterCompositeMISClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ProfitCenter_CompositeMISClass");
    private final static QName _FCUBSCreateCustomerRequestLanguage_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Language");
    private final static QName _FCUBSCreateCustomerRequestOJCAcctCompositeMISCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "OJCAcct_CompositeMISCode");
    private final static QName _FCUBSCreateCustomerRequestPinCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "PinCode");
    private final static QName _FCUBSCreateCustomerRequestResidentStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ResidentStatus");
    private final static QName _FCUBSCreateCustomerRequestAccountHolderMaritalStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AccountHolderMaritalStatus");
    private final static QName _FCUBSCreateCustomerRequestUDFReligionFieldValue_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UDF_ReligionFieldValue");
    private final static QName _FCUBSCreateCustomerRequestDomiciledPinCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DomiciledPinCode");
    private final static QName _FCUBSCreateCustomerRequestSpouseEmploymentStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SpouseEmploymentStatus");
    private final static QName _FCUBSCreateCustomerRequestEmployerDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerDesc");
    private final static QName _FCUBSCreateCustomerRequestBUZSECTORCustomerMISCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BUZSECTOR_CustomerMISCode");
    private final static QName _FCUBSCreateCustomerRequestLocalBranch_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LocalBranch");
    private final static QName _FCUBSCreateCustomerRequestAcctOfficerCompositeMISClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AcctOfficer_CompositeMISClass");
    private final static QName _FCUBSCreateCustomerRequestEmployerEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerEmail");
    private final static QName _FCUBSCreateCustomerRequestMobileNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MobileNumber");
    private final static QName _FCUBSCreateCustomerRequestAUCCustomerMISCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AUC_CustomerMISCode");
    private final static QName _FCUBSCreateCustomerRequestTelephoneISDNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "TelephoneISDNo");
    private final static QName _FCUBSCreateCustomerRequestEmployerAddress3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerAddress3");
    private final static QName _FCUBSCreateCustomerRequestNationality_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Nationality");
    private final static QName _FCUBSCreateCustomerRequestEmployerAddress1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerAddress1");
    private final static QName _FCUBSCreateCustomerRequestMiddleName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MiddleName");
    private final static QName _FCUBSCreateCustomerRequestMobileISDNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MobileISDNo");
    private final static QName _FCUBSCreateCustomerRequestBirthCountry_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BirthCountry");
    private final static QName _FCUBSCreateCustomerRequestEmployerAddress2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerAddress2");
    private final static QName _FCUBSCreateCustomerRequestSpouseName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "SpouseName");
    private final static QName _FCUBSCreateCustomerRequestDomiciledAddressLine1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DomiciledAddressLine1");
    private final static QName _FCUBSCreateCustomerRequestTitle_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "Title");
    private final static QName _FCUBSCreateCustomerRequestDomiciledAddressLine2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DomiciledAddressLine2");
    private final static QName _FCUBSCreateCustomerRequestFirstName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "FirstName");
    private final static QName _FCUBSCreateCustomerRequestDomiciledAddressLine3_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DomiciledAddressLine3");
    private final static QName _FCUBSCreateCustomerRequestDomiciledAddressLine4_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "DomiciledAddressLine4");
    private final static QName _FCUBSCreateCustomerRequestEmployerTelNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EmployerTelNo");
    private final static QName _FCUBSCreateCustomerRequestLiabilityBranch_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "LiabilityBranch");
    private final static QName _SingleTransactionDetailsTransactionNameDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionNameDesc");
    private final static QName _SingleTransactionDetailsTranRefNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TranRefNo");
    private final static QName _SingleTransactionDetailsDatePost_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "datePost");
    private final static QName _SingleTransactionDetailsMessageTypeCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "MessageTypeCode");
    private final static QName _SingleTransactionDetailsControlBatchNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "controlBatchNumber");
    private final static QName _SingleTransactionDetailsTransactionType_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionType");
    private final static QName _SingleTransactionDetailsTransactionOfficerInputter_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionOfficerInputter");
    private final static QName _SingleTransactionDetailsTranAmount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "tranAmount");
    private final static QName _SingleTransactionDetailsTransactionDateTime_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionDateTime");
    private final static QName _SingleTransactionDetailsTransactionOfficerAuthorizer_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionOfficerAuthorizer");
    private final static QName _SingleTransactionDetailsCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ccyCode");
    private final static QName _SingleTransactionDetailsCcyName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ccyName");
    private final static QName _SingleTransactionDetailsAccountNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountNumber");
    private final static QName _SingleTransactionDetailsTransactionName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionName");
    private final static QName _SingleTransactionDetailsTransactionCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionCode");
    private final static QName _SingleTransactionDetailsValueDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "valueDate");
    private final static QName _SingleTransactionDetailsTransactionDescription_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionDescription");
    private final static QName _FCUBSCreateStopPaymentRequestAMTONCHEQUE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "AMT_ON_CHEQUE");
    private final static QName _FCUBSCreateStopPaymentRequestACCOUNTNUMBER_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ACCOUNT_NUMBER");
    private final static QName _FCUBSCreateStopPaymentRequestCCYCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CCY_CODE");
    private final static QName _FCUBSCreateStopPaymentRequestSTOPREMARK_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "STOP_REMARK");
    private final static QName _FCUBSCreateStopPaymentRequestACCOUNTBRANCHCODE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "ACCOUNT_BRANCHCODE");
    private final static QName _FCUBSCreateStopPaymentRequestEXPIRYDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EXPIRY_DATE");
    private final static QName _FCUBSCreateStopPaymentRequestCHEQUENO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CHEQUE_NO");
    private final static QName _FCUBSCreateStopPaymentRequestEFFECTIVEDATE_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "EFFECTIVE_DATE");
    private final static QName _FCUBSCreateLDContractRequestCcyCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "CcyCode");
    private final static QName _FCUBSCreateLDContractRequestInterestRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "InterestRate");
    private final static QName _FCUBSCreateLDContractRequestBookDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "BookDate");
    private final static QName _FCUBSCreateLDContractRequestUniqueReferenceNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "UniqueReferenceNo");
    private final static QName _FCUBSCreateLDContractRequestMaturityDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", "MaturityDate");
    private final static QName _TransactionRefNoDetailsAccountname_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountname");
    private final static QName _TransactionRefNoDetailsAccountno_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountno");
    private final static QName _TransactionRefNoDetailsEventserialno_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "eventserialno");
    private final static QName _TransactionRefNoDetailsValuedate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "valuedate");
    private final static QName _TransactionRefNoDetailsCustomerID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "customerID");
    private final static QName _TransactionRefNoDetailsTransactioncode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactioncode");
    private final static QName _TransactionRefNoDetailsAccountbranch_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountbranch");
    private final static QName _TransactionRefNoDetailsAccountopendate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountopendate");
    private final static QName _TransactionRefNoDetailsDrcrInd_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "drcr_ind");
    private final static QName _TransactionRefNoDetailsTransactiondate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactiondate");
    private final static QName _TransactionRefNoDetailsBatchno_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "batchno");
    private final static QName _TransactionRefNoDetailsEventdesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "eventdesc");
    private final static QName _TransactionRefNoDetailsCurrencycode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "currencycode");
    private final static QName _TransactionRefNoDetailsTraninitiationdate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "traninitiationdate");
    private final static QName _TransactionRefNoDetailsAccountclass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "accountclass");
    private final static QName _TransactionRefNoDetailsTransactionamount_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionamount");
    private final static QName _TransactionRefNoDetailsTransactionrefno_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "transactionrefno");
    private final static QName _GetChequeValueDateRespInstrumentNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "instrument_no");
    private final static QName _GetChequeValueDateRespValDateCust_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "val_date_cust");
    private final static QName _AccountSearchResponseLinesAccountClass_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountClass");
    private final static QName _AccountSearchResponseLinesEmail_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Email");
    private final static QName _AccountSearchResponseLinesCurrencyCodeDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CurrencyCodeDesc");
    private final static QName _AccountSearchResponseLinesDateAcctOpened_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "DateAcctOpened");
    private final static QName _AccountSearchResponseLinesTelephoneNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TelephoneNo");
    private final static QName _UserLimitResponseRoleId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "role_id");
    private final static QName _UserLimitResponseUserId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "user_id");
    private final static QName _UserLimitResponseAuthLimit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "auth_limit");
    private final static QName _UserLimitResponseInputLimit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "input_limit");
    private final static QName _CustomerInformationEffectiveBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "EffectiveBalance");
    private final static QName _CustomerInformationPhoneNumber_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PhoneNumber");
    private final static QName _CustomerInformationTIN_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TIN");
    private final static QName _CustomerInformationLastName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LastName");
    private final static QName _CustomerInformationFirstName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "FirstName");
    private final static QName _CustomerInformationAccountBranch_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountBranch");
    private final static QName _CustomerInformationMiddleName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "MiddleName");
    private final static QName _CustomerInformationAccountClassDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountClassDesc");
    private final static QName _CustomerInformationAccountOfficerCode_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountOfficerCode");
    private final static QName _CustomerInformationDivision_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Division");
    private final static QName _CustomerInformationTypeofClient_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TypeofClient");
    private final static QName _CustomerInformationBranchState_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BranchState");
    private final static QName _CustomerInformationCurrencyDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CurrencyDesc");
    private final static QName _CustomerInformationBookBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BookBalance");
    private final static QName _GetAccountExistResponseAccountInfo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountInfo");
    private final static QName _CreateAmtBlockResponseAMTBLKNO_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AMTBLKNO");
    private final static QName _NubanAcctsDetailsCodAcctNoOld_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_acct_no_old");
    private final static QName _NubanAcctsDetailsCodAcctNoNew_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_acct_no_new");
    private final static QName _ImageCollectionDetailsCifSigName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cif_sig_name");
    private final static QName _ImageCollectionDetailsTxtAcctOperInstrs1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "txt_acct_oper_instrs1");
    private final static QName _ImageCollectionDetailsCustomerImageBytes_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerImage_Bytes");
    private final static QName _ImageCollectionDetailsCustImageURL1Bytes_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cust_imageURL1_Bytes");
    private final static QName _ImageCollectionDetailsNamCustShrt_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "nam_cust_shrt");
    private final static QName _ImageCollectionDetailsCustomerImage_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustomerImage");
    private final static QName _ImageCollectionDetailsNamCustShrtX_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "nam_cust_shrt_X");
    private final static QName _ImageCollectionDetailsCodCust_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_cust");
    private final static QName _ImageCollectionDetailsCustImageURL2_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cust_imageURL2");
    private final static QName _ImageCollectionDetailsCustImageURL1_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cust_imageURL1");
    private final static QName _ImageCollectionDetailsCodCustId_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cod_cust_id");
    private final static QName _ImageCollectionDetailsCustImageURL2Bytes_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "cust_imageURL2_Bytes");
    private final static QName _GetEffectiveBalanceRespAcyAvlBal_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "acy_avl_bal");
    private final static QName _LoanSummaryLineOverdueInterest_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "OverdueInterest");
    private final static QName _LoanSummaryLinePrincipalDisbursed_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PrincipalDisbursed");
    private final static QName _LoanSummaryLineOverduePrincipal_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "OverduePrincipal");
    private final static QName _LoanSummaryLineLoanTenor_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanTenor");
    private final static QName _LoanSummaryLineLoanStatus_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanStatus");
    private final static QName _TellerLimitResponseMaxTxnAmt_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "max_txn_amt");
    private final static QName _TellerLimitResponseLimitsCcy_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "limits_ccy");
    private final static QName _TellerLimitResponseMaxOverrideAmt_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "max_override_amt");
    private final static QName _TellerLimitResponseMaxAuthAmt_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "max_auth_amt");
    private final static QName _AccountSearchResponseAccountSearchResult_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountSearchResult");
    private final static QName _AccountSummaryLineCustID_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CustID");
    private final static QName _AccountSummaryLineLodgement_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Lodgement");
    private final static QName _AccountSummaryLineNetBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "NetBalance");
    private final static QName _AccountSummaryLineTotalBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "TotalBalance");
    private final static QName _AccountSummaryLineBranchCity_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "BranchCity");
    private final static QName _AccountSummaryLineWithdrawal_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "Withdrawal");
    private final static QName _AccountSummaryLineUnclearedBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "UnclearedBalance");
    private final static QName _AccountSummaryLineCurrencyName_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "CurrencyName");
    private final static QName _AccountSummaryLineAcctStatusDesc_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AcctStatusDesc");
    private final static QName _AccountSummaryLineAmountBlocked_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AmountBlocked");
    private final static QName _AccountSummaryLineAvailableToWithdraw_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AvailableToWithdraw");
    private final static QName _AccountSummaryLineAmtOdLimit_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "amt_od_limit");
    private final static QName _AccountSummaryLineAccountOpenDate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "AccountOpenDate");
    private final static QName _AccountSummaryLineHasCOT_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "HasCOT");
    private final static QName _AccountSummaryLineClearedBalance_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "ClearedBalance");
    private final static QName _AccountSummaryLineHasImage_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "hasImage");
    private final static QName _RelatedAccountSummaryLinesPhoneNo_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "PhoneNo");
    private final static QName _CurrencyRateResponseMidRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "mid_rate");
    private final static QName _CurrencyRateResponseSaleRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "sale_rate");
    private final static QName _CurrencyRateResponseBuyRate_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "buy_rate");
    private final static QName _LoanInfoSummaryResponseLoanInfoSummary_QNAME = new QName("http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", "LoanInfoSummary");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.fcubs_webservices_shared
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SampleAccountResponse }
     * 
     */
    public SampleAccountResponse createSampleAccountResponse() {
        return new SampleAccountResponse();
    }

    /**
     * Create an instance of {@link ArrayOfImageCollectionDetails }
     * 
     */
    public ArrayOfImageCollectionDetails createArrayOfImageCollectionDetails() {
        return new ArrayOfImageCollectionDetails();
    }

    /**
     * Create an instance of {@link FcubsResponse }
     * 
     */
    public FcubsResponse createFcubsResponse() {
        return new FcubsResponse();
    }

    /**
     * Create an instance of {@link SingleJournalTrx }
     * 
     */
    public SingleJournalTrx createSingleJournalTrx() {
        return new SingleJournalTrx();
    }

    /**
     * Create an instance of {@link AccountSummaryAndTrxResponse }
     * 
     */
    public AccountSummaryAndTrxResponse createAccountSummaryAndTrxResponse() {
        return new AccountSummaryAndTrxResponse();
    }

    /**
     * Create an instance of {@link CustomerODLimitResponse }
     * 
     */
    public CustomerODLimitResponse createCustomerODLimitResponse() {
        return new CustomerODLimitResponse();
    }

    /**
     * Create an instance of {@link AuthenticateCBAUserResp }
     * 
     */
    public AuthenticateCBAUserResp createAuthenticateCBAUserResp() {
        return new AuthenticateCBAUserResp();
    }

    /**
     * Create an instance of {@link TellerLimitResponse }
     * 
     */
    public TellerLimitResponse createTellerLimitResponse() {
        return new TellerLimitResponse();
    }

    /**
     * Create an instance of {@link AccountSummaryResponse }
     * 
     */
    public AccountSummaryResponse createAccountSummaryResponse() {
        return new AccountSummaryResponse();
    }

    /**
     * Create an instance of {@link AccountSearchResponse }
     * 
     */
    public AccountSearchResponse createAccountSearchResponse() {
        return new AccountSearchResponse();
    }

    /**
     * Create an instance of {@link CreateStopPaymentResponse }
     * 
     */
    public CreateStopPaymentResponse createCreateStopPaymentResponse() {
        return new CreateStopPaymentResponse();
    }

    /**
     * Create an instance of {@link ChequeDetailsResponse }
     * 
     */
    public ChequeDetailsResponse createChequeDetailsResponse() {
        return new ChequeDetailsResponse();
    }

    /**
     * Create an instance of {@link FCUBSCustomerServiceAmtBlkRequest }
     * 
     */
    public FCUBSCustomerServiceAmtBlkRequest createFCUBSCustomerServiceAmtBlkRequest() {
        return new FCUBSCustomerServiceAmtBlkRequest();
    }

    /**
     * Create an instance of {@link GetAccountBranchResp }
     * 
     */
    public GetAccountBranchResp createGetAccountBranchResp() {
        return new GetAccountBranchResp();
    }

    /**
     * Create an instance of {@link FCUBSCreateTDAccountRequest }
     * 
     */
    public FCUBSCreateTDAccountRequest createFCUBSCreateTDAccountRequest() {
        return new FCUBSCreateTDAccountRequest();
    }

    /**
     * Create an instance of {@link UserLimitResponse }
     * 
     */
    public UserLimitResponse createUserLimitResponse() {
        return new UserLimitResponse();
    }

    /**
     * Create an instance of {@link LoanSummaryResponseBulk }
     * 
     */
    public LoanSummaryResponseBulk createLoanSummaryResponseBulk() {
        return new LoanSummaryResponseBulk();
    }

    /**
     * Create an instance of {@link GetCurrencyCodeResp }
     * 
     */
    public GetCurrencyCodeResp createGetCurrencyCodeResp() {
        return new GetCurrencyCodeResp();
    }

    /**
     * Create an instance of {@link FCUBSFTCreateTransactionRequest }
     * 
     */
    public FCUBSFTCreateTransactionRequest createFCUBSFTCreateTransactionRequest() {
        return new FCUBSFTCreateTransactionRequest();
    }

    /**
     * Create an instance of {@link AccountCotAndVatResponse }
     * 
     */
    public AccountCotAndVatResponse createAccountCotAndVatResponse() {
        return new AccountCotAndVatResponse();
    }

    /**
     * Create an instance of {@link CreateMMContractResponse }
     * 
     */
    public CreateMMContractResponse createCreateMMContractResponse() {
        return new CreateMMContractResponse();
    }

    /**
     * Create an instance of {@link CreateRTResponse }
     * 
     */
    public CreateRTResponse createCreateRTResponse() {
        return new CreateRTResponse();
    }

    /**
     * Create an instance of {@link CreateCustomerResponse }
     * 
     */
    public CreateCustomerResponse createCreateCustomerResponse() {
        return new CreateCustomerResponse();
    }

    /**
     * Create an instance of {@link MultipleJournalMasterTrx }
     * 
     */
    public MultipleJournalMasterTrx createMultipleJournalMasterTrx() {
        return new MultipleJournalMasterTrx();
    }

    /**
     * Create an instance of {@link LoanInfoSummaryResponse }
     * 
     */
    public LoanInfoSummaryResponse createLoanInfoSummaryResponse() {
        return new LoanInfoSummaryResponse();
    }

    /**
     * Create an instance of {@link AccountRelatedResponse }
     * 
     */
    public AccountRelatedResponse createAccountRelatedResponse() {
        return new AccountRelatedResponse();
    }

    /**
     * Create an instance of {@link FCUBSRTCreateTransactionRequest }
     * 
     */
    public FCUBSRTCreateTransactionRequest createFCUBSRTCreateTransactionRequest() {
        return new FCUBSRTCreateTransactionRequest();
    }

    /**
     * Create an instance of {@link ValidateUserTillResp }
     * 
     */
    public ValidateUserTillResp createValidateUserTillResp() {
        return new ValidateUserTillResp();
    }

    /**
     * Create an instance of {@link ArrayOfNubanAcctsDetails }
     * 
     */
    public ArrayOfNubanAcctsDetails createArrayOfNubanAcctsDetails() {
        return new ArrayOfNubanAcctsDetails();
    }

    /**
     * Create an instance of {@link FCUBSCreateCustAccountRequest }
     * 
     */
    public FCUBSCreateCustAccountRequest createFCUBSCreateCustAccountRequest() {
        return new FCUBSCreateCustAccountRequest();
    }

    /**
     * Create an instance of {@link FCUBSCreateLDContractRequest }
     * 
     */
    public FCUBSCreateLDContractRequest createFCUBSCreateLDContractRequest() {
        return new FCUBSCreateLDContractRequest();
    }

    /**
     * Create an instance of {@link CreateTDAccountResponse }
     * 
     */
    public CreateTDAccountResponse createCreateTDAccountResponse() {
        return new CreateTDAccountResponse();
    }

    /**
     * Create an instance of {@link CustomerDetails }
     * 
     */
    public CustomerDetails createCustomerDetails() {
        return new CustomerDetails();
    }

    /**
     * Create an instance of {@link FCUBSCreateStopPaymentRequest }
     * 
     */
    public FCUBSCreateStopPaymentRequest createFCUBSCreateStopPaymentRequest() {
        return new FCUBSCreateStopPaymentRequest();
    }

    /**
     * Create an instance of {@link CreateCustAccountResponse }
     * 
     */
    public CreateCustAccountResponse createCreateCustAccountResponse() {
        return new CreateCustAccountResponse();
    }

    /**
     * Create an instance of {@link FCUBSCGServiceCreateTransactionRequest }
     * 
     */
    public FCUBSCGServiceCreateTransactionRequest createFCUBSCGServiceCreateTransactionRequest() {
        return new FCUBSCGServiceCreateTransactionRequest();
    }

    /**
     * Create an instance of {@link GetChequeValueDateResp }
     * 
     */
    public GetChequeValueDateResp createGetChequeValueDateResp() {
        return new GetChequeValueDateResp();
    }

    /**
     * Create an instance of {@link FCUBSCreateMMContractRequest }
     * 
     */
    public FCUBSCreateMMContractRequest createFCUBSCreateMMContractRequest() {
        return new FCUBSCreateMMContractRequest();
    }

    /**
     * Create an instance of {@link VisaCardLimitResponse }
     * 
     */
    public VisaCardLimitResponse createVisaCardLimitResponse() {
        return new VisaCardLimitResponse();
    }

    /**
     * Create an instance of {@link GetAccountExistResponse }
     * 
     */
    public GetAccountExistResponse createGetAccountExistResponse() {
        return new GetAccountExistResponse();
    }

    /**
     * Create an instance of {@link CreateAmtBlockResponse }
     * 
     */
    public CreateAmtBlockResponse createCreateAmtBlockResponse() {
        return new CreateAmtBlockResponse();
    }

    /**
     * Create an instance of {@link CreateLDContractResponse }
     * 
     */
    public CreateLDContractResponse createCreateLDContractResponse() {
        return new CreateLDContractResponse();
    }

    /**
     * Create an instance of {@link RepaymentScheduleResponse }
     * 
     */
    public RepaymentScheduleResponse createRepaymentScheduleResponse() {
        return new RepaymentScheduleResponse();
    }

    /**
     * Create an instance of {@link ScreenLimitResponse }
     * 
     */
    public ScreenLimitResponse createScreenLimitResponse() {
        return new ScreenLimitResponse();
    }

    /**
     * Create an instance of {@link FCUBSCreateCustomerRequest }
     * 
     */
    public FCUBSCreateCustomerRequest createFCUBSCreateCustomerRequest() {
        return new FCUBSCreateCustomerRequest();
    }

    /**
     * Create an instance of {@link FCUBSRTReverseTransactionRequest }
     * 
     */
    public FCUBSRTReverseTransactionRequest createFCUBSRTReverseTransactionRequest() {
        return new FCUBSRTReverseTransactionRequest();
    }

    /**
     * Create an instance of {@link FCUBSCreateSIContractRequest }
     * 
     */
    public FCUBSCreateSIContractRequest createFCUBSCreateSIContractRequest() {
        return new FCUBSCreateSIContractRequest();
    }

    /**
     * Create an instance of {@link CustomerInformationResponse }
     * 
     */
    public CustomerInformationResponse createCustomerInformationResponse() {
        return new CustomerInformationResponse();
    }

    /**
     * Create an instance of {@link LoanSummaryResponse }
     * 
     */
    public LoanSummaryResponse createLoanSummaryResponse() {
        return new LoanSummaryResponse();
    }

    /**
     * Create an instance of {@link FCUBSCloseCustAccRequest }
     * 
     */
    public FCUBSCloseCustAccRequest createFCUBSCloseCustAccRequest() {
        return new FCUBSCloseCustAccRequest();
    }

    /**
     * Create an instance of {@link AuthorizeWithdrawalRequest }
     * 
     */
    public AuthorizeWithdrawalRequest createAuthorizeWithdrawalRequest() {
        return new AuthorizeWithdrawalRequest();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerAcctsDetailResponse }
     * 
     */
    public ArrayOfCustomerAcctsDetailResponse createArrayOfCustomerAcctsDetailResponse() {
        return new ArrayOfCustomerAcctsDetailResponse();
    }

    /**
     * Create an instance of {@link ActivateChequeBookResponse }
     * 
     */
    public ActivateChequeBookResponse createActivateChequeBookResponse() {
        return new ActivateChequeBookResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSweepAcctsDetails }
     * 
     */
    public ArrayOfSweepAcctsDetails createArrayOfSweepAcctsDetails() {
        return new ArrayOfSweepAcctsDetails();
    }

    /**
     * Create an instance of {@link FixedDepositResponse }
     * 
     */
    public FixedDepositResponse createFixedDepositResponse() {
        return new FixedDepositResponse();
    }

    /**
     * Create an instance of {@link GetUserBranchResp }
     * 
     */
    public GetUserBranchResp createGetUserBranchResp() {
        return new GetUserBranchResp();
    }

    /**
     * Create an instance of {@link CurrencyRateResponse }
     * 
     */
    public CurrencyRateResponse createCurrencyRateResponse() {
        return new CurrencyRateResponse();
    }

    /**
     * Create an instance of {@link ArrayOfSingleTransactionDetails }
     * 
     */
    public ArrayOfSingleTransactionDetails createArrayOfSingleTransactionDetails() {
        return new ArrayOfSingleTransactionDetails();
    }

    /**
     * Create an instance of {@link AccountTransactionResponseBulk }
     * 
     */
    public AccountTransactionResponseBulk createAccountTransactionResponseBulk() {
        return new AccountTransactionResponseBulk();
    }

    /**
     * Create an instance of {@link CreateSIContractResponse }
     * 
     */
    public CreateSIContractResponse createCreateSIContractResponse() {
        return new CreateSIContractResponse();
    }

    /**
     * Create an instance of {@link GetAcctMemoInfoResponse }
     * 
     */
    public GetAcctMemoInfoResponse createGetAcctMemoInfoResponse() {
        return new GetAcctMemoInfoResponse();
    }

    /**
     * Create an instance of {@link CustomerAcctsDetailResponse }
     * 
     */
    public CustomerAcctsDetailResponse createCustomerAcctsDetailResponse() {
        return new CustomerAcctsDetailResponse();
    }

    /**
     * Create an instance of {@link GetEffectiveBalanceResp }
     * 
     */
    public GetEffectiveBalanceResp createGetEffectiveBalanceResp() {
        return new GetEffectiveBalanceResp();
    }

    /**
     * Create an instance of {@link ArrayOfMultipleJournalDetailTrx }
     * 
     */
    public ArrayOfMultipleJournalDetailTrx createArrayOfMultipleJournalDetailTrx() {
        return new ArrayOfMultipleJournalDetailTrx();
    }

    /**
     * Create an instance of {@link MultipleJournalDetailTrx }
     * 
     */
    public MultipleJournalDetailTrx createMultipleJournalDetailTrx() {
        return new MultipleJournalDetailTrx();
    }

    /**
     * Create an instance of {@link ArrayOfRepaymentScheduleInfo }
     * 
     */
    public ArrayOfRepaymentScheduleInfo createArrayOfRepaymentScheduleInfo() {
        return new ArrayOfRepaymentScheduleInfo();
    }

    /**
     * Create an instance of {@link ChequeNumberDetails }
     * 
     */
    public ChequeNumberDetails createChequeNumberDetails() {
        return new ChequeNumberDetails();
    }

    /**
     * Create an instance of {@link CustomerInformation }
     * 
     */
    public CustomerInformation createCustomerInformation() {
        return new CustomerInformation();
    }

    /**
     * Create an instance of {@link ArrayOfChequeNumberDetails }
     * 
     */
    public ArrayOfChequeNumberDetails createArrayOfChequeNumberDetails() {
        return new ArrayOfChequeNumberDetails();
    }

    /**
     * Create an instance of {@link VisaCardLimitSummaryLines }
     * 
     */
    public VisaCardLimitSummaryLines createVisaCardLimitSummaryLines() {
        return new VisaCardLimitSummaryLines();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerInformation }
     * 
     */
    public ArrayOfCustomerInformation createArrayOfCustomerInformation() {
        return new ArrayOfCustomerInformation();
    }

    /**
     * Create an instance of {@link ArrayOfAccountMemoInfo }
     * 
     */
    public ArrayOfAccountMemoInfo createArrayOfAccountMemoInfo() {
        return new ArrayOfAccountMemoInfo();
    }

    /**
     * Create an instance of {@link ChequeDetails }
     * 
     */
    public ChequeDetails createChequeDetails() {
        return new ChequeDetails();
    }

    /**
     * Create an instance of {@link LoanInfoSummaryLines }
     * 
     */
    public LoanInfoSummaryLines createLoanInfoSummaryLines() {
        return new LoanInfoSummaryLines();
    }

    /**
     * Create an instance of {@link RepaymentScheduleInfo }
     * 
     */
    public RepaymentScheduleInfo createRepaymentScheduleInfo() {
        return new RepaymentScheduleInfo();
    }

    /**
     * Create an instance of {@link ArrayOfVisaCardLimitSummaryLines }
     * 
     */
    public ArrayOfVisaCardLimitSummaryLines createArrayOfVisaCardLimitSummaryLines() {
        return new ArrayOfVisaCardLimitSummaryLines();
    }

    /**
     * Create an instance of {@link ArrayOfTransactionRefNoDetails }
     * 
     */
    public ArrayOfTransactionRefNoDetails createArrayOfTransactionRefNoDetails() {
        return new ArrayOfTransactionRefNoDetails();
    }

    /**
     * Create an instance of {@link TransactionRefNoDetails }
     * 
     */
    public TransactionRefNoDetails createTransactionRefNoDetails() {
        return new TransactionRefNoDetails();
    }

    /**
     * Create an instance of {@link LoanSummaryLine }
     * 
     */
    public LoanSummaryLine createLoanSummaryLine() {
        return new LoanSummaryLine();
    }

    /**
     * Create an instance of {@link AccountSearchResponseLines }
     * 
     */
    public AccountSearchResponseLines createAccountSearchResponseLines() {
        return new AccountSearchResponseLines();
    }

    /**
     * Create an instance of {@link FixedDepositInfo }
     * 
     */
    public FixedDepositInfo createFixedDepositInfo() {
        return new FixedDepositInfo();
    }

    /**
     * Create an instance of {@link NubanAcctsDetails }
     * 
     */
    public NubanAcctsDetails createNubanAcctsDetails() {
        return new NubanAcctsDetails();
    }

    /**
     * Create an instance of {@link AccountCotAndVatLines }
     * 
     */
    public AccountCotAndVatLines createAccountCotAndVatLines() {
        return new AccountCotAndVatLines();
    }

    /**
     * Create an instance of {@link AccountMemoInfo }
     * 
     */
    public AccountMemoInfo createAccountMemoInfo() {
        return new AccountMemoInfo();
    }

    /**
     * Create an instance of {@link RelatedAccountSummaryLines }
     * 
     */
    public RelatedAccountSummaryLines createRelatedAccountSummaryLines() {
        return new RelatedAccountSummaryLines();
    }

    /**
     * Create an instance of {@link AccountSummaryAndTrxLines }
     * 
     */
    public AccountSummaryAndTrxLines createAccountSummaryAndTrxLines() {
        return new AccountSummaryAndTrxLines();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerAcctsDetail }
     * 
     */
    public ArrayOfCustomerAcctsDetail createArrayOfCustomerAcctsDetail() {
        return new ArrayOfCustomerAcctsDetail();
    }

    /**
     * Create an instance of {@link SingleTransactionDetails }
     * 
     */
    public SingleTransactionDetails createSingleTransactionDetails() {
        return new SingleTransactionDetails();
    }

    /**
     * Create an instance of {@link ArrayOfAccountSummaryAndTrxResponse }
     * 
     */
    public ArrayOfAccountSummaryAndTrxResponse createArrayOfAccountSummaryAndTrxResponse() {
        return new ArrayOfAccountSummaryAndTrxResponse();
    }

    /**
     * Create an instance of {@link ArrayOfAccountSummaryAndTrxLines }
     * 
     */
    public ArrayOfAccountSummaryAndTrxLines createArrayOfAccountSummaryAndTrxLines() {
        return new ArrayOfAccountSummaryAndTrxLines();
    }

    /**
     * Create an instance of {@link ArrayOfAccountSearchResponseLines }
     * 
     */
    public ArrayOfAccountSearchResponseLines createArrayOfAccountSearchResponseLines() {
        return new ArrayOfAccountSearchResponseLines();
    }

    /**
     * Create an instance of {@link SweepAcctsDetails }
     * 
     */
    public SweepAcctsDetails createSweepAcctsDetails() {
        return new SweepAcctsDetails();
    }

    /**
     * Create an instance of {@link ArrayOfFixedDepositInfo }
     * 
     */
    public ArrayOfFixedDepositInfo createArrayOfFixedDepositInfo() {
        return new ArrayOfFixedDepositInfo();
    }

    /**
     * Create an instance of {@link ArrayOfLoanSummaryResponse }
     * 
     */
    public ArrayOfLoanSummaryResponse createArrayOfLoanSummaryResponse() {
        return new ArrayOfLoanSummaryResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCustomerODLimitLines }
     * 
     */
    public ArrayOfCustomerODLimitLines createArrayOfCustomerODLimitLines() {
        return new ArrayOfCustomerODLimitLines();
    }

    /**
     * Create an instance of {@link ArrayOfChequeDetails }
     * 
     */
    public ArrayOfChequeDetails createArrayOfChequeDetails() {
        return new ArrayOfChequeDetails();
    }

    /**
     * Create an instance of {@link ArrayOfAccountSummaryLine }
     * 
     */
    public ArrayOfAccountSummaryLine createArrayOfAccountSummaryLine() {
        return new ArrayOfAccountSummaryLine();
    }

    /**
     * Create an instance of {@link AccountSummaryLine }
     * 
     */
    public AccountSummaryLine createAccountSummaryLine() {
        return new AccountSummaryLine();
    }

    /**
     * Create an instance of {@link ArrayOfCheckAccountExistInfo }
     * 
     */
    public ArrayOfCheckAccountExistInfo createArrayOfCheckAccountExistInfo() {
        return new ArrayOfCheckAccountExistInfo();
    }

    /**
     * Create an instance of {@link ArrayOfRelatedAccountSummaryLines }
     * 
     */
    public ArrayOfRelatedAccountSummaryLines createArrayOfRelatedAccountSummaryLines() {
        return new ArrayOfRelatedAccountSummaryLines();
    }

    /**
     * Create an instance of {@link ArrayOfLoanSummaryLine }
     * 
     */
    public ArrayOfLoanSummaryLine createArrayOfLoanSummaryLine() {
        return new ArrayOfLoanSummaryLine();
    }

    /**
     * Create an instance of {@link ImageCollectionDetails }
     * 
     */
    public ImageCollectionDetails createImageCollectionDetails() {
        return new ImageCollectionDetails();
    }

    /**
     * Create an instance of {@link CustomerODLimitLines }
     * 
     */
    public CustomerODLimitLines createCustomerODLimitLines() {
        return new CustomerODLimitLines();
    }

    /**
     * Create an instance of {@link ArrayOfAccountCotAndVatLines }
     * 
     */
    public ArrayOfAccountCotAndVatLines createArrayOfAccountCotAndVatLines() {
        return new ArrayOfAccountCotAndVatLines();
    }

    /**
     * Create an instance of {@link CustomerAcctsDetail }
     * 
     */
    public CustomerAcctsDetail createCustomerAcctsDetail() {
        return new CustomerAcctsDetail();
    }

    /**
     * Create an instance of {@link CheckAccountExistInfo }
     * 
     */
    public CheckAccountExistInfo createCheckAccountExistInfo() {
        return new CheckAccountExistInfo();
    }

    /**
     * Create an instance of {@link ArrayOfLoanInfoSummaryLines }
     * 
     */
    public ArrayOfLoanInfoSummaryLines createArrayOfLoanInfoSummaryLines() {
        return new ArrayOfLoanInfoSummaryLines();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSingleTransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfSingleTransactionDetails")
    public JAXBElement<ArrayOfSingleTransactionDetails> createArrayOfSingleTransactionDetails(ArrayOfSingleTransactionDetails value) {
        return new JAXBElement<ArrayOfSingleTransactionDetails>(_ArrayOfSingleTransactionDetails_QNAME, ArrayOfSingleTransactionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VisaCardLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "VisaCardLimitResponse")
    public JAXBElement<VisaCardLimitResponse> createVisaCardLimitResponse(VisaCardLimitResponse value) {
        return new JAXBElement<VisaCardLimitResponse>(_VisaCardLimitResponse_QNAME, VisaCardLimitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSRTCreateTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSRTCreateTransactionRequest")
    public JAXBElement<FCUBSRTCreateTransactionRequest> createFCUBSRTCreateTransactionRequest(FCUBSRTCreateTransactionRequest value) {
        return new JAXBElement<FCUBSRTCreateTransactionRequest>(_FCUBSRTCreateTransactionRequest_QNAME, FCUBSRTCreateTransactionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateRTResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateRTResponse")
    public JAXBElement<CreateRTResponse> createCreateRTResponse(CreateRTResponse value) {
        return new JAXBElement<CreateRTResponse>(_CreateRTResponse_QNAME, CreateRTResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfSweepAcctsDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfSweepAcctsDetails")
    public JAXBElement<ArrayOfSweepAcctsDetails> createArrayOfSweepAcctsDetails(ArrayOfSweepAcctsDetails value) {
        return new JAXBElement<ArrayOfSweepAcctsDetails>(_ArrayOfSweepAcctsDetails_QNAME, ArrayOfSweepAcctsDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateCustomerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateCustomerRequest")
    public JAXBElement<FCUBSCreateCustomerRequest> createFCUBSCreateCustomerRequest(FCUBSCreateCustomerRequest value) {
        return new JAXBElement<FCUBSCreateCustomerRequest>(_FCUBSCreateCustomerRequest_QNAME, FCUBSCreateCustomerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateCustomerResponse")
    public JAXBElement<CreateCustomerResponse> createCreateCustomerResponse(CreateCustomerResponse value) {
        return new JAXBElement<CreateCustomerResponse>(_CreateCustomerResponse_QNAME, CreateCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticateCBAUserResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AuthenticateCBAUserResp")
    public JAXBElement<AuthenticateCBAUserResp> createAuthenticateCBAUserResp(AuthenticateCBAUserResp value) {
        return new JAXBElement<AuthenticateCBAUserResp>(_AuthenticateCBAUserResp_QNAME, AuthenticateCBAUserResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerODLimitLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerODLimitLines")
    public JAXBElement<CustomerODLimitLines> createCustomerODLimitLines(CustomerODLimitLines value) {
        return new JAXBElement<CustomerODLimitLines>(_CustomerODLimitLines_QNAME, CustomerODLimitLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImageCollectionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ImageCollectionDetails")
    public JAXBElement<ImageCollectionDetails> createImageCollectionDetails(ImageCollectionDetails value) {
        return new JAXBElement<ImageCollectionDetails>(_ImageCollectionDetails_QNAME, ImageCollectionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "UserLimitResponse")
    public JAXBElement<UserLimitResponse> createUserLimitResponse(UserLimitResponse value) {
        return new JAXBElement<UserLimitResponse>(_UserLimitResponse_QNAME, UserLimitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountCotAndVatLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfAccountCotAndVatLines")
    public JAXBElement<ArrayOfAccountCotAndVatLines> createArrayOfAccountCotAndVatLines(ArrayOfAccountCotAndVatLines value) {
        return new JAXBElement<ArrayOfAccountCotAndVatLines>(_ArrayOfAccountCotAndVatLines_QNAME, ArrayOfAccountCotAndVatLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CurrencyRateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyRateResponse")
    public JAXBElement<CurrencyRateResponse> createCurrencyRateResponse(CurrencyRateResponse value) {
        return new JAXBElement<CurrencyRateResponse>(_CurrencyRateResponse_QNAME, CurrencyRateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSummaryLine")
    public JAXBElement<AccountSummaryLine> createAccountSummaryLine(AccountSummaryLine value) {
        return new JAXBElement<AccountSummaryLine>(_AccountSummaryLine_QNAME, AccountSummaryLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateChequeBookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ActivateChequeBookResponse")
    public JAXBElement<ActivateChequeBookResponse> createActivateChequeBookResponse(ActivateChequeBookResponse value) {
        return new JAXBElement<ActivateChequeBookResponse>(_ActivateChequeBookResponse_QNAME, ActivateChequeBookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanInfoSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanInfoSummaryResponse")
    public JAXBElement<LoanInfoSummaryResponse> createLoanInfoSummaryResponse(LoanInfoSummaryResponse value) {
        return new JAXBElement<LoanInfoSummaryResponse>(_LoanInfoSummaryResponse_QNAME, LoanInfoSummaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCheckAccountExistInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfCheckAccountExistInfo")
    public JAXBElement<ArrayOfCheckAccountExistInfo> createArrayOfCheckAccountExistInfo(ArrayOfCheckAccountExistInfo value) {
        return new JAXBElement<ArrayOfCheckAccountExistInfo>(_ArrayOfCheckAccountExistInfo_QNAME, ArrayOfCheckAccountExistInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountExistResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetAccountExistResponse")
    public JAXBElement<GetAccountExistResponse> createGetAccountExistResponse(GetAccountExistResponse value) {
        return new JAXBElement<GetAccountExistResponse>(_GetAccountExistResponse_QNAME, GetAccountExistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCustomerServiceAmtBlkRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCustomerServiceAmtBlkRequest")
    public JAXBElement<FCUBSCustomerServiceAmtBlkRequest> createFCUBSCustomerServiceAmtBlkRequest(FCUBSCustomerServiceAmtBlkRequest value) {
        return new JAXBElement<FCUBSCustomerServiceAmtBlkRequest>(_FCUBSCustomerServiceAmtBlkRequest_QNAME, FCUBSCustomerServiceAmtBlkRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateCustAccountResponse")
    public JAXBElement<CreateCustAccountResponse> createCreateCustAccountResponse(CreateCustAccountResponse value) {
        return new JAXBElement<CreateCustAccountResponse>(_CreateCustAccountResponse_QNAME, CreateCustAccountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanSummaryResponse")
    public JAXBElement<LoanSummaryResponse> createLoanSummaryResponse(LoanSummaryResponse value) {
        return new JAXBElement<LoanSummaryResponse>(_LoanSummaryResponse_QNAME, LoanSummaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfLoanSummaryResponse")
    public JAXBElement<ArrayOfLoanSummaryResponse> createArrayOfLoanSummaryResponse(ArrayOfLoanSummaryResponse value) {
        return new JAXBElement<ArrayOfLoanSummaryResponse>(_ArrayOfLoanSummaryResponse_QNAME, ArrayOfLoanSummaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLDContractResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateLDContractResponse")
    public JAXBElement<CreateLDContractResponse> createCreateLDContractResponse(CreateLDContractResponse value) {
        return new JAXBElement<CreateLDContractResponse>(_CreateLDContractResponse_QNAME, CreateLDContractResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfAccountSummaryLine")
    public JAXBElement<ArrayOfAccountSummaryLine> createArrayOfAccountSummaryLine(ArrayOfAccountSummaryLine value) {
        return new JAXBElement<ArrayOfAccountSummaryLine>(_ArrayOfAccountSummaryLine_QNAME, ArrayOfAccountSummaryLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerODLimitLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfCustomerODLimitLines")
    public JAXBElement<ArrayOfCustomerODLimitLines> createArrayOfCustomerODLimitLines(ArrayOfCustomerODLimitLines value) {
        return new JAXBElement<ArrayOfCustomerODLimitLines>(_ArrayOfCustomerODLimitLines_QNAME, ArrayOfCustomerODLimitLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfAccountSummaryAndTrxResponse")
    public JAXBElement<ArrayOfAccountSummaryAndTrxResponse> createArrayOfAccountSummaryAndTrxResponse(ArrayOfAccountSummaryAndTrxResponse value) {
        return new JAXBElement<ArrayOfAccountSummaryAndTrxResponse>(_ArrayOfAccountSummaryAndTrxResponse_QNAME, ArrayOfAccountSummaryAndTrxResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfImageCollectionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfImageCollectionDetails")
    public JAXBElement<ArrayOfImageCollectionDetails> createArrayOfImageCollectionDetails(ArrayOfImageCollectionDetails value) {
        return new JAXBElement<ArrayOfImageCollectionDetails>(_ArrayOfImageCollectionDetails_QNAME, ArrayOfImageCollectionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEffectiveBalanceResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetEffectiveBalanceResp")
    public JAXBElement<GetEffectiveBalanceResp> createGetEffectiveBalanceResp(GetEffectiveBalanceResp value) {
        return new JAXBElement<GetEffectiveBalanceResp>(_GetEffectiveBalanceResp_QNAME, GetEffectiveBalanceResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSummaryResponse")
    public JAXBElement<AccountSummaryResponse> createAccountSummaryResponse(AccountSummaryResponse value) {
        return new JAXBElement<AccountSummaryResponse>(_AccountSummaryResponse_QNAME, AccountSummaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAcctMemoInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetAcctMemoInfoResponse")
    public JAXBElement<GetAcctMemoInfoResponse> createGetAcctMemoInfoResponse(GetAcctMemoInfoResponse value) {
        return new JAXBElement<GetAcctMemoInfoResponse>(_GetAcctMemoInfoResponse_QNAME, GetAcctMemoInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSearchResponseLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfAccountSearchResponseLines")
    public JAXBElement<ArrayOfAccountSearchResponseLines> createArrayOfAccountSearchResponseLines(ArrayOfAccountSearchResponseLines value) {
        return new JAXBElement<ArrayOfAccountSearchResponseLines>(_ArrayOfAccountSearchResponseLines_QNAME, ArrayOfAccountSearchResponseLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSearchResponse")
    public JAXBElement<AccountSearchResponse> createAccountSearchResponse(AccountSearchResponse value) {
        return new JAXBElement<AccountSearchResponse>(_AccountSearchResponse_QNAME, AccountSearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NubanAcctsDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "NubanAcctsDetails")
    public JAXBElement<NubanAcctsDetails> createNubanAcctsDetails(NubanAcctsDetails value) {
        return new JAXBElement<NubanAcctsDetails>(_NubanAcctsDetails_QNAME, NubanAcctsDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SingleTransactionDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SingleTransactionDetails")
    public JAXBElement<SingleTransactionDetails> createSingleTransactionDetails(SingleTransactionDetails value) {
        return new JAXBElement<SingleTransactionDetails>(_SingleTransactionDetails_QNAME, SingleTransactionDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSummaryAndTrxLines")
    public JAXBElement<AccountSummaryAndTrxLines> createAccountSummaryAndTrxLines(AccountSummaryAndTrxLines value) {
        return new JAXBElement<AccountSummaryAndTrxLines>(_AccountSummaryAndTrxLines_QNAME, AccountSummaryAndTrxLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TellerLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TellerLimitResponse")
    public JAXBElement<TellerLimitResponse> createTellerLimitResponse(TellerLimitResponse value) {
        return new JAXBElement<TellerLimitResponse>(_TellerLimitResponse_QNAME, TellerLimitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelatedAccountSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RelatedAccountSummaryLines")
    public JAXBElement<RelatedAccountSummaryLines> createRelatedAccountSummaryLines(RelatedAccountSummaryLines value) {
        return new JAXBElement<RelatedAccountSummaryLines>(_RelatedAccountSummaryLines_QNAME, RelatedAccountSummaryLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FcubsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "FcubsResponse")
    public JAXBElement<FcubsResponse> createFcubsResponse(FcubsResponse value) {
        return new JAXBElement<FcubsResponse>(_FcubsResponse_QNAME, FcubsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVisaCardLimitSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfVisaCardLimitSummaryLines")
    public JAXBElement<ArrayOfVisaCardLimitSummaryLines> createArrayOfVisaCardLimitSummaryLines(ArrayOfVisaCardLimitSummaryLines value) {
        return new JAXBElement<ArrayOfVisaCardLimitSummaryLines>(_ArrayOfVisaCardLimitSummaryLines_QNAME, ArrayOfVisaCardLimitSummaryLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerAcctsDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerAcctsDetailResponse")
    public JAXBElement<CustomerAcctsDetailResponse> createCustomerAcctsDetailResponse(CustomerAcctsDetailResponse value) {
        return new JAXBElement<CustomerAcctsDetailResponse>(_CustomerAcctsDetailResponse_QNAME, CustomerAcctsDetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSFTCreateTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSFTCreateTransactionRequest")
    public JAXBElement<FCUBSFTCreateTransactionRequest> createFCUBSFTCreateTransactionRequest(FCUBSFTCreateTransactionRequest value) {
        return new JAXBElement<FCUBSFTCreateTransactionRequest>(_FCUBSFTCreateTransactionRequest_QNAME, FCUBSFTCreateTransactionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChequeValueDateResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetChequeValueDateResp")
    public JAXBElement<GetChequeValueDateResp> createGetChequeValueDateResp(GetChequeValueDateResp value) {
        return new JAXBElement<GetChequeValueDateResp>(_GetChequeValueDateResp_QNAME, GetChequeValueDateResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSearchResponseLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSearchResponseLines")
    public JAXBElement<AccountSearchResponseLines> createAccountSearchResponseLines(AccountSearchResponseLines value) {
        return new JAXBElement<AccountSearchResponseLines>(_AccountSearchResponseLines_QNAME, AccountSearchResponseLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionRefNoDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TransactionRefNoDetails")
    public JAXBElement<TransactionRefNoDetails> createTransactionRefNoDetails(TransactionRefNoDetails value) {
        return new JAXBElement<TransactionRefNoDetails>(_TransactionRefNoDetails_QNAME, TransactionRefNoDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepaymentScheduleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RepaymentScheduleResponse")
    public JAXBElement<RepaymentScheduleResponse> createRepaymentScheduleResponse(RepaymentScheduleResponse value) {
        return new JAXBElement<RepaymentScheduleResponse>(_RepaymentScheduleResponse_QNAME, RepaymentScheduleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfCustomerInformation")
    public JAXBElement<ArrayOfCustomerInformation> createArrayOfCustomerInformation(ArrayOfCustomerInformation value) {
        return new JAXBElement<ArrayOfCustomerInformation>(_ArrayOfCustomerInformation_QNAME, ArrayOfCustomerInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerInformation")
    public JAXBElement<CustomerInformation> createCustomerInformation(CustomerInformation value) {
        return new JAXBElement<CustomerInformation>(_CustomerInformation_QNAME, CustomerInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateMMContractRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateMMContractRequest")
    public JAXBElement<FCUBSCreateMMContractRequest> createFCUBSCreateMMContractRequest(FCUBSCreateMMContractRequest value) {
        return new JAXBElement<FCUBSCreateMMContractRequest>(_FCUBSCreateMMContractRequest_QNAME, FCUBSCreateMMContractRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanInfoSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanInfoSummaryLines")
    public JAXBElement<LoanInfoSummaryLines> createLoanInfoSummaryLines(LoanInfoSummaryLines value) {
        return new JAXBElement<LoanInfoSummaryLines>(_LoanInfoSummaryLines_QNAME, LoanInfoSummaryLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepaymentScheduleInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RepaymentScheduleInfo")
    public JAXBElement<RepaymentScheduleInfo> createRepaymentScheduleInfo(RepaymentScheduleInfo value) {
        return new JAXBElement<RepaymentScheduleInfo>(_RepaymentScheduleInfo_QNAME, RepaymentScheduleInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerDetails")
    public JAXBElement<CustomerDetails> createCustomerDetails(CustomerDetails value) {
        return new JAXBElement<CustomerDetails>(_CustomerDetails_QNAME, CustomerDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ChequeDetails")
    public JAXBElement<ChequeDetails> createChequeDetails(ChequeDetails value) {
        return new JAXBElement<ChequeDetails>(_ChequeDetails_QNAME, ChequeDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyCodeResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetCurrencyCodeResp")
    public JAXBElement<GetCurrencyCodeResp> createGetCurrencyCodeResp(GetCurrencyCodeResp value) {
        return new JAXBElement<GetCurrencyCodeResp>(_GetCurrencyCodeResp_QNAME, GetCurrencyCodeResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSRTReverseTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSRTReverseTransactionRequest")
    public JAXBElement<FCUBSRTReverseTransactionRequest> createFCUBSRTReverseTransactionRequest(FCUBSRTReverseTransactionRequest value) {
        return new JAXBElement<FCUBSRTReverseTransactionRequest>(_FCUBSRTReverseTransactionRequest_QNAME, FCUBSRTReverseTransactionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLoanInfoSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfLoanInfoSummaryLines")
    public JAXBElement<ArrayOfLoanInfoSummaryLines> createArrayOfLoanInfoSummaryLines(ArrayOfLoanInfoSummaryLines value) {
        return new JAXBElement<ArrayOfLoanInfoSummaryLines>(_ArrayOfLoanInfoSummaryLines_QNAME, ArrayOfLoanInfoSummaryLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScreenLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ScreenLimitResponse")
    public JAXBElement<ScreenLimitResponse> createScreenLimitResponse(ScreenLimitResponse value) {
        return new JAXBElement<ScreenLimitResponse>(_ScreenLimitResponse_QNAME, ScreenLimitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateSIContractRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateSIContractRequest")
    public JAXBElement<FCUBSCreateSIContractRequest> createFCUBSCreateSIContractRequest(FCUBSCreateSIContractRequest value) {
        return new JAXBElement<FCUBSCreateSIContractRequest>(_FCUBSCreateSIContractRequest_QNAME, FCUBSCreateSIContractRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultipleJournalMasterTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MultipleJournalMasterTrx")
    public JAXBElement<MultipleJournalMasterTrx> createMultipleJournalMasterTrx(MultipleJournalMasterTrx value) {
        return new JAXBElement<MultipleJournalMasterTrx>(_MultipleJournalMasterTrx_QNAME, MultipleJournalMasterTrx.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryResponseBulk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanSummaryResponseBulk")
    public JAXBElement<LoanSummaryResponseBulk> createLoanSummaryResponseBulk(LoanSummaryResponseBulk value) {
        return new JAXBElement<LoanSummaryResponseBulk>(_LoanSummaryResponseBulk_QNAME, LoanSummaryResponseBulk.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateCustAccountRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateCustAccountRequest")
    public JAXBElement<FCUBSCreateCustAccountRequest> createFCUBSCreateCustAccountRequest(FCUBSCreateCustAccountRequest value) {
        return new JAXBElement<FCUBSCreateCustAccountRequest>(_FCUBSCreateCustAccountRequest_QNAME, FCUBSCreateCustAccountRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SingleJournalTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SingleJournalTrx")
    public JAXBElement<SingleJournalTrx> createSingleJournalTrx(SingleJournalTrx value) {
        return new JAXBElement<SingleJournalTrx>(_SingleJournalTrx_QNAME, SingleJournalTrx.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckAccountExistInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CheckAccountExistInfo")
    public JAXBElement<CheckAccountExistInfo> createCheckAccountExistInfo(CheckAccountExistInfo value) {
        return new JAXBElement<CheckAccountExistInfo>(_CheckAccountExistInfo_QNAME, CheckAccountExistInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerAcctsDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerAcctsDetail")
    public JAXBElement<CustomerAcctsDetail> createCustomerAcctsDetail(CustomerAcctsDetail value) {
        return new JAXBElement<CustomerAcctsDetail>(_CustomerAcctsDetail_QNAME, CustomerAcctsDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserBranchResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetUserBranchResp")
    public JAXBElement<GetUserBranchResp> createGetUserBranchResp(GetUserBranchResp value) {
        return new JAXBElement<GetUserBranchResp>(_GetUserBranchResp_QNAME, GetUserBranchResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountTransactionResponseBulk }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountTransactionResponseBulk")
    public JAXBElement<AccountTransactionResponseBulk> createAccountTransactionResponseBulk(AccountTransactionResponseBulk value) {
        return new JAXBElement<AccountTransactionResponseBulk>(_AccountTransactionResponseBulk_QNAME, AccountTransactionResponseBulk.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCGServiceCreateTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCGServiceCreateTransactionRequest")
    public JAXBElement<FCUBSCGServiceCreateTransactionRequest> createFCUBSCGServiceCreateTransactionRequest(FCUBSCGServiceCreateTransactionRequest value) {
        return new JAXBElement<FCUBSCGServiceCreateTransactionRequest>(_FCUBSCGServiceCreateTransactionRequest_QNAME, FCUBSCGServiceCreateTransactionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRelatedAccountSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfRelatedAccountSummaryLines")
    public JAXBElement<ArrayOfRelatedAccountSummaryLines> createArrayOfRelatedAccountSummaryLines(ArrayOfRelatedAccountSummaryLines value) {
        return new JAXBElement<ArrayOfRelatedAccountSummaryLines>(_ArrayOfRelatedAccountSummaryLines_QNAME, ArrayOfRelatedAccountSummaryLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSummaryAndTrxResponse")
    public JAXBElement<AccountSummaryAndTrxResponse> createAccountSummaryAndTrxResponse(AccountSummaryAndTrxResponse value) {
        return new JAXBElement<AccountSummaryAndTrxResponse>(_AccountSummaryAndTrxResponse_QNAME, AccountSummaryAndTrxResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateStopPaymentRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateStopPaymentRequest")
    public JAXBElement<FCUBSCreateStopPaymentRequest> createFCUBSCreateStopPaymentRequest(FCUBSCreateStopPaymentRequest value) {
        return new JAXBElement<FCUBSCreateStopPaymentRequest>(_FCUBSCreateStopPaymentRequest_QNAME, FCUBSCreateStopPaymentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateStopPaymentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateStopPaymentResponse")
    public JAXBElement<CreateStopPaymentResponse> createCreateStopPaymentResponse(CreateStopPaymentResponse value) {
        return new JAXBElement<CreateStopPaymentResponse>(_CreateStopPaymentResponse_QNAME, CreateStopPaymentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfLoanSummaryLine")
    public JAXBElement<ArrayOfLoanSummaryLine> createArrayOfLoanSummaryLine(ArrayOfLoanSummaryLine value) {
        return new JAXBElement<ArrayOfLoanSummaryLine>(_ArrayOfLoanSummaryLine_QNAME, ArrayOfLoanSummaryLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfNubanAcctsDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfNubanAcctsDetails")
    public JAXBElement<ArrayOfNubanAcctsDetails> createArrayOfNubanAcctsDetails(ArrayOfNubanAcctsDetails value) {
        return new JAXBElement<ArrayOfNubanAcctsDetails>(_ArrayOfNubanAcctsDetails_QNAME, ArrayOfNubanAcctsDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateTDAccountRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateTDAccountRequest")
    public JAXBElement<FCUBSCreateTDAccountRequest> createFCUBSCreateTDAccountRequest(FCUBSCreateTDAccountRequest value) {
        return new JAXBElement<FCUBSCreateTDAccountRequest>(_FCUBSCreateTDAccountRequest_QNAME, FCUBSCreateTDAccountRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFixedDepositInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfFixedDepositInfo")
    public JAXBElement<ArrayOfFixedDepositInfo> createArrayOfFixedDepositInfo(ArrayOfFixedDepositInfo value) {
        return new JAXBElement<ArrayOfFixedDepositInfo>(_ArrayOfFixedDepositInfo_QNAME, ArrayOfFixedDepositInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfCustomerAcctsDetailResponse")
    public JAXBElement<ArrayOfCustomerAcctsDetailResponse> createArrayOfCustomerAcctsDetailResponse(ArrayOfCustomerAcctsDetailResponse value) {
        return new JAXBElement<ArrayOfCustomerAcctsDetailResponse>(_ArrayOfCustomerAcctsDetailResponse_QNAME, ArrayOfCustomerAcctsDetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MultipleJournalDetailTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MultipleJournalDetailTrx")
    public JAXBElement<MultipleJournalDetailTrx> createMultipleJournalDetailTrx(MultipleJournalDetailTrx value) {
        return new JAXBElement<MultipleJournalDetailTrx>(_MultipleJournalDetailTrx_QNAME, MultipleJournalDetailTrx.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAmtBlockResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateAmtBlockResponse")
    public JAXBElement<CreateAmtBlockResponse> createCreateAmtBlockResponse(CreateAmtBlockResponse value) {
        return new JAXBElement<CreateAmtBlockResponse>(_CreateAmtBlockResponse_QNAME, CreateAmtBlockResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthorizeWithdrawalRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AuthorizeWithdrawalRequest")
    public JAXBElement<AuthorizeWithdrawalRequest> createAuthorizeWithdrawalRequest(AuthorizeWithdrawalRequest value) {
        return new JAXBElement<AuthorizeWithdrawalRequest>(_AuthorizeWithdrawalRequest_QNAME, AuthorizeWithdrawalRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerInformationResponse")
    public JAXBElement<CustomerInformationResponse> createCustomerInformationResponse(CustomerInformationResponse value) {
        return new JAXBElement<CustomerInformationResponse>(_CustomerInformationResponse_QNAME, CustomerInformationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChequeDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfChequeDetails")
    public JAXBElement<ArrayOfChequeDetails> createArrayOfChequeDetails(ArrayOfChequeDetails value) {
        return new JAXBElement<ArrayOfChequeDetails>(_ArrayOfChequeDetails_QNAME, ArrayOfChequeDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateUserTillResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ValidateUserTillResp")
    public JAXBElement<ValidateUserTillResp> createValidateUserTillResp(ValidateUserTillResp value) {
        return new JAXBElement<ValidateUserTillResp>(_ValidateUserTillResp_QNAME, ValidateUserTillResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SampleAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SampleAccountResponse")
    public JAXBElement<SampleAccountResponse> createSampleAccountResponse(SampleAccountResponse value) {
        return new JAXBElement<SampleAccountResponse>(_SampleAccountResponse_QNAME, SampleAccountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfAccountSummaryAndTrxLines")
    public JAXBElement<ArrayOfAccountSummaryAndTrxLines> createArrayOfAccountSummaryAndTrxLines(ArrayOfAccountSummaryAndTrxLines value) {
        return new JAXBElement<ArrayOfAccountSummaryAndTrxLines>(_ArrayOfAccountSummaryAndTrxLines_QNAME, ArrayOfAccountSummaryAndTrxLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SweepAcctsDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SweepAcctsDetails")
    public JAXBElement<SweepAcctsDetails> createSweepAcctsDetails(SweepAcctsDetails value) {
        return new JAXBElement<SweepAcctsDetails>(_SweepAcctsDetails_QNAME, SweepAcctsDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSIContractResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateSIContractResponse")
    public JAXBElement<CreateSIContractResponse> createCreateSIContractResponse(CreateSIContractResponse value) {
        return new JAXBElement<CreateSIContractResponse>(_CreateSIContractResponse_QNAME, CreateSIContractResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMMContractResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateMMContractResponse")
    public JAXBElement<CreateMMContractResponse> createCreateMMContractResponse(CreateMMContractResponse value) {
        return new JAXBElement<CreateMMContractResponse>(_CreateMMContractResponse_QNAME, CreateMMContractResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountRelatedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountRelatedResponse")
    public JAXBElement<AccountRelatedResponse> createAccountRelatedResponse(AccountRelatedResponse value) {
        return new JAXBElement<AccountRelatedResponse>(_AccountRelatedResponse_QNAME, AccountRelatedResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTDAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CreateTDAccountResponse")
    public JAXBElement<CreateTDAccountResponse> createCreateTDAccountResponse(CreateTDAccountResponse value) {
        return new JAXBElement<CreateTDAccountResponse>(_CreateTDAccountResponse_QNAME, CreateTDAccountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountCotAndVatLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountCotAndVatLines")
    public JAXBElement<AccountCotAndVatLines> createAccountCotAndVatLines(AccountCotAndVatLines value) {
        return new JAXBElement<AccountCotAndVatLines>(_AccountCotAndVatLines_QNAME, AccountCotAndVatLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountMemoInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountMemoInfo")
    public JAXBElement<AccountMemoInfo> createAccountMemoInfo(AccountMemoInfo value) {
        return new JAXBElement<AccountMemoInfo>(_AccountMemoInfo_QNAME, AccountMemoInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FixedDepositInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "FixedDepositInfo")
    public JAXBElement<FixedDepositInfo> createFixedDepositInfo(FixedDepositInfo value) {
        return new JAXBElement<FixedDepositInfo>(_FixedDepositInfo_QNAME, FixedDepositInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfCustomerAcctsDetail")
    public JAXBElement<ArrayOfCustomerAcctsDetail> createArrayOfCustomerAcctsDetail(ArrayOfCustomerAcctsDetail value) {
        return new JAXBElement<ArrayOfCustomerAcctsDetail>(_ArrayOfCustomerAcctsDetail_QNAME, ArrayOfCustomerAcctsDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTransactionRefNoDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfTransactionRefNoDetails")
    public JAXBElement<ArrayOfTransactionRefNoDetails> createArrayOfTransactionRefNoDetails(ArrayOfTransactionRefNoDetails value) {
        return new JAXBElement<ArrayOfTransactionRefNoDetails>(_ArrayOfTransactionRefNoDetails_QNAME, ArrayOfTransactionRefNoDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCloseCustAccRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCloseCustAccRequest")
    public JAXBElement<FCUBSCloseCustAccRequest> createFCUBSCloseCustAccRequest(FCUBSCloseCustAccRequest value) {
        return new JAXBElement<FCUBSCloseCustAccRequest>(_FCUBSCloseCustAccRequest_QNAME, FCUBSCloseCustAccRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAccountBranchResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "GetAccountBranchResp")
    public JAXBElement<GetAccountBranchResp> createGetAccountBranchResp(GetAccountBranchResp value) {
        return new JAXBElement<GetAccountBranchResp>(_GetAccountBranchResp_QNAME, GetAccountBranchResp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FCUBSCreateLDContractRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCUBSCreateLDContractRequest")
    public JAXBElement<FCUBSCreateLDContractRequest> createFCUBSCreateLDContractRequest(FCUBSCreateLDContractRequest value) {
        return new JAXBElement<FCUBSCreateLDContractRequest>(_FCUBSCreateLDContractRequest_QNAME, FCUBSCreateLDContractRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSummaryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanSummaryLine")
    public JAXBElement<LoanSummaryLine> createLoanSummaryLine(LoanSummaryLine value) {
        return new JAXBElement<LoanSummaryLine>(_LoanSummaryLine_QNAME, LoanSummaryLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FixedDepositResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "FixedDepositResponse")
    public JAXBElement<FixedDepositResponse> createFixedDepositResponse(FixedDepositResponse value) {
        return new JAXBElement<FixedDepositResponse>(_FixedDepositResponse_QNAME, FixedDepositResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerODLimitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerODLimitResponse")
    public JAXBElement<CustomerODLimitResponse> createCustomerODLimitResponse(CustomerODLimitResponse value) {
        return new JAXBElement<CustomerODLimitResponse>(_CustomerODLimitResponse_QNAME, CustomerODLimitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChequeNumberDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfChequeNumberDetails")
    public JAXBElement<ArrayOfChequeNumberDetails> createArrayOfChequeNumberDetails(ArrayOfChequeNumberDetails value) {
        return new JAXBElement<ArrayOfChequeNumberDetails>(_ArrayOfChequeNumberDetails_QNAME, ArrayOfChequeNumberDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VisaCardLimitSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "VisaCardLimitSummaryLines")
    public JAXBElement<VisaCardLimitSummaryLines> createVisaCardLimitSummaryLines(VisaCardLimitSummaryLines value) {
        return new JAXBElement<VisaCardLimitSummaryLines>(_VisaCardLimitSummaryLines_QNAME, VisaCardLimitSummaryLines.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeNumberDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ChequeNumberDetails")
    public JAXBElement<ChequeNumberDetails> createChequeNumberDetails(ChequeNumberDetails value) {
        return new JAXBElement<ChequeNumberDetails>(_ChequeNumberDetails_QNAME, ChequeNumberDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountMemoInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfAccountMemoInfo")
    public JAXBElement<ArrayOfAccountMemoInfo> createArrayOfAccountMemoInfo(ArrayOfAccountMemoInfo value) {
        return new JAXBElement<ArrayOfAccountMemoInfo>(_ArrayOfAccountMemoInfo_QNAME, ArrayOfAccountMemoInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChequeDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ChequeDetailsResponse")
    public JAXBElement<ChequeDetailsResponse> createChequeDetailsResponse(ChequeDetailsResponse value) {
        return new JAXBElement<ChequeDetailsResponse>(_ChequeDetailsResponse_QNAME, ChequeDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRepaymentScheduleInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ArrayOfRepaymentScheduleInfo")
    public JAXBElement<ArrayOfRepaymentScheduleInfo> createArrayOfRepaymentScheduleInfo(ArrayOfRepaymentScheduleInfo value) {
        return new JAXBElement<ArrayOfRepaymentScheduleInfo>(_ArrayOfRepaymentScheduleInfo_QNAME, ArrayOfRepaymentScheduleInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountCotAndVatResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountCotAndVatResponse")
    public JAXBElement<AccountCotAndVatResponse> createAccountCotAndVatResponse(AccountCotAndVatResponse value) {
        return new JAXBElement<AccountCotAndVatResponse>(_AccountCotAndVatResponse_QNAME, AccountCotAndVatResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMultipleJournalDetailTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ArrayOfMultipleJournalDetailTrx")
    public JAXBElement<ArrayOfMultipleJournalDetailTrx> createArrayOfMultipleJournalDetailTrx(ArrayOfMultipleJournalDetailTrx value) {
        return new JAXBElement<ArrayOfMultipleJournalDetailTrx>(_ArrayOfMultipleJournalDetailTrx_QNAME, ArrayOfMultipleJournalDetailTrx.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "beneficiary", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsBeneficiary(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsBeneficiary_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "presentation_date", scope = ChequeNumberDetails.class)
    public JAXBElement<XMLGregorianCalendar> createChequeNumberDetailsPresentationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ChequeNumberDetailsPresentationDate_QNAME, XMLGregorianCalendar.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "check_no", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsCheckNo(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsCheckNo_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amount", scope = ChequeNumberDetails.class)
    public JAXBElement<BigDecimal> createChequeNumberDetailsAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ChequeNumberDetailsAmount_QNAME, BigDecimal.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branch", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsBranch(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsBranch_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "check_book_no", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsCheckBookNo(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsCheckBookNo_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "status", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsStatus(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsStatus_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "value_date", scope = ChequeNumberDetails.class)
    public JAXBElement<XMLGregorianCalendar> createChequeNumberDetailsValueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ChequeNumberDetailsValueDate_QNAME, XMLGregorianCalendar.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "account", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsAccount(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsAccount_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ac_desc", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsAcDesc(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsAcDesc_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cust_ac_no", scope = ChequeNumberDetails.class)
    public JAXBElement<String> createChequeNumberDetailsCustAcNo(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsCustAcNo_QNAME, String.class, ChequeNumberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CONREFNO", scope = CreateSIContractResponse.class)
    public JAXBElement<String> createCreateSIContractResponseCONREFNO(String value) {
        return new JAXBElement<String>(_CreateSIContractResponseCONREFNO_QNAME, String.class, CreateSIContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "REFERENCE_NO", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestREFERENCENO(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestREFERENCENO_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "HOLDDESC", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestHOLDDESC(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestHOLDDESC_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECKER_ID", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestCHECKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCHECKERID_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "REMARK", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestREMARK(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestREMARK_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ABLKTYPE", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestABLKTYPE(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestABLKTYPE_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EFF_DATE", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestEFFDATE(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestEFFDATE_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECKER_STAMP", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestCHECKERSTAMP(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCHECKERSTAMP_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "HPCODE", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestHPCODE(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestHPCODE_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AMTBLKNO", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestAMTBLKNO(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestAMTBLKNO_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MsgId", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestMsgId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AMT", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestAMT(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestAMT_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER_ID", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestMAKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMAKERID_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER_STAMP", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestMAKERSTAMP(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMAKERSTAMP_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CORRELID", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestCORRELID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCORRELID_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ACC", scope = FCUBSCustomerServiceAmtBlkRequest.class)
    public JAXBElement<String> createFCUBSCustomerServiceAmtBlkRequestACC(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestACC_QNAME, String.class, FCUBSCustomerServiceAmtBlkRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custEmail", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustEmail(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustEmail_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MaritalStatus", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsMaritalStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsMaritalStatus_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerCategory", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustomerCategory(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerCategory_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custFax", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustFax(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustFax_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerAddress", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustomerAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerAddress_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "customerNo", scope = CustomerDetails.class)
    public JAXBElement<BigDecimal> createCustomerDetailsCustomerNo(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerDetailsCustomerNo_QNAME, BigDecimal.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custid", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustid(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustid_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "director_name", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsDirectorName(String value) {
        return new JAXBElement<String>(_CustomerDetailsDirectorName_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BusinessGroup", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsBusinessGroup(String value) {
        return new JAXBElement<String>(_CustomerDetailsBusinessGroup_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProfitCenter", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsProfitCenter(String value) {
        return new JAXBElement<String>(_CustomerDetailsProfitCenter_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "companyRC", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCompanyRC(String value) {
        return new JAXBElement<String>(_CustomerDetailsCompanyRC_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerCategoryDesc", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustomerCategoryDesc(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerCategoryDesc_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custNationality", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustNationality(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustNationality_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "datCustOpen", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsDatCustOpen(String value) {
        return new JAXBElement<String>(_CustomerDetailsDatCustOpen_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "UNIQUE_ID_VALUE", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsUNIQUEIDVALUE(String value) {
        return new JAXBElement<String>(_CustomerDetailsUNIQUEIDVALUE_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "customerName", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustomerName(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerName_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branchName", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsBranchName(String value) {
        return new JAXBElement<String>(_CustomerDetailsBranchName_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountOfficer", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsAccountOfficer(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountOfficer_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "maillingAddress", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsMaillingAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsMaillingAddress_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DOB", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsDOB(String value) {
        return new JAXBElement<String>(_CustomerDetailsDOB_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountStatus", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountStatus_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "mISCODE3", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsMISCODE3(String value) {
        return new JAXBElement<String>(_CustomerDetailsMISCODE3_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "permanentAddress", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsPermanentAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsPermanentAddress_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "IsStaff", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsIsStaff(String value) {
        return new JAXBElement<String>(_CustomerDetailsIsStaff_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "gender", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsGender(String value) {
        return new JAXBElement<String>(_CustomerDetailsGender_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "dateIncoporated", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsDateIncoporated(String value) {
        return new JAXBElement<String>(_CustomerDetailsDateIncoporated_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custPhone", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsCustPhone(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustPhone_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "phoneNo", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsPhoneNo(String value) {
        return new JAXBElement<String>(_CustomerDetailsPhoneNo_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchAddress", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsBranchAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsBranchAddress_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BusinessDivision", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsBusinessDivision(String value) {
        return new JAXBElement<String>(_CustomerDetailsBusinessDivision_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SPOUSE_NAME", scope = CustomerDetails.class)
    public JAXBElement<String> createCustomerDetailsSPOUSENAME(String value) {
        return new JAXBElement<String>(_CustomerDetailsSPOUSENAME_QNAME, String.class, CustomerDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSummaryLines", scope = AccountSummaryResponse.class)
    public JAXBElement<ArrayOfAccountSummaryLine> createAccountSummaryResponseAccountSummaryLines(ArrayOfAccountSummaryLine value) {
        return new JAXBElement<ArrayOfAccountSummaryLine>(_AccountSummaryResponseAccountSummaryLines_QNAME, ArrayOfAccountSummaryLine.class, AccountSummaryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerInformation", scope = CustomerInformationResponse.class)
    public JAXBElement<ArrayOfCustomerInformation> createCustomerInformationResponseCustomerInformation(ArrayOfCustomerInformation value) {
        return new JAXBElement<ArrayOfCustomerInformation>(_CustomerInformation_QNAME, ArrayOfCustomerInformation.class, CustomerInformationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TRNREFNO", scope = CreateRTResponse.class)
    public JAXBElement<String> createCreateRTResponseTRNREFNO(String value) {
        return new JAXBElement<String>(_CreateRTResponseTRNREFNO_QNAME, String.class, CreateRTResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "interestPaidYTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailInterestPaidYTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailInterestPaidYTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountLastDebit", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountLastDebit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountLastDebit_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "productCode", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailProductCode(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailProductCode_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "currencyCode", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCurrencyCode(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailCurrencyCode_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "COMP_MIS_8", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCOMPMIS8(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailCOMPMIS8_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "customerNo", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCustomerNo(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerNo_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strCity", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrCity(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrCity_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BVN", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailBVN(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailBVN_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strZip", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrZip(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrZip_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "clearedBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailClearedBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailClearedBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastDebitInterestAccrued", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailLastDebitInterestAccrued(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailLastDebitInterestAccrued_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastMaintainedBy", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailLastMaintainedBy(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailLastMaintainedBy_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "aTMStatus", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailATMStatus(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailATMStatus_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "daueLimit", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailDaueLimit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailDaueLimit_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branchName", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailBranchName(String value) {
        return new JAXBElement<String>(_CustomerDetailsBranchName_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "netBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailNetBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailNetBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "staff", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStaff(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStaff_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountNo", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailAccountNo(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailAccountNo_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountClassType", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailAccountClassType(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailAccountClassType_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountDebitYTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountDebitYTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountDebitYTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "currency", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCurrency(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailCurrency_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountName", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailAccountName(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailAccountName_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strAdd1", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrAdd1(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrAdd1_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountDebitMTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountDebitMTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountDebitMTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strAdd3", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrAdd3(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrAdd3_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strAdd2", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrAdd2(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrAdd2_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "maintenanceAuthorizedBy", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailMaintenanceAuthorizedBy(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailMaintenanceAuthorizedBy_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "availableBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAvailableBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAvailableBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "interestReceivedYTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailInterestReceivedYTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailInterestReceivedYTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "profitCenter", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailProfitCenter(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailProfitCenter_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "phone", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailPhone(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailPhone_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastDebitDate", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailLastDebitDate(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailLastDebitDate_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "taxAccrued", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailTaxAccrued(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailTaxAccrued_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastCreditDate", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailLastCreditDate(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailLastCreditDate_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerCategory", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCustomerCategory(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerCategory_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branchCode", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailBranchCode(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailBranchCode_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strCountry", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrCountry(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrCountry_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountCreditYTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountCreditYTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountCreditYTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custID", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCustID(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailCustID_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountHold", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountHold(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountHold_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "oDLimit", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailODLimit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailODLimit_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "signatory", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailSignatory(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailSignatory_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerCategoryDesc", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCustomerCategoryDesc(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerCategoryDesc_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "dateofbirth", scope = CustomerAcctsDetail.class)
    public JAXBElement<XMLGregorianCalendar> createCustomerAcctsDetailDateofbirth(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CustomerAcctsDetailDateofbirth_QNAME, XMLGregorianCalendar.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "customerName", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCustomerName(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerName_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastSerialofCheque", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailLastSerialofCheque(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailLastSerialofCheque_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountCreditMTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountCreditMTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountCreditMTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountOfficer", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailAccountOfficer(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountOfficer_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "serviceChargeYTD", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailServiceChargeYTD(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailServiceChargeYTD_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "strState", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailStrState(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailStrState_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "unavailableBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailUnavailableBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailUnavailableBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amountLastCredit", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailAmountLastCredit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailAmountLastCredit_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "e_mail", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailEMail(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailEMail_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountStatus", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountStatus_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "dateOpened", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailDateOpened(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailDateOpened_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "openingBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailOpeningBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailOpeningBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "COMP_MIS_2", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCOMPMIS2(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailCOMPMIS2_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "dAUEStartDate", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailDAUEStartDate(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailDAUEStartDate_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "COMP_MIS_4", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailCOMPMIS4(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailCOMPMIS4_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastUsedChequeNo", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailLastUsedChequeNo(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailLastUsedChequeNo_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "unclearedBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailUnclearedBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailUnclearedBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "productName", scope = CustomerAcctsDetail.class)
    public JAXBElement<String> createCustomerAcctsDetailProductName(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailProductName_QNAME, String.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "lastCreditInterestAccrued", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailLastCreditInterestAccrued(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailLastCreditInterestAccrued_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "closingBalance", scope = CustomerAcctsDetail.class)
    public JAXBElement<BigDecimal> createCustomerAcctsDetailClosingBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerAcctsDetailClosingBalance_QNAME, BigDecimal.class, CustomerAcctsDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "USERREF_NO1", scope = CreateMMContractResponse.class)
    public JAXBElement<String> createCreateMMContractResponseUSERREFNO1(String value) {
        return new JAXBElement<String>(_CreateMMContractResponseUSERREFNO1_QNAME, String.class, CreateMMContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CONREFNO", scope = CreateMMContractResponse.class)
    public JAXBElement<String> createCreateMMContractResponseCONREFNO(String value) {
        return new JAXBElement<String>(_CreateSIContractResponseCONREFNO_QNAME, String.class, CreateMMContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS4", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCompMIS4(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS4_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CrAccountCcyCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCrAccountCcyCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCrAccountCcyCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS2", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCompMIS2(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS2_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SIAmountCcyCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestSIAmountCcyCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestSIAmountCcyCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ExecDayInterval", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestExecDayInterval(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestExecDayInterval_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FirstExecutionDate", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestFirstExecutionDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestFirstExecutionDate_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ExecMonthInterval", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestExecMonthInterval(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestExecMonthInterval_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MakerDateStamp", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestMakerDateStamp(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestMakerDateStamp_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CheckerID", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCheckerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCheckerID_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CrAccountBranchCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCrAccountBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCrAccountBranchCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ExecYearInterval", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestExecYearInterval(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestExecYearInterval_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SIAmount", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestSIAmount(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestSIAmount_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SIExpiryDate", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestSIExpiryDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestSIExpiryDate_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DrAccountCcyCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestDrAccountCcyCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestDrAccountCcyCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS8", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCompMIS8(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS8_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CheckerDateStamp", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCheckerDateStamp(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCheckerDateStamp_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DrAccountBranchCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestDrAccountBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestDrAccountBranchCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DrAccountNo", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestDrAccountNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestDrAccountNo_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DrAccountName", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestDrAccountName(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestDrAccountName_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "InternalRemarks", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestInternalRemarks(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestInternalRemarks_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MakerID", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestMakerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestMakerID_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "NextExecutionDate", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestNextExecutionDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestNextExecutionDate_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CrAccountNo", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestCrAccountNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCrAccountNo_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ProductCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestProductCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestProductCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCreateSIContractRequest.class)
    public JAXBElement<String> createFCUBSCreateSIContractRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCreateSIContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountFinanced", scope = RepaymentScheduleInfo.class)
    public JAXBElement<BigDecimal> createRepaymentScheduleInfoAmountFinanced(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoAmountFinanced_QNAME, BigDecimal.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CR_PROD_AC", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoCRPRODAC(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCRPRODAC_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCode", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoCurrencyCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCurrencyCode_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanAccountNo", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoLoanAccountNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoLoanAccountNo_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OverDueAmount", scope = RepaymentScheduleInfo.class)
    public JAXBElement<BigDecimal> createRepaymentScheduleInfoOverDueAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoOverDueAmount_QNAME, BigDecimal.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ValueDate", scope = RepaymentScheduleInfo.class)
    public JAXBElement<XMLGregorianCalendar> createRepaymentScheduleInfoValueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoValueDate_QNAME, XMLGregorianCalendar.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BookDate", scope = RepaymentScheduleInfo.class)
    public JAXBElement<XMLGregorianCalendar> createRepaymentScheduleInfoBookDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoBookDate_QNAME, XMLGregorianCalendar.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCategory", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoProductCategory(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductCategory_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ScheduleDueDate", scope = RepaymentScheduleInfo.class)
    public JAXBElement<XMLGregorianCalendar> createRepaymentScheduleInfoScheduleDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoScheduleDueDate_QNAME, XMLGregorianCalendar.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductDescription", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoProductDescription(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductDescription_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ComponentName", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoComponentName(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoComponentName_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountDisbursed", scope = RepaymentScheduleInfo.class)
    public JAXBElement<BigDecimal> createRepaymentScheduleInfoAmountDisbursed(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoAmountDisbursed_QNAME, BigDecimal.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountDue", scope = RepaymentScheduleInfo.class)
    public JAXBElement<BigDecimal> createRepaymentScheduleInfoAmountDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoAmountDue_QNAME, BigDecimal.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerNo", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoCustomerNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCustomerNo_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DR_ACC_BRN", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoDRACCBRN(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoDRACCBRN_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DR_PROD_AC", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoDRPRODAC(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoDRPRODAC_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OriginalStartDate", scope = RepaymentScheduleInfo.class)
    public JAXBElement<XMLGregorianCalendar> createRepaymentScheduleInfoOriginalStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoOriginalStartDate_QNAME, XMLGregorianCalendar.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CR_ACC_BRN", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoCRACCBRN(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCRACCBRN_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountSettled", scope = RepaymentScheduleInfo.class)
    public JAXBElement<BigDecimal> createRepaymentScheduleInfoAmountSettled(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoAmountSettled_QNAME, BigDecimal.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ApplicantName", scope = RepaymentScheduleInfo.class)
    public JAXBElement<String> createRepaymentScheduleInfoApplicantName(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoApplicantName_QNAME, String.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MaturityDate", scope = RepaymentScheduleInfo.class)
    public JAXBElement<XMLGregorianCalendar> createRepaymentScheduleInfoMaturityDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoMaturityDate_QNAME, XMLGregorianCalendar.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ScheduleStartDate", scope = RepaymentScheduleInfo.class)
    public JAXBElement<XMLGregorianCalendar> createRepaymentScheduleInfoScheduleStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoScheduleStartDate_QNAME, XMLGregorianCalendar.class, RepaymentScheduleInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis7", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis7(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis7_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis6", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis6(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis6_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TotalCredit", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxTotalCredit(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTotalCredit_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis5", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis5(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis5_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis4", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis4(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis4_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis3", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis3(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis3_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis2", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis2(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis2_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis1", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis1(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis1_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FUNCTIONID", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxFUNCTIONID(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxFUNCTIONID_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ValueDate", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxValueDate(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxValueDate_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CurrencyCode", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCurrencyCode(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCurrencyCode_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKDTTIME", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxMAKDTTIME(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxMAKDTTIME_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECHKERID", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCHECHKERID(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCHECHKERID_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MSGSTAT", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxMSGSTAT(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxMSGSTAT_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis9", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis9(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis9_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis8", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis8(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis8_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxMAKER(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxMAKER_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CurrencyNumber", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCurrencyNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCurrencyNumber_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ACTION", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxACTION(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxACTION_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MsgId", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxMsgId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionMisCode1", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxTransactionMisCode1(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTransactionMisCode1_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "RelativeAccount", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxRelativeAccount(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxRelativeAccount_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMultipleJournalDetailTrx }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionDetails", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<ArrayOfMultipleJournalDetailTrx> createMultipleJournalMasterTrxTransactionDetails(ArrayOfMultipleJournalDetailTrx value) {
        return new JAXBElement<ArrayOfMultipleJournalDetailTrx>(_MultipleJournalMasterTrxTransactionDetails_QNAME, ArrayOfMultipleJournalDetailTrx.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TotalDebit", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxTotalDebit(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTotalDebit_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionMisCode2", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxTransactionMisCode2(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTransactionMisCode2_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMis10", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCompMis10(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCompMis10_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BatchNumber", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxBatchNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxBatchNumber_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHKDTTIME", scope = MultipleJournalMasterTrx.class)
    public JAXBElement<String> createMultipleJournalMasterTrxCHKDTTIME(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCHKDTTIME_QNAME, String.class, MultipleJournalMasterTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TotalPrincipalOutstanding", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesTotalPrincipalOutstanding(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesTotalPrincipalOutstanding_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "NextPrincipalDue", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesNextPrincipalDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesNextPrincipalDue_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CcyCode", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesCcyCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesCcyCode_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerAddress", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesCustomerAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerAddress_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCategory", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesProductCategory(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductCategory_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductDescription", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesProductDescription(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductDescription_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCode", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesProductCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesProductCode_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InterestPaid", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesInterestPaid(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesInterestPaid_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InterestPenaltyPaid", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesInterestPenaltyPaid(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesInterestPenaltyPaid_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OriginalStartDate", scope = LoanInfoSummaryLines.class)
    public JAXBElement<XMLGregorianCalendar> createLoanInfoSummaryLinesOriginalStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoOriginalStartDate_QNAME, XMLGregorianCalendar.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PrincipalPaid", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesPrincipalPaid(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesPrincipalPaid_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ApplicantName", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesApplicantName(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoApplicantName_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MaturityDate", scope = LoanInfoSummaryLines.class)
    public JAXBElement<XMLGregorianCalendar> createLoanInfoSummaryLinesMaturityDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoMaturityDate_QNAME, XMLGregorianCalendar.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountFinanced", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesAmountFinanced(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoAmountFinanced_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InterestNotDue", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesInterestNotDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesInterestNotDue_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountStatus", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountStatus_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanAccountNumber", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesLoanAccountNumber(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesLoanAccountNumber_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ValueDate", scope = LoanInfoSummaryLines.class)
    public JAXBElement<XMLGregorianCalendar> createLoanInfoSummaryLinesValueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoValueDate_QNAME, XMLGregorianCalendar.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BookDate", scope = LoanInfoSummaryLines.class)
    public JAXBElement<XMLGregorianCalendar> createLoanInfoSummaryLinesBookDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoBookDate_QNAME, XMLGregorianCalendar.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DomiciliaryAddress", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesDomiciliaryAddress(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesDomiciliaryAddress_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PrincipalOverDue", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesPrincipalOverDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesPrincipalOverDue_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerName", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesCustomerName(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesCustomerName_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "NextInterestDue", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesNextInterestDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesNextInterestDue_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InterestOverDue", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesInterestOverDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesInterestOverDue_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerNo", scope = LoanInfoSummaryLines.class)
    public JAXBElement<String> createLoanInfoSummaryLinesCustomerNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCustomerNo_QNAME, String.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PrincipalNotDue", scope = LoanInfoSummaryLines.class)
    public JAXBElement<BigDecimal> createLoanInfoSummaryLinesPrincipalNotDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesPrincipalNotDue_QNAME, BigDecimal.class, LoanInfoSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MaturityAmount", scope = FixedDepositInfo.class)
    public JAXBElement<BigDecimal> createFixedDepositInfoMaturityAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_FixedDepositInfoMaturityAmount_QNAME, BigDecimal.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ReferenceNo", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoReferenceNo(String value) {
        return new JAXBElement<String>(_FixedDepositInfoReferenceNo_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PaymentMethod", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoPaymentMethod(String value) {
        return new JAXBElement<String>(_FixedDepositInfoPaymentMethod_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SettlementAccount", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoSettlementAccount(String value) {
        return new JAXBElement<String>(_FixedDepositInfoSettlementAccount_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ValueDate", scope = FixedDepositInfo.class)
    public JAXBElement<XMLGregorianCalendar> createFixedDepositInfoValueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoValueDate_QNAME, XMLGregorianCalendar.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerName", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoCustomerName(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesCustomerName_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "UserReferenceNo", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoUserReferenceNo(String value) {
        return new JAXBElement<String>(_FixedDepositInfoUserReferenceNo_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BookingDate", scope = FixedDepositInfo.class)
    public JAXBElement<XMLGregorianCalendar> createFixedDepositInfoBookingDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FixedDepositInfoBookingDate_QNAME, XMLGregorianCalendar.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RolloverAllowed", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoRolloverAllowed(String value) {
        return new JAXBElement<String>(_FixedDepositInfoRolloverAllowed_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InitialDeposit", scope = FixedDepositInfo.class)
    public JAXBElement<BigDecimal> createFixedDepositInfoInitialDeposit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_FixedDepositInfoInitialDeposit_QNAME, BigDecimal.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Tenor", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoTenor(String value) {
        return new JAXBElement<String>(_FixedDepositInfoTenor_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OriginalStartDate", scope = FixedDepositInfo.class)
    public JAXBElement<XMLGregorianCalendar> createFixedDepositInfoOriginalStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoOriginalStartDate_QNAME, XMLGregorianCalendar.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SettlementAccCcyCode", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoSettlementAccCcyCode(String value) {
        return new JAXBElement<String>(_FixedDepositInfoSettlementAccCcyCode_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Product", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoProduct(String value) {
        return new JAXBElement<String>(_FixedDepositInfoProduct_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MaturityDate", scope = FixedDepositInfo.class)
    public JAXBElement<XMLGregorianCalendar> createFixedDepositInfoMaturityDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoMaturityDate_QNAME, XMLGregorianCalendar.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SettlementAccBranchCode", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoSettlementAccBranchCode(String value) {
        return new JAXBElement<String>(_FixedDepositInfoSettlementAccBranchCode_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ContractStatus", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoContractStatus(String value) {
        return new JAXBElement<String>(_FixedDepositInfoContractStatus_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DepositCurrencyCode", scope = FixedDepositInfo.class)
    public JAXBElement<String> createFixedDepositInfoDepositCurrencyCode(String value) {
        return new JAXBElement<String>(_FixedDepositInfoDepositCurrencyCode_QNAME, String.class, FixedDepositInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CUSTNO", scope = CreateCustomerResponse.class)
    public JAXBElement<String> createCreateCustomerResponseCUSTNO(String value) {
        return new JAXBElement<String>(_CreateCustomerResponseCUSTNO_QNAME, String.class, CreateCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENEFICIARY_ADDRESS1", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestBENEFICIARYADDRESS1(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestBENEFICIARYADDRESS1_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CREDIT_ACCOUNT", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestCREDITACCOUNT(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestCREDITACCOUNT_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MSGID", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestMSGID(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestMSGID_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CREDIT_BRANCH", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestCREDITBRANCH(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestCREDITBRANCH_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SOURCE", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestSOURCE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestSOURCE_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "USERID", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestUSERID(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestUSERID_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "USER_BRANCH_CODE", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestUSERBRANCHCODE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestUSERBRANCHCODE_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENEFICIARY_NAME", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestBENEFICIARYNAME(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestBENEFICIARYNAME_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENEFICIARY_ADDRESS3", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestBENEFICIARYADDRESS3(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestBENEFICIARYADDRESS3_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENEFICIARY_ADDRESS2", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestBENEFICIARYADDRESS2(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestBENEFICIARYADDRESS2_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DEBIT_CCY", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestDEBITCCY(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestDEBITCCY_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DEBIT_BRANCH", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestDEBITBRANCH(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestDEBITBRANCH_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DEBIT_AMOUNT", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestDEBITAMOUNT(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestDEBITAMOUNT_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DEBIT_VALUEDATE", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestDEBITVALUEDATE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestDEBITVALUEDATE_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CREDIT_CCY", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestCREDITCCY(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestCREDITCCY_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CREDIT_AMOUNT", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestCREDITAMOUNT(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestCREDITAMOUNT_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DEBIT_ACCOUNT", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestDEBITACCOUNT(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestDEBITACCOUNT_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CREDIT_VALUEDATE", scope = FCUBSFTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSFTCreateTransactionRequestCREDITVALUEDATE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestCREDITVALUEDATE_QNAME, String.class, FCUBSFTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Function_Name", scope = ScreenLimitResponse.class)
    public JAXBElement<String> createScreenLimitResponseFunctionName(String value) {
        return new JAXBElement<String>(_ScreenLimitResponseFunctionName_QNAME, String.class, ScreenLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "function_group", scope = ScreenLimitResponse.class)
    public JAXBElement<String> createScreenLimitResponseFunctionGroup(String value) {
        return new JAXBElement<String>(_ScreenLimitResponseFunctionGroup_QNAME, String.class, ScreenLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branch_code", scope = ScreenLimitResponse.class)
    public JAXBElement<String> createScreenLimitResponseBranchCode(String value) {
        return new JAXBElement<String>(_ScreenLimitResponseBranchCode_QNAME, String.class, ScreenLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amount", scope = ScreenLimitResponse.class)
    public JAXBElement<BigDecimal> createScreenLimitResponseAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ChequeNumberDetailsAmount_QNAME, BigDecimal.class, ScreenLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ccy_code", scope = ScreenLimitResponse.class)
    public JAXBElement<String> createScreenLimitResponseCcyCode(String value) {
        return new JAXBElement<String>(_ScreenLimitResponseCcyCode_QNAME, String.class, ScreenLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Till_Count", scope = ValidateUserTillResp.class)
    public JAXBElement<BigDecimal> createValidateUserTillRespTillCount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ValidateUserTillRespTillCount_QNAME, BigDecimal.class, ValidateUserTillResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ccy_code", scope = GetCurrencyCodeResp.class)
    public JAXBElement<String> createGetCurrencyCodeRespCcyCode(String value) {
        return new JAXBElement<String>(_ScreenLimitResponseCcyCode_QNAME, String.class, GetCurrencyCodeResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "home_branch", scope = GetUserBranchResp.class)
    public JAXBElement<String> createGetUserBranchRespHomeBranch(String value) {
        return new JAXBElement<String>(_GetUserBranchRespHomeBranch_QNAME, String.class, GetUserBranchResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "USER_STATUS", scope = AuthenticateCBAUserResp.class)
    public JAXBElement<String> createAuthenticateCBAUserRespUSERSTATUS(String value) {
        return new JAXBElement<String>(_AuthenticateCBAUserRespUSERSTATUS_QNAME, String.class, AuthenticateCBAUserResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "USER_ID", scope = AuthenticateCBAUserResp.class)
    public JAXBElement<String> createAuthenticateCBAUserRespUSERID(String value) {
        return new JAXBElement<String>(_AuthenticateCBAUserRespUSERID_QNAME, String.class, AuthenticateCBAUserResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountPostNoDebit", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountPostNoDebit(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountPostNoDebit_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "IsSalaryAccount", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestIsSalaryAccount(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestIsSalaryAccount_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountClassType", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountClassType(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountClassType_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountCompMIS3", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountCompMIS3(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountCompMIS3_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountCompMIS4", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountCompMIS4(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountCompMIS4_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerNo", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestCustomerNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestCustomerNo_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountCcyCode", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountCcyCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountCcyCode_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Dormant", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestDormant(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestDormant_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CheckerID", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestCheckerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCheckerID_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountCompMIS8", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountCompMIS8(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountCompMIS8_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ChequeBookAccount", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestChequeBookAccount(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestChequeBookAccount_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountOpenDate", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountOpenDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountOpenDate_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountBranchCode", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountBranchCode_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountPoolCD", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountPoolCD(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountPoolCD_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountCompMIS2", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountCompMIS2(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountCompMIS2_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountName", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountName_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AuthStatus", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAuthStatus(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAuthStatus_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ATMAccount", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestATMAccount(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestATMAccount_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountPostNoCredit", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountPostNoCredit(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountPostNoCredit_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Frozen", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestFrozen(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestFrozen_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MakerID", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestMakerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestMakerID_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MinBalanceReqd", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestMinBalanceReqd(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestMinBalanceReqd_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountClass", scope = FCUBSCreateCustAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateCustAccountRequestAccountClass(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountClass_QNAME, String.class, FCUBSCreateCustAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BOOKDATE", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestBOOKDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestBOOKDATE_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "VALDATE", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestVALDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestVALDATE_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETCCY", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestOFFSETCCY(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestOFFSETCCY_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNCCY", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTXNCCY(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNCCY_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TRACKREVERSE", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTRACKREVERSE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTRACKREVERSE_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNDATE", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTXNDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNDATE_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETACC", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestOFFSETACC(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestOFFSETACC_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TIMERECV", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTIMERECV(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTIMERECV_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Xref", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestXref(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestXref_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AUTHSTAT", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestAUTHSTAT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestAUTHSTAT_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCCREF", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestFCCREF(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestFCCREF_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MsgId", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestMsgId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LCYAMT", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestLCYAMT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestLCYAMT_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETAMT", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestOFFSETAMT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestOFFSETAMT_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNAMT", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTXNAMT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNAMT_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHEQUE_ISSUE_DATE", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestCHEQUEISSUEDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestCHEQUEISSUEDATE_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNDRCR", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTXNDRCR(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNDRCR_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNACC", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestTXNACC(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNACC_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "INSTRDATE", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestINSTRDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestINSTRDATE_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PRD", scope = AuthorizeWithdrawalRequest.class)
    public JAXBElement<String> createAuthorizeWithdrawalRequestPRD(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestPRD_QNAME, String.class, AuthorizeWithdrawalRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountTransactionResponses", scope = AccountTransactionResponseBulk.class)
    public JAXBElement<ArrayOfAccountSummaryAndTrxResponse> createAccountTransactionResponseBulkAccountTransactionResponses(ArrayOfAccountSummaryAndTrxResponse value) {
        return new JAXBElement<ArrayOfAccountSummaryAndTrxResponse>(_AccountTransactionResponseBulkAccountTransactionResponses_QNAME, ArrayOfAccountSummaryAndTrxResponse.class, AccountTransactionResponseBulk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ReferenceNo", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestReferenceNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestReferenceNo_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS5", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCompMIS5(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestCompMIS5_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS4", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCompMIS4(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS4_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS3", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCompMIS3(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestCompMIS3_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS2", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCompMIS2(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS2_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CurrencyCode", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCurrencyCode(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCurrencyCode_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DebitAccount", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestDebitAccount(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestDebitAccount_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerNo", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCustomerNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestCustomerNo_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CrAccBranchCode", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCrAccBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestCrAccBranchCode_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Amount", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestAmount(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestAmount_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DrAccBranchCode", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestDrAccBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestDrAccBranchCode_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CreditAccount", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCreditAccount(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestCreditAccount_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS8", scope = FCUBSCreateMMContractRequest.class)
    public JAXBElement<String> createFCUBSCreateMMContractRequestCompMIS8(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS8_QNAME, String.class, FCUBSCreateMMContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LimitAmount", scope = CustomerODLimitLines.class)
    public JAXBElement<String> createCustomerODLimitLinesLimitAmount(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesLimitAmount_QNAME, String.class, CustomerODLimitLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OdIntRate", scope = CustomerODLimitLines.class)
    public JAXBElement<BigDecimal> createCustomerODLimitLinesOdIntRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerODLimitLinesOdIntRate_QNAME, BigDecimal.class, CustomerODLimitLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LineExpiryDate", scope = CustomerODLimitLines.class)
    public JAXBElement<String> createCustomerODLimitLinesLineExpiryDate(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesLineExpiryDate_QNAME, String.class, CustomerODLimitLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LineStartDate", scope = CustomerODLimitLines.class)
    public JAXBElement<String> createCustomerODLimitLinesLineStartDate(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesLineStartDate_QNAME, String.class, CustomerODLimitLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNumber", scope = CustomerODLimitLines.class)
    public JAXBElement<String> createCustomerODLimitLinesAccountNumber(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesAccountNumber_QNAME, String.class, CustomerODLimitLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountCotAndVatLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountCotAndVatTransactions", scope = AccountCotAndVatResponse.class)
    public JAXBElement<ArrayOfAccountCotAndVatLines> createAccountCotAndVatResponseAccountCotAndVatTransactions(ArrayOfAccountCotAndVatLines value) {
        return new JAXBElement<ArrayOfAccountCotAndVatLines>(_AccountCotAndVatResponseAccountCotAndVatTransactions_QNAME, ArrayOfAccountCotAndVatLines.class, AccountCotAndVatResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LDCONTRACTREFNO", scope = CreateLDContractResponse.class)
    public JAXBElement<String> createCreateLDContractResponseLDCONTRACTREFNO(String value) {
        return new JAXBElement<String>(_CreateLDContractResponseLDCONTRACTREFNO_QNAME, String.class, CreateLDContractResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSummaryAndTransactions", scope = AccountSummaryAndTrxResponse.class)
    public JAXBElement<ArrayOfAccountSummaryAndTrxLines> createAccountSummaryAndTrxResponseAccountSummaryAndTransactions(ArrayOfAccountSummaryAndTrxLines value) {
        return new JAXBElement<ArrayOfAccountSummaryAndTrxLines>(_AccountSummaryAndTrxResponseAccountSummaryAndTransactions_QNAME, ArrayOfAccountSummaryAndTrxLines.class, AccountSummaryAndTrxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_last_mnt_makerid", scope = AccountMemoInfo.class)
    public JAXBElement<String> createAccountMemoInfoCodLastMntMakerid(String value) {
        return new JAXBElement<String>(_AccountMemoInfoCodLastMntMakerid_QNAME, String.class, AccountMemoInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_last_mnt_chkrid", scope = AccountMemoInfo.class)
    public JAXBElement<String> createAccountMemoInfoCodLastMntChkrid(String value) {
        return new JAXBElement<String>(_AccountMemoInfoCodLastMntChkrid_QNAME, String.class, AccountMemoInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "memotext", scope = AccountMemoInfo.class)
    public JAXBElement<String> createAccountMemoInfoMemotext(String value) {
        return new JAXBElement<String>(_AccountMemoInfoMemotext_QNAME, String.class, AccountMemoInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "custid", scope = AccountMemoInfo.class)
    public JAXBElement<String> createAccountMemoInfoCustid(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustid_QNAME, String.class, AccountMemoInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "dat_last_mntx", scope = AccountMemoInfo.class)
    public JAXBElement<String> createAccountMemoInfoDatLastMntx(String value) {
        return new JAXBElement<String>(_AccountMemoInfoDatLastMntx_QNAME, String.class, AccountMemoInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCloseCustAccRequest.class)
    public JAXBElement<String> createFCUBSCloseCustAccRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCloseCustAccRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AcountBranchCode", scope = FCUBSCloseCustAccRequest.class)
    public JAXBElement<String> createFCUBSCloseCustAccRequestAcountBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCloseCustAccRequestAcountBranchCode_QNAME, String.class, FCUBSCloseCustAccRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCloseCustAccRequest.class)
    public JAXBElement<String> createFCUBSCloseCustAccRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCloseCustAccRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountNo", scope = FCUBSCloseCustAccRequest.class)
    public JAXBElement<String> createFCUBSCloseCustAccRequestAccountNo(String value) {
        return new JAXBElement<String>(_FCUBSCloseCustAccRequestAccountNo_QNAME, String.class, FCUBSCloseCustAccRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCloseCustAccRequest.class)
    public JAXBElement<String> createFCUBSCloseCustAccRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCloseCustAccRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "STOPPAYMENTNO", scope = CreateStopPaymentResponse.class)
    public JAXBElement<String> createCreateStopPaymentResponseSTOPPAYMENTNO(String value) {
        return new JAXBElement<String>(_CreateStopPaymentResponseSTOPPAYMENTNO_QNAME, String.class, CreateStopPaymentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRelatedAccountSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RelatedAccountSummary", scope = AccountRelatedResponse.class)
    public JAXBElement<ArrayOfRelatedAccountSummaryLines> createAccountRelatedResponseRelatedAccountSummary(ArrayOfRelatedAccountSummaryLines value) {
        return new JAXBElement<ArrayOfRelatedAccountSummaryLines>(_AccountRelatedResponseRelatedAccountSummary_QNAME, ArrayOfRelatedAccountSummaryLines.class, AccountRelatedResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerODLimitLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerODLimitTransactions", scope = CustomerODLimitResponse.class)
    public JAXBElement<ArrayOfCustomerODLimitLines> createCustomerODLimitResponseCustomerODLimitTransactions(ArrayOfCustomerODLimitLines value) {
        return new JAXBElement<ArrayOfCustomerODLimitLines>(_CustomerODLimitResponseCustomerODLimitTransactions_QNAME, ArrayOfCustomerODLimitLines.class, CustomerODLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFixedDepositInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "FixedDepositInfo", scope = FixedDepositResponse.class)
    public JAXBElement<ArrayOfFixedDepositInfo> createFixedDepositResponseFixedDepositInfo(ArrayOfFixedDepositInfo value) {
        return new JAXBElement<ArrayOfFixedDepositInfo>(_FixedDepositInfo_QNAME, ArrayOfFixedDepositInfo.class, FixedDepositResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfChequeDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ChequeDetails", scope = ChequeDetailsResponse.class)
    public JAXBElement<ArrayOfChequeDetails> createChequeDetailsResponseChequeDetails(ArrayOfChequeDetails value) {
        return new JAXBElement<ArrayOfChequeDetails>(_ChequeDetails_QNAME, ArrayOfChequeDetails.class, ChequeDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CUST_AC_NO", scope = CreateTDAccountResponse.class)
    public JAXBElement<String> createCreateTDAccountResponseCUSTACNO(String value) {
        return new JAXBElement<String>(_CreateTDAccountResponseCUSTACNO_QNAME, String.class, CreateTDAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ResponseData", scope = FcubsResponse.class)
    public JAXBElement<String> createFcubsResponseResponseData(String value) {
        return new JAXBElement<String>(_FcubsResponseResponseData_QNAME, String.class, FcubsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ResponseCode", scope = FcubsResponse.class)
    public JAXBElement<String> createFcubsResponseResponseCode(String value) {
        return new JAXBElement<String>(_FcubsResponseResponseCode_QNAME, String.class, FcubsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branch_code", scope = GetAccountBranchResp.class)
    public JAXBElement<String> createGetAccountBranchRespBranchCode(String value) {
        return new JAXBElement<String>(_ScreenLimitResponseBranchCode_QNAME, String.class, GetAccountBranchResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_acct_no", scope = SweepAcctsDetails.class)
    public JAXBElement<String> createSweepAcctsDetailsCodAcctNo(String value) {
        return new JAXBElement<String>(_SweepAcctsDetailsCodAcctNo_QNAME, String.class, SweepAcctsDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerAcctsDetail", scope = CustomerAcctsDetailResponse.class)
    public JAXBElement<ArrayOfCustomerAcctsDetail> createCustomerAcctsDetailResponseCustomerAcctsDetail(ArrayOfCustomerAcctsDetail value) {
        return new JAXBElement<ArrayOfCustomerAcctsDetail>(_CustomerAcctsDetail_QNAME, ArrayOfCustomerAcctsDetail.class, CustomerAcctsDetailResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DrCrInd", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesDrCrInd(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesDrCrInd_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InstrumentCode", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesInstrumentCode(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesInstrumentCode_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "UserId", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesUserId(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesUserId_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ClosingBalance", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesClosingBalance(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesClosingBalance_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ExternalRefNumber", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesExternalRefNumber(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesExternalRefNumber_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "productCode", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesProductCode(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailProductCode_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RowNumber", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesRowNumber(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesRowNumber_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranAmount", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTranAmount(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTranAmount_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "event_sr_no", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesEventSrNo(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesEventSrNo_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CcyCode", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesCcyCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesCcyCode_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Module", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesModule(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesModule_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TxnInitiationDate", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<XMLGregorianCalendar> createAccountSummaryAndTrxLinesTxnInitiationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountSummaryAndTrxLinesTxnInitiationDate_QNAME, XMLGregorianCalendar.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "customerNo", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesCustomerNo(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerNo_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PostedDate", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesPostedDate(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesPostedDate_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TotalWithdrawal", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTotalWithdrawal(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTotalWithdrawal_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "clearedBalance", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesClearedBalance(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailClearedBalance_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AcctOpeningBalance", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesAcctOpeningBalance(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesAcctOpeningBalance_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranEvent", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTranEvent(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTranEvent_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranDescription", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTranDescription(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTranDescription_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "noofLodgement", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesNoofLodgement(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesNoofLodgement_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LcyAmount", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesLcyAmount(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesLcyAmount_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNumber", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesAccountNumber(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesAccountNumber_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Stmt_Dt", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesStmtDt(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesStmtDt_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranIndicator", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTranIndicator(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTranIndicator_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TrnRefNo", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTrnRefNo(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTrnRefNo_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AuthId", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesAuthId(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesAuthId_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AvailableBalance", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesAvailableBalance(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesAvailableBalance_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ac_entry_sr_no", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesAcEntrySrNo(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesAcEntrySrNo_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TransactionDate", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTransactionDate(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTransactionDate_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "noofWithdrawal", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesNoofWithdrawal(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesNoofWithdrawal_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TotalLodgement", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTotalLodgement(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTotalLodgement_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ValueDate", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesValueDate(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoValueDate_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TransactionCode", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTransactionCode(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTransactionCode_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "UnClearedBalance", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesUnClearedBalance(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesUnClearedBalance_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchName", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesBranchName(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesBranchName_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RunningBalance", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesRunningBalance(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesRunningBalance_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountName", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesAccountName(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailAccountName_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCodeDesc", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesProductCodeDesc(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesProductCodeDesc_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "FcyAmount", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesFcyAmount(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesFcyAmount_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranNarration", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesTranNarration(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesTranNarration_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BatchNo", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesBatchNo(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesBatchNo_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchAddress", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesBranchAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsBranchAddress_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "hasCOT", scope = AccountSummaryAndTrxLines.class)
    public JAXBElement<String> createAccountSummaryAndTrxLinesHasCOT(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesHasCOT_QNAME, String.class, AccountSummaryAndTrxLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVisaCardLimitSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "VisaCardLimitSummary", scope = VisaCardLimitResponse.class)
    public JAXBElement<ArrayOfVisaCardLimitSummaryLines> createVisaCardLimitResponseVisaCardLimitSummary(ArrayOfVisaCardLimitSummaryLines value) {
        return new JAXBElement<ArrayOfVisaCardLimitSummaryLines>(_VisaCardLimitResponseVisaCardLimitSummary_QNAME, ArrayOfVisaCardLimitSummaryLines.class, VisaCardLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MSGID", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestMSGID(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestMSGID_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TRAN_DATE", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestTRANDATE(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestTRANDATE_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNCCY", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestTXNCCY(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNCCY_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ACC_CCY", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestACCCCY(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestACCCCY_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECKER_ID", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestCHECKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCHECKERID_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BRANCH_CODE", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestBRANCHCODE(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestBRANCHCODE_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENEFICIARY_ACCTNO", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestBENEFICIARYACCTNO(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestBENEFICIARYACCTNO_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "INSTRAMT", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestINSTRAMT(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestINSTRAMT_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SOURCE", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestSOURCE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestSOURCE_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "USERID", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestUSERID(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestUSERID_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "USER_BRANCH_CODE", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestUSERBRANCHCODE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestUSERBRANCHCODE_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECKER_STAMP", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestCHECKERSTAMP(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCHECKERSTAMP_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PAYERACCOUNT", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestPAYERACCOUNT(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestPAYERACCOUNT_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "XREF", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestXREF(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestXREF_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PAYERINSTRNO", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestPAYERINSTRNO(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestPAYERINSTRNO_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "VALUE_DATE", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestVALUEDATE(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestVALUEDATE_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "REMARKS", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestREMARKS(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestREMARKS_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER_ID", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestMAKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMAKERID_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ROUTING_NO", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestROUTINGNO(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestROUTINGNO_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "INSTRCCY", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestINSTRCCY(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestINSTRCCY_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER_STAMP", scope = FCUBSCGServiceCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSCGServiceCreateTransactionRequestMAKERSTAMP(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMAKERSTAMP_QNAME, String.class, FCUBSCGServiceCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "checkbookstatus", scope = ActivateChequeBookResponse.class)
    public JAXBElement<String> createActivateChequeBookResponseCheckbookstatus(String value) {
        return new JAXBElement<String>(_ActivateChequeBookResponseCheckbookstatus_QNAME, String.class, ActivateChequeBookResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "err_code", scope = ActivateChequeBookResponse.class)
    public JAXBElement<String> createActivateChequeBookResponseErrCode(String value) {
        return new JAXBElement<String>(_ActivateChequeBookResponseErrCode_QNAME, String.class, ActivateChequeBookResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanSummaryResponses", scope = LoanSummaryResponseBulk.class)
    public JAXBElement<ArrayOfLoanSummaryResponse> createLoanSummaryResponseBulkLoanSummaryResponses(ArrayOfLoanSummaryResponse value) {
        return new JAXBElement<ArrayOfLoanSummaryResponse>(_LoanSummaryResponseBulkLoanSummaryResponses_QNAME, ArrayOfLoanSummaryResponse.class, LoanSummaryResponseBulk.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountMemoInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountMemo", scope = GetAcctMemoInfoResponse.class)
    public JAXBElement<ArrayOfAccountMemoInfo> createGetAcctMemoInfoResponseAccountMemo(ArrayOfAccountMemoInfo value) {
        return new JAXBElement<ArrayOfAccountMemoInfo>(_GetAcctMemoInfoResponseAccountMemo_QNAME, ArrayOfAccountMemoInfo.class, GetAcctMemoInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanSummaryLines", scope = LoanSummaryResponse.class)
    public JAXBElement<ArrayOfLoanSummaryLine> createLoanSummaryResponseLoanSummaryLines(ArrayOfLoanSummaryLine value) {
        return new JAXBElement<ArrayOfLoanSummaryLine>(_LoanSummaryResponseLoanSummaryLines_QNAME, ArrayOfLoanSummaryLine.class, LoanSummaryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHQNO", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestCHQNO(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestCHQNO_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECKER_ID", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestCHECKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCHECKERID_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNDATE", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestTXNDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNDATE_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETACC", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestOFFSETACC(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestOFFSETACC_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKDTTIME", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestMAKDTTIME(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxMAKDTTIME_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETAMT", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestOFFSETAMT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestOFFSETAMT_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNAMT", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestTXNAMT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNAMT_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER_ID", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestMAKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMAKERID_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNACC", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestTXNACC(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNACC_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "INSTRDATE", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestINSTRDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestINSTRDATE_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PRD", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestPRD(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestPRD_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BOOKDATE", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBOOKDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestBOOKDATE_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "VALDATE", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestVALDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestVALDATE_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "NARRATIVE", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestNARRATIVE(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestNARRATIVE_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETCCY", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestOFFSETCCY(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestOFFSETCCY_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNCCY", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestTXNCCY(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNCCY_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENFNAME", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBENFNAME(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestBENFNAME_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "XREF", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestXREF(String value) {
        return new JAXBElement<String>(_FCUBSCGServiceCreateTransactionRequestXREF_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OFFSETBRN", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestOFFSETBRN(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestOFFSETBRN_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MsgId", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestMsgId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LCYAMT", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestLCYAMT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestLCYAMT_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENFADDR4", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBENFADDR4(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestBENFADDR4_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHEQUE_ISSUE_DATE", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestCHEQUEISSUEDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestCHEQUEISSUEDATE_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNDRCR", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestTXNDRCR(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNDRCR_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENFADDR1", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBENFADDR1(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestBENFADDR1_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNBRN", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestTXNBRN(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestTXNBRN_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENFADDR2", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBENFADDR2(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestBENFADDR2_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BENFADDR3", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestBENFADDR3(String value) {
        return new JAXBElement<String>(_FCUBSRTCreateTransactionRequestBENFADDR3_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHKDTTIME", scope = FCUBSRTCreateTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTCreateTransactionRequestCHKDTTIME(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCHKDTTIME_QNAME, String.class, FCUBSRTCreateTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNo", scope = ChequeDetails.class)
    public JAXBElement<String> createChequeDetailsAccountNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsAccountNo_QNAME, String.class, ChequeDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SectorCode", scope = ChequeDetails.class)
    public JAXBElement<String> createChequeDetailsSectorCode(String value) {
        return new JAXBElement<String>(_ChequeDetailsSectorCode_QNAME, String.class, ChequeDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TotalChequeLeafNo", scope = ChequeDetails.class)
    public JAXBElement<String> createChequeDetailsTotalChequeLeafNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsTotalChequeLeafNo_QNAME, String.class, ChequeDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ChequeBookNo", scope = ChequeDetails.class)
    public JAXBElement<String> createChequeDetailsChequeBookNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsChequeBookNo_QNAME, String.class, ChequeDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = ChequeDetails.class)
    public JAXBElement<String> createChequeDetailsBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, ChequeDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BOOKDATE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestBOOKDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestBOOKDATE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "VALDATE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestVALDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestVALDATE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TRACKREVERSE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestTRACKREVERSE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTRACKREVERSE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EXPDT", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestEXPDT(String value) {
        return new JAXBElement<String>(_FCUBSRTReverseTransactionRequestEXPDT_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNDATE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestTXNDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNDATE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "REJECT_CODE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestREJECTCODE(String value) {
        return new JAXBElement<String>(_FCUBSRTReverseTransactionRequestREJECTCODE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TIMERECV", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestTIMERECV(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTIMERECV_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AUTHSTAT", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestAUTHSTAT(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestAUTHSTAT_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FCCREF", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestFCCREF(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestFCCREF_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MsgId", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestMsgId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "VDATEDR", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestVDATEDR(String value) {
        return new JAXBElement<String>(_FCUBSRTReverseTransactionRequestVDATEDR_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "VDATECR", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestVDATECR(String value) {
        return new JAXBElement<String>(_FCUBSRTReverseTransactionRequestVDATECR_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHEQUE_ISSUE_DATE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestCHEQUEISSUEDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestCHEQUEISSUEDATE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CALCMETH", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestCALCMETH(String value) {
        return new JAXBElement<String>(_FCUBSRTReverseTransactionRequestCALCMETH_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TXNDRCR", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestTXNDRCR(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestTXNDRCR_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "INSTRDATE", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestINSTRDATE(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestINSTRDATE_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PRD", scope = FCUBSRTReverseTransactionRequest.class)
    public JAXBElement<String> createFCUBSRTReverseTransactionRequestPRD(String value) {
        return new JAXBElement<String>(_AuthorizeWithdrawalRequestPRD_QNAME, String.class, FCUBSRTReverseTransactionRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = CheckAccountExistInfo.class)
    public JAXBElement<String> createCheckAccountExistInfoAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, CheckAccountExistInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNumber", scope = CheckAccountExistInfo.class)
    public JAXBElement<String> createCheckAccountExistInfoAccountNumber(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesAccountNumber_QNAME, String.class, CheckAccountExistInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountName", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxAccountName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestAccountName_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionCode", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxTransactionCode(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxTransactionCode_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ACCORGL", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxACCORGL(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxACCORGL_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SerialNumber", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxSerialNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxSerialNumber_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CurrencyCode", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxCurrencyCode(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCurrencyCode_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LocalCurrencyAmount", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxLocalCurrencyAmount(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxLocalCurrencyAmount_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountNumber", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxAccountNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxAccountNumber_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AdditionalText", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxAdditionalText(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxAdditionalText_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerId", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxCustomerId(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxCustomerId_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "InstrumentNumber", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxInstrumentNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxInstrumentNumber_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Amount", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxAmount(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestAmount_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ExchangeRate", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxExchangeRate(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxExchangeRate_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionType", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxTransactionType(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxTransactionType_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserReferenceNumber", scope = MultipleJournalDetailTrx.class)
    public JAXBElement<String> createMultipleJournalDetailTrxUserReferenceNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxUserReferenceNumber_QNAME, String.class, MultipleJournalDetailTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = SampleAccountResponse.class)
    public JAXBElement<String> createSampleAccountResponseAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, SampleAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNumber", scope = SampleAccountResponse.class)
    public JAXBElement<String> createSampleAccountResponseAccountNumber(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesAccountNumber_QNAME, String.class, SampleAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = SampleAccountResponse.class)
    public JAXBElement<String> createSampleAccountResponseBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, SampleAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "RolloverType", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestRolloverType(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestRolloverType_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS4", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestCompMIS4(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS4_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TDAccountName", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestTDAccountName(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestTDAccountName_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS2", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestCompMIS2(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS2_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OffsetAccountNo", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestOffsetAccountNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestOffsetAccountNo_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AutoRollover", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestAutoRollover(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestAutoRollover_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TDTenor", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestTDTenor(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestTDTenor_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TDInterestRate", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestTDInterestRate(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestTDInterestRate_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OffsetBranchCode", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestOffsetBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestOffsetBranchCode_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PoolCD", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestPoolCD(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestPoolCD_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerNo", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestCustomerNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestCustomerNo_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CheckerID", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestCheckerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCheckerID_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TDCcyCode", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestTDCcyCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestTDCcyCode_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MakerID", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestMakerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestMakerID_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS8", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestCompMIS8(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS8_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TDAmount", scope = FCUBSCreateTDAccountRequest.class)
    public JAXBElement<String> createFCUBSCreateTDAccountRequestTDAmount(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestTDAmount_QNAME, String.class, FCUBSCreateTDAccountRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TotalCredit", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTotalCredit(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTotalCredit_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PeriodCode", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxPeriodCode(String value) {
        return new JAXBElement<String>(_SingleJournalTrxPeriodCode_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionCode", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTransactionCode(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxTransactionCode_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ValueDate", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxValueDate(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxValueDate_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Currency", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCurrency(String value) {
        return new JAXBElement<String>(_SingleJournalTrxCurrency_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CurrencyCode", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCurrencyCode(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCurrencyCode_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LocalCurrencyAmount", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxLocalCurrencyAmount(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxLocalCurrencyAmount_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FinancialCycle", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxFinancialCycle(String value) {
        return new JAXBElement<String>(_SingleJournalTrxFinancialCycle_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TaxationCode", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTaxationCode(String value) {
        return new JAXBElement<String>(_SingleJournalTrxTaxationCode_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMisCode8", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCompMisCode8(String value) {
        return new JAXBElement<String>(_SingleJournalTrxCompMisCode8_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ExchangeRate", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxExchangeRate(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxExchangeRate_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionType", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTransactionType(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxTransactionType_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Description", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxDescription(String value) {
        return new JAXBElement<String>(_SingleJournalTrxDescription_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserReferenceNumber", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxUserReferenceNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxUserReferenceNumber_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BranchCode", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestBranchCode_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CurrencyNumber", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCurrencyNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxCurrencyNumber_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMisCode2", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCompMisCode2(String value) {
        return new JAXBElement<String>(_SingleJournalTrxCompMisCode2_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMisCode1", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCompMisCode1(String value) {
        return new JAXBElement<String>(_SingleJournalTrxCompMisCode1_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMisCode4", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCompMisCode4(String value) {
        return new JAXBElement<String>(_SingleJournalTrxCompMisCode4_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMisCode3", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCompMisCode3(String value) {
        return new JAXBElement<String>(_SingleJournalTrxCompMisCode3_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BatchDescription", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxBatchDescription(String value) {
        return new JAXBElement<String>(_SingleJournalTrxBatchDescription_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MsgId", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxMsgId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMsgId_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionMisCode1", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTransactionMisCode1(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTransactionMisCode1_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountNumber", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxAccountNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxAccountNumber_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "RelativeAccount", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxRelativeAccount(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxRelativeAccount_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerId", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxCustomerId(String value) {
        return new JAXBElement<String>(_MultipleJournalDetailTrxCustomerId_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TotalDebit", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTotalDebit(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTotalDebit_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TransactionMisCode2", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxTransactionMisCode2(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxTransactionMisCode2_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Amount", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxAmount(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestAmount_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BatchNumber", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxBatchNumber(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxBatchNumber_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountDescription", scope = SingleJournalTrx.class)
    public JAXBElement<String> createSingleJournalTrxAccountDescription(String value) {
        return new JAXBElement<String>(_SingleJournalTrxAccountDescription_QNAME, String.class, SingleJournalTrx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNo", scope = AccountCotAndVatLines.class)
    public JAXBElement<String> createAccountCotAndVatLinesAccountNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsAccountNo_QNAME, String.class, AccountCotAndVatLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CotDetails", scope = AccountCotAndVatLines.class)
    public JAXBElement<String> createAccountCotAndVatLinesCotDetails(String value) {
        return new JAXBElement<String>(_AccountCotAndVatLinesCotDetails_QNAME, String.class, AccountCotAndVatLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranDate", scope = AccountCotAndVatLines.class)
    public JAXBElement<XMLGregorianCalendar> createAccountCotAndVatLinesTranDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountCotAndVatLinesTranDate_QNAME, XMLGregorianCalendar.class, AccountCotAndVatLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountStatus", scope = AccountCotAndVatLines.class)
    public JAXBElement<String> createAccountCotAndVatLinesAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountStatus_QNAME, String.class, AccountCotAndVatLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranAmount", scope = AccountCotAndVatLines.class)
    public JAXBElement<BigDecimal> createAccountCotAndVatLinesTranAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryAndTrxLinesTranAmount_QNAME, BigDecimal.class, AccountCotAndVatLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRepaymentScheduleInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RepaymentScheduleInfo", scope = RepaymentScheduleResponse.class)
    public JAXBElement<ArrayOfRepaymentScheduleInfo> createRepaymentScheduleResponseRepaymentScheduleInfo(ArrayOfRepaymentScheduleInfo value) {
        return new JAXBElement<ArrayOfRepaymentScheduleInfo>(_RepaymentScheduleInfo_QNAME, ArrayOfRepaymentScheduleInfo.class, RepaymentScheduleResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CardType", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<String> createVisaCardLimitSummaryLinesCardType(String value) {
        return new JAXBElement<String>(_VisaCardLimitSummaryLinesCardType_QNAME, String.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CardCurrency", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<String> createVisaCardLimitSummaryLinesCardCurrency(String value) {
        return new JAXBElement<String>(_VisaCardLimitSummaryLinesCardCurrency_QNAME, String.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CardStatus", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<String> createVisaCardLimitSummaryLinesCardStatus(String value) {
        return new JAXBElement<String>(_VisaCardLimitSummaryLinesCardStatus_QNAME, String.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "NewCreditLimit", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<BigDecimal> createVisaCardLimitSummaryLinesNewCreditLimit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_VisaCardLimitSummaryLinesNewCreditLimit_QNAME, BigDecimal.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<String> createVisaCardLimitSummaryLinesAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNumber", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<String> createVisaCardLimitSummaryLinesAccountNumber(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesAccountNumber_QNAME, String.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LimitUtilization", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<BigDecimal> createVisaCardLimitSummaryLinesLimitUtilization(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_VisaCardLimitSummaryLinesLimitUtilization_QNAME, BigDecimal.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "SignStat", scope = VisaCardLimitSummaryLines.class)
    public JAXBElement<String> createVisaCardLimitSummaryLinesSignStat(String value) {
        return new JAXBElement<String>(_VisaCardLimitSummaryLinesSignStat_QNAME, String.class, VisaCardLimitSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BUZSECTOR_CustomerMISClass", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestBUZSECTORCustomerMISClass(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestBUZSECTORCustomerMISClass_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UDF_IncomeBandFieldName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUDFIncomeBandFieldName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestUDFIncomeBandFieldName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Guardian", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestGuardian(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestGuardian_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UDF_ProfessionFieldName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUDFProfessionFieldName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestUDFProfessionFieldName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MothersMaidenName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestMothersMaidenName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestMothersMaidenName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "HomePhoneNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestHomePhoneNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestHomePhoneNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UDF_ReligionFieldName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUDFReligionFieldName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestUDFReligionFieldName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "HomeTelISDNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestHomeTelISDNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestHomeTelISDNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TelephoneNumber", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestTelephoneNumber(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestTelephoneNumber_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ShortName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestShortName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestShortName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Gender", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestGender(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestGender_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "NationalID", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestNationalID(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestNationalID_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AcctOfficer_CompositeMISCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestAcctOfficerCompositeMISCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestAcctOfficerCompositeMISCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ProfitCenter_CompositeMISCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestProfitCenterCompositeMISCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestProfitCenterCompositeMISCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmailID", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmailID(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmailID_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "IdentityExpiryDate", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestIdentityExpiryDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestIdentityExpiryDate_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MinorYesorNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestMinorYesorNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestMinorYesorNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PermanentAddressLine4", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestPermanentAddressLine4(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestPermanentAddressLine4_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PermanentAddressLine3", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestPermanentAddressLine3(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestPermanentAddressLine3_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PermanentAddressLine2", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestPermanentAddressLine2(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestPermanentAddressLine2_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DateofBirth", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestDateofBirth(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestDateofBirth_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LastName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestLastName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestLastName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OJCAcct_CompositeMISClass", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestOJCAcctCompositeMISClass(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestOJCAcctCompositeMISClass_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LiabilityCcy", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestLiabilityCcy(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestLiabilityCcy_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AUC_CustomerMISClass", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestAUCCustomerMISClass(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestAUCCustomerMISClass_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Country", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestCountry(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestCountry_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PlaceofBirth", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestPlaceofBirth(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestPlaceofBirth_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UDF_IncomeBandFieldValue", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUDFIncomeBandFieldValue(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestUDFIncomeBandFieldValue_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PermanentAddressLine1", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestPermanentAddressLine1(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestPermanentAddressLine1_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerFullname", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestCustomerFullname(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestCustomerFullname_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerCat", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestCustomerCat(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestCustomerCat_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerType", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestCustomerType(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestCustomerType_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerFax", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerFax(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerFax_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmploymentStatus", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmploymentStatus(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmploymentStatus_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SamePermanentAddress", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestSamePermanentAddress(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestSamePermanentAddress_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "IdentityIssueDate", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestIdentityIssueDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestIdentityIssueDate_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UDF_ProfessionFieldValue", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUDFProfessionFieldValue(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestUDFProfessionFieldValue_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "IdentityNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestIdentityNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestIdentityNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LiabilityName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestLiabilityName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestLiabilityName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerCommMode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestCustomerCommMode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestCustomerCommMode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ProfitCenter_CompositeMISClass", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestProfitCenterCompositeMISClass(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestProfitCenterCompositeMISClass_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Language", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestLanguage(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestLanguage_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OJCAcct_CompositeMISCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestOJCAcctCompositeMISCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestOJCAcctCompositeMISCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "PinCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestPinCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestPinCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ResidentStatus", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestResidentStatus(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestResidentStatus_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AccountHolderMaritalStatus", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestAccountHolderMaritalStatus(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestAccountHolderMaritalStatus_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CheckerID", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestCheckerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCheckerID_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UDF_ReligionFieldValue", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUDFReligionFieldValue(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestUDFReligionFieldValue_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DomiciledPinCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestDomiciledPinCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestDomiciledPinCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SpouseEmploymentStatus", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestSpouseEmploymentStatus(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestSpouseEmploymentStatus_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerDesc", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerDesc(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerDesc_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BUZSECTOR_CustomerMISCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestBUZSECTORCustomerMISCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestBUZSECTORCustomerMISCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LocalBranch", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestLocalBranch(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestLocalBranch_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AcctOfficer_CompositeMISClass", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestAcctOfficerCompositeMISClass(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestAcctOfficerCompositeMISClass_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerEmail", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerEmail(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerEmail_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MobileNumber", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestMobileNumber(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestMobileNumber_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AUC_CustomerMISCode", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestAUCCustomerMISCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestAUCCustomerMISCode_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "TelephoneISDNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestTelephoneISDNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestTelephoneISDNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerAddress3", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerAddress3(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerAddress3_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Nationality", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestNationality(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestNationality_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerAddress1", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerAddress1(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerAddress1_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MiddleName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestMiddleName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestMiddleName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MobileISDNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestMobileISDNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestMobileISDNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BirthCountry", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestBirthCountry(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestBirthCountry_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerAddress2", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerAddress2(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerAddress2_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SpouseName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestSpouseName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestSpouseName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DomiciledAddressLine1", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestDomiciledAddressLine1(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestDomiciledAddressLine1_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Title", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestTitle(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestTitle_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DomiciledAddressLine2", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestDomiciledAddressLine2(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestDomiciledAddressLine2_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "FirstName", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestFirstName(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestFirstName_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MakerID", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestMakerID(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestMakerID_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DomiciledAddressLine3", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestDomiciledAddressLine3(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestDomiciledAddressLine3_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "DomiciledAddressLine4", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestDomiciledAddressLine4(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestDomiciledAddressLine4_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EmployerTelNo", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestEmployerTelNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestEmployerTelNo_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "LiabilityBranch", scope = FCUBSCreateCustomerRequest.class)
    public JAXBElement<String> createFCUBSCreateCustomerRequestLiabilityBranch(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustomerRequestLiabilityBranch_QNAME, String.class, FCUBSCreateCustomerRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionNameDesc", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionNameDesc(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionNameDesc_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TranRefNo", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTranRefNo(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTranRefNo_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "datePost", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsDatePost(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsDatePost_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "InstrumentCode", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsInstrumentCode(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesInstrumentCode_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MessageTypeCode", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsMessageTypeCode(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsMessageTypeCode_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "controlBatchNumber", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsControlBatchNumber(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsControlBatchNumber_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionType", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionType(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionType_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "RowNumber", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsRowNumber(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesRowNumber_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionOfficerInputter", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionOfficerInputter(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionOfficerInputter_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "tranAmount", scope = SingleTransactionDetails.class)
    public JAXBElement<BigDecimal> createSingleTransactionDetailsTranAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_SingleTransactionDetailsTranAmount_QNAME, BigDecimal.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionDateTime", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionDateTime(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionDateTime_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionOfficerAuthorizer", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionOfficerAuthorizer(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionOfficerAuthorizer_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ccyCode", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsCcyCode(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsCcyCode_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ccyName", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsCcyName(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsCcyName_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountNumber", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsAccountNumber(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsAccountNumber_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionName", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionName(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionName_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "branchName", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsBranchName(String value) {
        return new JAXBElement<String>(_CustomerDetailsBranchName_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionCode", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionCode(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionCode_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "valueDate", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsValueDate(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsValueDate_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionDescription", scope = SingleTransactionDetails.class)
    public JAXBElement<String> createSingleTransactionDetailsTransactionDescription(String value) {
        return new JAXBElement<String>(_SingleTransactionDetailsTransactionDescription_QNAME, String.class, SingleTransactionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "AMT_ON_CHEQUE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestAMTONCHEQUE(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestAMTONCHEQUE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ACCOUNT_NUMBER", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestACCOUNTNUMBER(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestACCOUNTNUMBER_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHECKER_ID", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestCHECKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestCHECKERID_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CCY_CODE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestCCYCODE(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestCCYCODE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "SOURCE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestSOURCE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestSOURCE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "USERID", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestUSERID(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestUSERID_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "USER_BRANCH_CODE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestUSERBRANCHCODE(String value) {
        return new JAXBElement<String>(_FCUBSFTCreateTransactionRequestUSERBRANCHCODE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "STOP_REMARK", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestSTOPREMARK(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestSTOPREMARK_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ACCOUNT_BRANCHCODE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestACCOUNTBRANCHCODE(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestACCOUNTBRANCHCODE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EXPIRY_DATE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestEXPIRYDATE(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestEXPIRYDATE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CHEQUE_NO", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestCHEQUENO(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestCHEQUENO_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MAKER_ID", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestMAKERID(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestMAKERID_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "EFFECTIVE_DATE", scope = FCUBSCreateStopPaymentRequest.class)
    public JAXBElement<String> createFCUBSCreateStopPaymentRequestEFFECTIVEDATE(String value) {
        return new JAXBElement<String>(_FCUBSCreateStopPaymentRequestEFFECTIVEDATE_QNAME, String.class, FCUBSCreateStopPaymentRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CcyCode", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestCcyCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateLDContractRequestCcyCode_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "InterestRate", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestInterestRate(String value) {
        return new JAXBElement<String>(_FCUBSCreateLDContractRequestInterestRate_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS4", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestCompMIS4(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS4_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS2", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestCompMIS2(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS2_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "BookDate", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestBookDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateLDContractRequestBookDate_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OffsetAccountNo", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestOffsetAccountNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestOffsetAccountNo_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UniqueReferenceNo", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestUniqueReferenceNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateLDContractRequestUniqueReferenceNo_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "ValueDate", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestValueDate(String value) {
        return new JAXBElement<String>(_MultipleJournalMasterTrxValueDate_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserId", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestUserId(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserId_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "OffsetBranchCode", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestOffsetBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCreateTDAccountRequestOffsetBranchCode_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Source", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestSource(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestSource_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CustomerNo", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestCustomerNo(String value) {
        return new JAXBElement<String>(_FCUBSCreateCustAccountRequestCustomerNo_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "MaturityDate", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestMaturityDate(String value) {
        return new JAXBElement<String>(_FCUBSCreateLDContractRequestMaturityDate_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "Amount", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestAmount(String value) {
        return new JAXBElement<String>(_FCUBSCreateMMContractRequestAmount_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "UserBranchCode", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestUserBranchCode(String value) {
        return new JAXBElement<String>(_FCUBSCustomerServiceAmtBlkRequestUserBranchCode_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", name = "CompMIS8", scope = FCUBSCreateLDContractRequest.class)
    public JAXBElement<String> createFCUBSCreateLDContractRequestCompMIS8(String value) {
        return new JAXBElement<String>(_FCUBSCreateSIContractRequestCompMIS8_QNAME, String.class, FCUBSCreateLDContractRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountname", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsAccountname(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsAccountname_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountno", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsAccountno(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsAccountno_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "eventserialno", scope = TransactionRefNoDetails.class)
    public JAXBElement<BigDecimal> createTransactionRefNoDetailsEventserialno(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionRefNoDetailsEventserialno_QNAME, BigDecimal.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "valuedate", scope = TransactionRefNoDetails.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionRefNoDetailsValuedate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionRefNoDetailsValuedate_QNAME, XMLGregorianCalendar.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "customerID", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsCustomerID(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsCustomerID_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactioncode", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsTransactioncode(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsTransactioncode_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountbranch", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsAccountbranch(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsAccountbranch_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountopendate", scope = TransactionRefNoDetails.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionRefNoDetailsAccountopendate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionRefNoDetailsAccountopendate_QNAME, XMLGregorianCalendar.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "drcr_ind", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsDrcrInd(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsDrcrInd_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactiondate", scope = TransactionRefNoDetails.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionRefNoDetailsTransactiondate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionRefNoDetailsTransactiondate_QNAME, XMLGregorianCalendar.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "batchno", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsBatchno(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsBatchno_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "eventdesc", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsEventdesc(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsEventdesc_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "currencycode", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsCurrencycode(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsCurrencycode_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "traninitiationdate", scope = TransactionRefNoDetails.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionRefNoDetailsTraninitiationdate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionRefNoDetailsTraninitiationdate_QNAME, XMLGregorianCalendar.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "accountclass", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsAccountclass(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsAccountclass_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionamount", scope = TransactionRefNoDetails.class)
    public JAXBElement<BigDecimal> createTransactionRefNoDetailsTransactionamount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionRefNoDetailsTransactionamount_QNAME, BigDecimal.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "transactionrefno", scope = TransactionRefNoDetails.class)
    public JAXBElement<String> createTransactionRefNoDetailsTransactionrefno(String value) {
        return new JAXBElement<String>(_TransactionRefNoDetailsTransactionrefno_QNAME, String.class, TransactionRefNoDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "instrument_no", scope = GetChequeValueDateResp.class)
    public JAXBElement<String> createGetChequeValueDateRespInstrumentNo(String value) {
        return new JAXBElement<String>(_GetChequeValueDateRespInstrumentNo_QNAME, String.class, GetChequeValueDateResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "val_date_cust", scope = GetChequeValueDateResp.class)
    public JAXBElement<XMLGregorianCalendar> createGetChequeValueDateRespValDateCust(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetChequeValueDateRespValDateCust_QNAME, XMLGregorianCalendar.class, GetChequeValueDateResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNo", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesAccountNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsAccountNo_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCode", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesProductCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesProductCode_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountClass", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesAccountClass(String value) {
        return new JAXBElement<String>(_AccountSearchResponseLinesAccountClass_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCode", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesCurrencyCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCurrencyCode_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Email", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesEmail(String value) {
        return new JAXBElement<String>(_AccountSearchResponseLinesEmail_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCodeDesc", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesProductCodeDesc(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesProductCodeDesc_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCodeDesc", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesCurrencyCodeDesc(String value) {
        return new JAXBElement<String>(_AccountSearchResponseLinesCurrencyCodeDesc_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerNo", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesCustomerNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCustomerNo_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "DateAcctOpened", scope = AccountSearchResponseLines.class)
    public JAXBElement<XMLGregorianCalendar> createAccountSearchResponseLinesDateAcctOpened(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountSearchResponseLinesDateAcctOpened_QNAME, XMLGregorianCalendar.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TelephoneNo", scope = AccountSearchResponseLines.class)
    public JAXBElement<String> createAccountSearchResponseLinesTelephoneNo(String value) {
        return new JAXBElement<String>(_AccountSearchResponseLinesTelephoneNo_QNAME, String.class, AccountSearchResponseLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "role_id", scope = UserLimitResponse.class)
    public JAXBElement<String> createUserLimitResponseRoleId(String value) {
        return new JAXBElement<String>(_UserLimitResponseRoleId_QNAME, String.class, UserLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "user_id", scope = UserLimitResponse.class)
    public JAXBElement<String> createUserLimitResponseUserId(String value) {
        return new JAXBElement<String>(_UserLimitResponseUserId_QNAME, String.class, UserLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "auth_limit", scope = UserLimitResponse.class)
    public JAXBElement<BigDecimal> createUserLimitResponseAuthLimit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_UserLimitResponseAuthLimit_QNAME, BigDecimal.class, UserLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "input_limit", scope = UserLimitResponse.class)
    public JAXBElement<BigDecimal> createUserLimitResponseInputLimit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_UserLimitResponseInputLimit_QNAME, BigDecimal.class, UserLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "EffectiveBalance", scope = CustomerInformation.class)
    public JAXBElement<BigDecimal> createCustomerInformationEffectiveBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerInformationEffectiveBalance_QNAME, BigDecimal.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerAddress", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationCustomerAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsCustomerAddress_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PhoneNumber", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationPhoneNumber(String value) {
        return new JAXBElement<String>(_CustomerInformationPhoneNumber_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TIN", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationTIN(String value) {
        return new JAXBElement<String>(_CustomerInformationTIN_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BVN", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationBVN(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailBVN_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LastName", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationLastName(String value) {
        return new JAXBElement<String>(_CustomerInformationLastName_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNo", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationAccountNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsAccountNo_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCode", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationProductCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesProductCode_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProfitCenter", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationProfitCenter(String value) {
        return new JAXBElement<String>(_CustomerDetailsProfitCenter_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "FirstName", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationFirstName(String value) {
        return new JAXBElement<String>(_CustomerInformationFirstName_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountBranch", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationAccountBranch(String value) {
        return new JAXBElement<String>(_CustomerInformationAccountBranch_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MiddleName", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationMiddleName(String value) {
        return new JAXBElement<String>(_CustomerInformationMiddleName_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountClassDesc", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationAccountClassDesc(String value) {
        return new JAXBElement<String>(_CustomerInformationAccountClassDesc_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountStatus", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountStatus_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCode", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationCurrencyCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCurrencyCode_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AvailableBalance", scope = CustomerInformation.class)
    public JAXBElement<BigDecimal> createCustomerInformationAvailableBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryAndTrxLinesAvailableBalance_QNAME, BigDecimal.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountOfficerCode", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationAccountOfficerCode(String value) {
        return new JAXBElement<String>(_CustomerInformationAccountOfficerCode_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Division", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationDivision(String value) {
        return new JAXBElement<String>(_CustomerInformationDivision_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TypeofClient", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationTypeofClient(String value) {
        return new JAXBElement<String>(_CustomerInformationTypeofClient_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Email", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationEmail(String value) {
        return new JAXBElement<String>(_AccountSearchResponseLinesEmail_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchState", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationBranchState(String value) {
        return new JAXBElement<String>(_CustomerInformationBranchState_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyDesc", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationCurrencyDesc(String value) {
        return new JAXBElement<String>(_CustomerInformationCurrencyDesc_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerNo", scope = CustomerInformation.class)
    public JAXBElement<String> createCustomerInformationCustomerNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCustomerNo_QNAME, String.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BookBalance", scope = CustomerInformation.class)
    public JAXBElement<BigDecimal> createCustomerInformationBookBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CustomerInformationBookBalance_QNAME, BigDecimal.class, CustomerInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCheckAccountExistInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountInfo", scope = GetAccountExistResponse.class)
    public JAXBElement<ArrayOfCheckAccountExistInfo> createGetAccountExistResponseAccountInfo(ArrayOfCheckAccountExistInfo value) {
        return new JAXBElement<ArrayOfCheckAccountExistInfo>(_GetAccountExistResponseAccountInfo_QNAME, ArrayOfCheckAccountExistInfo.class, GetAccountExistResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AMTBLKNO", scope = CreateAmtBlockResponse.class)
    public JAXBElement<String> createCreateAmtBlockResponseAMTBLKNO(String value) {
        return new JAXBElement<String>(_CreateAmtBlockResponseAMTBLKNO_QNAME, String.class, CreateAmtBlockResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_acct_no_old", scope = NubanAcctsDetails.class)
    public JAXBElement<String> createNubanAcctsDetailsCodAcctNoOld(String value) {
        return new JAXBElement<String>(_NubanAcctsDetailsCodAcctNoOld_QNAME, String.class, NubanAcctsDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_acct_no_new", scope = NubanAcctsDetails.class)
    public JAXBElement<String> createNubanAcctsDetailsCodAcctNoNew(String value) {
        return new JAXBElement<String>(_NubanAcctsDetailsCodAcctNoNew_QNAME, String.class, NubanAcctsDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cif_sig_name", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCifSigName(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsCifSigName_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "txt_acct_oper_instrs1", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsTxtAcctOperInstrs1(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsTxtAcctOperInstrs1_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerImage_Bytes", scope = ImageCollectionDetails.class)
    public JAXBElement<byte[]> createImageCollectionDetailsCustomerImageBytes(byte[] value) {
        return new JAXBElement<byte[]>(_ImageCollectionDetailsCustomerImageBytes_QNAME, byte[].class, ImageCollectionDetails.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cust_imageURL1_Bytes", scope = ImageCollectionDetails.class)
    public JAXBElement<byte[]> createImageCollectionDetailsCustImageURL1Bytes(byte[] value) {
        return new JAXBElement<byte[]>(_ImageCollectionDetailsCustImageURL1Bytes_QNAME, byte[].class, ImageCollectionDetails.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "nam_cust_shrt", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsNamCustShrt(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsNamCustShrt_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerImage", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCustomerImage(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsCustomerImage_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "nam_cust_shrt_X", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsNamCustShrtX(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsNamCustShrtX_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cust_ac_no", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCustAcNo(String value) {
        return new JAXBElement<String>(_ChequeNumberDetailsCustAcNo_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_cust", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCodCust(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsCodCust_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cust_imageURL2", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCustImageURL2(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsCustImageURL2_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cust_imageURL1", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCustImageURL1(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsCustImageURL1_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cod_cust_id", scope = ImageCollectionDetails.class)
    public JAXBElement<String> createImageCollectionDetailsCodCustId(String value) {
        return new JAXBElement<String>(_ImageCollectionDetailsCodCustId_QNAME, String.class, ImageCollectionDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "cust_imageURL2_Bytes", scope = ImageCollectionDetails.class)
    public JAXBElement<byte[]> createImageCollectionDetailsCustImageURL2Bytes(byte[] value) {
        return new JAXBElement<byte[]>(_ImageCollectionDetailsCustImageURL2Bytes_QNAME, byte[].class, ImageCollectionDetails.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "acy_avl_bal", scope = GetEffectiveBalanceResp.class)
    public JAXBElement<BigDecimal> createGetEffectiveBalanceRespAcyAvlBal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetEffectiveBalanceRespAcyAvlBal_QNAME, BigDecimal.class, GetEffectiveBalanceResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OverdueInterest", scope = LoanSummaryLine.class)
    public JAXBElement<BigDecimal> createLoanSummaryLineOverdueInterest(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanSummaryLineOverdueInterest_QNAME, BigDecimal.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCode", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineCurrencyCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCurrencyCode_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TotalPrincipalOutstanding", scope = LoanSummaryLine.class)
    public JAXBElement<BigDecimal> createLoanSummaryLineTotalPrincipalOutstanding(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesTotalPrincipalOutstanding_QNAME, BigDecimal.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PrincipalDisbursed", scope = LoanSummaryLine.class)
    public JAXBElement<BigDecimal> createLoanSummaryLinePrincipalDisbursed(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanSummaryLinePrincipalDisbursed_QNAME, BigDecimal.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ValueDate", scope = LoanSummaryLine.class)
    public JAXBElement<XMLGregorianCalendar> createLoanSummaryLineValueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoValueDate_QNAME, XMLGregorianCalendar.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BookDate", scope = LoanSummaryLine.class)
    public JAXBElement<XMLGregorianCalendar> createLoanSummaryLineBookDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoBookDate_QNAME, XMLGregorianCalendar.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCategory", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineProductCategory(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductCategory_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductDescription", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineProductDescription(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductDescription_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OverduePrincipal", scope = LoanSummaryLine.class)
    public JAXBElement<BigDecimal> createLoanSummaryLineOverduePrincipal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanSummaryLineOverduePrincipal_QNAME, BigDecimal.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanTenor", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineLoanTenor(String value) {
        return new JAXBElement<String>(_LoanSummaryLineLoanTenor_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountDisbursed", scope = LoanSummaryLine.class)
    public JAXBElement<BigDecimal> createLoanSummaryLineAmountDisbursed(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RepaymentScheduleInfoAmountDisbursed_QNAME, BigDecimal.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerNo", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineCustomerNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCustomerNo_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNumber", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineAccountNumber(String value) {
        return new JAXBElement<String>(_CustomerODLimitLinesAccountNumber_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "OriginalStartDate", scope = LoanSummaryLine.class)
    public JAXBElement<XMLGregorianCalendar> createLoanSummaryLineOriginalStartDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoOriginalStartDate_QNAME, XMLGregorianCalendar.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanStatus", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineLoanStatus(String value) {
        return new JAXBElement<String>(_LoanSummaryLineLoanStatus_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ApplicantName", scope = LoanSummaryLine.class)
    public JAXBElement<String> createLoanSummaryLineApplicantName(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoApplicantName_QNAME, String.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PrincipalNotDue", scope = LoanSummaryLine.class)
    public JAXBElement<BigDecimal> createLoanSummaryLinePrincipalNotDue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_LoanInfoSummaryLinesPrincipalNotDue_QNAME, BigDecimal.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "MaturityDate", scope = LoanSummaryLine.class)
    public JAXBElement<XMLGregorianCalendar> createLoanSummaryLineMaturityDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RepaymentScheduleInfoMaturityDate_QNAME, XMLGregorianCalendar.class, LoanSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "user_id", scope = TellerLimitResponse.class)
    public JAXBElement<String> createTellerLimitResponseUserId(String value) {
        return new JAXBElement<String>(_UserLimitResponseUserId_QNAME, String.class, TellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "max_txn_amt", scope = TellerLimitResponse.class)
    public JAXBElement<BigDecimal> createTellerLimitResponseMaxTxnAmt(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TellerLimitResponseMaxTxnAmt_QNAME, BigDecimal.class, TellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "limits_ccy", scope = TellerLimitResponse.class)
    public JAXBElement<String> createTellerLimitResponseLimitsCcy(String value) {
        return new JAXBElement<String>(_TellerLimitResponseLimitsCcy_QNAME, String.class, TellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "max_override_amt", scope = TellerLimitResponse.class)
    public JAXBElement<BigDecimal> createTellerLimitResponseMaxOverrideAmt(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TellerLimitResponseMaxOverrideAmt_QNAME, BigDecimal.class, TellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "max_auth_amt", scope = TellerLimitResponse.class)
    public JAXBElement<BigDecimal> createTellerLimitResponseMaxAuthAmt(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TellerLimitResponseMaxAuthAmt_QNAME, BigDecimal.class, TellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "home_branch", scope = TellerLimitResponse.class)
    public JAXBElement<String> createTellerLimitResponseHomeBranch(String value) {
        return new JAXBElement<String>(_GetUserBranchRespHomeBranch_QNAME, String.class, TellerLimitResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfAccountSearchResponseLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountSearchResult", scope = AccountSearchResponse.class)
    public JAXBElement<ArrayOfAccountSearchResponseLines> createAccountSearchResponseAccountSearchResult(ArrayOfAccountSearchResponseLines value) {
        return new JAXBElement<ArrayOfAccountSearchResponseLines>(_AccountSearchResponseAccountSearchResult_QNAME, ArrayOfAccountSearchResponseLines.class, AccountSearchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustID", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineCustID(String value) {
        return new JAXBElement<String>(_AccountSummaryLineCustID_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Lodgement", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineLodgement(String value) {
        return new JAXBElement<String>(_AccountSummaryLineLodgement_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "NetBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineNetBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineNetBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ClosingBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineClosingBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryAndTrxLinesClosingBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "TotalBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineTotalBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineTotalBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCity", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineBranchCity(String value) {
        return new JAXBElement<String>(_AccountSummaryLineBranchCity_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BVN", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineBVN(String value) {
        return new JAXBElement<String>(_CustomerAcctsDetailBVN_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNo", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineAccountNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsAccountNo_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCode", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineProductCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesProductCode_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Withdrawal", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineWithdrawal(String value) {
        return new JAXBElement<String>(_AccountSummaryLineWithdrawal_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AcctOpeningBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineAcctOpeningBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryAndTrxLinesAcctOpeningBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "UnclearedBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineUnclearedBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineUnclearedBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyName", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineCurrencyName(String value) {
        return new JAXBElement<String>(_AccountSummaryLineCurrencyName_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AcctStatusDesc", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineAcctStatusDesc(String value) {
        return new JAXBElement<String>(_AccountSummaryLineAcctStatusDesc_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AmountBlocked", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineAmountBlocked(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineAmountBlocked_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountStatus", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineAccountStatus(String value) {
        return new JAXBElement<String>(_CustomerDetailsAccountStatus_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCode", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineCurrencyCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCurrencyCode_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AvailableBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineAvailableBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryAndTrxLinesAvailableBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AvailableToWithdraw", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineAvailableToWithdraw(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineAvailableToWithdraw_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchName", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineBranchName(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesBranchName_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "IsStaff", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineIsStaff(String value) {
        return new JAXBElement<String>(_CustomerDetailsIsStaff_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "amt_od_limit", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineAmtOdLimit(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineAmtOdLimit_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountOpenDate", scope = AccountSummaryLine.class)
    public JAXBElement<XMLGregorianCalendar> createAccountSummaryLineAccountOpenDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AccountSummaryLineAccountOpenDate_QNAME, XMLGregorianCalendar.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "HasCOT", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineHasCOT(String value) {
        return new JAXBElement<String>(_AccountSummaryLineHasCOT_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCodeDesc", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineProductCodeDesc(String value) {
        return new JAXBElement<String>(_AccountSummaryAndTrxLinesProductCodeDesc_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchState", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineBranchState(String value) {
        return new JAXBElement<String>(_CustomerInformationBranchState_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchAddress", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineBranchAddress(String value) {
        return new JAXBElement<String>(_CustomerDetailsBranchAddress_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanStatus", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineLoanStatus(String value) {
        return new JAXBElement<String>(_LoanSummaryLineLoanStatus_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ClearedBalance", scope = AccountSummaryLine.class)
    public JAXBElement<BigDecimal> createAccountSummaryLineClearedBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineClearedBalance_QNAME, BigDecimal.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "hasImage", scope = AccountSummaryLine.class)
    public JAXBElement<String> createAccountSummaryLineHasImage(String value) {
        return new JAXBElement<String>(_AccountSummaryLineHasImage_QNAME, String.class, AccountSummaryLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountNo", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesAccountNo(String value) {
        return new JAXBElement<String>(_ChequeDetailsAccountNo_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "PhoneNo", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesPhoneNo(String value) {
        return new JAXBElement<String>(_RelatedAccountSummaryLinesPhoneNo_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductCode", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesProductCode(String value) {
        return new JAXBElement<String>(_LoanInfoSummaryLinesProductCode_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "NetBalance", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<BigDecimal> createRelatedAccountSummaryLinesNetBalance(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_AccountSummaryLineNetBalance_QNAME, BigDecimal.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CurrencyCode", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesCurrencyCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCurrencyCode_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "Email", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesEmail(String value) {
        return new JAXBElement<String>(_AccountSearchResponseLinesEmail_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CustomerNo", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesCustomerNo(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoCustomerNo_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "AccountName", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesAccountName(String value) {
        return new JAXBElement<String>(_CheckAccountExistInfoAccountName_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "BranchCode", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesBranchCode(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoBranchCode_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "ProductDescription", scope = RelatedAccountSummaryLines.class)
    public JAXBElement<String> createRelatedAccountSummaryLinesProductDescription(String value) {
        return new JAXBElement<String>(_RepaymentScheduleInfoProductDescription_QNAME, String.class, RelatedAccountSummaryLines.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "CUST_AC_NO", scope = CreateCustAccountResponse.class)
    public JAXBElement<String> createCreateCustAccountResponseCUSTACNO(String value) {
        return new JAXBElement<String>(_CreateTDAccountResponseCUSTACNO_QNAME, String.class, CreateCustAccountResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "mid_rate", scope = CurrencyRateResponse.class)
    public JAXBElement<BigDecimal> createCurrencyRateResponseMidRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CurrencyRateResponseMidRate_QNAME, BigDecimal.class, CurrencyRateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "sale_rate", scope = CurrencyRateResponse.class)
    public JAXBElement<BigDecimal> createCurrencyRateResponseSaleRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CurrencyRateResponseSaleRate_QNAME, BigDecimal.class, CurrencyRateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "buy_rate", scope = CurrencyRateResponse.class)
    public JAXBElement<BigDecimal> createCurrencyRateResponseBuyRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_CurrencyRateResponseBuyRate_QNAME, BigDecimal.class, CurrencyRateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLoanInfoSummaryLines }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", name = "LoanInfoSummary", scope = LoanInfoSummaryResponse.class)
    public JAXBElement<ArrayOfLoanInfoSummaryLines> createLoanInfoSummaryResponseLoanInfoSummary(ArrayOfLoanInfoSummaryLines value) {
        return new JAXBElement<ArrayOfLoanInfoSummaryLines>(_LoanInfoSummaryResponseLoanInfoSummary_QNAME, ArrayOfLoanInfoSummaryLines.class, LoanInfoSummaryResponse.class, value);
    }

}
