
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfLoanSummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfLoanSummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanSummaryResponse" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}LoanSummaryResponse" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfLoanSummaryResponse", propOrder = {
    "loanSummaryResponse"
})
public class ArrayOfLoanSummaryResponse {

    @XmlElement(name = "LoanSummaryResponse", nillable = true)
    protected List<LoanSummaryResponse> loanSummaryResponse;

    /**
     * Gets the value of the loanSummaryResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loanSummaryResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoanSummaryResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LoanSummaryResponse }
     * 
     * 
     */
    public List<LoanSummaryResponse> getLoanSummaryResponse() {
        if (loanSummaryResponse == null) {
            loanSummaryResponse = new ArrayList<LoanSummaryResponse>();
        }
        return this.loanSummaryResponse;
    }

}
