
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountRelatedResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountRelatedResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RelatedAccountSummary" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfRelatedAccountSummaryLines" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountRelatedResponse", propOrder = {
    "relatedAccountSummary"
})
public class AccountRelatedResponse {

    @XmlElementRef(name = "RelatedAccountSummary", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfRelatedAccountSummaryLines> relatedAccountSummary;

    /**
     * Gets the value of the relatedAccountSummary property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRelatedAccountSummaryLines }{@code >}
     *     
     */
    public JAXBElement<ArrayOfRelatedAccountSummaryLines> getRelatedAccountSummary() {
        return relatedAccountSummary;
    }

    /**
     * Sets the value of the relatedAccountSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRelatedAccountSummaryLines }{@code >}
     *     
     */
    public void setRelatedAccountSummary(JAXBElement<ArrayOfRelatedAccountSummaryLines> value) {
        this.relatedAccountSummary = value;
    }

}
