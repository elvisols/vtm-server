
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RepaymentScheduleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RepaymentScheduleResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FcubsResponse">
 *       &lt;sequence>
 *         &lt;element name="RepaymentScheduleInfo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfRepaymentScheduleInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RepaymentScheduleResponse", propOrder = {
    "repaymentScheduleInfo"
})
public class RepaymentScheduleResponse
    extends FcubsResponse
{

    @XmlElementRef(name = "RepaymentScheduleInfo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfRepaymentScheduleInfo> repaymentScheduleInfo;

    /**
     * Gets the value of the repaymentScheduleInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRepaymentScheduleInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfRepaymentScheduleInfo> getRepaymentScheduleInfo() {
        return repaymentScheduleInfo;
    }

    /**
     * Sets the value of the repaymentScheduleInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRepaymentScheduleInfo }{@code >}
     *     
     */
    public void setRepaymentScheduleInfo(JAXBElement<ArrayOfRepaymentScheduleInfo> value) {
        this.repaymentScheduleInfo = value;
    }

}
