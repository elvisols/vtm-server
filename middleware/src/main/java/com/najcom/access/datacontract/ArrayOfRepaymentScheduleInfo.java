
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRepaymentScheduleInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRepaymentScheduleInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RepaymentScheduleInfo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}RepaymentScheduleInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRepaymentScheduleInfo", propOrder = {
    "repaymentScheduleInfo"
})
public class ArrayOfRepaymentScheduleInfo {

    @XmlElement(name = "RepaymentScheduleInfo", nillable = true)
    protected List<RepaymentScheduleInfo> repaymentScheduleInfo;

    /**
     * Gets the value of the repaymentScheduleInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the repaymentScheduleInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRepaymentScheduleInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RepaymentScheduleInfo }
     * 
     * 
     */
    public List<RepaymentScheduleInfo> getRepaymentScheduleInfo() {
        if (repaymentScheduleInfo == null) {
            repaymentScheduleInfo = new ArrayList<RepaymentScheduleInfo>();
        }
        return this.repaymentScheduleInfo;
    }

}
