
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAccountSearchResponseLines complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAccountSearchResponseLines">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountSearchResponseLines" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountSearchResponseLines" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAccountSearchResponseLines", propOrder = {
    "accountSearchResponseLines"
})
public class ArrayOfAccountSearchResponseLines {

    @XmlElement(name = "AccountSearchResponseLines", nillable = true)
    protected List<AccountSearchResponseLines> accountSearchResponseLines;

    /**
     * Gets the value of the accountSearchResponseLines property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountSearchResponseLines property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountSearchResponseLines().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountSearchResponseLines }
     * 
     * 
     */
    public List<AccountSearchResponseLines> getAccountSearchResponseLines() {
        if (accountSearchResponseLines == null) {
            accountSearchResponseLines = new ArrayList<AccountSearchResponseLines>();
        }
        return this.accountSearchResponseLines;
    }

}
