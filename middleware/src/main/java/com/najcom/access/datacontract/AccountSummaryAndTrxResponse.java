
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountSummaryAndTrxResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountSummaryAndTrxResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountSummaryAndTransactions" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfAccountSummaryAndTrxLines" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountSummaryAndTrxResponse", propOrder = {
    "accountSummaryAndTransactions"
})
public class AccountSummaryAndTrxResponse {

    @XmlElementRef(name = "AccountSummaryAndTransactions", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAccountSummaryAndTrxLines> accountSummaryAndTransactions;

    /**
     * Gets the value of the accountSummaryAndTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxLines }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAccountSummaryAndTrxLines> getAccountSummaryAndTransactions() {
        return accountSummaryAndTransactions;
    }

    /**
     * Sets the value of the accountSummaryAndTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxLines }{@code >}
     *     
     */
    public void setAccountSummaryAndTransactions(JAXBElement<ArrayOfAccountSummaryAndTrxLines> value) {
        this.accountSummaryAndTransactions = value;
    }

}
