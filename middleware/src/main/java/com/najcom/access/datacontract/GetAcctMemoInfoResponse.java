
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetAcctMemoInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAcctMemoInfoResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FcubsResponse">
 *       &lt;sequence>
 *         &lt;element name="accountMemo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfAccountMemoInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAcctMemoInfoResponse", propOrder = {
    "accountMemo"
})
public class GetAcctMemoInfoResponse
    extends FcubsResponse
{

    @XmlElementRef(name = "accountMemo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAccountMemoInfo> accountMemo;

    /**
     * Gets the value of the accountMemo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountMemoInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAccountMemoInfo> getAccountMemo() {
        return accountMemo;
    }

    /**
     * Sets the value of the accountMemo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountMemoInfo }{@code >}
     *     
     */
    public void setAccountMemo(JAXBElement<ArrayOfAccountMemoInfo> value) {
        this.accountMemo = value;
    }

}
