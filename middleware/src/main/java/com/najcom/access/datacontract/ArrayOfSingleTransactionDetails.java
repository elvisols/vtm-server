
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSingleTransactionDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSingleTransactionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SingleTransactionDetails" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}SingleTransactionDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSingleTransactionDetails", propOrder = {
    "singleTransactionDetails"
})
public class ArrayOfSingleTransactionDetails {

    @XmlElement(name = "SingleTransactionDetails", nillable = true)
    protected List<SingleTransactionDetails> singleTransactionDetails;

    /**
     * Gets the value of the singleTransactionDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the singleTransactionDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSingleTransactionDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SingleTransactionDetails }
     * 
     * 
     */
    public List<SingleTransactionDetails> getSingleTransactionDetails() {
        if (singleTransactionDetails == null) {
            singleTransactionDetails = new ArrayList<SingleTransactionDetails>();
        }
        return this.singleTransactionDetails;
    }

}
