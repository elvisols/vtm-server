
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanSummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanSummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanSummaryLines" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfLoanSummaryLine" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanSummaryResponse", propOrder = {
    "loanSummaryLines"
})
public class LoanSummaryResponse {

    @XmlElementRef(name = "LoanSummaryLines", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLoanSummaryLine> loanSummaryLines;

    /**
     * Gets the value of the loanSummaryLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLoanSummaryLine> getLoanSummaryLines() {
        return loanSummaryLines;
    }

    /**
     * Sets the value of the loanSummaryLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLoanSummaryLine }{@code >}
     *     
     */
    public void setLoanSummaryLines(JAXBElement<ArrayOfLoanSummaryLine> value) {
        this.loanSummaryLines = value;
    }

}
