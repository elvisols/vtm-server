
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerAcctsDetailResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerAcctsDetailResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FcubsResponse">
 *       &lt;sequence>
 *         &lt;element name="CustomerAcctsDetail" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfCustomerAcctsDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerAcctsDetailResponse", propOrder = {
    "customerAcctsDetail"
})
public class CustomerAcctsDetailResponse
    extends FcubsResponse
{

    @XmlElementRef(name = "CustomerAcctsDetail", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomerAcctsDetail> customerAcctsDetail;

    /**
     * Gets the value of the customerAcctsDetail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetail }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomerAcctsDetail> getCustomerAcctsDetail() {
        return customerAcctsDetail;
    }

    /**
     * Sets the value of the customerAcctsDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerAcctsDetail }{@code >}
     *     
     */
    public void setCustomerAcctsDetail(JAXBElement<ArrayOfCustomerAcctsDetail> value) {
        this.customerAcctsDetail = value;
    }

}
