
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VisaCardLimitResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VisaCardLimitResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VisaCardLimitSummary" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfVisaCardLimitSummaryLines" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VisaCardLimitResponse", propOrder = {
    "visaCardLimitSummary"
})
public class VisaCardLimitResponse {

    @XmlElementRef(name = "VisaCardLimitSummary", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfVisaCardLimitSummaryLines> visaCardLimitSummary;

    /**
     * Gets the value of the visaCardLimitSummary property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVisaCardLimitSummaryLines }{@code >}
     *     
     */
    public JAXBElement<ArrayOfVisaCardLimitSummaryLines> getVisaCardLimitSummary() {
        return visaCardLimitSummary;
    }

    /**
     * Sets the value of the visaCardLimitSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfVisaCardLimitSummaryLines }{@code >}
     *     
     */
    public void setVisaCardLimitSummary(JAXBElement<ArrayOfVisaCardLimitSummaryLines> value) {
        this.visaCardLimitSummary = value;
    }

}
