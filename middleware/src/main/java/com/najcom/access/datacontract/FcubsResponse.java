
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.webservicewrappers.AccountVerificationResponse;
import com.najcom.access.datacontract.webservicewrappers.ChequeNoInquiryResponse;
import com.najcom.access.datacontract.webservicewrappers.MessageIDStatusResponse;
import com.najcom.access.datacontract.webservicewrappers.MobileAccountDetail;
import com.najcom.access.datacontract.webservicewrappers.MobileTransactionDetail;
import com.najcom.access.datacontract.webservicewrappers.TransactionRefNoDetailsResponse;
import com.najcom.access.datacontract.webservicewrappers.VansoCustomerIdResponse;


/**
 * <p>Java class for FcubsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FcubsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FcubsResponse", propOrder = {
    "responseCode",
    "responseData"
})
@XmlSeeAlso({
    SampleAccountResponse.class,
    AuthenticateCBAUserResp.class,
    TellerLimitResponse.class,
    CreateStopPaymentResponse.class,
    ChequeDetailsResponse.class,
    GetAccountBranchResp.class,
    TransactionRefNoDetailsResponse.class,
    ChequeNoInquiryResponse.class,
    UserLimitResponse.class,
    GetCurrencyCodeResp.class,
    CreateMMContractResponse.class,
    CreateRTResponse.class,
    AccountVerificationResponse.class,
    CreateCustomerResponse.class,
    ValidateUserTillResp.class,
    MessageIDStatusResponse.class,
    VansoCustomerIdResponse.class,
    CreateTDAccountResponse.class,
    CustomerDetails.class,
    CreateCustAccountResponse.class,
    GetChequeValueDateResp.class,
    GetAccountExistResponse.class,
    CreateAmtBlockResponse.class,
    CreateLDContractResponse.class,
    RepaymentScheduleResponse.class,
    ScreenLimitResponse.class,
    CustomerInformationResponse.class,
    ActivateChequeBookResponse.class,
    FixedDepositResponse.class,
    GetUserBranchResp.class,
    CurrencyRateResponse.class,
    CreateSIContractResponse.class,
    GetAcctMemoInfoResponse.class,
    CustomerAcctsDetailResponse.class,
    GetEffectiveBalanceResp.class,
    MobileTransactionDetail.class,
    MobileAccountDetail.class
})
public class FcubsResponse {

    @XmlElementRef(name = "ResponseCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> responseCode;
    @XmlElementRef(name = "ResponseData", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> responseData;

    /**
     * Gets the value of the responseCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResponseCode(JAXBElement<String> value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResponseData(JAXBElement<String> value) {
        this.responseData = value;
    }

}
