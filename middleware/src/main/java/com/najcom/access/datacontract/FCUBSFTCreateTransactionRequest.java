
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FCUBSFTCreateTransactionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FCUBSFTCreateTransactionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BENEFICIARY_ADDRESS1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENEFICIARY_ADDRESS2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENEFICIARY_ADDRESS3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENEFICIARY_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREDIT_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREDIT_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREDIT_BRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREDIT_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CREDIT_VALUEDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEBIT_ACCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEBIT_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEBIT_BRANCH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEBIT_CCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEBIT_VALUEDATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSGID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SOURCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="USER_BRANCH_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FCUBSFTCreateTransactionRequest", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", propOrder = {
    "beneficiaryaddress1",
    "beneficiaryaddress2",
    "beneficiaryaddress3",
    "beneficiaryname",
    "creditaccount",
    "creditamount",
    "creditbranch",
    "creditccy",
    "creditvaluedate",
    "debitaccount",
    "debitamount",
    "debitbranch",
    "debitccy",
    "debitvaluedate",
    "msgid",
    "source",
    "userid",
    "userbranchcode"
})
public class FCUBSFTCreateTransactionRequest {

    @XmlElementRef(name = "BENEFICIARY_ADDRESS1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> beneficiaryaddress1;
    @XmlElementRef(name = "BENEFICIARY_ADDRESS2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> beneficiaryaddress2;
    @XmlElementRef(name = "BENEFICIARY_ADDRESS3", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> beneficiaryaddress3;
    @XmlElementRef(name = "BENEFICIARY_NAME", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> beneficiaryname;
    @XmlElementRef(name = "CREDIT_ACCOUNT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditaccount;
    @XmlElementRef(name = "CREDIT_AMOUNT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditamount;
    @XmlElementRef(name = "CREDIT_BRANCH", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditbranch;
    @XmlElementRef(name = "CREDIT_CCY", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditccy;
    @XmlElementRef(name = "CREDIT_VALUEDATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> creditvaluedate;
    @XmlElementRef(name = "DEBIT_ACCOUNT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> debitaccount;
    @XmlElementRef(name = "DEBIT_AMOUNT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> debitamount;
    @XmlElementRef(name = "DEBIT_BRANCH", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> debitbranch;
    @XmlElementRef(name = "DEBIT_CCY", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> debitccy;
    @XmlElementRef(name = "DEBIT_VALUEDATE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> debitvaluedate;
    @XmlElementRef(name = "MSGID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgid;
    @XmlElementRef(name = "SOURCE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> source;
    @XmlElementRef(name = "USERID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userid;
    @XmlElementRef(name = "USER_BRANCH_CODE", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userbranchcode;

    /**
     * Gets the value of the beneficiaryaddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBENEFICIARYADDRESS1() {
        return beneficiaryaddress1;
    }

    /**
     * Sets the value of the beneficiaryaddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBENEFICIARYADDRESS1(JAXBElement<String> value) {
        this.beneficiaryaddress1 = value;
    }

    /**
     * Gets the value of the beneficiaryaddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBENEFICIARYADDRESS2() {
        return beneficiaryaddress2;
    }

    /**
     * Sets the value of the beneficiaryaddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBENEFICIARYADDRESS2(JAXBElement<String> value) {
        this.beneficiaryaddress2 = value;
    }

    /**
     * Gets the value of the beneficiaryaddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBENEFICIARYADDRESS3() {
        return beneficiaryaddress3;
    }

    /**
     * Sets the value of the beneficiaryaddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBENEFICIARYADDRESS3(JAXBElement<String> value) {
        this.beneficiaryaddress3 = value;
    }

    /**
     * Gets the value of the beneficiaryname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBENEFICIARYNAME() {
        return beneficiaryname;
    }

    /**
     * Sets the value of the beneficiaryname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBENEFICIARYNAME(JAXBElement<String> value) {
        this.beneficiaryname = value;
    }

    /**
     * Gets the value of the creditaccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCREDITACCOUNT() {
        return creditaccount;
    }

    /**
     * Sets the value of the creditaccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCREDITACCOUNT(JAXBElement<String> value) {
        this.creditaccount = value;
    }

    /**
     * Gets the value of the creditamount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCREDITAMOUNT() {
        return creditamount;
    }

    /**
     * Sets the value of the creditamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCREDITAMOUNT(JAXBElement<String> value) {
        this.creditamount = value;
    }

    /**
     * Gets the value of the creditbranch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCREDITBRANCH() {
        return creditbranch;
    }

    /**
     * Sets the value of the creditbranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCREDITBRANCH(JAXBElement<String> value) {
        this.creditbranch = value;
    }

    /**
     * Gets the value of the creditccy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCREDITCCY() {
        return creditccy;
    }

    /**
     * Sets the value of the creditccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCREDITCCY(JAXBElement<String> value) {
        this.creditccy = value;
    }

    /**
     * Gets the value of the creditvaluedate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCREDITVALUEDATE() {
        return creditvaluedate;
    }

    /**
     * Sets the value of the creditvaluedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCREDITVALUEDATE(JAXBElement<String> value) {
        this.creditvaluedate = value;
    }

    /**
     * Gets the value of the debitaccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDEBITACCOUNT() {
        return debitaccount;
    }

    /**
     * Sets the value of the debitaccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDEBITACCOUNT(JAXBElement<String> value) {
        this.debitaccount = value;
    }

    /**
     * Gets the value of the debitamount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDEBITAMOUNT() {
        return debitamount;
    }

    /**
     * Sets the value of the debitamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDEBITAMOUNT(JAXBElement<String> value) {
        this.debitamount = value;
    }

    /**
     * Gets the value of the debitbranch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDEBITBRANCH() {
        return debitbranch;
    }

    /**
     * Sets the value of the debitbranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDEBITBRANCH(JAXBElement<String> value) {
        this.debitbranch = value;
    }

    /**
     * Gets the value of the debitccy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDEBITCCY() {
        return debitccy;
    }

    /**
     * Sets the value of the debitccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDEBITCCY(JAXBElement<String> value) {
        this.debitccy = value;
    }

    /**
     * Gets the value of the debitvaluedate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDEBITVALUEDATE() {
        return debitvaluedate;
    }

    /**
     * Sets the value of the debitvaluedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDEBITVALUEDATE(JAXBElement<String> value) {
        this.debitvaluedate = value;
    }

    /**
     * Gets the value of the msgid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMSGID() {
        return msgid;
    }

    /**
     * Sets the value of the msgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMSGID(JAXBElement<String> value) {
        this.msgid = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSOURCE() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSOURCE(JAXBElement<String> value) {
        this.source = value;
    }

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUSERID() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUSERID(JAXBElement<String> value) {
        this.userid = value;
    }

    /**
     * Gets the value of the userbranchcode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUSERBRANCHCODE() {
        return userbranchcode;
    }

    /**
     * Sets the value of the userbranchcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUSERBRANCHCODE(JAXBElement<String> value) {
        this.userbranchcode = value;
    }

}
