
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FCUBSCreateSIContractRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FCUBSCreateSIContractRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CheckerDateStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMIS2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMIS4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMIS8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CrAccountBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CrAccountCcyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CrAccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrAccountBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrAccountCcyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrAccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrAccountNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExecDayInterval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExecMonthInterval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExecYearInterval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstExecutionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InternalRemarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MakerDateStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MakerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NextExecutionDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SIAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SIAmountCcyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SIExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FCUBSCreateSIContractRequest", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", propOrder = {
    "checkerDateStamp",
    "checkerID",
    "compMIS2",
    "compMIS4",
    "compMIS8",
    "crAccountBranchCode",
    "crAccountCcyCode",
    "crAccountNo",
    "drAccountBranchCode",
    "drAccountCcyCode",
    "drAccountName",
    "drAccountNo",
    "execDayInterval",
    "execMonthInterval",
    "execYearInterval",
    "firstExecutionDate",
    "internalRemarks",
    "makerDateStamp",
    "makerID",
    "nextExecutionDate",
    "productCode",
    "siAmount",
    "siAmountCcyCode",
    "siExpiryDate",
    "source",
    "userBranchCode",
    "userId"
})
public class FCUBSCreateSIContractRequest {

    @XmlElementRef(name = "CheckerDateStamp", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> checkerDateStamp;
    @XmlElementRef(name = "CheckerID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> checkerID;
    @XmlElementRef(name = "CompMIS2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMIS2;
    @XmlElementRef(name = "CompMIS4", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMIS4;
    @XmlElementRef(name = "CompMIS8", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMIS8;
    @XmlElementRef(name = "CrAccountBranchCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> crAccountBranchCode;
    @XmlElementRef(name = "CrAccountCcyCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> crAccountCcyCode;
    @XmlElementRef(name = "CrAccountNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> crAccountNo;
    @XmlElementRef(name = "DrAccountBranchCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drAccountBranchCode;
    @XmlElementRef(name = "DrAccountCcyCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drAccountCcyCode;
    @XmlElementRef(name = "DrAccountName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drAccountName;
    @XmlElementRef(name = "DrAccountNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drAccountNo;
    @XmlElementRef(name = "ExecDayInterval", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> execDayInterval;
    @XmlElementRef(name = "ExecMonthInterval", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> execMonthInterval;
    @XmlElementRef(name = "ExecYearInterval", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> execYearInterval;
    @XmlElementRef(name = "FirstExecutionDate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> firstExecutionDate;
    @XmlElementRef(name = "InternalRemarks", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> internalRemarks;
    @XmlElementRef(name = "MakerDateStamp", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> makerDateStamp;
    @XmlElementRef(name = "MakerID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> makerID;
    @XmlElementRef(name = "NextExecutionDate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nextExecutionDate;
    @XmlElementRef(name = "ProductCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productCode;
    @XmlElementRef(name = "SIAmount", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> siAmount;
    @XmlElementRef(name = "SIAmountCcyCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> siAmountCcyCode;
    @XmlElementRef(name = "SIExpiryDate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> siExpiryDate;
    @XmlElementRef(name = "Source", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> source;
    @XmlElementRef(name = "UserBranchCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userBranchCode;
    @XmlElementRef(name = "UserId", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userId;

    /**
     * Gets the value of the checkerDateStamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheckerDateStamp() {
        return checkerDateStamp;
    }

    /**
     * Sets the value of the checkerDateStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheckerDateStamp(JAXBElement<String> value) {
        this.checkerDateStamp = value;
    }

    /**
     * Gets the value of the checkerID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheckerID() {
        return checkerID;
    }

    /**
     * Sets the value of the checkerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheckerID(JAXBElement<String> value) {
        this.checkerID = value;
    }

    /**
     * Gets the value of the compMIS2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMIS2() {
        return compMIS2;
    }

    /**
     * Sets the value of the compMIS2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMIS2(JAXBElement<String> value) {
        this.compMIS2 = value;
    }

    /**
     * Gets the value of the compMIS4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMIS4() {
        return compMIS4;
    }

    /**
     * Sets the value of the compMIS4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMIS4(JAXBElement<String> value) {
        this.compMIS4 = value;
    }

    /**
     * Gets the value of the compMIS8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMIS8() {
        return compMIS8;
    }

    /**
     * Sets the value of the compMIS8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMIS8(JAXBElement<String> value) {
        this.compMIS8 = value;
    }

    /**
     * Gets the value of the crAccountBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCrAccountBranchCode() {
        return crAccountBranchCode;
    }

    /**
     * Sets the value of the crAccountBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCrAccountBranchCode(JAXBElement<String> value) {
        this.crAccountBranchCode = value;
    }

    /**
     * Gets the value of the crAccountCcyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCrAccountCcyCode() {
        return crAccountCcyCode;
    }

    /**
     * Sets the value of the crAccountCcyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCrAccountCcyCode(JAXBElement<String> value) {
        this.crAccountCcyCode = value;
    }

    /**
     * Gets the value of the crAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCrAccountNo() {
        return crAccountNo;
    }

    /**
     * Sets the value of the crAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCrAccountNo(JAXBElement<String> value) {
        this.crAccountNo = value;
    }

    /**
     * Gets the value of the drAccountBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrAccountBranchCode() {
        return drAccountBranchCode;
    }

    /**
     * Sets the value of the drAccountBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrAccountBranchCode(JAXBElement<String> value) {
        this.drAccountBranchCode = value;
    }

    /**
     * Gets the value of the drAccountCcyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrAccountCcyCode() {
        return drAccountCcyCode;
    }

    /**
     * Sets the value of the drAccountCcyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrAccountCcyCode(JAXBElement<String> value) {
        this.drAccountCcyCode = value;
    }

    /**
     * Gets the value of the drAccountName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrAccountName() {
        return drAccountName;
    }

    /**
     * Sets the value of the drAccountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrAccountName(JAXBElement<String> value) {
        this.drAccountName = value;
    }

    /**
     * Gets the value of the drAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrAccountNo() {
        return drAccountNo;
    }

    /**
     * Sets the value of the drAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrAccountNo(JAXBElement<String> value) {
        this.drAccountNo = value;
    }

    /**
     * Gets the value of the execDayInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExecDayInterval() {
        return execDayInterval;
    }

    /**
     * Sets the value of the execDayInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExecDayInterval(JAXBElement<String> value) {
        this.execDayInterval = value;
    }

    /**
     * Gets the value of the execMonthInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExecMonthInterval() {
        return execMonthInterval;
    }

    /**
     * Sets the value of the execMonthInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExecMonthInterval(JAXBElement<String> value) {
        this.execMonthInterval = value;
    }

    /**
     * Gets the value of the execYearInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExecYearInterval() {
        return execYearInterval;
    }

    /**
     * Sets the value of the execYearInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExecYearInterval(JAXBElement<String> value) {
        this.execYearInterval = value;
    }

    /**
     * Gets the value of the firstExecutionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFirstExecutionDate() {
        return firstExecutionDate;
    }

    /**
     * Sets the value of the firstExecutionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFirstExecutionDate(JAXBElement<String> value) {
        this.firstExecutionDate = value;
    }

    /**
     * Gets the value of the internalRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInternalRemarks() {
        return internalRemarks;
    }

    /**
     * Sets the value of the internalRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInternalRemarks(JAXBElement<String> value) {
        this.internalRemarks = value;
    }

    /**
     * Gets the value of the makerDateStamp property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMakerDateStamp() {
        return makerDateStamp;
    }

    /**
     * Sets the value of the makerDateStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMakerDateStamp(JAXBElement<String> value) {
        this.makerDateStamp = value;
    }

    /**
     * Gets the value of the makerID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMakerID() {
        return makerID;
    }

    /**
     * Sets the value of the makerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMakerID(JAXBElement<String> value) {
        this.makerID = value;
    }

    /**
     * Gets the value of the nextExecutionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNextExecutionDate() {
        return nextExecutionDate;
    }

    /**
     * Sets the value of the nextExecutionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNextExecutionDate(JAXBElement<String> value) {
        this.nextExecutionDate = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductCode(JAXBElement<String> value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the siAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSIAmount() {
        return siAmount;
    }

    /**
     * Sets the value of the siAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSIAmount(JAXBElement<String> value) {
        this.siAmount = value;
    }

    /**
     * Gets the value of the siAmountCcyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSIAmountCcyCode() {
        return siAmountCcyCode;
    }

    /**
     * Sets the value of the siAmountCcyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSIAmountCcyCode(JAXBElement<String> value) {
        this.siAmountCcyCode = value;
    }

    /**
     * Gets the value of the siExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSIExpiryDate() {
        return siExpiryDate;
    }

    /**
     * Sets the value of the siExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSIExpiryDate(JAXBElement<String> value) {
        this.siExpiryDate = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSource(JAXBElement<String> value) {
        this.source = value;
    }

    /**
     * Gets the value of the userBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserBranchCode() {
        return userBranchCode;
    }

    /**
     * Sets the value of the userBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserBranchCode(JAXBElement<String> value) {
        this.userBranchCode = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserId(JAXBElement<String> value) {
        this.userId = value;
    }

}
