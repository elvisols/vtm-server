
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FCUBSCreateCustomerRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FCUBSCreateCustomerRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AUC_CustomerMISClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AUC_CustomerMISCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountHolderMaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AcctOfficer_CompositeMISClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AcctOfficer_CompositeMISCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUZSECTOR_CustomerMISClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BUZSECTOR_CustomerMISCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BirthCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerCat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerCommMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerFullname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateofBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomiciledAddressLine1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomiciledAddressLine2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomiciledAddressLine3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomiciledAddressLine4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomiciledPinCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerFax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployerTelNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmploymentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Guardian" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HomePhoneNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HomeTelISDNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentityExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentityIssueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdentityNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LiabilityBranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LiabilityCcy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LiabilityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocalBranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MakerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinorYesorNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobileISDNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MobileNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MothersMaidenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NationalID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OJCAcct_CompositeMISClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OJCAcct_CompositeMISCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PermanentAddressLine1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PermanentAddressLine2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PermanentAddressLine3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PermanentAddressLine4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PinCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlaceofBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfitCenter_CompositeMISClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfitCenter_CompositeMISCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResidentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SamePermanentAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpouseEmploymentStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpouseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TelephoneISDNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TelephoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UDF_IncomeBandFieldName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UDF_IncomeBandFieldValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UDF_ProfessionFieldName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UDF_ProfessionFieldValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UDF_ReligionFieldName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UDF_ReligionFieldValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FCUBSCreateCustomerRequest", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", propOrder = {
    "aucCustomerMISClass",
    "aucCustomerMISCode",
    "accountHolderMaritalStatus",
    "acctOfficerCompositeMISClass",
    "acctOfficerCompositeMISCode",
    "buzsectorCustomerMISClass",
    "buzsectorCustomerMISCode",
    "birthCountry",
    "checkerID",
    "country",
    "customerCat",
    "customerCommMode",
    "customerFullname",
    "customerType",
    "dateofBirth",
    "domiciledAddressLine1",
    "domiciledAddressLine2",
    "domiciledAddressLine3",
    "domiciledAddressLine4",
    "domiciledPinCode",
    "emailID",
    "employerAddress1",
    "employerAddress2",
    "employerAddress3",
    "employerDesc",
    "employerEmail",
    "employerFax",
    "employerTelNo",
    "employmentStatus",
    "firstName",
    "gender",
    "guardian",
    "homePhoneNo",
    "homeTelISDNo",
    "identityExpiryDate",
    "identityIssueDate",
    "identityNo",
    "language",
    "lastName",
    "liabilityBranch",
    "liabilityCcy",
    "liabilityName",
    "localBranch",
    "makerID",
    "middleName",
    "minorYesorNo",
    "mobileISDNo",
    "mobileNumber",
    "mothersMaidenName",
    "nationalID",
    "nationality",
    "ojcAcctCompositeMISClass",
    "ojcAcctCompositeMISCode",
    "permanentAddressLine1",
    "permanentAddressLine2",
    "permanentAddressLine3",
    "permanentAddressLine4",
    "pinCode",
    "placeofBirth",
    "profitCenterCompositeMISClass",
    "profitCenterCompositeMISCode",
    "residentStatus",
    "samePermanentAddress",
    "shortName",
    "source",
    "spouseEmploymentStatus",
    "spouseName",
    "telephoneISDNo",
    "telephoneNumber",
    "title",
    "udfIncomeBandFieldName",
    "udfIncomeBandFieldValue",
    "udfProfessionFieldName",
    "udfProfessionFieldValue",
    "udfReligionFieldName",
    "udfReligionFieldValue",
    "userBranchCode",
    "userId"
})
public class FCUBSCreateCustomerRequest {

    @XmlElementRef(name = "AUC_CustomerMISClass", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> aucCustomerMISClass;
    @XmlElementRef(name = "AUC_CustomerMISCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> aucCustomerMISCode;
    @XmlElementRef(name = "AccountHolderMaritalStatus", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountHolderMaritalStatus;
    @XmlElementRef(name = "AcctOfficer_CompositeMISClass", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> acctOfficerCompositeMISClass;
    @XmlElementRef(name = "AcctOfficer_CompositeMISCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> acctOfficerCompositeMISCode;
    @XmlElementRef(name = "BUZSECTOR_CustomerMISClass", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> buzsectorCustomerMISClass;
    @XmlElementRef(name = "BUZSECTOR_CustomerMISCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> buzsectorCustomerMISCode;
    @XmlElementRef(name = "BirthCountry", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> birthCountry;
    @XmlElementRef(name = "CheckerID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> checkerID;
    @XmlElementRef(name = "Country", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> country;
    @XmlElementRef(name = "CustomerCat", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerCat;
    @XmlElementRef(name = "CustomerCommMode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerCommMode;
    @XmlElementRef(name = "CustomerFullname", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerFullname;
    @XmlElementRef(name = "CustomerType", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerType;
    @XmlElementRef(name = "DateofBirth", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dateofBirth;
    @XmlElementRef(name = "DomiciledAddressLine1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> domiciledAddressLine1;
    @XmlElementRef(name = "DomiciledAddressLine2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> domiciledAddressLine2;
    @XmlElementRef(name = "DomiciledAddressLine3", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> domiciledAddressLine3;
    @XmlElementRef(name = "DomiciledAddressLine4", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> domiciledAddressLine4;
    @XmlElementRef(name = "DomiciledPinCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> domiciledPinCode;
    @XmlElementRef(name = "EmailID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailID;
    @XmlElementRef(name = "EmployerAddress1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerAddress1;
    @XmlElementRef(name = "EmployerAddress2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerAddress2;
    @XmlElementRef(name = "EmployerAddress3", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerAddress3;
    @XmlElementRef(name = "EmployerDesc", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerDesc;
    @XmlElementRef(name = "EmployerEmail", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerEmail;
    @XmlElementRef(name = "EmployerFax", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerFax;
    @XmlElementRef(name = "EmployerTelNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employerTelNo;
    @XmlElementRef(name = "EmploymentStatus", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employmentStatus;
    @XmlElementRef(name = "FirstName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> firstName;
    @XmlElementRef(name = "Gender", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> gender;
    @XmlElementRef(name = "Guardian", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> guardian;
    @XmlElementRef(name = "HomePhoneNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> homePhoneNo;
    @XmlElementRef(name = "HomeTelISDNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> homeTelISDNo;
    @XmlElementRef(name = "IdentityExpiryDate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> identityExpiryDate;
    @XmlElementRef(name = "IdentityIssueDate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> identityIssueDate;
    @XmlElementRef(name = "IdentityNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> identityNo;
    @XmlElementRef(name = "Language", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> language;
    @XmlElementRef(name = "LastName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lastName;
    @XmlElementRef(name = "LiabilityBranch", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> liabilityBranch;
    @XmlElementRef(name = "LiabilityCcy", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> liabilityCcy;
    @XmlElementRef(name = "LiabilityName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> liabilityName;
    @XmlElementRef(name = "LocalBranch", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> localBranch;
    @XmlElementRef(name = "MakerID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> makerID;
    @XmlElementRef(name = "MiddleName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> middleName;
    @XmlElementRef(name = "MinorYesorNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> minorYesorNo;
    @XmlElementRef(name = "MobileISDNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mobileISDNo;
    @XmlElementRef(name = "MobileNumber", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mobileNumber;
    @XmlElementRef(name = "MothersMaidenName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> mothersMaidenName;
    @XmlElementRef(name = "NationalID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nationalID;
    @XmlElementRef(name = "Nationality", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nationality;
    @XmlElementRef(name = "OJCAcct_CompositeMISClass", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ojcAcctCompositeMISClass;
    @XmlElementRef(name = "OJCAcct_CompositeMISCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ojcAcctCompositeMISCode;
    @XmlElementRef(name = "PermanentAddressLine1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> permanentAddressLine1;
    @XmlElementRef(name = "PermanentAddressLine2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> permanentAddressLine2;
    @XmlElementRef(name = "PermanentAddressLine3", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> permanentAddressLine3;
    @XmlElementRef(name = "PermanentAddressLine4", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> permanentAddressLine4;
    @XmlElementRef(name = "PinCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> pinCode;
    @XmlElementRef(name = "PlaceofBirth", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> placeofBirth;
    @XmlElementRef(name = "ProfitCenter_CompositeMISClass", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> profitCenterCompositeMISClass;
    @XmlElementRef(name = "ProfitCenter_CompositeMISCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> profitCenterCompositeMISCode;
    @XmlElementRef(name = "ResidentStatus", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> residentStatus;
    @XmlElementRef(name = "SamePermanentAddress", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> samePermanentAddress;
    @XmlElementRef(name = "ShortName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shortName;
    @XmlElementRef(name = "Source", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> source;
    @XmlElementRef(name = "SpouseEmploymentStatus", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> spouseEmploymentStatus;
    @XmlElementRef(name = "SpouseName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> spouseName;
    @XmlElementRef(name = "TelephoneISDNo", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telephoneISDNo;
    @XmlElementRef(name = "TelephoneNumber", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telephoneNumber;
    @XmlElementRef(name = "Title", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> title;
    @XmlElementRef(name = "UDF_IncomeBandFieldName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> udfIncomeBandFieldName;
    @XmlElementRef(name = "UDF_IncomeBandFieldValue", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> udfIncomeBandFieldValue;
    @XmlElementRef(name = "UDF_ProfessionFieldName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> udfProfessionFieldName;
    @XmlElementRef(name = "UDF_ProfessionFieldValue", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> udfProfessionFieldValue;
    @XmlElementRef(name = "UDF_ReligionFieldName", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> udfReligionFieldName;
    @XmlElementRef(name = "UDF_ReligionFieldValue", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> udfReligionFieldValue;
    @XmlElementRef(name = "UserBranchCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userBranchCode;
    @XmlElementRef(name = "UserId", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userId;

    /**
     * Gets the value of the aucCustomerMISClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAUCCustomerMISClass() {
        return aucCustomerMISClass;
    }

    /**
     * Sets the value of the aucCustomerMISClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAUCCustomerMISClass(JAXBElement<String> value) {
        this.aucCustomerMISClass = value;
    }

    /**
     * Gets the value of the aucCustomerMISCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAUCCustomerMISCode() {
        return aucCustomerMISCode;
    }

    /**
     * Sets the value of the aucCustomerMISCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAUCCustomerMISCode(JAXBElement<String> value) {
        this.aucCustomerMISCode = value;
    }

    /**
     * Gets the value of the accountHolderMaritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountHolderMaritalStatus() {
        return accountHolderMaritalStatus;
    }

    /**
     * Sets the value of the accountHolderMaritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountHolderMaritalStatus(JAXBElement<String> value) {
        this.accountHolderMaritalStatus = value;
    }

    /**
     * Gets the value of the acctOfficerCompositeMISClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAcctOfficerCompositeMISClass() {
        return acctOfficerCompositeMISClass;
    }

    /**
     * Sets the value of the acctOfficerCompositeMISClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAcctOfficerCompositeMISClass(JAXBElement<String> value) {
        this.acctOfficerCompositeMISClass = value;
    }

    /**
     * Gets the value of the acctOfficerCompositeMISCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAcctOfficerCompositeMISCode() {
        return acctOfficerCompositeMISCode;
    }

    /**
     * Sets the value of the acctOfficerCompositeMISCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAcctOfficerCompositeMISCode(JAXBElement<String> value) {
        this.acctOfficerCompositeMISCode = value;
    }

    /**
     * Gets the value of the buzsectorCustomerMISClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBUZSECTORCustomerMISClass() {
        return buzsectorCustomerMISClass;
    }

    /**
     * Sets the value of the buzsectorCustomerMISClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBUZSECTORCustomerMISClass(JAXBElement<String> value) {
        this.buzsectorCustomerMISClass = value;
    }

    /**
     * Gets the value of the buzsectorCustomerMISCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBUZSECTORCustomerMISCode() {
        return buzsectorCustomerMISCode;
    }

    /**
     * Sets the value of the buzsectorCustomerMISCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBUZSECTORCustomerMISCode(JAXBElement<String> value) {
        this.buzsectorCustomerMISCode = value;
    }

    /**
     * Gets the value of the birthCountry property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBirthCountry() {
        return birthCountry;
    }

    /**
     * Sets the value of the birthCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBirthCountry(JAXBElement<String> value) {
        this.birthCountry = value;
    }

    /**
     * Gets the value of the checkerID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCheckerID() {
        return checkerID;
    }

    /**
     * Sets the value of the checkerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCheckerID(JAXBElement<String> value) {
        this.checkerID = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCountry(JAXBElement<String> value) {
        this.country = value;
    }

    /**
     * Gets the value of the customerCat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerCat() {
        return customerCat;
    }

    /**
     * Sets the value of the customerCat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerCat(JAXBElement<String> value) {
        this.customerCat = value;
    }

    /**
     * Gets the value of the customerCommMode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerCommMode() {
        return customerCommMode;
    }

    /**
     * Sets the value of the customerCommMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerCommMode(JAXBElement<String> value) {
        this.customerCommMode = value;
    }

    /**
     * Gets the value of the customerFullname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerFullname() {
        return customerFullname;
    }

    /**
     * Sets the value of the customerFullname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerFullname(JAXBElement<String> value) {
        this.customerFullname = value;
    }

    /**
     * Gets the value of the customerType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerType(JAXBElement<String> value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the dateofBirth property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDateofBirth() {
        return dateofBirth;
    }

    /**
     * Sets the value of the dateofBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDateofBirth(JAXBElement<String> value) {
        this.dateofBirth = value;
    }

    /**
     * Gets the value of the domiciledAddressLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDomiciledAddressLine1() {
        return domiciledAddressLine1;
    }

    /**
     * Sets the value of the domiciledAddressLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDomiciledAddressLine1(JAXBElement<String> value) {
        this.domiciledAddressLine1 = value;
    }

    /**
     * Gets the value of the domiciledAddressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDomiciledAddressLine2() {
        return domiciledAddressLine2;
    }

    /**
     * Sets the value of the domiciledAddressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDomiciledAddressLine2(JAXBElement<String> value) {
        this.domiciledAddressLine2 = value;
    }

    /**
     * Gets the value of the domiciledAddressLine3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDomiciledAddressLine3() {
        return domiciledAddressLine3;
    }

    /**
     * Sets the value of the domiciledAddressLine3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDomiciledAddressLine3(JAXBElement<String> value) {
        this.domiciledAddressLine3 = value;
    }

    /**
     * Gets the value of the domiciledAddressLine4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDomiciledAddressLine4() {
        return domiciledAddressLine4;
    }

    /**
     * Sets the value of the domiciledAddressLine4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDomiciledAddressLine4(JAXBElement<String> value) {
        this.domiciledAddressLine4 = value;
    }

    /**
     * Gets the value of the domiciledPinCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDomiciledPinCode() {
        return domiciledPinCode;
    }

    /**
     * Sets the value of the domiciledPinCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDomiciledPinCode(JAXBElement<String> value) {
        this.domiciledPinCode = value;
    }

    /**
     * Gets the value of the emailID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailID() {
        return emailID;
    }

    /**
     * Sets the value of the emailID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailID(JAXBElement<String> value) {
        this.emailID = value;
    }

    /**
     * Gets the value of the employerAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerAddress1() {
        return employerAddress1;
    }

    /**
     * Sets the value of the employerAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerAddress1(JAXBElement<String> value) {
        this.employerAddress1 = value;
    }

    /**
     * Gets the value of the employerAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerAddress2() {
        return employerAddress2;
    }

    /**
     * Sets the value of the employerAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerAddress2(JAXBElement<String> value) {
        this.employerAddress2 = value;
    }

    /**
     * Gets the value of the employerAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerAddress3() {
        return employerAddress3;
    }

    /**
     * Sets the value of the employerAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerAddress3(JAXBElement<String> value) {
        this.employerAddress3 = value;
    }

    /**
     * Gets the value of the employerDesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerDesc() {
        return employerDesc;
    }

    /**
     * Sets the value of the employerDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerDesc(JAXBElement<String> value) {
        this.employerDesc = value;
    }

    /**
     * Gets the value of the employerEmail property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerEmail() {
        return employerEmail;
    }

    /**
     * Sets the value of the employerEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerEmail(JAXBElement<String> value) {
        this.employerEmail = value;
    }

    /**
     * Gets the value of the employerFax property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerFax() {
        return employerFax;
    }

    /**
     * Sets the value of the employerFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerFax(JAXBElement<String> value) {
        this.employerFax = value;
    }

    /**
     * Gets the value of the employerTelNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmployerTelNo() {
        return employerTelNo;
    }

    /**
     * Sets the value of the employerTelNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmployerTelNo(JAXBElement<String> value) {
        this.employerTelNo = value;
    }

    /**
     * Gets the value of the employmentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmploymentStatus() {
        return employmentStatus;
    }

    /**
     * Sets the value of the employmentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmploymentStatus(JAXBElement<String> value) {
        this.employmentStatus = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFirstName(JAXBElement<String> value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGender(JAXBElement<String> value) {
        this.gender = value;
    }

    /**
     * Gets the value of the guardian property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGuardian() {
        return guardian;
    }

    /**
     * Sets the value of the guardian property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGuardian(JAXBElement<String> value) {
        this.guardian = value;
    }

    /**
     * Gets the value of the homePhoneNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHomePhoneNo() {
        return homePhoneNo;
    }

    /**
     * Sets the value of the homePhoneNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHomePhoneNo(JAXBElement<String> value) {
        this.homePhoneNo = value;
    }

    /**
     * Gets the value of the homeTelISDNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHomeTelISDNo() {
        return homeTelISDNo;
    }

    /**
     * Sets the value of the homeTelISDNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHomeTelISDNo(JAXBElement<String> value) {
        this.homeTelISDNo = value;
    }

    /**
     * Gets the value of the identityExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdentityExpiryDate() {
        return identityExpiryDate;
    }

    /**
     * Sets the value of the identityExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdentityExpiryDate(JAXBElement<String> value) {
        this.identityExpiryDate = value;
    }

    /**
     * Gets the value of the identityIssueDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdentityIssueDate() {
        return identityIssueDate;
    }

    /**
     * Sets the value of the identityIssueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdentityIssueDate(JAXBElement<String> value) {
        this.identityIssueDate = value;
    }

    /**
     * Gets the value of the identityNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdentityNo() {
        return identityNo;
    }

    /**
     * Sets the value of the identityNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdentityNo(JAXBElement<String> value) {
        this.identityNo = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLanguage(JAXBElement<String> value) {
        this.language = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLastName(JAXBElement<String> value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the liabilityBranch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLiabilityBranch() {
        return liabilityBranch;
    }

    /**
     * Sets the value of the liabilityBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLiabilityBranch(JAXBElement<String> value) {
        this.liabilityBranch = value;
    }

    /**
     * Gets the value of the liabilityCcy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLiabilityCcy() {
        return liabilityCcy;
    }

    /**
     * Sets the value of the liabilityCcy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLiabilityCcy(JAXBElement<String> value) {
        this.liabilityCcy = value;
    }

    /**
     * Gets the value of the liabilityName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLiabilityName() {
        return liabilityName;
    }

    /**
     * Sets the value of the liabilityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLiabilityName(JAXBElement<String> value) {
        this.liabilityName = value;
    }

    /**
     * Gets the value of the localBranch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocalBranch() {
        return localBranch;
    }

    /**
     * Sets the value of the localBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocalBranch(JAXBElement<String> value) {
        this.localBranch = value;
    }

    /**
     * Gets the value of the makerID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMakerID() {
        return makerID;
    }

    /**
     * Sets the value of the makerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMakerID(JAXBElement<String> value) {
        this.makerID = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMiddleName(JAXBElement<String> value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the minorYesorNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMinorYesorNo() {
        return minorYesorNo;
    }

    /**
     * Sets the value of the minorYesorNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMinorYesorNo(JAXBElement<String> value) {
        this.minorYesorNo = value;
    }

    /**
     * Gets the value of the mobileISDNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMobileISDNo() {
        return mobileISDNo;
    }

    /**
     * Sets the value of the mobileISDNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMobileISDNo(JAXBElement<String> value) {
        this.mobileISDNo = value;
    }

    /**
     * Gets the value of the mobileNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMobileNumber() {
        return mobileNumber;
    }

    /**
     * Sets the value of the mobileNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMobileNumber(JAXBElement<String> value) {
        this.mobileNumber = value;
    }

    /**
     * Gets the value of the mothersMaidenName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMothersMaidenName() {
        return mothersMaidenName;
    }

    /**
     * Sets the value of the mothersMaidenName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMothersMaidenName(JAXBElement<String> value) {
        this.mothersMaidenName = value;
    }

    /**
     * Gets the value of the nationalID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNationalID() {
        return nationalID;
    }

    /**
     * Sets the value of the nationalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNationalID(JAXBElement<String> value) {
        this.nationalID = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNationality(JAXBElement<String> value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the ojcAcctCompositeMISClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOJCAcctCompositeMISClass() {
        return ojcAcctCompositeMISClass;
    }

    /**
     * Sets the value of the ojcAcctCompositeMISClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOJCAcctCompositeMISClass(JAXBElement<String> value) {
        this.ojcAcctCompositeMISClass = value;
    }

    /**
     * Gets the value of the ojcAcctCompositeMISCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOJCAcctCompositeMISCode() {
        return ojcAcctCompositeMISCode;
    }

    /**
     * Sets the value of the ojcAcctCompositeMISCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOJCAcctCompositeMISCode(JAXBElement<String> value) {
        this.ojcAcctCompositeMISCode = value;
    }

    /**
     * Gets the value of the permanentAddressLine1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPermanentAddressLine1() {
        return permanentAddressLine1;
    }

    /**
     * Sets the value of the permanentAddressLine1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPermanentAddressLine1(JAXBElement<String> value) {
        this.permanentAddressLine1 = value;
    }

    /**
     * Gets the value of the permanentAddressLine2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPermanentAddressLine2() {
        return permanentAddressLine2;
    }

    /**
     * Sets the value of the permanentAddressLine2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPermanentAddressLine2(JAXBElement<String> value) {
        this.permanentAddressLine2 = value;
    }

    /**
     * Gets the value of the permanentAddressLine3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPermanentAddressLine3() {
        return permanentAddressLine3;
    }

    /**
     * Sets the value of the permanentAddressLine3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPermanentAddressLine3(JAXBElement<String> value) {
        this.permanentAddressLine3 = value;
    }

    /**
     * Gets the value of the permanentAddressLine4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPermanentAddressLine4() {
        return permanentAddressLine4;
    }

    /**
     * Sets the value of the permanentAddressLine4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPermanentAddressLine4(JAXBElement<String> value) {
        this.permanentAddressLine4 = value;
    }

    /**
     * Gets the value of the pinCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPinCode() {
        return pinCode;
    }

    /**
     * Sets the value of the pinCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPinCode(JAXBElement<String> value) {
        this.pinCode = value;
    }

    /**
     * Gets the value of the placeofBirth property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlaceofBirth() {
        return placeofBirth;
    }

    /**
     * Sets the value of the placeofBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlaceofBirth(JAXBElement<String> value) {
        this.placeofBirth = value;
    }

    /**
     * Gets the value of the profitCenterCompositeMISClass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProfitCenterCompositeMISClass() {
        return profitCenterCompositeMISClass;
    }

    /**
     * Sets the value of the profitCenterCompositeMISClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProfitCenterCompositeMISClass(JAXBElement<String> value) {
        this.profitCenterCompositeMISClass = value;
    }

    /**
     * Gets the value of the profitCenterCompositeMISCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProfitCenterCompositeMISCode() {
        return profitCenterCompositeMISCode;
    }

    /**
     * Sets the value of the profitCenterCompositeMISCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProfitCenterCompositeMISCode(JAXBElement<String> value) {
        this.profitCenterCompositeMISCode = value;
    }

    /**
     * Gets the value of the residentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getResidentStatus() {
        return residentStatus;
    }

    /**
     * Sets the value of the residentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setResidentStatus(JAXBElement<String> value) {
        this.residentStatus = value;
    }

    /**
     * Gets the value of the samePermanentAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSamePermanentAddress() {
        return samePermanentAddress;
    }

    /**
     * Sets the value of the samePermanentAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSamePermanentAddress(JAXBElement<String> value) {
        this.samePermanentAddress = value;
    }

    /**
     * Gets the value of the shortName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShortName() {
        return shortName;
    }

    /**
     * Sets the value of the shortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShortName(JAXBElement<String> value) {
        this.shortName = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSource(JAXBElement<String> value) {
        this.source = value;
    }

    /**
     * Gets the value of the spouseEmploymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSpouseEmploymentStatus() {
        return spouseEmploymentStatus;
    }

    /**
     * Sets the value of the spouseEmploymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSpouseEmploymentStatus(JAXBElement<String> value) {
        this.spouseEmploymentStatus = value;
    }

    /**
     * Gets the value of the spouseName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSpouseName() {
        return spouseName;
    }

    /**
     * Sets the value of the spouseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSpouseName(JAXBElement<String> value) {
        this.spouseName = value;
    }

    /**
     * Gets the value of the telephoneISDNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelephoneISDNo() {
        return telephoneISDNo;
    }

    /**
     * Sets the value of the telephoneISDNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelephoneISDNo(JAXBElement<String> value) {
        this.telephoneISDNo = value;
    }

    /**
     * Gets the value of the telephoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * Sets the value of the telephoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelephoneNumber(JAXBElement<String> value) {
        this.telephoneNumber = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTitle(JAXBElement<String> value) {
        this.title = value;
    }

    /**
     * Gets the value of the udfIncomeBandFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUDFIncomeBandFieldName() {
        return udfIncomeBandFieldName;
    }

    /**
     * Sets the value of the udfIncomeBandFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUDFIncomeBandFieldName(JAXBElement<String> value) {
        this.udfIncomeBandFieldName = value;
    }

    /**
     * Gets the value of the udfIncomeBandFieldValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUDFIncomeBandFieldValue() {
        return udfIncomeBandFieldValue;
    }

    /**
     * Sets the value of the udfIncomeBandFieldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUDFIncomeBandFieldValue(JAXBElement<String> value) {
        this.udfIncomeBandFieldValue = value;
    }

    /**
     * Gets the value of the udfProfessionFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUDFProfessionFieldName() {
        return udfProfessionFieldName;
    }

    /**
     * Sets the value of the udfProfessionFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUDFProfessionFieldName(JAXBElement<String> value) {
        this.udfProfessionFieldName = value;
    }

    /**
     * Gets the value of the udfProfessionFieldValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUDFProfessionFieldValue() {
        return udfProfessionFieldValue;
    }

    /**
     * Sets the value of the udfProfessionFieldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUDFProfessionFieldValue(JAXBElement<String> value) {
        this.udfProfessionFieldValue = value;
    }

    /**
     * Gets the value of the udfReligionFieldName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUDFReligionFieldName() {
        return udfReligionFieldName;
    }

    /**
     * Sets the value of the udfReligionFieldName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUDFReligionFieldName(JAXBElement<String> value) {
        this.udfReligionFieldName = value;
    }

    /**
     * Gets the value of the udfReligionFieldValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUDFReligionFieldValue() {
        return udfReligionFieldValue;
    }

    /**
     * Sets the value of the udfReligionFieldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUDFReligionFieldValue(JAXBElement<String> value) {
        this.udfReligionFieldValue = value;
    }

    /**
     * Gets the value of the userBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserBranchCode() {
        return userBranchCode;
    }

    /**
     * Sets the value of the userBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserBranchCode(JAXBElement<String> value) {
        this.userBranchCode = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserId(JAXBElement<String> value) {
        this.userId = value;
    }

}
