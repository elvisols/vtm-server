
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountSummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountSummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountSummaryLines" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfAccountSummaryLine" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountSummaryResponse", propOrder = {
    "accountSummaryLines"
})
public class AccountSummaryResponse {

    @XmlElementRef(name = "AccountSummaryLines", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAccountSummaryLine> accountSummaryLines;

    /**
     * Gets the value of the accountSummaryLines property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryLine }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAccountSummaryLine> getAccountSummaryLines() {
        return accountSummaryLines;
    }

    /**
     * Sets the value of the accountSummaryLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryLine }{@code >}
     *     
     */
    public void setAccountSummaryLines(JAXBElement<ArrayOfAccountSummaryLine> value) {
        this.accountSummaryLines = value;
    }

}
