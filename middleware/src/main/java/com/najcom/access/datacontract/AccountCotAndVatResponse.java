
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountCotAndVatResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountCotAndVatResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountCotAndVatTransactions" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfAccountCotAndVatLines" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountCotAndVatResponse", propOrder = {
    "accountCotAndVatTransactions"
})
public class AccountCotAndVatResponse {

    @XmlElementRef(name = "AccountCotAndVatTransactions", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAccountCotAndVatLines> accountCotAndVatTransactions;

    /**
     * Gets the value of the accountCotAndVatTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountCotAndVatLines }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAccountCotAndVatLines> getAccountCotAndVatTransactions() {
        return accountCotAndVatTransactions;
    }

    /**
     * Sets the value of the accountCotAndVatTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountCotAndVatLines }{@code >}
     *     
     */
    public void setAccountCotAndVatTransactions(JAXBElement<ArrayOfAccountCotAndVatLines> value) {
        this.accountCotAndVatTransactions = value;
    }

}
