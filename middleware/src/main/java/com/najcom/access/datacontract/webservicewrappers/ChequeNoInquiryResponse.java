
package com.najcom.access.datacontract.webservicewrappers;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import com.najcom.access.datacontract.ArrayOfChequeNumberDetails;
import com.najcom.access.datacontract.FcubsResponse;


/**
 * <p>Java class for ChequeNoInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChequeNoInquiryResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FcubsResponse">
 *       &lt;sequence>
 *         &lt;element name="chequeNumberDetails" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfChequeNumberDetails" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChequeNoInquiryResponse", propOrder = {
    "chequeNumberDetails"
})
public class ChequeNoInquiryResponse
    extends FcubsResponse
{

    @XmlElementRef(name = "chequeNumberDetails", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfChequeNumberDetails> chequeNumberDetails;

    /**
     * Gets the value of the chequeNumberDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfChequeNumberDetails }{@code >}
     *     
     */
    public JAXBElement<ArrayOfChequeNumberDetails> getChequeNumberDetails() {
        return chequeNumberDetails;
    }

    /**
     * Sets the value of the chequeNumberDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfChequeNumberDetails }{@code >}
     *     
     */
    public void setChequeNumberDetails(JAXBElement<ArrayOfChequeNumberDetails> value) {
        this.chequeNumberDetails = value;
    }

}
