
package com.najcom.access.datacontract.webservicewrappers;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMobileAccountDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMobileAccountDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MobileAccountDetail" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.WebserviceWrappers.VO}MobileAccountDetail" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMobileAccountDetail", propOrder = {
    "mobileAccountDetail"
})
public class ArrayOfMobileAccountDetail {

    @XmlElement(name = "MobileAccountDetail", nillable = true)
    protected List<MobileAccountDetail> mobileAccountDetail;

    /**
     * Gets the value of the mobileAccountDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mobileAccountDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMobileAccountDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MobileAccountDetail }
     * 
     * 
     */
    public List<MobileAccountDetail> getMobileAccountDetail() {
        if (mobileAccountDetail == null) {
            mobileAccountDetail = new ArrayList<MobileAccountDetail>();
        }
        return this.mobileAccountDetail;
    }

}
