
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NubanAcctsDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NubanAcctsDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cod_acct_no_new" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_acct_no_old" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NubanAcctsDetails", propOrder = {
    "codAcctNoNew",
    "codAcctNoOld"
})
public class NubanAcctsDetails {

    @XmlElementRef(name = "cod_acct_no_new", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAcctNoNew;
    @XmlElementRef(name = "cod_acct_no_old", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codAcctNoOld;

    /**
     * Gets the value of the codAcctNoNew property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAcctNoNew() {
        return codAcctNoNew;
    }

    /**
     * Sets the value of the codAcctNoNew property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAcctNoNew(JAXBElement<String> value) {
        this.codAcctNoNew = value;
    }

    /**
     * Gets the value of the codAcctNoOld property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodAcctNoOld() {
        return codAcctNoOld;
    }

    /**
     * Sets the value of the codAcctNoOld property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodAcctNoOld(JAXBElement<String> value) {
        this.codAcctNoOld = value;
    }

}
