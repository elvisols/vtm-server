
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTransactionRefNoDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTransactionRefNoDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionRefNoDetails" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}TransactionRefNoDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTransactionRefNoDetails", propOrder = {
    "transactionRefNoDetails"
})
public class ArrayOfTransactionRefNoDetails {

    @XmlElement(name = "TransactionRefNoDetails", nillable = true)
    protected List<TransactionRefNoDetails> transactionRefNoDetails;

    /**
     * Gets the value of the transactionRefNoDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionRefNoDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionRefNoDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionRefNoDetails }
     * 
     * 
     */
    public List<TransactionRefNoDetails> getTransactionRefNoDetails() {
        if (transactionRefNoDetails == null) {
            transactionRefNoDetails = new ArrayList<TransactionRefNoDetails>();
        }
        return this.transactionRefNoDetails;
    }

}
