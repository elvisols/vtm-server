
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAccountMemoInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAccountMemoInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountMemoInfo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}AccountMemoInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAccountMemoInfo", propOrder = {
    "accountMemoInfo"
})
public class ArrayOfAccountMemoInfo {

    @XmlElement(name = "AccountMemoInfo", nillable = true)
    protected List<AccountMemoInfo> accountMemoInfo;

    /**
     * Gets the value of the accountMemoInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountMemoInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountMemoInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountMemoInfo }
     * 
     * 
     */
    public List<AccountMemoInfo> getAccountMemoInfo() {
        if (accountMemoInfo == null) {
            accountMemoInfo = new ArrayList<AccountMemoInfo>();
        }
        return this.accountMemoInfo;
    }

}
