
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImageCollectionDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImageCollectionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerImage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerImage_Bytes" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="cif_sig_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_cust" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_cust_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cust_ac_no" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cust_imageURL1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cust_imageURL1_Bytes" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="cust_imageURL2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cust_imageURL2_Bytes" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="nam_cust_shrt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nam_cust_shrt_X" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txt_acct_oper_instrs1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImageCollectionDetails", propOrder = {
    "customerImage",
    "customerImageBytes",
    "cifSigName",
    "codCust",
    "codCustId",
    "custAcNo",
    "custImageURL1",
    "custImageURL1Bytes",
    "custImageURL2",
    "custImageURL2Bytes",
    "namCustShrt",
    "namCustShrtX",
    "txtAcctOperInstrs1"
})
public class ImageCollectionDetails {

    @XmlElementRef(name = "CustomerImage", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerImage;
    @XmlElementRef(name = "CustomerImage_Bytes", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> customerImageBytes;
    @XmlElementRef(name = "cif_sig_name", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cifSigName;
    @XmlElementRef(name = "cod_cust", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCust;
    @XmlElementRef(name = "cod_cust_id", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCustId;
    @XmlElementRef(name = "cust_ac_no", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custAcNo;
    @XmlElementRef(name = "cust_imageURL1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custImageURL1;
    @XmlElementRef(name = "cust_imageURL1_Bytes", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> custImageURL1Bytes;
    @XmlElementRef(name = "cust_imageURL2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custImageURL2;
    @XmlElementRef(name = "cust_imageURL2_Bytes", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> custImageURL2Bytes;
    @XmlElementRef(name = "nam_cust_shrt", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> namCustShrt;
    @XmlElementRef(name = "nam_cust_shrt_X", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> namCustShrtX;
    @XmlElementRef(name = "txt_acct_oper_instrs1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> txtAcctOperInstrs1;

    /**
     * Gets the value of the customerImage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerImage() {
        return customerImage;
    }

    /**
     * Sets the value of the customerImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerImage(JAXBElement<String> value) {
        this.customerImage = value;
    }

    /**
     * Gets the value of the customerImageBytes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getCustomerImageBytes() {
        return customerImageBytes;
    }

    /**
     * Sets the value of the customerImageBytes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setCustomerImageBytes(JAXBElement<byte[]> value) {
        this.customerImageBytes = value;
    }

    /**
     * Gets the value of the cifSigName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCifSigName() {
        return cifSigName;
    }

    /**
     * Sets the value of the cifSigName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCifSigName(JAXBElement<String> value) {
        this.cifSigName = value;
    }

    /**
     * Gets the value of the codCust property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCust() {
        return codCust;
    }

    /**
     * Sets the value of the codCust property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCust(JAXBElement<String> value) {
        this.codCust = value;
    }

    /**
     * Gets the value of the codCustId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodCustId() {
        return codCustId;
    }

    /**
     * Sets the value of the codCustId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodCustId(JAXBElement<String> value) {
        this.codCustId = value;
    }

    /**
     * Gets the value of the custAcNo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustAcNo() {
        return custAcNo;
    }

    /**
     * Sets the value of the custAcNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustAcNo(JAXBElement<String> value) {
        this.custAcNo = value;
    }

    /**
     * Gets the value of the custImageURL1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustImageURL1() {
        return custImageURL1;
    }

    /**
     * Sets the value of the custImageURL1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustImageURL1(JAXBElement<String> value) {
        this.custImageURL1 = value;
    }

    /**
     * Gets the value of the custImageURL1Bytes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getCustImageURL1Bytes() {
        return custImageURL1Bytes;
    }

    /**
     * Sets the value of the custImageURL1Bytes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setCustImageURL1Bytes(JAXBElement<byte[]> value) {
        this.custImageURL1Bytes = value;
    }

    /**
     * Gets the value of the custImageURL2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustImageURL2() {
        return custImageURL2;
    }

    /**
     * Sets the value of the custImageURL2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustImageURL2(JAXBElement<String> value) {
        this.custImageURL2 = value;
    }

    /**
     * Gets the value of the custImageURL2Bytes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getCustImageURL2Bytes() {
        return custImageURL2Bytes;
    }

    /**
     * Sets the value of the custImageURL2Bytes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setCustImageURL2Bytes(JAXBElement<byte[]> value) {
        this.custImageURL2Bytes = value;
    }

    /**
     * Gets the value of the namCustShrt property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNamCustShrt() {
        return namCustShrt;
    }

    /**
     * Sets the value of the namCustShrt property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNamCustShrt(JAXBElement<String> value) {
        this.namCustShrt = value;
    }

    /**
     * Gets the value of the namCustShrtX property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNamCustShrtX() {
        return namCustShrtX;
    }

    /**
     * Sets the value of the namCustShrtX property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNamCustShrtX(JAXBElement<String> value) {
        this.namCustShrtX = value;
    }

    /**
     * Gets the value of the txtAcctOperInstrs1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTxtAcctOperInstrs1() {
        return txtAcctOperInstrs1;
    }

    /**
     * Sets the value of the txtAcctOperInstrs1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTxtAcctOperInstrs1(JAXBElement<String> value) {
        this.txtAcctOperInstrs1 = value;
    }

}
