
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MultipleJournalMasterTrx complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MultipleJournalMasterTrx">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BatchNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHECHKERID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CHKDTTIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompMis9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FUNCTIONID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKDTTIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAKER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MSGSTAT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MsgId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RelativeAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalCredit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalDebit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDetails" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans}ArrayOfMultipleJournalDetailTrx" minOccurs="0"/>
 *         &lt;element name="TransactionMisCode1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionMisCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MultipleJournalMasterTrx", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", propOrder = {
    "action",
    "batchNumber",
    "chechkerid",
    "chkdttime",
    "compMis1",
    "compMis10",
    "compMis2",
    "compMis3",
    "compMis4",
    "compMis5",
    "compMis6",
    "compMis7",
    "compMis8",
    "compMis9",
    "currencyCode",
    "currencyNumber",
    "functionid",
    "makdttime",
    "maker",
    "msgstat",
    "msgId",
    "relativeAccount",
    "source",
    "totalCredit",
    "totalDebit",
    "transactionDetails",
    "transactionMisCode1",
    "transactionMisCode2",
    "userBranchCode",
    "userId",
    "valueDate"
})
public class MultipleJournalMasterTrx {

    @XmlElementRef(name = "ACTION", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> action;
    @XmlElementRef(name = "BatchNumber", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> batchNumber;
    @XmlElementRef(name = "CHECHKERID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> chechkerid;
    @XmlElementRef(name = "CHKDTTIME", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> chkdttime;
    @XmlElementRef(name = "CompMis1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis1;
    @XmlElementRef(name = "CompMis10", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis10;
    @XmlElementRef(name = "CompMis2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis2;
    @XmlElementRef(name = "CompMis3", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis3;
    @XmlElementRef(name = "CompMis4", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis4;
    @XmlElementRef(name = "CompMis5", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis5;
    @XmlElementRef(name = "CompMis6", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis6;
    @XmlElementRef(name = "CompMis7", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis7;
    @XmlElementRef(name = "CompMis8", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis8;
    @XmlElementRef(name = "CompMis9", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> compMis9;
    @XmlElementRef(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyCode;
    @XmlElementRef(name = "CurrencyNumber", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencyNumber;
    @XmlElementRef(name = "FUNCTIONID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> functionid;
    @XmlElementRef(name = "MAKDTTIME", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> makdttime;
    @XmlElementRef(name = "MAKER", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> maker;
    @XmlElementRef(name = "MSGSTAT", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgstat;
    @XmlElementRef(name = "MsgId", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> msgId;
    @XmlElementRef(name = "RelativeAccount", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> relativeAccount;
    @XmlElementRef(name = "Source", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> source;
    @XmlElementRef(name = "TotalCredit", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> totalCredit;
    @XmlElementRef(name = "TotalDebit", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> totalDebit;
    @XmlElementRef(name = "TransactionDetails", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfMultipleJournalDetailTrx> transactionDetails;
    @XmlElementRef(name = "TransactionMisCode1", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionMisCode1;
    @XmlElementRef(name = "TransactionMisCode2", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionMisCode2;
    @XmlElementRef(name = "UserBranchCode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userBranchCode;
    @XmlElementRef(name = "UserId", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userId;
    @XmlElementRef(name = "ValueDate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Beans", type = JAXBElement.class, required = false)
    protected JAXBElement<String> valueDate;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getACTION() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setACTION(JAXBElement<String> value) {
        this.action = value;
    }

    /**
     * Gets the value of the batchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBatchNumber() {
        return batchNumber;
    }

    /**
     * Sets the value of the batchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBatchNumber(JAXBElement<String> value) {
        this.batchNumber = value;
    }

    /**
     * Gets the value of the chechkerid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCHECHKERID() {
        return chechkerid;
    }

    /**
     * Sets the value of the chechkerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCHECHKERID(JAXBElement<String> value) {
        this.chechkerid = value;
    }

    /**
     * Gets the value of the chkdttime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCHKDTTIME() {
        return chkdttime;
    }

    /**
     * Sets the value of the chkdttime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCHKDTTIME(JAXBElement<String> value) {
        this.chkdttime = value;
    }

    /**
     * Gets the value of the compMis1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis1() {
        return compMis1;
    }

    /**
     * Sets the value of the compMis1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis1(JAXBElement<String> value) {
        this.compMis1 = value;
    }

    /**
     * Gets the value of the compMis10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis10() {
        return compMis10;
    }

    /**
     * Sets the value of the compMis10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis10(JAXBElement<String> value) {
        this.compMis10 = value;
    }

    /**
     * Gets the value of the compMis2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis2() {
        return compMis2;
    }

    /**
     * Sets the value of the compMis2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis2(JAXBElement<String> value) {
        this.compMis2 = value;
    }

    /**
     * Gets the value of the compMis3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis3() {
        return compMis3;
    }

    /**
     * Sets the value of the compMis3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis3(JAXBElement<String> value) {
        this.compMis3 = value;
    }

    /**
     * Gets the value of the compMis4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis4() {
        return compMis4;
    }

    /**
     * Sets the value of the compMis4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis4(JAXBElement<String> value) {
        this.compMis4 = value;
    }

    /**
     * Gets the value of the compMis5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis5() {
        return compMis5;
    }

    /**
     * Sets the value of the compMis5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis5(JAXBElement<String> value) {
        this.compMis5 = value;
    }

    /**
     * Gets the value of the compMis6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis6() {
        return compMis6;
    }

    /**
     * Sets the value of the compMis6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis6(JAXBElement<String> value) {
        this.compMis6 = value;
    }

    /**
     * Gets the value of the compMis7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis7() {
        return compMis7;
    }

    /**
     * Sets the value of the compMis7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis7(JAXBElement<String> value) {
        this.compMis7 = value;
    }

    /**
     * Gets the value of the compMis8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis8() {
        return compMis8;
    }

    /**
     * Sets the value of the compMis8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis8(JAXBElement<String> value) {
        this.compMis8 = value;
    }

    /**
     * Gets the value of the compMis9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompMis9() {
        return compMis9;
    }

    /**
     * Sets the value of the compMis9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompMis9(JAXBElement<String> value) {
        this.compMis9 = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyCode(JAXBElement<String> value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the currencyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencyNumber() {
        return currencyNumber;
    }

    /**
     * Sets the value of the currencyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencyNumber(JAXBElement<String> value) {
        this.currencyNumber = value;
    }

    /**
     * Gets the value of the functionid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFUNCTIONID() {
        return functionid;
    }

    /**
     * Sets the value of the functionid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFUNCTIONID(JAXBElement<String> value) {
        this.functionid = value;
    }

    /**
     * Gets the value of the makdttime property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMAKDTTIME() {
        return makdttime;
    }

    /**
     * Sets the value of the makdttime property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMAKDTTIME(JAXBElement<String> value) {
        this.makdttime = value;
    }

    /**
     * Gets the value of the maker property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMAKER() {
        return maker;
    }

    /**
     * Sets the value of the maker property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMAKER(JAXBElement<String> value) {
        this.maker = value;
    }

    /**
     * Gets the value of the msgstat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMSGSTAT() {
        return msgstat;
    }

    /**
     * Sets the value of the msgstat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMSGSTAT(JAXBElement<String> value) {
        this.msgstat = value;
    }

    /**
     * Gets the value of the msgId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMsgId() {
        return msgId;
    }

    /**
     * Sets the value of the msgId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMsgId(JAXBElement<String> value) {
        this.msgId = value;
    }

    /**
     * Gets the value of the relativeAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRelativeAccount() {
        return relativeAccount;
    }

    /**
     * Sets the value of the relativeAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRelativeAccount(JAXBElement<String> value) {
        this.relativeAccount = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSource(JAXBElement<String> value) {
        this.source = value;
    }

    /**
     * Gets the value of the totalCredit property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTotalCredit() {
        return totalCredit;
    }

    /**
     * Sets the value of the totalCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTotalCredit(JAXBElement<String> value) {
        this.totalCredit = value;
    }

    /**
     * Gets the value of the totalDebit property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTotalDebit() {
        return totalDebit;
    }

    /**
     * Sets the value of the totalDebit property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTotalDebit(JAXBElement<String> value) {
        this.totalDebit = value;
    }

    /**
     * Gets the value of the transactionDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfMultipleJournalDetailTrx }{@code >}
     *     
     */
    public JAXBElement<ArrayOfMultipleJournalDetailTrx> getTransactionDetails() {
        return transactionDetails;
    }

    /**
     * Sets the value of the transactionDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfMultipleJournalDetailTrx }{@code >}
     *     
     */
    public void setTransactionDetails(JAXBElement<ArrayOfMultipleJournalDetailTrx> value) {
        this.transactionDetails = value;
    }

    /**
     * Gets the value of the transactionMisCode1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransactionMisCode1() {
        return transactionMisCode1;
    }

    /**
     * Sets the value of the transactionMisCode1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransactionMisCode1(JAXBElement<String> value) {
        this.transactionMisCode1 = value;
    }

    /**
     * Gets the value of the transactionMisCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransactionMisCode2() {
        return transactionMisCode2;
    }

    /**
     * Sets the value of the transactionMisCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransactionMisCode2(JAXBElement<String> value) {
        this.transactionMisCode2 = value;
    }

    /**
     * Gets the value of the userBranchCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserBranchCode() {
        return userBranchCode;
    }

    /**
     * Sets the value of the userBranchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserBranchCode(JAXBElement<String> value) {
        this.userBranchCode = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserId(JAXBElement<String> value) {
        this.userId = value;
    }

    /**
     * Gets the value of the valueDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getValueDate() {
        return valueDate;
    }

    /**
     * Sets the value of the valueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setValueDate(JAXBElement<String> value) {
        this.valueDate = value;
    }

}
