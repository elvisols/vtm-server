
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanInfoSummaryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanInfoSummaryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanInfoSummary" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfLoanInfoSummaryLines" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanInfoSummaryResponse", propOrder = {
    "loanInfoSummary"
})
public class LoanInfoSummaryResponse {

    @XmlElementRef(name = "LoanInfoSummary", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLoanInfoSummaryLines> loanInfoSummary;

    /**
     * Gets the value of the loanInfoSummary property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLoanInfoSummaryLines }{@code >}
     *     
     */
    public JAXBElement<ArrayOfLoanInfoSummaryLines> getLoanInfoSummary() {
        return loanInfoSummary;
    }

    /**
     * Sets the value of the loanInfoSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfLoanInfoSummaryLines }{@code >}
     *     
     */
    public void setLoanInfoSummary(JAXBElement<ArrayOfLoanInfoSummaryLines> value) {
        this.loanInfoSummary = value;
    }

}
