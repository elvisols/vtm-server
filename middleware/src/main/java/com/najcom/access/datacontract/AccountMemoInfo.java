
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AccountMemoInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountMemoInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cod_last_mnt_chkrid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_last_mnt_makerid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dat_last_mntx" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datlastmnt" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="memotext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountMemoInfo", propOrder = {
    "codLastMntChkrid",
    "codLastMntMakerid",
    "custid",
    "datLastMntx",
    "datlastmnt",
    "memotext"
})
public class AccountMemoInfo {

    @XmlElementRef(name = "cod_last_mnt_chkrid", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codLastMntChkrid;
    @XmlElementRef(name = "cod_last_mnt_makerid", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codLastMntMakerid;
    @XmlElementRef(name = "custid", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> custid;
    @XmlElementRef(name = "dat_last_mntx", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> datLastMntx;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datlastmnt;
    @XmlElementRef(name = "memotext", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> memotext;

    /**
     * Gets the value of the codLastMntChkrid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodLastMntChkrid() {
        return codLastMntChkrid;
    }

    /**
     * Sets the value of the codLastMntChkrid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodLastMntChkrid(JAXBElement<String> value) {
        this.codLastMntChkrid = value;
    }

    /**
     * Gets the value of the codLastMntMakerid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodLastMntMakerid() {
        return codLastMntMakerid;
    }

    /**
     * Sets the value of the codLastMntMakerid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodLastMntMakerid(JAXBElement<String> value) {
        this.codLastMntMakerid = value;
    }

    /**
     * Gets the value of the custid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustid() {
        return custid;
    }

    /**
     * Sets the value of the custid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustid(JAXBElement<String> value) {
        this.custid = value;
    }

    /**
     * Gets the value of the datLastMntx property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDatLastMntx() {
        return datLastMntx;
    }

    /**
     * Sets the value of the datLastMntx property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDatLastMntx(JAXBElement<String> value) {
        this.datLastMntx = value;
    }

    /**
     * Gets the value of the datlastmnt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatlastmnt() {
        return datlastmnt;
    }

    /**
     * Sets the value of the datlastmnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatlastmnt(XMLGregorianCalendar value) {
        this.datlastmnt = value;
    }

    /**
     * Gets the value of the memotext property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMemotext() {
        return memotext;
    }

    /**
     * Sets the value of the memotext property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMemotext(JAXBElement<String> value) {
        this.memotext = value;
    }

}
