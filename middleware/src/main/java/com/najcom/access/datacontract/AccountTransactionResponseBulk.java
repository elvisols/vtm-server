
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountTransactionResponseBulk complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountTransactionResponseBulk">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountTransactionResponses" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfAccountSummaryAndTrxResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountTransactionResponseBulk", propOrder = {
    "accountTransactionResponses"
})
public class AccountTransactionResponseBulk {

    @XmlElementRef(name = "AccountTransactionResponses", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfAccountSummaryAndTrxResponse> accountTransactionResponses;

    /**
     * Gets the value of the accountTransactionResponses property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxResponse }{@code >}
     *     
     */
    public JAXBElement<ArrayOfAccountSummaryAndTrxResponse> getAccountTransactionResponses() {
        return accountTransactionResponses;
    }

    /**
     * Sets the value of the accountTransactionResponses property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfAccountSummaryAndTrxResponse }{@code >}
     *     
     */
    public void setAccountTransactionResponses(JAXBElement<ArrayOfAccountSummaryAndTrxResponse> value) {
        this.accountTransactionResponses = value;
    }

}
