
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfNubanAcctsDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfNubanAcctsDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NubanAcctsDetails" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}NubanAcctsDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfNubanAcctsDetails", propOrder = {
    "nubanAcctsDetails"
})
public class ArrayOfNubanAcctsDetails {

    @XmlElement(name = "NubanAcctsDetails", nillable = true)
    protected List<NubanAcctsDetails> nubanAcctsDetails;

    /**
     * Gets the value of the nubanAcctsDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nubanAcctsDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNubanAcctsDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NubanAcctsDetails }
     * 
     * 
     */
    public List<NubanAcctsDetails> getNubanAcctsDetails() {
        if (nubanAcctsDetails == null) {
            nubanAcctsDetails = new ArrayList<NubanAcctsDetails>();
        }
        return this.nubanAcctsDetails;
    }

}
