
package com.najcom.access.datacontract;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TransactionRefNoDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionRefNoDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountbranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountclass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountopendate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="batchno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencycode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="drcr_ind" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventdesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventserialno" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="traninitiationdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="transactionamount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="transactioncode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactiondate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="transactionrefno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valuedate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionRefNoDetails", propOrder = {
    "accountbranch",
    "accountclass",
    "accountname",
    "accountno",
    "accountopendate",
    "batchno",
    "currencycode",
    "customerID",
    "drcrInd",
    "eventdesc",
    "eventserialno",
    "traninitiationdate",
    "transactionamount",
    "transactioncode",
    "transactiondate",
    "transactionrefno",
    "valuedate"
})
public class TransactionRefNoDetails {

    @XmlElementRef(name = "accountbranch", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountbranch;
    @XmlElementRef(name = "accountclass", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountclass;
    @XmlElementRef(name = "accountname", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountname;
    @XmlElementRef(name = "accountno", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> accountno;
    @XmlElementRef(name = "accountopendate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> accountopendate;
    @XmlElementRef(name = "batchno", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> batchno;
    @XmlElementRef(name = "currencycode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> currencycode;
    @XmlElementRef(name = "customerID", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerID;
    @XmlElementRef(name = "drcr_ind", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drcrInd;
    @XmlElementRef(name = "eventdesc", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> eventdesc;
    @XmlElementRef(name = "eventserialno", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> eventserialno;
    @XmlElementRef(name = "traninitiationdate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> traninitiationdate;
    @XmlElementRef(name = "transactionamount", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> transactionamount;
    @XmlElementRef(name = "transactioncode", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactioncode;
    @XmlElementRef(name = "transactiondate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> transactiondate;
    @XmlElementRef(name = "transactionrefno", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionrefno;
    @XmlElementRef(name = "valuedate", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> valuedate;

    /**
     * Gets the value of the accountbranch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountbranch() {
        return accountbranch;
    }

    /**
     * Sets the value of the accountbranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountbranch(JAXBElement<String> value) {
        this.accountbranch = value;
    }

    /**
     * Gets the value of the accountclass property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountclass() {
        return accountclass;
    }

    /**
     * Sets the value of the accountclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountclass(JAXBElement<String> value) {
        this.accountclass = value;
    }

    /**
     * Gets the value of the accountname property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountname() {
        return accountname;
    }

    /**
     * Sets the value of the accountname property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountname(JAXBElement<String> value) {
        this.accountname = value;
    }

    /**
     * Gets the value of the accountno property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAccountno() {
        return accountno;
    }

    /**
     * Sets the value of the accountno property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAccountno(JAXBElement<String> value) {
        this.accountno = value;
    }

    /**
     * Gets the value of the accountopendate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAccountopendate() {
        return accountopendate;
    }

    /**
     * Sets the value of the accountopendate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAccountopendate(JAXBElement<XMLGregorianCalendar> value) {
        this.accountopendate = value;
    }

    /**
     * Gets the value of the batchno property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBatchno() {
        return batchno;
    }

    /**
     * Sets the value of the batchno property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBatchno(JAXBElement<String> value) {
        this.batchno = value;
    }

    /**
     * Gets the value of the currencycode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCurrencycode() {
        return currencycode;
    }

    /**
     * Sets the value of the currencycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCurrencycode(JAXBElement<String> value) {
        this.currencycode = value;
    }

    /**
     * Gets the value of the customerID property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerID() {
        return customerID;
    }

    /**
     * Sets the value of the customerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerID(JAXBElement<String> value) {
        this.customerID = value;
    }

    /**
     * Gets the value of the drcrInd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrcrInd() {
        return drcrInd;
    }

    /**
     * Sets the value of the drcrInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrcrInd(JAXBElement<String> value) {
        this.drcrInd = value;
    }

    /**
     * Gets the value of the eventdesc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEventdesc() {
        return eventdesc;
    }

    /**
     * Sets the value of the eventdesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEventdesc(JAXBElement<String> value) {
        this.eventdesc = value;
    }

    /**
     * Gets the value of the eventserialno property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getEventserialno() {
        return eventserialno;
    }

    /**
     * Sets the value of the eventserialno property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setEventserialno(JAXBElement<BigDecimal> value) {
        this.eventserialno = value;
    }

    /**
     * Gets the value of the traninitiationdate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTraninitiationdate() {
        return traninitiationdate;
    }

    /**
     * Sets the value of the traninitiationdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTraninitiationdate(JAXBElement<XMLGregorianCalendar> value) {
        this.traninitiationdate = value;
    }

    /**
     * Gets the value of the transactionamount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getTransactionamount() {
        return transactionamount;
    }

    /**
     * Sets the value of the transactionamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setTransactionamount(JAXBElement<BigDecimal> value) {
        this.transactionamount = value;
    }

    /**
     * Gets the value of the transactioncode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransactioncode() {
        return transactioncode;
    }

    /**
     * Sets the value of the transactioncode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransactioncode(JAXBElement<String> value) {
        this.transactioncode = value;
    }

    /**
     * Gets the value of the transactiondate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTransactiondate() {
        return transactiondate;
    }

    /**
     * Sets the value of the transactiondate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTransactiondate(JAXBElement<XMLGregorianCalendar> value) {
        this.transactiondate = value;
    }

    /**
     * Gets the value of the transactionrefno property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransactionrefno() {
        return transactionrefno;
    }

    /**
     * Sets the value of the transactionrefno property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransactionrefno(JAXBElement<String> value) {
        this.transactionrefno = value;
    }

    /**
     * Gets the value of the valuedate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getValuedate() {
        return valuedate;
    }

    /**
     * Sets the value of the valuedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setValuedate(JAXBElement<XMLGregorianCalendar> value) {
        this.valuedate = value;
    }

}
