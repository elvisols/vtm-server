
package com.najcom.access.datacontract;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerODLimitResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerODLimitResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerODLimitTransactions" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}ArrayOfCustomerODLimitLines" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerODLimitResponse", propOrder = {
    "customerODLimitTransactions"
})
public class CustomerODLimitResponse {

    @XmlElementRef(name = "CustomerODLimitTransactions", namespace = "http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomerODLimitLines> customerODLimitTransactions;

    /**
     * Gets the value of the customerODLimitTransactions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerODLimitLines }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomerODLimitLines> getCustomerODLimitTransactions() {
        return customerODLimitTransactions;
    }

    /**
     * Sets the value of the customerODLimitTransactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomerODLimitLines }{@code >}
     *     
     */
    public void setCustomerODLimitTransactions(JAXBElement<ArrayOfCustomerODLimitLines> value) {
        this.customerODLimitTransactions = value;
    }

}
