
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFixedDepositInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFixedDepositInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FixedDepositInfo" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}FixedDepositInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFixedDepositInfo", propOrder = {
    "fixedDepositInfo"
})
public class ArrayOfFixedDepositInfo {

    @XmlElement(name = "FixedDepositInfo", nillable = true)
    protected List<FixedDepositInfo> fixedDepositInfo;

    /**
     * Gets the value of the fixedDepositInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fixedDepositInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFixedDepositInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FixedDepositInfo }
     * 
     * 
     */
    public List<FixedDepositInfo> getFixedDepositInfo() {
        if (fixedDepositInfo == null) {
            fixedDepositInfo = new ArrayList<FixedDepositInfo>();
        }
        return this.fixedDepositInfo;
    }

}
