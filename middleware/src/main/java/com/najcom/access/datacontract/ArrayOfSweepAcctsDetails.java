
package com.najcom.access.datacontract;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfSweepAcctsDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfSweepAcctsDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SweepAcctsDetails" type="{http://schemas.datacontract.org/2004/07/FCUBS.Webservices.Shared.Responses}SweepAcctsDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfSweepAcctsDetails", propOrder = {
    "sweepAcctsDetails"
})
public class ArrayOfSweepAcctsDetails {

    @XmlElement(name = "SweepAcctsDetails", nillable = true)
    protected List<SweepAcctsDetails> sweepAcctsDetails;

    /**
     * Gets the value of the sweepAcctsDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sweepAcctsDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSweepAcctsDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SweepAcctsDetails }
     * 
     * 
     */
    public List<SweepAcctsDetails> getSweepAcctsDetails() {
        if (sweepAcctsDetails == null) {
            sweepAcctsDetails = new ArrayList<SweepAcctsDetails>();
        }
        return this.sweepAcctsDetails;
    }

}
