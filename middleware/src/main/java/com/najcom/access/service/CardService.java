package com.najcom.access.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBElement;

import com.najcom.access.card.BlockRequest;
import com.najcom.access.card.CardEnquiryRequest;
import com.najcom.access.card.CardEnquiryResponse;
import com.najcom.access.card.CardInfo;
import com.najcom.access.card.CardIssuance;
import com.najcom.access.card.CardIssueRequest;
import com.najcom.access.card.CardIssueResponse;
import com.najcom.access.card.CardList;
import com.najcom.access.card.CardProductRequest;
import com.najcom.access.card.CardProductResponse;
import com.najcom.access.card.HotlistCode;
import com.najcom.access.card.ICardIssuance;
import com.najcom.access.card.IssueRequest;
import com.najcom.access.card.ObjectFactory;
import com.najcom.access.logger.JaxWsHandlerResolver;
import com.najcom.access.tempuri.IPostingService;
import com.najcom.access.tempuri.PostingService;


public class CardService {
	
	ObjectFactory objectFactory = new ObjectFactory();
	
	private final static ICardIssuance SERVICE;
	private final int SUCCESS_CODE = 200;
	private final int ERROR_CODE = 500;
	
	static {
        CardIssuance iCardInstance = CardIssuance.getInstance();
        iCardInstance.setHandlerResolver(new JaxWsHandlerResolver());
        SERVICE = iCardInstance.getBasicHttpBindingICardIssuance();
    }
	
    private Map<String, Object> getCardEnquiry(HttpServletRequest request) {
    	CardEnquiryRequest req =  new CardEnquiryRequest();
//    	req.setAccountNumber(value); //JAXBElement<String>
//    	req.setCardType(value); //Integer
    	CardEnquiryResponse res = SERVICE.getCustomerCards(req);
    	Map<String, Object> cardEnquiry = new HashMap<String, Object>();
    	cardEnquiry.put("cardEnquiry", "It works!!!" + res);
    	return cardEnquiry;
    }
	
	private Map<String, Object> getCardProducts(HttpServletRequest request) {
		CardProductRequest req =  new CardProductRequest();
		CardProductResponse res = SERVICE.retrieveAllCardProducts(req);
		Map<String, Object> cardProduct = new HashMap<String, Object>();
		cardProduct.put("cardProduct", "It works!!!" + res);
		return cardProduct;
	}
	
    public Map<String, Object> blockCard(Map<String, String> bc) {
		BlockRequest req = new BlockRequest();
		req.setBlockCode(HotlistCode.UNSPECIFIED);
		req.setModuleName(objectFactory.createBlockRequestModuleName(bc.getOrDefault("moduleName", null))); // JAXBElement<String>
		req.setCardNumber(objectFactory.createBlockRequestCardNumber(bc.getOrDefault("cardNumber", null))); // "
		req.setExpiryDate(objectFactory.createBlockRequestExpiryDate(bc.getOrDefault("expiryDate", null))); // "
		req.setUserID(objectFactory.createBlockRequestUserID(bc.getOrDefault("userID", null))); // "
		String res = SERVICE.blockCard(req);
        Map<String, Object> blockCard = new HashMap<String, Object>();
        blockCard.put("status", SUCCESS_CODE);
        blockCard.put("message", res);
        return blockCard;
    }
	
	private Map<String, Object> unblockCard(HttpServletRequest request) {
//    	Transfer transferBody = req.getBody();
		BlockRequest req = new BlockRequest();
//		HotlistCode v;
//		req.setBlockCode(v);
//		req.setModuleName(value); // JAXBElement<String>
//		req.setCardNumber(value); // "
//		req.setExpiryDate(value); // "
//		req.setUserID(value); // "
		String res = SERVICE.unBlockCard(req);
		Map<String, Object> unBlockCard = new HashMap<String, Object>();
		return unBlockCard;
	}
	
	public Map<String, Object> issueDebitCard(Map<String, String> idc) {
		CardIssueRequest req = new CardIssueRequest();
		IssueRequest ir = new IssueRequest();
		ir.setAccountNumber(objectFactory.createIssueRequestAccountNumber(idc.getOrDefault("accountNumber", null))); // JAXBElement<String>
		ir.setCardProductID(Long.parseLong(idc.getOrDefault("cardProductID", "1"))); // Long
		ir.setChargeWaived(Boolean.parseBoolean(idc.getOrDefault("chargeWaived", "1"))); // Boolean
		ir.setCustomerID(objectFactory.createIssueRequestCustomerID(idc.getOrDefault("customerID", null))); // JAXBElement<String>
		ir.setIsAuxilliaryCard(Boolean.parseBoolean(idc.getOrDefault("isAuxilliaryCard", "0"))); // Boolean
		ir.setNameOnCard(objectFactory.createIssueRequestNameOnCard(idc.getOrDefault("nameOnCard", "Great Anonymous"))); // JAXBElement<String>
		ir.setPan(objectFactory.createIssueRequestPan(idc.getOrDefault("pan", null))); // JAXBElement<String>
		ir.setPickupBranch(objectFactory.createIssueRequestPickupBranch(idc.getOrDefault("pickupBranch", null))); // JAXBElement<String>
		ir.setPictureId(objectFactory.createIssueRequestPictureId(idc.getOrDefault("pictureId", null))); // JAXBElement<String>
		ir.setProductCode(objectFactory.createIssueRequestProductCode(idc.getOrDefault("productCode", null))); // JAXBElement<String>
		ir.setUserID(objectFactory.createIssueRequestUserID(idc.getOrDefault("userID", null))); // JAXBElement<String>
		ir.setWaiveReason(objectFactory.createIssueRequestWaiveReason(idc.getOrDefault("waiverReason", null))); // JAXBElement<String>
		JAXBElement<IssueRequest> jIRq = objectFactory.createIssueRequest(ir);
		JAXBElement<String> jStr = objectFactory.createCardIssueRequestApplicationName("AppName");
		req.setRequest(jIRq); // JAXBElement<IssueRequest>
		req.setApplicationName(jStr); // JAXBElement<String>
		CardIssueResponse res = SERVICE.issueDebitCard(req);
		Map<String, Object> issueDebitCard = new HashMap<String, Object>();
		Map<String, String> tmp = new HashMap<>();
		tmp.put("responseCode", res.getResponseCode().getValue());
		tmp.put("responseData", res.getResponseMessage().getValue());
		tmp.put("holdID", res.getHoldID().getValue());
		issueDebitCard.put("message", tmp);
		return issueDebitCard;
	}
	
	private Map<String, Object> issuePrepaidCard(HttpServletRequest request) {
//    	Transfer transferBody = req.getBody();
		CardIssueRequest req = new CardIssueRequest();
		IssueRequest ir = new IssueRequest();
//		ir.setAccountNumber(value); // JAXBElement<String>
//		ir.setCardProductID(value); // Long
//		ir.setChargeWaived(value); // Boolean
//		ir.setCustomerID(value); // JAXBElement<String>
//		ir.setIsAuxilliaryCard(value); // Boolean
//		ir.setNameOnCard(value); // JAXBElement<String>
//		ir.setPan(value); // JAXBElement<String>
//		ir.setPickupBranch(value); // JAXBElement<String>
//		ir.setPictureId(value); // JAXBElement<String>
//		ir.setProductCode(value); // JAXBElement<String>
//		ir.setUserID(value); // JAXBElement<String>
//		ir.setWaiveReason(value); // JAXBElement<String>
		JAXBElement<IssueRequest> jIRq = objectFactory.createIssueRequest(ir);
		JAXBElement<String> jStr = objectFactory.createCardIssueRequestApplicationName("AppName");
		req.setRequest(jIRq); // JAXBElement<IssueRequest>
		req.setApplicationName(jStr); // JAXBElement<String>
		CardIssueResponse res = SERVICE.issuePrepaidCard(req);
		Map<String, Object> issuePrepaidCard = new HashMap<String, Object>();
		return issuePrepaidCard;
	}
	
	private Map<String, Object> issueVirtualCard(HttpServletRequest request) {
//    	Transfer transferBody = req.getBody();
		CardIssueRequest req = new CardIssueRequest();
		IssueRequest ir = new IssueRequest();
//		ir.setAccountNumber(value); // JAXBElement<String>
//		ir.setCardProductID(value); // Long
//		ir.setChargeWaived(value); // Boolean
//		ir.setCustomerID(value); // JAXBElement<String>
//		ir.setIsAuxilliaryCard(value); // Boolean
//		ir.setNameOnCard(value); // JAXBElement<String>
//		ir.setPan(value); // JAXBElement<String>
//		ir.setPickupBranch(value); // JAXBElement<String>
//		ir.setPictureId(value); // JAXBElement<String>
//		ir.setProductCode(value); // JAXBElement<String>
//		ir.setUserID(value); // JAXBElement<String>
//		ir.setWaiveReason(value); // JAXBElement<String>
		JAXBElement<IssueRequest> jIRq = objectFactory.createIssueRequest(ir);
		JAXBElement<String> jStr = objectFactory.createCardIssueRequestApplicationName("AppName");
		req.setRequest(jIRq); // JAXBElement<IssueRequest>
		req.setApplicationName(jStr); // JAXBElement<String>
		CardIssueResponse res = SERVICE.issueVirtualCard(req);
		Map<String, Object> issueVirtualCard = new HashMap<String, Object>();
		return issueVirtualCard;
	}
	
}
