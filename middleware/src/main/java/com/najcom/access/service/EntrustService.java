package com.najcom.access.service;

import java.util.HashMap;
import java.util.Map;

import com.najcom.access.card.ObjectFactory;
import com.najcom.access.entrust.Service;
import com.najcom.access.entrust.ServiceSoap;
import com.najcom.access.logger.JaxWsHandlerResolver;


public class EntrustService {
	
	Service entrustService;
	
	private static ServiceSoap SERVICE;
	private final int SUCCESS_CODE = 200;
	private final int FAIL_CODE = -1;
	
	static {
		Service eInstance = Service.getInstance();
		eInstance.setHandlerResolver(new JaxWsHandlerResolver());
        SERVICE = eInstance.getServiceSoap();
    }
	
	ObjectFactory objectFactory = new ObjectFactory();
	
    public Map<String, Object> getResponseOnly(Map<String, String> userDetail) {
    	String res = SERVICE.responseOnly(userDetail.getOrDefault("username", null), userDetail.getOrDefault("password", null));
    	Map<String, Object> resOnly = new HashMap<String, Object>();
    	if(res.startsWith("0")) {
    		resOnly.put("status", SUCCESS_CODE);	
    	} else {
    		resOnly.put("status", FAIL_CODE);	
    	}
    	resOnly.put("message", res);
    	return resOnly;
    }
	
	public Map<String, Object> getResponseOnlyForGroup(Map<String, String> userDetail) {
		String res = SERVICE.responseOnlyForGroup(userDetail.getOrDefault("username", null), userDetail.getOrDefault("password", null), userDetail.getOrDefault("groupName", null));
		Map<String, Object> resOnly4G = new HashMap<String, Object>();
		resOnly4G.put("status", SUCCESS_CODE);
    	resOnly4G.put("message", res);
    	return resOnly4G;
	}
	
}
