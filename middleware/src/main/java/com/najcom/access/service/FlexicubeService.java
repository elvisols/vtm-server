package com.najcom.access.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;

import com.najcom.access.datacontract.AccountCotAndVatResponse;
import com.najcom.access.datacontract.AccountRelatedResponse;
import com.najcom.access.datacontract.AccountSearchResponse;
import com.najcom.access.datacontract.AccountSearchResponseLines;
import com.najcom.access.datacontract.AccountSummaryAndTrxLines;
import com.najcom.access.datacontract.AccountSummaryAndTrxResponse;
import com.najcom.access.datacontract.AccountSummaryLine;
import com.najcom.access.datacontract.AccountSummaryResponse;
import com.najcom.access.datacontract.AccountTransactionResponseBulk;
import com.najcom.access.datacontract.ActivateChequeBookResponse;
import com.najcom.access.datacontract.ArrayOfAccountSearchResponseLines;
import com.najcom.access.datacontract.ArrayOfAccountSummaryAndTrxLines;
import com.najcom.access.datacontract.ArrayOfAccountSummaryLine;
import com.najcom.access.datacontract.ArrayOfCustomerAcctsDetailResponse;
import com.najcom.access.datacontract.ArrayOfImageCollectionDetails;
import com.najcom.access.datacontract.ArrayOfSingleTransactionDetails;
import com.najcom.access.datacontract.AuthenticateCBAUserResp;
import com.najcom.access.datacontract.CreateCustAccountResponse;
import com.najcom.access.datacontract.CreateCustomerResponse;
import com.najcom.access.datacontract.CurrencyRateResponse;
import com.najcom.access.datacontract.CustomerAcctsDetail;
import com.najcom.access.datacontract.CustomerAcctsDetailResponse;
import com.najcom.access.datacontract.CustomerDetails;
import com.najcom.access.datacontract.CustomerInformationResponse;
import com.najcom.access.datacontract.CustomerODLimitResponse;
import com.najcom.access.datacontract.FCUBSCGServiceCreateTransactionRequest;
import com.najcom.access.datacontract.FCUBSCreateCustAccountRequest;
import com.najcom.access.datacontract.FCUBSCreateCustomerRequest;
import com.najcom.access.datacontract.FCUBSCreateStopPaymentRequest;
import com.najcom.access.datacontract.FcubsResponse;
import com.najcom.access.datacontract.FixedDepositResponse;
import com.najcom.access.datacontract.GetAccountBranchResp;
import com.najcom.access.datacontract.GetAccountExistResponse;
import com.najcom.access.datacontract.GetAcctMemoInfoResponse;
import com.najcom.access.datacontract.GetChequeValueDateResp;
import com.najcom.access.datacontract.GetCurrencyCodeResp;
import com.najcom.access.datacontract.GetEffectiveBalanceResp;
import com.najcom.access.datacontract.GetUserBranchResp;
import com.najcom.access.datacontract.LoanInfoSummaryResponse;
import com.najcom.access.datacontract.LoanSummaryResponse;
import com.najcom.access.datacontract.LoanSummaryResponseBulk;
import com.najcom.access.datacontract.ObjectFactory;
import com.najcom.access.datacontract.RepaymentScheduleResponse;
import com.najcom.access.datacontract.SampleAccountResponse;
import com.najcom.access.datacontract.ScreenLimitResponse;
import com.najcom.access.datacontract.TellerLimitResponse;
import com.najcom.access.datacontract.UserLimitResponse;
import com.najcom.access.datacontract.VisaCardLimitResponse;
import com.najcom.access.datacontract.webservicewrappers.ChequeNoInquiryResponse;
import com.najcom.access.datacontract.webservicewrappers.MessageIDStatusResponse;
import com.najcom.access.datacontract.webservicewrappers.TransactionRefNoDetailsResponse;
import com.najcom.access.flex._12.Account;
import com.najcom.access.flex._12.AccountType;
import com.najcom.access.flex._12.FlexcubeUser;
import com.najcom.access.flex._12.ISOCurrency;
import com.najcom.access.flex._12_0.post.FlexPost;
import com.najcom.access.flex._12_0.post.FlexPostRsp;
import com.najcom.access.flex._12_0.post.IFlexPost;
import com.najcom.access.flex._12_0.post.SingleEntry;
import com.najcom.access.flex._12_0.post.SinglePostReq;
import com.najcom.access.logger.JaxWsHandlerResolver;
import com.najcom.access.microsoft.arrays.ArrayOfstring;
import com.najcom.access.tempuri.IPostingService;
import com.najcom.access.tempuri.PostingService;


public class FlexicubeService {
	
	ObjectFactory objectFactory = new ObjectFactory();
	
	private final static IPostingService SERVICE;
	private final static IFlexPost SERVICE_1;
	private final int SUCCESS_CODE = 200;
	private final int ERROR_CODE = 500;
	
	static {
        PostingService iPostingInstance = PostingService.getInstance();
        iPostingInstance.setHandlerResolver(new JaxWsHandlerResolver());
        SERVICE = iPostingInstance.getBasicHttpBindingIPostingService();
        FlexPost iFlexPostInstance = FlexPost.getInstance();
        iFlexPostInstance.setHandlerResolver(new JaxWsHandlerResolver());
        SERVICE_1 = iFlexPostInstance.getBasicHttpBindingIFlexPost();
    }
	
    private Map<String, Object> getAccordGL() {
    	String res = SERVICE.getAccordGL("accountNumber");
    	Map<String, Object> getAccordGL = new HashMap<String, Object>();
    	getAccordGL.put("cardEnquiry", "It works!!!" + res);
    	return getAccordGL;
    }
	
	private Map<String, Object> getCustomerInfoForIso() {
		String res = SERVICE.getCustomerInfoForIso("accountNumber");
		Map<String, Object> getCustomerInfoForIso = new HashMap<String, Object>();
		getCustomerInfoForIso.put("getCustomerInfoForIso", "It works!!!" + res);
		return getCustomerInfoForIso;
	}
	
	private Map<String, Object> getAccountBranch() {
		GetAccountBranchResp res = SERVICE.getAccountBranch("accountNumber");
		Map<String, Object> getAccountBranch = new HashMap<String, Object>();
		getAccountBranch.put("getAccountBranch", "It works!!!" + res);
		return getAccountBranch;
	}
	
	private Map<String, Object> getAccountMemoInfo() {
		GetAcctMemoInfoResponse res = SERVICE.getAccountMemoInfo("accountNumber");
		Map<String, Object> getAccountMemoInfo = new HashMap<String, Object>();
		getAccountMemoInfo.put("getAccountMemoInfo", "It works!!!" + res);
		return getAccountMemoInfo;
	}
	
	private Map<String, Object> getAccountSearchByAccountNumber() {
		AccountSearchResponse res = SERVICE.getAccountSearchByAccountNumber("accountNumber");
		Map<String, Object> getAccountSearchByAccountNumber = new HashMap<String, Object>();
		getAccountSearchByAccountNumber.put("getAccountSearchByAccountNumber", "It works!!!" + res);
		return getAccountSearchByAccountNumber;
	}
	
	private Map<String, Object> getAccountSearchByAccountTitle() {
		AccountSearchResponse res = SERVICE.getAccountSearchByAccountTitle("accountNumber");
		Map<String, Object> getAccountSearchByAccountTitle = new HashMap<String, Object>();
		getAccountSearchByAccountTitle.put("getAccountSearchByAccountNumber", "It works!!!" + res);
		return getAccountSearchByAccountTitle;
	}
	
	private Map<String, Object> getAccountSummary(String accountNumber) {
		AccountSummaryResponse res = SERVICE.getAccountSummary(accountNumber);
		Map<String, Object> getAccountSummary = new HashMap<String, Object>();
		ArrayOfAccountSummaryLine aas = res.getAccountSummaryLines().getValue();
		getAccountSummary.put("status", SUCCESS_CODE);
		List<Map<String, String>> accSAT = new ArrayList<>();
		for(AccountSummaryLine as: aas.getAccountSummaryLine()) {
			Map<String, String> tmp = new HashMap<>();
			tmp.put("accountName", as.getAccountName().getValue());
			tmp.put("accountNo", as.getAccountNo().getValue());
			accSAT.add(tmp);
		}
		getAccountSummary.put("message", accSAT);
		return getAccountSummary;
	}
	
	private Map<String, Object> getAccountSummaryByCustomerID() {
		AccountSummaryResponse res = SERVICE.getAccountSummaryByCustomerID("customerID");
		Map<String, Object> getAccountSummaryByCustomerID = new HashMap<String, Object>();
		getAccountSummaryByCustomerID.put("getAccountSummaryByCustomerID", "It works!!!" + res);
		return getAccountSummaryByCustomerID;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> getAccountSummaryAndTransactions(Map<String, String> req) {
		AccountSummaryAndTrxResponse res = SERVICE.getAccountSummaryAndTransactions(req.getOrDefault("accountNumber", null), req.getOrDefault("startDate", null), req.getOrDefault("endDate", null));
		Map<String, Object> getAccountSummaryAndTransactions = new HashMap<String, Object>();
		JAXBElement<ArrayOfAccountSummaryAndTrxLines> asat = res.getAccountSummaryAndTransactions();
		
		if(!asat.isNil()) {
			getAccountSummaryAndTransactions.put("status", SUCCESS_CODE);
			ArrayOfAccountSummaryAndTrxLines asatValue =  asat.getValue();
			List accSAT = new ArrayList<>();
			for(AccountSummaryAndTrxLines a: asatValue.getAccountSummaryAndTrxLines()) {
				Map<String, String> tmp = new HashMap<>();
				SimpleDateFormat fromAccess = new SimpleDateFormat("dd-MMM-yy");
				SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
	    			tmp.put("accountName", a.getAccountName().getValue());
	        		tmp.put("accountNumber", a.getAccountNumber().getValue());
	        		tmp.put("accountOpeningBalance", a.getAcctOpeningBalance().getValue());
	        		tmp.put("acEntrySrNo", a.getAcEntrySrNo().getValue());
	        		tmp.put("authId", a.getAuthId().getValue());
	        		tmp.put("workingBalance", a.getAvailableBalance().getValue());
	        		tmp.put("batchNo", a.getBatchNo().getValue());
	        		tmp.put("branchAddress", a.getBranchAddress().getValue());
	        		tmp.put("branchCode", a.getBranchCode().getValue());
	        		tmp.put("branch", a.getBranchName().getValue());
	        		tmp.put("currency", a.getCcyCode().getValue());
	        		tmp.put("clearedBalance", a.getClearedBalance().getValue());
	        		tmp.put("closingBalance", a.getClosingBalance().getValue());
	        		tmp.put("customerNumber", a.getCustomerNo().getValue());
	        		tmp.put("drCr", a.getDrCrInd().getValue());
	        		tmp.put("eventSrNo", a.getEventSrNo().getValue());
	        		tmp.put("externalRefNumber", a.getExternalRefNumber().getValue());
	        		tmp.put("fcyAmount", a.getFcyAmount().getValue());
	        		tmp.put("hasCOT", a.getHasCOT().getValue());
	        		tmp.put("instrumentCode", a.getInstrumentCode().getValue());
	        		tmp.put("lcyAmount", a.getLcyAmount().getValue());
	        		tmp.put("module", a.getModule().getValue());
	        		tmp.put("noofLoggement", a.getNoofLodgement().getValue());
	        		tmp.put("noofWithdrawal", a.getNoofWithdrawal().getValue());
					tmp.put("postedDate", myFormat.format(fromAccess.parse(a.getPostedDate().getValue())));
	        		tmp.put("accountType", a.getProductCode().getValue());
	        		tmp.put("productionCodeDesc", a.getProductCodeDesc().getValue());
	        		tmp.put("rowNumber", a.getRowNumber().getValue());
	        		tmp.put("runningBalance", a.getRunningBalance().getValue());
	        		tmp.put("stmtDt", a.getStmtDt().getValue());
	        		tmp.put("totalLodgement", a.getTotalLodgement().getValue());
	        		tmp.put("totalWithdrawal", a.getTotalWithdrawal().getValue());
	        		tmp.put("tranAmount", a.getTranAmount().getValue());
	        		tmp.put("totalTranDescription", a.getTranDescription().getValue());
	        		tmp.put("tranEvent", a.getTranEvent().getValue());
	        		tmp.put("tranIndicator", a.getTranIndicator().getValue());
	        		tmp.put("narration", a.getTranNarration().getValue());
	        		tmp.put("tranCode", a.getTransactionCode().getValue());
	        		tmp.put("tranDate", myFormat.format(fromAccess.parse(a.getTransactionDate().getValue())));
	        		tmp.put("trnRefNo", a.getTrnRefNo().getValue());
	        		XMLGregorianCalendar xmlCal = a.getTxnInitiationDate().getValue();
	        		tmp.put("txnInitiationDate", xmlCal.getYear() +"-"+xmlCal.getMonth()+"-"+xmlCal.getDay());
	        		tmp.put("unclearedBalance", a.getUnClearedBalance().getValue());
	        		tmp.put("unserId", a.getUserId().getValue());
	        		tmp.put("valueDate", myFormat.format(fromAccess.parse(a.getValueDate().getValue())));
	    			accSAT.add(tmp);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			getAccountSummaryAndTransactions.put("message", accSAT);
    	} else {
    		getAccountSummaryAndTransactions.put("status", ERROR_CODE);
    		getAccountSummaryAndTransactions.put("message", "Could not retrieve records!");
    	}
		return getAccountSummaryAndTransactions;
	}
	
	private Map<String, Object> getAccountSummaryAndTransactions2() {
		ArrayOfstring aS = new ArrayOfstring();
		AccountTransactionResponseBulk res = SERVICE.getAccountSummaryAndTransactions2(aS, "startDate", "endDate");
		Map<String, Object> getAccountSummaryAndTransactions2 = new HashMap<String, Object>();
		getAccountSummaryAndTransactions2.put("getAccountSummaryAndTransactions2", "It works!!!" + res);
		return getAccountSummaryAndTransactions2;
	}
	
	private Map<String, Object> getAccountTransactionStatement() {
		AccountSummaryAndTrxResponse res = SERVICE.getAccountTransactionStatement("accountNumber", "startDate", "endDate");
		Map<String, Object> getAccountTransactionStatement = new HashMap<String, Object>();
		getAccountTransactionStatement.put("getAccountSummaryAndTransactions2", "It works!!!" + res);
		return getAccountTransactionStatement;
	}
	
	private Map<String, Object> getChequeValueDate() {
		XMLGregorianCalendar txnDate = null;
		GetChequeValueDateResp res = SERVICE.getChequeValueDate("routingNo", "branchCode", txnDate);
		Map<String, Object> getChequeValueDate = new HashMap<String, Object>();
		getChequeValueDate.put("getAccountSummaryAndTransactions2", "It works!!!" + res);
		return getChequeValueDate;
	}
	
	private Map<String, Object> getCotVatDetails() {
		AccountCotAndVatResponse res = SERVICE.getCotVatDetails("accountNumber");
		Map<String, Object> getCotVatDetails = new HashMap<String, Object>();
		getCotVatDetails.put("getCotVatDetails", "It works!!!" + res);
		return  getCotVatDetails;
	}
	
	private Map<String, Object> getCurrencyCode() {
		GetCurrencyCodeResp res = SERVICE.getCurrencyCode("isoNumCcyCode");
		Map<String, Object> getCurrencyCode = new HashMap<String, Object>();
		getCurrencyCode.put("getCurrencyCode", "It works!!!" + res);
		if(getCurrencyCode.isEmpty()) {
			return null;
		}
		return (getCurrencyCode);
	}
	
	private Map<String, Object> getCurrencyRate() {
		CurrencyRateResponse res = SERVICE.getCurrencyRate("branchCode", "ccyCode1", "ccyCode2");
		Map<String, Object> getCurrencyRate = new HashMap<String, Object>();
		getCurrencyRate.put("getCurrencyRate", "It works!!!" + res);
		if(getCurrencyRate.isEmpty()) {
			return null;
		}
		return (getCurrencyRate);
	}
	
	public Map<String, Object> getCustomerAcctsDetail(String accountNumber) {
		CustomerAcctsDetailResponse res = SERVICE.getCustomerAcctsDetail(accountNumber);
		Map<String, Object> getCustomerAcctsDetail = new HashMap<String, Object>();
		getCustomerAcctsDetail.put("status", SUCCESS_CODE);
		
		Map<String, Object> tmp = new HashMap<>();
		tmp.put("responseCode", res.getResponseCode().getValue());
		tmp.put("responseData", res.getResponseData().getValue());
		List<CustomerAcctsDetail> lstCAD = res.getCustomerAcctsDetail().getValue().getCustomerAcctsDetail();
		List<Map<String, Object>> lstTmp = new ArrayList<>();
		for(CustomerAcctsDetail cad: lstCAD) {
			Map<String, Object> tmp2 = new HashMap<>();
			tmp2.put("accountName", cad.getAccountName().getValue());
			tmp2.put("accountNumber", cad.getAccountNo().getValue());
			tmp2.put("accountStatus", cad.getAccountStatus().getValue());
			tmp2.put("accountAvailableBalance", cad.getAvailableBalance().getValue());
			tmp2.put("accountBranchCode", cad.getBranchCode().getValue());
			tmp2.put("accountBranchName", cad.getBranchName().getValue());
			tmp2.put("accountCustomerID", cad.getCustID().getValue());
			tmp2.put("accountCustomerName", cad.getCustomerName().getValue());
			tmp2.put("accountCustomerNumber", cad.getCustomerNo().getValue());
			lstTmp.add(tmp2);
		}
		tmp.put("customerAcctsDetail", lstTmp);
		getCustomerAcctsDetail.put("message", tmp);
		return (getCustomerAcctsDetail);
	}
	
	private Map<String, Object> getCustomerAcctsDetailBulk() {
		ArrayOfstring aNList = new ArrayOfstring();
		ArrayOfCustomerAcctsDetailResponse res = SERVICE.getCustomerAcctsDetailBulk(aNList);
		Map<String, Object> getCustomerAcctsDetailBulk = new HashMap<String, Object>();
		getCustomerAcctsDetailBulk.put("getCustomerAcctsDetailBulk", "It works!!!" + res);
		if(getCustomerAcctsDetailBulk.isEmpty()) {
			return null;
		}
		return (getCustomerAcctsDetailBulk);
	}
	
	public Map<String, Object> getCustomerDetails(String customerID) {
		CustomerDetails res = SERVICE.getCustomerDetails(customerID);
		Map<String, Object> getCustomerDetails = new HashMap<String, Object>();
		getCustomerDetails.put("status", SUCCESS_CODE);
		Map<String, Object> tmp = new HashMap<>();
		tmp.put("responseCode", res.getResponseCode().getValue());
		tmp.put("responseData", res.getResponseData().getValue());
		tmp.put("customerEmail", res.getCustEmail().getValue());
		tmp.put("customerID", res.getCustid().getValue());
		tmp.put("customerNo", res.getCustomerNo().getValue());
		tmp.put("customerPhone", res.getCustPhone().getValue());
		getCustomerDetails.put("message", tmp);
		return getCustomerDetails;
	}
	
	private Map<String, Object> getCustomerInformation() {
		CustomerInformationResponse res = SERVICE.getCustomerInformation("accountNumber");
		Map<String, Object> getCustomerInformation = new HashMap<String, Object>();
		getCustomerInformation.put("getCustomerInformation", "It works!!!" + res);
		if(getCustomerInformation.isEmpty()) {
			return null;
		}
		return (getCustomerInformation);
	}
	
	private Map<String, Object> getCustomerODLimit() {
		CustomerODLimitResponse res = SERVICE.getCustomerODLimit("accountNumber");
		Map<String, Object> getCustomerODLimit = new HashMap<String, Object>();
		getCustomerODLimit.put("getCustomerODLimit", "It works!!!" + res);
		if(getCustomerODLimit.isEmpty()) {
			return null;
		}
		return (getCustomerODLimit);
	}
	
	public Map<String, Object> getEffectiveBalance(String custAcNo) {
		GetEffectiveBalanceResp res = SERVICE.getEffectiveBalance(custAcNo);
		Map<String, Object> getEffectiveBalance = new HashMap<String, Object>();
		getEffectiveBalance.put("status", SUCCESS_CODE);
		Map<String, Object> tmp = new HashMap<>();
		tmp.put("responseCode", res.getResponseCode().getValue());
		tmp.put("responseData", res.getResponseData().getValue());
		tmp.put("balance", res.getAcyAvlBal().getValue());
		getEffectiveBalance.put("message", tmp);
		return (getEffectiveBalance);
	}
	
	private Map<String, Object> getFixedDepositInfo() {
		FixedDepositResponse res = SERVICE.getFixedDepositInfo("customerNo");
		Map<String, Object> getFixedDepositInfo = new HashMap<String, Object>();
		getFixedDepositInfo.put("getFixedDepositInfo", "It works!!!" + res);
		if(getFixedDepositInfo.isEmpty()) {
			return null;
		}
		return (getFixedDepositInfo);
	}
	
	private Map<String, Object> getLoanDetailByAccountNumber() {
		LoanSummaryResponse res = SERVICE.getLoanDetailByAccountNumber("loanAccountNumber");
		Map<String, Object> getLoanDetailByAccountNumber = new HashMap<String, Object>();
		getLoanDetailByAccountNumber.put("getLoanDetailByAccountNumber", "It works!!!" + res);
		if(getLoanDetailByAccountNumber.isEmpty()) {
			return null;
		}
		return (getLoanDetailByAccountNumber);
	}
	
	private Map<String, Object> getLoanDetailByAccountNumber2() {
		LoanSummaryResponseBulk res = SERVICE.getLoanDetailByAccountNumber2(new ArrayOfstring());
		Map<String, Object> getLoanDetailByAccountNumber2 = new HashMap<String, Object>();
		getLoanDetailByAccountNumber2.put("getLoanDetailByAccountNumber2", "It works!!!" + res);
		if(getLoanDetailByAccountNumber2.isEmpty()) {
			return null;
		}
		return (getLoanDetailByAccountNumber2);
	}
	
	private Map<String, Object> getLoanInfoByAccountNumber() {
		LoanInfoSummaryResponse res = SERVICE.getLoanInfoByAccountNumber("loanAccountNumber", "reviewDate");
		Map<String, Object> getLoanInfoByAccountNumber = new HashMap<String, Object>();
		getLoanInfoByAccountNumber.put("getLoanInfoByAccountNumber", "It works!!!" + res);
		if(getLoanInfoByAccountNumber.isEmpty()) {
			return null;
		}
		return (getLoanInfoByAccountNumber);
	}
	
	private Map<String, Object> getLoanStatement() {
		LoanSummaryResponse res = SERVICE.getLoanStatement("loanAccountNumber");
		Map<String, Object> getLoanStatement = new HashMap<String, Object>();
		getLoanStatement.put("getLoanStatement", "It works!!!" + res);
		if(getLoanStatement.isEmpty()) {
			return null;
		}
		return (getLoanStatement);
	}
	
	private Map<String, Object> getLoanStatement2() {
		LoanSummaryResponseBulk res = SERVICE.getLoanStatement2(new ArrayOfstring());
		Map<String, Object> getLoanStatement2 = new HashMap<String, Object>();
		getLoanStatement2.put("getLoanStatement2", "It works!!!" + res);
		if(getLoanStatement2.isEmpty()) {
			return null;
		}
		return (getLoanStatement2);
	}
	
	private Map<String, Object> getMessageIDStatus() {
		MessageIDStatusResponse res = SERVICE.getMessageIDStatus("extSystem", "msgId");
		Map<String, Object> getMessageIDStatus = new HashMap<String, Object>();
		getMessageIDStatus.put("getMessageIDStatus", "It works!!!" + res);
		if(getMessageIDStatus.isEmpty()) {
			return null;
		}
		return (getMessageIDStatus);
	}
	
	private Map<String, Object> getNoTransactionSummary() {
		AccountSummaryAndTrxResponse res = SERVICE.getNoTransactionSummary("accountNumber", "startDate", "endDate");
		Map<String, Object> getNoTransactionSummary = new HashMap<String, Object>();
		getNoTransactionSummary.put("getNoTransactionSummary", "It works!!!" + res);
		if(getNoTransactionSummary.isEmpty()) {
			return null;
		}
		return (getNoTransactionSummary);
	}
	
	private Map<String, Object> getRelatedAccounts() {
		AccountRelatedResponse res = SERVICE.getRelatedAccounts("accountNumber");
		Map<String, Object> getRelatedAccounts = new HashMap<String, Object>();
		getRelatedAccounts.put("getRelatedAccounts", "It works!!!" + res);
		if(getRelatedAccounts.isEmpty()) {
			return null;
		}
		return (getRelatedAccounts);
	}
	
	private Map<String, Object> getRepaymentSchedule() {
		RepaymentScheduleResponse res = SERVICE.getRepaymentSchedule("loanAccountNumber");
		Map<String, Object> getRepaymentSchedule = new HashMap<String, Object>();
		getRepaymentSchedule.put("getRelatedAccounts", "It works!!!" + res);
		if(getRepaymentSchedule.isEmpty()) {
			return null;
		}
		return (getRepaymentSchedule);
	}
	
	private Map<String, Object> getSample() {
		SampleAccountResponse res = SERVICE.getSample("accountNumber");
		Map<String, Object> getSample = new HashMap<String, Object>();
		getSample.put("getSample", "It works!!!" + res);
		if(getSample.isEmpty()) {
			return null;
		}
		return (getSample);
	}
	
	private Map<String, Object> getScreenLimit() {
		ScreenLimitResponse res = SERVICE.getScreenLimit("functionNo", "ccyCode");
		Map<String, Object> getScreenLimit = new HashMap<String, Object>();
		getScreenLimit.put("getScreenLimit", "It works!!!" + res);
		if(getScreenLimit.isEmpty()) {
			return null;
		}
		return (getScreenLimit);
	}
	
	private Map<String, Object> getSingleTransactionDetails() {
		ArrayOfSingleTransactionDetails res = SERVICE.getSingleTransactionDetails("accountNumber", "transDate", "rowID");
		Map<String, Object> getSingleTransactionDetails = new HashMap<String, Object>();
		getSingleTransactionDetails.put("getSingleTransactionDetails", "It works!!!" + res);
		if(getSingleTransactionDetails.isEmpty()) {
			return null;
		}
		return (getSingleTransactionDetails);
	}
	
	private Map<String, Object> getTellerLimit() {
		TellerLimitResponse res = SERVICE.getTellerLimit("userId", "limitsCcy");
		Map<String, Object> getTellerLimit = new HashMap<String, Object>();
		getTellerLimit.put("getTellerLimit", "It works!!!" + res);
		if(getTellerLimit.isEmpty()) {
			return null;
		}
		return (getTellerLimit);
	}
	
	private Map<String, Object> getTransactionRefNoDetails() {
		TransactionRefNoDetailsResponse res = SERVICE.getTransactionRefNoDetails("trnRefNo");
		Map<String, Object> getTransactionRefNoDetails = new HashMap<String, Object>();
		getTransactionRefNoDetails.put("getTransactionRefNoDetails", "It works!!!" + res);
		if(getTransactionRefNoDetails.isEmpty()) {
			return null;
		}
		return (getTransactionRefNoDetails);
	}
	
	private Map<String, Object> getTransactionSummaryByValueDate() {
		AccountSummaryAndTrxResponse res = SERVICE.getTransactionSummaryByValueDate("accountNumber", "startDate", "endDate");
		Map<String, Object> getTransactionSummaryByValueDate = new HashMap<String, Object>();
		getTransactionSummaryByValueDate.put("getTransactionSummaryByValueDate", "It works!!!" + res);
		if(getTransactionSummaryByValueDate.isEmpty()) {
			return null;
		}
		return (getTransactionSummaryByValueDate);
	}
	
	private Map<String, Object> getUserBranch() {
		GetUserBranchResp res = SERVICE.getUserBranch("userId");
		Map<String, Object> getUserBranch = new HashMap<String, Object>();
		getUserBranch.put("getUserBranch", "It works!!!" + res);
		if(getUserBranch.isEmpty()) {
			return null;
		}
		return (getUserBranch);
	}
	
	private Map<String, Object> getUserLimit() {
		UserLimitResponse res = SERVICE.getUserLimit("userId", "branchCode");
		Map<String, Object> getUserLimit = new HashMap<String, Object>();
		getUserLimit.put("getUserLimit", "It works!!!" + res);
		if(getUserLimit.isEmpty()) {
			return null;
		}
		return (getUserLimit);
	}
	
	private Map<String, Object> getVisaCardLimits() {
		VisaCardLimitResponse res = SERVICE.getVisaCardLimits("accountNumber");
		Map<String, Object> getVisaCardLimits = new HashMap<String, Object>();
		getVisaCardLimits.put("getVisaCardLimits", "It works!!!" + res);
		if(getVisaCardLimits.isEmpty()) {
			return null;
		}
		return (getVisaCardLimits);
	}
	
	private Map<String, Object> getLoanSummaryByCustomerID() {
		LoanSummaryResponse res = SERVICE.getLoanSummaryByCustomerID("customerId");
		Map<String, Object> getLoanSummaryByCustomerID = new HashMap<String, Object>();
		getLoanSummaryByCustomerID.put("getVisaCardLimits", "It works!!!" + res);
		if(getLoanSummaryByCustomerID.isEmpty()) {
			return null;
		}
		return (getLoanSummaryByCustomerID);
	}
	
	private Map<String, Object> getAccountImageCollection() {
		ArrayOfImageCollectionDetails res = SERVICE.accountImageCollection("accountNo");
		Map<String, Object> accountImageCollection = new HashMap<String, Object>();
		accountImageCollection.put("accountImageCollection", "It works!!!" + res);
		if(accountImageCollection.isEmpty()) {
			return null;
		}
		return (accountImageCollection);
	}
	
	private Map<String, Object> activateChequeBook() {
		ActivateChequeBookResponse res = SERVICE.activateChequeBook("branchCode", "accountNumber", "chequeBookNo", "makerID", "checkerID");
		
		Map<String, Object> activateChequeBook = new HashMap<String, Object>();
		if(activateChequeBook.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (activateChequeBook);
	}
	
	private Map<String, Object> authenticateCBAUser() {
		AuthenticateCBAUserResp res = SERVICE.authenticateCBAUser("userId");
		
		Map<String, Object> authenticateCBAUser = new HashMap<String, Object>();
		if(authenticateCBAUser.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (authenticateCBAUser);
	}
	
	private Map<String, Object> checkifAccountClassExist() {
		AccountSearchResponse res = SERVICE.checkifAccountClassExist("customerNo", "accountClass");
		
		Map<String, Object> checkifAccountClassExist = new HashMap<String, Object>();
		if(checkifAccountClassExist.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (checkifAccountClassExist);
	}
	
	private Map<String, Object> checkIfAccountExist() {
		GetAccountExistResponse res = SERVICE.checkIfAccountExist("accountNo");
		
		Map<String, Object> checkIfAccountExist = new HashMap<String, Object>();
		if(checkIfAccountExist.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (checkIfAccountExist);
	}
	
	public Map<String, Object> checkifCustomerExist(String mobileNo) {
		AccountSearchResponse res = SERVICE.checkifCustomerExist(mobileNo, "telephoneNo", "dateofBirth", "accountClass", "email");
		ArrayOfAccountSearchResponseLines aasr = res.getAccountSearchResult().getValue();
		Map<String, Object> checkifCustomerExist = new HashMap<String, Object>();
		checkifCustomerExist.put("status", SUCCESS_CODE);
		List<Map<String, String>> lstASR = new ArrayList<>();
		for(AccountSearchResponseLines asr: aasr.getAccountSearchResponseLines()) {
			Map<String, String> tmp = new HashMap<String, String>();
			tmp.put("accountName", asr.getAccountName().getValue());
			tmp.put("accountNumber", asr.getAccountNo().getValue());
			tmp.put("accountType", asr.getAccountClass().getValue());
			tmp.put("currencyCode", asr.getCurrencyCode().getValue());
			tmp.put("customerNumber", asr.getCustomerNo().getValue());
			tmp.put("email", asr.getEmail().getValue());
			tmp.put("telephoneNo", asr.getTelephoneNo().getValue());
			tmp.put("productCode", asr.getProductCode().getValue());
			lstASR.add(tmp);
		}
		checkifCustomerExist.put("message", lstASR);
		return checkifCustomerExist;
	}
	
	private Map<String, Object> chequeNumberInquiry() {
		ChequeNoInquiryResponse res = SERVICE.chequeNumberInquiry("accountNumber", "chequeNumber");
		
		Map<String, Object> chequeNumberInquiry = new HashMap<String, Object>();
		if(chequeNumberInquiry.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (chequeNumberInquiry);
	}
	
	public Map<String, Object> fcubsCreateStopPayment() {
		FCUBSCreateStopPaymentRequest data = new FCUBSCreateStopPaymentRequest();
		data.setACCOUNTBRANCHCODE(objectFactory.createFCUBSCreateStopPaymentRequestACCOUNTBRANCHCODE(""));
		data.setACCOUNTNUMBER(objectFactory.createFCUBSCreateStopPaymentRequestACCOUNTNUMBER(""));
		data.setAMTONCHEQUE(objectFactory.createFCUBSCreateStopPaymentRequestAMTONCHEQUE(""));
		data.setCCYCODE(objectFactory.createFCUBSCreateStopPaymentRequestCCYCODE(""));
		data.setCHECKERID(objectFactory.createFCUBSCreateStopPaymentRequestCHECKERID(""));
		data.setCHEQUENO(objectFactory.createFCUBSCreateStopPaymentRequestCHEQUENO(""));
		data.setEFFECTIVEDATE(objectFactory.createFCUBSCreateStopPaymentRequestEFFECTIVEDATE(""));
		data.setEXPIRYDATE(objectFactory.createFCUBSCreateStopPaymentRequestEXPIRYDATE(""));
		data.setMAKERID(objectFactory.createFCUBSCreateStopPaymentRequestMAKERID(""));
		data.setSOURCE(objectFactory.createFCUBSCreateStopPaymentRequestSOURCE(""));
		data.setSTOPREMARK(objectFactory.createFCUBSCreateStopPaymentRequestSTOPREMARK(""));
		data.setUSERBRANCHCODE(objectFactory.createFCUBSCreateStopPaymentRequestUSERBRANCHCODE(""));
		data.setUSERID(objectFactory.createFCUBSCreateStopPaymentRequestUSERID(""));
		
		FcubsResponse res = SERVICE.fcubsCreateStopPayment(data, "action");
		
		Map<String, Object> fcubsCreateStopPayment = new HashMap<String, Object>();
		if(fcubsCreateStopPayment.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (fcubsCreateStopPayment);
	}
	
	private Map<String, Object> fcubscgServiceCreateTransaction() {
		FCUBSCreateCustAccountRequest data = new FCUBSCreateCustAccountRequest();
		objectFactory.createFCUBSCreateCustAccountRequestAccountBranchCode("");
		data.setAccountBranchCode(objectFactory.createFCUBSCreateCustAccountRequestAccountBranchCode("")); // JAXBElement<String>
		data.setAccountCcyCode(objectFactory.createFCUBSCreateCustAccountRequestAccountCcyCode(""));
		data.setAccountClass(objectFactory.createFCUBSCreateCustAccountRequestAccountClass(""));
		data.setAccountClassType(objectFactory.createFCUBSCreateCustAccountRequestAccountClassType(""));
		data.setAccountCompMIS2(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS2(""));
		data.setAccountCompMIS3(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS3(""));
		data.setAccountCompMIS4(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS4(""));
		data.setAccountCompMIS8(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS8(""));
		data.setAccountName(objectFactory.createFCUBSCreateCustAccountRequestAccountName(""));
		data.setAccountOpenDate(objectFactory.createFCUBSCreateCustAccountRequestAccountOpenDate(""));
		data.setAccountPoolCD(objectFactory.createFCUBSCreateCustAccountRequestAccountPoolCD(""));
		data.setAccountPostNoCredit(objectFactory.createFCUBSCreateCustAccountRequestAccountPostNoCredit(""));
		data.setAccountPostNoDebit(objectFactory.createFCUBSCreateCustAccountRequestAccountPostNoDebit(""));
		data.setATMAccount(objectFactory.createFCUBSCreateCustAccountRequestATMAccount(""));
		data.setAuthStatus(objectFactory.createFCUBSCreateCustAccountRequestAuthStatus(""));
		data.setCheckerID(objectFactory.createFCUBSCreateCustAccountRequestAuthStatus(""));
		data.setChequeBookAccount(objectFactory.createFCUBSCreateCustAccountRequestChequeBookAccount(""));
		data.setCustomerNo(objectFactory.createFCUBSCreateCustAccountRequestCustomerNo(""));
		data.setDormant(objectFactory.createFCUBSCreateCustAccountRequestDormant(""));
		data.setFrozen(objectFactory.createFCUBSCreateCustAccountRequestFrozen(""));
		data.setIsSalaryAccount(objectFactory.createFCUBSCreateCustAccountRequestIsSalaryAccount(""));
		data.setMakerID(objectFactory.createFCUBSCreateCustAccountRequestMakerID(""));
		data.setMinBalanceReqd(objectFactory.createFCUBSCreateCustAccountRequestMinBalanceReqd(""));
		data.setSource(objectFactory.createFCUBSCreateCustAccountRequestSource(""));
		data.setUserBranchCode(objectFactory.createFCUBSCreateCustAccountRequestUserBranchCode(""));
		data.setUserId(objectFactory.createFCUBSCreateCustAccountRequestUserId(""));
		
		FcubsResponse res = SERVICE.fcubsCreateCustAccount(data, "action");
		
		JAXBElement<String> rCode = res.getResponseCode();
		JAXBElement<String> rData = res.getResponseData();
		System.out.println("Response Code: " + rCode.getValue());
		
		Map<String, Object> fcubsCreateStopPayment = new HashMap<String, Object>();
		if(fcubsCreateStopPayment.isEmpty()) {
			// A very unlikely block
			return null;
		}
		return (fcubsCreateStopPayment);
	}
	
	@SuppressWarnings("unused")
	public Map<String, Object> createCustomerRequest(Map<String, String> reqEntity) {
		FCUBSCreateCustomerRequest data = new FCUBSCreateCustomerRequest();
		data.setAccountHolderMaritalStatus(objectFactory.createFCUBSCreateCustomerRequestAccountHolderMaritalStatus(reqEntity.getOrDefault("accountHolderMaritalStatus", "")));
		data.setCustomerFullname(objectFactory.createFCUBSCreateCustomerRequestCustomerFullname(reqEntity.getOrDefault("customerFullname", "Donald Trump")));
		data.setAcctOfficerCompositeMISClass(objectFactory.createFCUBSCreateCustomerRequestAcctOfficerCompositeMISClass(reqEntity.getOrDefault("accOfficerCompositeMISClass", "ACCT_OFF")));
		data.setAcctOfficerCompositeMISCode(objectFactory.createFCUBSCreateCustomerRequestAcctOfficerCompositeMISCode(reqEntity.getOrDefault("accOfficerCompositeMISCode", "OYI810C")));
		data.setAUCCustomerMISClass(objectFactory.createFCUBSCreateCustomerRequestAUCCustomerMISClass(reqEntity.getOrDefault("aucCustomerMISClass", "AUC")));
		data.setAUCCustomerMISCode(objectFactory.createFCUBSCreateCustomerRequestAUCCustomerMISCode(reqEntity.getOrDefault("aucCustomerMISCode", "727012")));
		data.setBirthCountry(objectFactory.createFCUBSCreateCustomerRequestBirthCountry(reqEntity.getOrDefault("birthCountry", "NG")));
		data.setBUZSECTORCustomerMISClass(objectFactory.createFCUBSCreateCustomerRequestBUZSECTORCustomerMISClass(reqEntity.getOrDefault("buzSectorClass", "BIZ_SECT")));
		data.setBUZSECTORCustomerMISCode(objectFactory.createFCUBSCreateCustomerRequestBUZSECTORCustomerMISCode(reqEntity.getOrDefault("buzSectorCode", "SM0001")));
		data.setCheckerID(objectFactory.createFCUBSCreateCustomerRequestCheckerID(reqEntity.getOrDefault("checkerID", "BPMSUSER")));
		data.setCountry(objectFactory.createFCUBSCreateCustomerRequestCountry(reqEntity.getOrDefault("country", "NG")));
		data.setCustomerCat(objectFactory.createFCUBSCreateCustomerRequestCustomerCat(reqEntity.getOrDefault("custCat", "IND")));
		data.setCustomerCommMode(objectFactory.createFCUBSCreateCustomerRequestCustomerCommMode(reqEntity.getOrDefault("commMode", "M")));
		data.setCustomerType(objectFactory.createFCUBSCreateCustomerRequestCustomerType(reqEntity.getOrDefault("customerType", "I")));
		data.setDateofBirth(objectFactory.createFCUBSCreateCustomerRequestDateofBirth(reqEntity.getOrDefault("dob", "1982-01-01")));
		data.setDomiciledAddressLine1(objectFactory.createFCUBSCreateCustomerRequestDomiciledAddressLine1(reqEntity.getOrDefault("dAddr1", "10 Washington DC")));
		data.setDomiciledAddressLine2(objectFactory.createFCUBSCreateCustomerRequestDomiciledAddressLine2(reqEntity.getOrDefault("dAddr2", "Washington DC")));
		data.setDomiciledAddressLine3(objectFactory.createFCUBSCreateCustomerRequestDomiciledAddressLine3(reqEntity.getOrDefault("dAddr3", "Washington DC")));
		data.setDomiciledAddressLine4(objectFactory.createFCUBSCreateCustomerRequestDomiciledAddressLine4(reqEntity.getOrDefault("dAddr4", "USA")));
		data.setDomiciledPinCode(objectFactory.createFCUBSCreateCustomerRequestDomiciledPinCode(reqEntity.getOrDefault("pinCode", "234")));
		data.setEmailID(objectFactory.createFCUBSCreateCustomerRequestEmailID(reqEntity.getOrDefault("email", "donaldtrump@wh.com")));
		data.setEmployerAddress1(objectFactory.createFCUBSCreateCustomerRequestEmployerAddress1(reqEntity.getOrDefault("eAddr1", "10 Washington DC")));
		data.setEmployerAddress2(objectFactory.createFCUBSCreateCustomerRequestEmployerAddress2(reqEntity.getOrDefault("eAddr2", "10 Washington DC")));
		data.setEmployerAddress3(objectFactory.createFCUBSCreateCustomerRequestEmployerAddress3(reqEntity.getOrDefault("eAddr3", "10 Washington DC")));
		data.setEmployerDesc(objectFactory.createFCUBSCreateCustomerRequestEmployerDesc(reqEntity.getOrDefault("eDesc", "WHITE HOUSE")));
		data.setEmployerEmail(objectFactory.createFCUBSCreateCustomerRequestEmployerEmail(reqEntity.getOrDefault("eEmail", "donaldtrump@wh.com")));
		data.setEmployerFax(objectFactory.createFCUBSCreateCustomerRequestEmployerFax(reqEntity.getOrDefault("eFax", "012332235")));
		data.setEmployerTelNo(objectFactory.createFCUBSCreateCustomerRequestEmployerTelNo(reqEntity.getOrDefault("eTelNo", "08038080000")));
		data.setEmploymentStatus(objectFactory.createFCUBSCreateCustomerRequestEmploymentStatus(reqEntity.getOrDefault("eStatus", "")));
		data.setFirstName(objectFactory.createFCUBSCreateCustomerRequestFirstName(reqEntity.getOrDefault("firstName", "Donald")));
		data.setGender(objectFactory.createFCUBSCreateCustomerRequestGender(reqEntity.getOrDefault("gender", "M")));
		data.setGuardian(objectFactory.createFCUBSCreateCustomerRequestGuardian(reqEntity.getOrDefault("guardian", "")));
		data.setHomePhoneNo(objectFactory.createFCUBSCreateCustomerRequestHomePhoneNo(reqEntity.getOrDefault("homePhone", "08038080000")));
		data.setHomeTelISDNo(objectFactory.createFCUBSCreateCustomerRequestHomeTelISDNo(reqEntity.getOrDefault("homeTel", "234")));
		data.setIdentityExpiryDate(objectFactory.createFCUBSCreateCustomerRequestIdentityExpiryDate(reqEntity.getOrDefault("iExpiryDate", "2017-08-23")));
		data.setIdentityIssueDate(objectFactory.createFCUBSCreateCustomerRequestIdentityIssueDate(reqEntity.getOrDefault("iIssueDate", "2016-06-01")));
		data.setIdentityNo(objectFactory.createFCUBSCreateCustomerRequestIdentityNo(reqEntity.getOrDefault("iNo", "US-00002")));
		data.setLanguage(objectFactory.createFCUBSCreateCustomerRequestLanguage(reqEntity.getOrDefault("language", "ENG")));
		data.setLastName(objectFactory.createFCUBSCreateCustomerRequestLastName(reqEntity.getOrDefault("lastName", "TRUMP")));
		data.setLocalBranch(objectFactory.createFCUBSCreateCustomerRequestLocalBranch(reqEntity.getOrDefault("localBranch", "238")));
		data.setLiabilityBranch(objectFactory.createFCUBSCreateCustomerRequestLiabilityBranch(reqEntity.getOrDefault("lBranch", "238")));
		data.setLiabilityCcy(objectFactory.createFCUBSCreateCustomerRequestLiabilityCcy(reqEntity.getOrDefault("lCcy", "NGN")));
		data.setLiabilityName(objectFactory.createFCUBSCreateCustomerRequestLiabilityName(reqEntity.getOrDefault("lName", "DONALD")));
		data.setMakerID(objectFactory.createFCUBSCreateCustomerRequestMakerID(reqEntity.getOrDefault("makerID", "BPMSUSER")));
		data.setMiddleName(objectFactory.createFCUBSCreateCustomerRequestMiddleName(reqEntity.getOrDefault("mName", "DONALD")));
		data.setMinorYesorNo(objectFactory.createFCUBSCreateCustomerRequestMinorYesorNo(reqEntity.getOrDefault("MinorYorN", "N")));
		data.setMobileISDNo(objectFactory.createFCUBSCreateCustomerRequestMobileISDNo(reqEntity.getOrDefault("MISDNo", "234")));
		data.setMobileNumber(objectFactory.createFCUBSCreateCustomerRequestMobileNumber(reqEntity.getOrDefault("mNumber", "08038080000")));
		data.setMothersMaidenName(objectFactory.createFCUBSCreateCustomerRequestMothersMaidenName(reqEntity.getOrDefault("mothersMName", "JAMES")));
		data.setNationalID(objectFactory.createFCUBSCreateCustomerRequestNationalID(reqEntity.getOrDefault("nationalID", "US-00005")));
		data.setNationality(objectFactory.createFCUBSCreateCustomerRequestNationality(reqEntity.getOrDefault("nationality", "NG")));
		data.setOJCAcctCompositeMISClass(objectFactory.createFCUBSCreateCustomerRequestOJCAcctCompositeMISClass(reqEntity.getOrDefault("ojcClass", "OJC_ACCT")));
		data.setOJCAcctCompositeMISCode(objectFactory.createFCUBSCreateCustomerRequestOJCAcctCompositeMISCode(reqEntity.getOrDefault("ojcCode", "727012")));
		data.setPermanentAddressLine1(objectFactory.createFCUBSCreateCustomerRequestPermanentAddressLine1(reqEntity.getOrDefault("pAddr1", "10 Washington DC")));
		data.setPermanentAddressLine2(objectFactory.createFCUBSCreateCustomerRequestPermanentAddressLine2(reqEntity.getOrDefault("pAddr2", "10 Washington DC")));
		data.setPermanentAddressLine3(objectFactory.createFCUBSCreateCustomerRequestPermanentAddressLine3(reqEntity.getOrDefault("pAddr3", "10 Washington DC")));
		data.setPermanentAddressLine4(objectFactory.createFCUBSCreateCustomerRequestPermanentAddressLine4(reqEntity.getOrDefault("pAddr4", "10 Washington DC")));
		data.setPinCode(objectFactory.createFCUBSCreateCustomerRequestPinCode(reqEntity.getOrDefault("pcode", "234")));
		data.setPlaceofBirth(objectFactory.createFCUBSCreateCustomerRequestPlaceofBirth(reqEntity.getOrDefault("pob", "NG")));
		data.setProfitCenterCompositeMISClass(objectFactory.createFCUBSCreateCustomerRequestProfitCenterCompositeMISClass(reqEntity.getOrDefault("profitClass", "PROF_CENT")));
		data.setProfitCenterCompositeMISCode(objectFactory.createFCUBSCreateCustomerRequestProfitCenterCompositeMISCode(reqEntity.getOrDefault("profitCode", "ABK")));
		data.setResidentStatus(objectFactory.createFCUBSCreateCustomerRequestResidentStatus(reqEntity.getOrDefault("rStatus", "R")));
		data.setSamePermanentAddress(objectFactory.createFCUBSCreateCustomerRequestSamePermanentAddress(reqEntity.getOrDefault("perAddr", "N")));
		data.setShortName(objectFactory.createFCUBSCreateCustomerRequestShortName(reqEntity.getOrDefault("shortName", "DONALD2")));
		data.setSource(objectFactory.createFCUBSCreateCustomerRequestSource(reqEntity.getOrDefault("source", "BPMINT")));
		data.setSpouseEmploymentStatus(objectFactory.createFCUBSCreateCustomerRequestSpouseEmploymentStatus(reqEntity.getOrDefault("spouseEStatus", "F")));
		data.setSpouseName(objectFactory.createFCUBSCreateCustomerRequestSpouseName(reqEntity.getOrDefault("spouseName", "STELLA TRUMP")));
		data.setTelephoneISDNo(objectFactory.createFCUBSCreateCustomerRequestTelephoneISDNo(reqEntity.getOrDefault("tISDNo", "234")));
		data.setTelephoneNumber(objectFactory.createFCUBSCreateCustomerRequestTelephoneNumber(reqEntity.getOrDefault("tNumber", "08038080000")));
		data.setTitle(objectFactory.createFCUBSCreateCustomerRequestTitle(reqEntity.getOrDefault("title", "MR")));
		data.setUDFIncomeBandFieldName(objectFactory.createFCUBSCreateCustomerRequestUDFIncomeBandFieldName(reqEntity.getOrDefault("incName", "INCOME_BAND")));
		data.setUDFIncomeBandFieldValue(objectFactory.createFCUBSCreateCustomerRequestUDFIncomeBandFieldValue(reqEntity.getOrDefault("incValue", "N5,000,000-N9,999,999")));
		data.setUDFProfessionFieldName(objectFactory.createFCUBSCreateCustomerRequestUDFProfessionFieldName(reqEntity.getOrDefault("profName", "PROFESSION")));
		data.setUDFProfessionFieldValue(objectFactory.createFCUBSCreateCustomerRequestUDFProfessionFieldValue(reqEntity.getOrDefault("profValue", "CIVIL SERVANT")));
		data.setUDFReligionFieldName(objectFactory.createFCUBSCreateCustomerRequestUDFReligionFieldName(reqEntity.getOrDefault("relName", "RELIGION")));
		data.setUDFReligionFieldValue(objectFactory.createFCUBSCreateCustomerRequestUDFReligionFieldValue(reqEntity.getOrDefault("relValue", "CHRISTIANITY")));
		data.setUserBranchCode(objectFactory.createFCUBSCreateCustomerRequestUserBranchCode(reqEntity.getOrDefault("userBranchCode", "238")));
		data.setUserId(objectFactory.createFCUBSCreateCustomerRequestUserId(reqEntity.getOrDefault("userId", "BPMSUSER")));
		// Marshall to xml before sending
		CreateCustomerResponse cca = SERVICE.fcubsCreateCustomer(data, reqEntity.getOrDefault("action", "NEW"));
		Map<String, Object> accResponse = new HashMap<String, Object>();
		accResponse.put("status", SUCCESS_CODE);
		Map<String, Object> tmp = new HashMap<>();
		tmp.put("responseCode", cca.getResponseCode().getValue());
		tmp.put("custNo", cca.getCUSTNO().getValue());
		tmp.put("responseData", cca.getResponseData().getValue());
		accResponse.put("message", tmp);
		return accResponse;
	}
	
	@SuppressWarnings("unused")
	public Map<String, Object> createCustAccountRequest(Map<String, String> reqEntity) {
		FCUBSCreateCustAccountRequest data = new FCUBSCreateCustAccountRequest();
		data.setAccountBranchCode(objectFactory.createFCUBSCreateCustAccountRequestAccountBranchCode(reqEntity.getOrDefault("accountBranchCode", "014")));
		data.setAccountCcyCode(objectFactory.createFCUBSCreateCustAccountRequestAccountCcyCode(reqEntity.getOrDefault("ccyCode", "NGN")));
		data.setAccountClass(objectFactory.createFCUBSCreateCustAccountRequestAccountClass(reqEntity.getOrDefault("accountClass", "020003")));
		data.setAccountClassType(objectFactory.createFCUBSCreateCustAccountRequestAccountClassType(reqEntity.getOrDefault("accountClassType", "S")));
		data.setAccountCompMIS2(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS2(reqEntity.getOrDefault("accountCompMIS2", "OYI810")));
		data.setAccountCompMIS4(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS4(reqEntity.getOrDefault("accountCompMIS4", "OYI810C")));
		data.setAccountCompMIS8(objectFactory.createFCUBSCreateCustAccountRequestAccountCompMIS8(reqEntity.getOrDefault("accountCompMIS8", "26610")));
		data.setAccountName(objectFactory.createFCUBSCreateCustAccountRequestAccountName(reqEntity.getOrDefault("accountName", "Great Anonymous")));
		data.setAccountOpenDate(objectFactory.createFCUBSCreateCustAccountRequestAccountOpenDate(reqEntity.getOrDefault("accountOpenDate", "2017-01-01")));
		data.setAccountPoolCD(objectFactory.createFCUBSCreateCustAccountRequestAccountPoolCD(reqEntity.getOrDefault("accountPoolCD", "DFLTPOOL")));
		data.setAccountPostNoCredit(objectFactory.createFCUBSCreateCustAccountRequestAccountPostNoCredit(reqEntity.getOrDefault("accountPostNoCredit", "N")));
		data.setAccountPostNoDebit(objectFactory.createFCUBSCreateCustAccountRequestAccountPostNoDebit(reqEntity.getOrDefault("accountPostNoDebit", "Y")));
		data.setATMAccount(objectFactory.createFCUBSCreateCustAccountRequestATMAccount(reqEntity.getOrDefault("atmAccount", "Y")));
		data.setAuthStatus(objectFactory.createFCUBSCreateCustAccountRequestAuthStatus(reqEntity.getOrDefault("authStatus", "A")));
		data.setCheckerID(objectFactory.createFCUBSCreateCustAccountRequestCheckerID(reqEntity.getOrDefault("checkerID", "BWORLDUSER")));
		data.setChequeBookAccount(objectFactory.createFCUBSCreateCustAccountRequestChequeBookAccount(reqEntity.getOrDefault("chequeBookAccount", "Y")));
		data.setCustomerNo(objectFactory.createFCUBSCreateCustAccountRequestCustomerNo(reqEntity.getOrDefault("customerNo", "000268025")));
		data.setDormant(objectFactory.createFCUBSCreateCustAccountRequestDormant(reqEntity.getOrDefault("dormant", "N")));
		data.setFrozen(objectFactory.createFCUBSCreateCustAccountRequestFrozen(reqEntity.getOrDefault("frozen", "N")));
		data.setIsSalaryAccount(objectFactory.createFCUBSCreateCustAccountRequestIsSalaryAccount(reqEntity.getOrDefault("isSalaryAccount", "N")));
		data.setMakerID(objectFactory.createFCUBSCreateCustAccountRequestMakerID(reqEntity.getOrDefault("makerID", "BWORLDUSER")));
		data.setMinBalanceReqd(objectFactory.createFCUBSCreateCustAccountRequestMinBalanceReqd(reqEntity.getOrDefault("minBalanceReqd", "0")));
		data.setSource(objectFactory.createFCUBSCreateCustAccountRequestSource(reqEntity.getOrDefault("source", "BWONLINE")));
		data.setUserBranchCode(objectFactory.createFCUBSCreateCustAccountRequestUserBranchCode(reqEntity.getOrDefault("userBranchCode", "099")));
		data.setUserId(objectFactory.createFCUBSCreateCustAccountRequestUserId(reqEntity.getOrDefault("userId", "BWORLDUSER")));
		// Marshall to xml before sending
		CreateCustAccountResponse cca = SERVICE.fcubsCreateCustAccount(data, reqEntity.getOrDefault("action", "NEW"));
		Map<String, Object> accResponse = new HashMap<String, Object>();
		accResponse.put("status", SUCCESS_CODE);
		Map<String, Object> tmp = new HashMap<>();
		tmp.put("responseCode", cca.getResponseCode().getValue());
		tmp.put("custAcNo", cca.getCUSTACNO().getValue());
		tmp.put("responseData", cca.getResponseData().getValue());
		accResponse.put("message", tmp);
		return accResponse;
	}
	
	public Map<String, Object> doIntraTransfer(Map<String, String> reqEntity) {
		FCUBSCGServiceCreateTransactionRequest data = new FCUBSCGServiceCreateTransactionRequest();
		data.setACCCCY(objectFactory.createFCUBSCGServiceCreateTransactionRequestACCCCY(reqEntity.getOrDefault("accCcy", null)));
		data.setBENEFICIARYACCTNO(objectFactory.createFCUBSCGServiceCreateTransactionRequestBENEFICIARYACCTNO(reqEntity.getOrDefault("beneficiaryAcctNo", null)));
		data.setBRANCHCODE(objectFactory.createFCUBSCGServiceCreateTransactionRequestBRANCHCODE(reqEntity.getOrDefault("branchCode", null)));
		data.setCHECKERID(objectFactory.createFCUBSCGServiceCreateTransactionRequestCHECKERID(reqEntity.getOrDefault("checkerID", null)));
		data.setCHECKERSTAMP(objectFactory.createFCUBSCGServiceCreateTransactionRequestCHECKERSTAMP(reqEntity.getOrDefault("checkerStamp", null)));
		data.setINSTRAMT(objectFactory.createFCUBSCGServiceCreateTransactionRequestINSTRAMT(reqEntity.getOrDefault("instrAmt", null)));
		data.setINSTRCCY(objectFactory.createFCUBSCGServiceCreateTransactionRequestINSTRCCY(reqEntity.getOrDefault("instrCcy", null)));
		data.setMAKERID(objectFactory.createFCUBSCGServiceCreateTransactionRequestMAKERID(reqEntity.getOrDefault("makerID", null)));
		data.setMAKERSTAMP(objectFactory.createFCUBSCGServiceCreateTransactionRequestMAKERSTAMP(reqEntity.getOrDefault("makerStamp", null)));
		data.setMSGID(objectFactory.createFCUBSCGServiceCreateTransactionRequestMSGID(reqEntity.getOrDefault("msgID", null)));
		data.setPAYERACCOUNT(objectFactory.createFCUBSCGServiceCreateTransactionRequestPAYERACCOUNT(reqEntity.getOrDefault("payerAccount", null)));
		data.setPAYERINSTRNO(objectFactory.createFCUBSCGServiceCreateTransactionRequestPAYERINSTRNO(reqEntity.getOrDefault("payerInstrNO", null)));
		data.setREMARKS(objectFactory.createFCUBSCGServiceCreateTransactionRequestREMARKS(reqEntity.getOrDefault("remarks", null)));
		data.setROUTINGNO(objectFactory.createFCUBSCGServiceCreateTransactionRequestROUTINGNO(reqEntity.getOrDefault("routingNO", null)));
		data.setSOURCE(objectFactory.createFCUBSCGServiceCreateTransactionRequestSOURCE(reqEntity.getOrDefault("source", null)));
		data.setTRANDATE(objectFactory.createFCUBSCGServiceCreateTransactionRequestTRANDATE(reqEntity.getOrDefault("tranDate", null)));
		data.setTXNCCY(objectFactory.createFCUBSCGServiceCreateTransactionRequestTXNCCY(reqEntity.getOrDefault("txnCcy", null)));
		data.setUSERBRANCHCODE(objectFactory.createFCUBSCGServiceCreateTransactionRequestUSERBRANCHCODE(reqEntity.getOrDefault("userBranchCode", null)));
		data.setUSERID(objectFactory.createFCUBSCGServiceCreateTransactionRequestUSERID(reqEntity.getOrDefault("userID", null)));
		data.setVALUEDATE(objectFactory.createFCUBSCGServiceCreateTransactionRequestVALUEDATE(reqEntity.getOrDefault("valueDate", null)));
		data.setXREF(objectFactory.createFCUBSCGServiceCreateTransactionRequestXREF(reqEntity.getOrDefault("xRef", null)));
		FcubsResponse res = SERVICE.fcubscgServiceCreateTransaction(data, reqEntity.getOrDefault("action", null));
		Map<String, Object> accResponse = new HashMap<String, Object>();
		accResponse.put("status", SUCCESS_CODE);
		Map<String, String> tmp = new HashMap<>();
		tmp.put("responseCode", res.getResponseCode().getValue());
		tmp.put("responseData", res.getResponseData().getValue());
		accResponse.put("message", tmp);
		return accResponse;
	}
	
	public Map<String, Object> doOwnTransfer(Map<String, String> reqEntity) throws DatatypeConfigurationException {
		SinglePostReq request = new SinglePostReq();
		request.setMsgId(reqEntity.getOrDefault("msgId", "BW15052017007"));
		request.setModuleName(reqEntity.getOrDefault("moduleName", "BWONLINE"));
		request.setAuditDetails(reqEntity.getOrDefault("auditDetails", "null"));
		FlexcubeUser user = new FlexcubeUser();
		XMLGregorianCalendar xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar());
		user.setActionDate(xgc);
		user.setUserId(reqEntity.getOrDefault("userId", "ACCESSDEV"));
		request.setApprover(user);
		FlexcubeUser init = new FlexcubeUser();
		init.setActionDate(xgc);
		init.setUserId(reqEntity.getOrDefault("userId", "ACCESSDEV"));
		request.setInitiator(init);
		SingleEntry posting = new SingleEntry();
		posting.setAmount(BigDecimal.valueOf(Long.parseLong(reqEntity.getOrDefault("amount", "15.0"))));
		posting.setBatchNumber(Integer.valueOf(reqEntity.getOrDefault("batchNumber", "0")));
		Account ca = new Account();
		ca.setAcctNo(reqEntity.getOrDefault("creditAccount", "0039382814"));
		ca.setAcctType(AccountType.CASA);
		ca.setCurrency(ISOCurrency.NGN);
		ca.setHomeBranch(Integer.valueOf(reqEntity.getOrDefault("homeBranch", "246")));
		ca.setInstrumentCode(reqEntity.getOrDefault("instrCode", "?"));
		posting.setCreditAccount(ca);
		posting.setCurrency(ISOCurrency.NGN);
		Account da = new Account();
		da.setAcctNo(reqEntity.getOrDefault("debitAccount", "0001721212"));
		da.setAcctType(AccountType.CASA);
		da.setCurrency(ISOCurrency.NGN);
		da.setHomeBranch(Integer.valueOf(reqEntity.getOrDefault("homeBranch", "246")));
		da.setInstrumentCode(reqEntity.getOrDefault("instrCode", "?"));
		posting.setDebitAccount(da);
		posting.setNarration(reqEntity.getOrDefault("narration", "BW Testing"));
		posting.setNoCOT(Boolean.valueOf(reqEntity.getOrDefault("noCOT", "false")));
		posting.setPaymentReference(reqEntity.getOrDefault("paymentReference", "BWONLINE"));
		posting.setPostingBranch(Integer.valueOf(reqEntity.getOrDefault("postingBranch", "99")));
		posting.setBatchNumber(Integer.valueOf(reqEntity.getOrDefault("batchNumber", "0")));
		posting.setValueDate(xgc);
		request.setPosting(posting);
		FlexPostRsp res = SERVICE_1.singlePost(request);
		Map<String, Object> trResponse = new HashMap<String, Object>();
		trResponse.put("status", SUCCESS_CODE);
		Map<String, Object> tmp = new HashMap<>();
		tmp.put("responseCode", res.getResponseCode());
		tmp.put("responseData", res.getResponseData());
		tmp.put("reference", res.getReference());
		tmp.put("responseMsg", res.getResponseMessage());
		trResponse.put("message", tmp);
		return trResponse;
	}
	
}
