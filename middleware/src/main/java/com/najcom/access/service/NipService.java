package com.najcom.access.service;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.najcom.access.logger.JaxWsHandlerResolver;
import com.najcom.access.logger.JaxWsLoggingHandler;
import com.najcom.access.nip.BankListRequest;
import com.najcom.access.nip.BankListResponse;
import com.najcom.access.nip.Banks;
import com.najcom.access.nip.EnquiryRequest;
import com.najcom.access.nip.EnquiryResponse;
import com.najcom.access.nip.IInterbank;
import com.najcom.access.nip.Interbank;
import com.najcom.access.nip.Location;
import com.najcom.access.nip.MemberBank;
import com.najcom.access.nip.StatusRequest;
import com.najcom.access.nip.StatusResponse;
import com.najcom.access.nip.TransferRequest;
import com.najcom.access.nip.TransferResponse;

public class NipService {
	
	private static IInterbank SERVICE;
	private final int SUCCESS_CODE = 200;
	private final int ERROR_CODE = 500;
	private final int NOT_FOUND_CODE = 404;
	
	static {
		Interbank iInstance = Interbank.getInstance();
		iInstance.setHandlerResolver(new JaxWsHandlerResolver());
        SERVICE = iInstance.getBasicHttpBindingIInterbank();
    }
	
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Object> getAllBanks(Map<String, String> reqEntity) {
    	BankListRequest req =  new BankListRequest();
    	req.setMessageID(reqEntity.getOrDefault("messageId", "123")); // nillable="true"
    	req.setModuleName(reqEntity.getOrDefault("moduleId", "abc")); // nillable="true"
    	BankListResponse res = SERVICE.getAllBanks(req);
    	Map<String, Object> banks = new HashMap<String, Object>();
    	if(res.isSuccess()) {
    		banks.put("status", SUCCESS_CODE);
    		Banks bnk = res.getBanks();
    		List<Map<String, String>> bankks = new ArrayList<>();
    		for(MemberBank mb: bnk.getBank()) {
    			Map<String, String> tmp = new HashMap<>();
    			tmp.put("code", mb.getCode());
        		tmp.put("name", mb.getName());
    			bankks.add(tmp);
    		}
    		banks.put("message", bankks);
    	} else {
    		banks.put("status", ERROR_CODE);
    		banks.put("message", res.getErrorCode());
    	}
    	return banks;
    }
	
	public Map<String, Object> getTransactionStatus(Map<String, String> reqEntity) {
		StatusRequest req =  new StatusRequest();
		req.setMessageID(reqEntity.getOrDefault("messageId", null)); // nillable="true"
		req.setModuleName(reqEntity.getOrDefault("moduleName", null)); // nillable="true"
		req.setSessionID(reqEntity.getOrDefault("sessionId", null)); // nillable="true"
		req.setBankCode(reqEntity.getOrDefault("bankCode", null)); // nillable="true"
		StatusResponse res = SERVICE.getTransactionStatus(req);
		Map<String, Object> transactionStatus = new HashMap<String, Object>();
		if(res.isSuccess()) {
    		transactionStatus.put("status", SUCCESS_CODE);
    		transactionStatus.put("message", res.getMessageID() + " " + res.getSessionID());
    	} else {
    		transactionStatus.put("status", ERROR_CODE);
    		transactionStatus.put("message", res.getMessageID() + " " + res.getSessionID());
    	}
		return transactionStatus;
	}
	
    public Map<String, Object> doInterTransfer(Map<String, String> reqEntity) {
		TransferRequest req = new TransferRequest();
		req.setAmount(new BigDecimal(reqEntity.get("amount")));
		req.setBankCode(reqEntity.get("bankcode"));
		req.setMessageID(reqEntity.getOrDefault("messageId", null)); // nillable true
		req.setModuleName(reqEntity.getOrDefault("moduleName", null)); // nillable true
		req.setNarration(reqEntity.getOrDefault("narration", null)); // "
		req.setPaymentReference(reqEntity.getOrDefault("paymentReference", null)); // "
		req.setRecipientAccountNumber(reqEntity.getOrDefault("recipientAccountNumber", null)); // "
		req.setRecipientBvn(reqEntity.getOrDefault("recipientBvn", null)); // "
		req.setRecipientKycLevel(Integer.parseInt(reqEntity.getOrDefault("kycLevel", "0"))); // Integer
		req.setRecipientName(reqEntity.getOrDefault("recipientName", null)); // nillable true
		req.setRelatedNameEnquiryRef(reqEntity.getOrDefault("relatedNameEnquiryRef", null)); // nillable true
		req.setSenderAccountNumber(reqEntity.getOrDefault("senderAccountNumber", null)); // nillable true
		req.setSenderName(reqEntity.getOrDefault("senderName", null)); // nillable true
		Location loc = new Location(); // nillable true
		loc.setLatitude(new BigDecimal(reqEntity.getOrDefault("latitude", "6.479652"))); // BigDecimal
		loc.setLongitude(new BigDecimal(reqEntity.getOrDefault("longitude", "3.572702"))); // BigDecimal
		req.setTransactionLocation(loc); // Location
		TransferResponse res = SERVICE.transferFunds(req);
        Map<String, Object> transfer = new HashMap<String, Object>();
        if(res.isSuccess()) {
    		transfer.put("status", SUCCESS_CODE);
    		transfer.put("message", res.getMessageID() + "-" + res.getSessionID());
    	} else {
    		transfer.put("status", ERROR_CODE);
    		transfer.put("message", res.getErrorCode());
    	}
		return transfer;
    }
	
    // aka CustomerName
	public Map<String, Object> getCreditAccount(Map<String, String> reqEntity) {
		EnquiryRequest req =  new EnquiryRequest();
		req.setMessageID(reqEntity.getOrDefault("messageId", "BW06092017001")); // nillable true
		req.setModuleName(reqEntity.getOrDefault("moduleName", "BWA")); // nillable true
		req.setAccountNumber(reqEntity.getOrDefault("accountNumber", null)); // nillable true
		req.setBankCode(reqEntity.getOrDefault("bankCode", null)); // nillable true
		EnquiryResponse res = SERVICE.getCustomerName(req);
		Map<String, Object> customerName = new HashMap<String, Object>();
		if(res.isSuccess()) {
    		customerName.put("status", SUCCESS_CODE);
    		Map<String, Object> tmp = new HashMap<>();
    		tmp.put("accountName", res.getAccountName());
    		tmp.put("bvn", res.getBvn());
    		tmp.put("kycLevel", res.getKycLevel());
    		tmp.put("msgId", res.getMessageID());
    		tmp.put("sessionId", res.getSessionID());
    		customerName.put("message", tmp);
    	} else {
    		customerName.put("status", NOT_FOUND_CODE);
    		customerName.put("message", res.getErrorCode());
    	}
		return customerName;
	}
	
}
